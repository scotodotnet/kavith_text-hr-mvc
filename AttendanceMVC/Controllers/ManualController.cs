﻿using AttendanceMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AttendanceMVC.Controllers
{
    public class ManualController : Controller
    {
        static string Ccode = "";
        static string Lcode = "";
        static string SessionCcode = "";
        static string SessionLcode = "";
        static string SessionUserName;
        static string SessionUserID;
        string PartyName;
        DateTime frmDate;
        DateTime toDate;
        string Machine_ID_Encrypt;
       
        string mIpAddress_IN;
        string mIpAddress_OUT;

        string SSQL = "";


        public JsonResult checkSession()
        {
            MasterDate c = new MasterDate();
            if (Session["Ccode"] != null)
            {
                c.sessionValue = true;
            }
            else
            {
                c.sessionValue = false;

            }
            return Json(c, JsonRequestBehavior.AllowGet);
        }
        // GET: Manual
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ManualTimeDelete()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult ManualAttendance()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult ManualOT()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult ManualLeave()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult SaveManualLeaveEntry(ManualAttend EmpDetails)
        {
            string Query = "";
            string status = "Insert";
            DataTable dt_Save = new DataTable();


            SSQL = "";
            SSQL = SSQL + "select * from ManLeave_Entry where  EmpNo='" + EmpDetails.EmpNo + "' and convert(datetime,FromDate,103)=convert(datetime,'" + EmpDetails.AttnDate + "',103) and  ";
            SSQL = SSQL + " convert(datetime,ToDate,103)=convert(datetime,'" + EmpDetails.AttnDateStr + "',103)  and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dt_Save = ManualAttend.GetDetails(SSQL);

            if (dt_Save.Rows.Count != 0)
            {
                SSQL = "";
                SSQL = SSQL + "delete from ManLeave_Entry where  EmpNo='" + EmpDetails.EmpNo + "' and convert(datetime,FromDate,103)=convert(datetime,'" + EmpDetails.AttnDate + "',103) and  ";
                SSQL = SSQL + " convert(datetime,ToDate,103)=convert(datetime,'" + EmpDetails.AttnDateStr + "',103)  and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                ManualAttend.GetDetails(SSQL);

                status = "Update";
            }

            SSQL = "";
            SSQL = SSQL + "Insert into ManLeave_Entry(CompCode,LocCode,EmpNo,EmpName,ExistingCode,FromDate,ToDate,TotayDays,LeaveType,LeaveDesc) values(";
            SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + EmpDetails.EmpNo + "','" + EmpDetails.ExistingCode + "','" + EmpDetails.EmpName + "',";
            SSQL = SSQL + "'" + EmpDetails.AttnDate + "','" + EmpDetails.AttnDateStr + "','2','" + EmpDetails.LeaveType + "','" + EmpDetails.LeaveDesc + "')";
            ManualAttend.GetDetails(SSQL);

            return Json(status);
        }
        public ActionResult SaveManualLeave(ManualAttend EmpDetails)
        {
            string Query = "";
            string status = "Insert";
            DataTable dt_Save = new DataTable();


            SSQL = "";
            SSQL = SSQL + "select * from ManLeave_Details where  LeaveType='" + EmpDetails.UserType + "' ";
            SSQL = SSQL + "and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dt_Save = ManualAttend.GetDetails(SSQL);

            if (dt_Save.Rows.Count != 0)
            {
                SSQL = "";
                SSQL = SSQL + "delete from ManLeave_Details where LeaveType='" + EmpDetails.UserType + "'  ";
                SSQL = SSQL + "and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                ManualAttend.GetDetails(SSQL);

                status = "Update";
            }

            SSQL = "";
            SSQL = SSQL + "Insert into ManLeave_Details(LeaveType,CompCode,LocCode) values(";
            SSQL = SSQL + "'" + EmpDetails.UserType + "','" + SessionCcode + "','" + SessionLcode + "')";
            ManualAttend.GetDetails(SSQL);

            return Json(status);
        }
        public ActionResult SaveManuaOT(ManualAttend EmpDetails)
        {
            string Query = "";
            string status = "Insert";
            DataTable dt_Save = new DataTable();


            SSQL = "";
            SSQL = SSQL + "select * from ManOT_Details where EmpNo='" + EmpDetails.EmpNo + "' and convert(datetime,AttnDate,103)=convert(datetime,'" + EmpDetails.AttnDate + "',103) and ";
            SSQL = SSQL + "OThours='" + EmpDetails.TimeIn + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dt_Save = ManualAttend.GetDetails(SSQL);

            if (dt_Save.Rows.Count != 0)
            {
                SSQL = "";
                SSQL = SSQL + "delete from ManOT_Details where EmpNo='" + EmpDetails.EmpNo + "' and convert(datetime,AttnDate,103)=convert(datetime,'" + EmpDetails.AttnDate + "',103) and ";
                SSQL = SSQL + "OThours='" + EmpDetails.TimeIn + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                ManualAttend.GetDetails(SSQL);

                status = "Update";
            }

            SSQL = "";
            SSQL = SSQL + "Insert into ManOT_Details(EmpNo,ExistingCode,EmpName,AttnDate,AttnDateStr,OThours,CompCode,LocCode) values(";
            SSQL = SSQL + "'" + EmpDetails.EmpNo + "','" + EmpDetails.ExistingCode + "','" + EmpDetails.EmpName + "','" + EmpDetails.AttnDate + "',";
            SSQL = SSQL + "convert(datetime,'" + EmpDetails.AttnDate + "',103),'" + EmpDetails.TimeIn + "','" + SessionCcode + "','" + SessionLcode + "')";
            ManualAttend.GetDetails(SSQL);

            return Json(status);
        }

        public ActionResult SaveManulAtt(ManualAttend EmpDetails)
        {
            string Query = "";
            string status = "Insert";
            string Date_Value_Str1;
            string Date_Value_Str;

            DataTable dt_Save = new DataTable();

            DataTable Da_Imp_IN = new DataTable();
            DataTable Da_Imp_OUT = new DataTable();
            DataTable Da_Manu_IN = new DataTable();
            DataTable Da_Manu_OUT = new DataTable();

            DateTime dt1_IN = DateTime.ParseExact(EmpDetails.AttnDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string loginIN = dt1_IN.ToString("dd/MM/yyyy");

            DateTime dt1_OUT = DateTime.ParseExact(EmpDetails.AttnDateStr, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string loginOUT = dt1_OUT.ToString("dd/MM/yyyy");

            string logtimein = loginIN + " " + EmpDetails.TimeIn;
            string logtimeout = loginOUT + " " + EmpDetails.TimeOut;

          

            

            DateTime TimeIN = Convert.ToDateTime(logtimein);
            DateTime TimeOUT = Convert.ToDateTime(logtimeout);

            Date_Value_Str = Convert.ToDateTime(TimeIN).ToString("yyyy/MM/dd");
            Date_Value_Str1 = Convert.ToDateTime(TimeIN).AddDays(1).ToString("yyyy/MM/dd");

        

            Machine_ID_Encrypt = UTF8Encryption(EmpDetails.EmpNo);

            //Delete LogTime IN
            Query = "";
            Query = Query + "Delete from LogTime_IN where MachineID='" + Machine_ID_Encrypt + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            Query = Query + "And TimeIN >='" + Date_Value_Str + " " + "02:00" + "'";
            Query = Query + "And TimeIN <='" + Date_Value_Str1 + " " + "02:00" + "'";
            ManualAttend.GetDetails(Query);


            //Delete LogTime OUT
            Query = "";
            Query = Query + "Delete from LogTime_OUT where MachineID='" + Machine_ID_Encrypt + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            Query = Query + "And TimeOUT >='" + Date_Value_Str + " " + "10:00" + "'";
            Query = Query + "And TimeOUT <='" + Date_Value_Str1 + " " + "10:00" + "'";
            ManualAttend.GetDetails(Query);

            Query = "";
            Query = Query + "delete from LogTime_Days where MachineID='" + EmpDetails.EmpNo + "' and   CONVERT(DATETIME, Attn_Date, 120) = CONVERT(DATETIME, '" + Convert.ToDateTime(EmpDetails.AttnDate).ToString("yyyy/MM/dd") + "', 120)  And  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' ";
            ManualAttend.GetDetails(Query);


            //Check Manual Attendance Table Doublicate Values
            Query = "select * from ManAttn_Details where EmpNo='" + EmpDetails.EmpNo + "' and convert(datetime,AttnDate,103)=convert(datetime,'" + EmpDetails.AttnDate + "',103) ";
            dt_Save = ManualAttend.GetDetails(Query);
            if (dt_Save.Rows.Count != 0)
            {
                Query = "delete from ManAttn_Details where EmpNo='" + EmpDetails.EmpNo + "' and convert(datetime,AttnDate,103)=convert(datetime,'" + EmpDetails.AttnDate + "',103) ";
                dt_Save = ManualAttend.GetDetails(Query);

                status = "Update";
            }

            Query = "insert into ManAttn_Details (EmpPrefix,EmpNo,ExistingCode,Machine_No,EmpName,AttnStatus,AttnDate,TimeIn,AttnDateStr,TimeOut,LogTimeIn,LogTimeOut,Enter_Date,CompCode,LocCode) values(";
            Query = Query + "'A','" + EmpDetails.EmpNo + "','" + EmpDetails.ExistingCode + "','" + EmpDetails.EmpNo + "','" + EmpDetails.EmpName + "',";
            Query = Query + "'P','" + EmpDetails.AttnDate + "','" + EmpDetails.TimeIn + "','" + EmpDetails.AttnDateStr + "','" + EmpDetails.TimeOut + "','" + EmpDetails.AttnDate + " " + EmpDetails.TimeIn + "','" + EmpDetails.AttnDateStr + " " + EmpDetails.TimeOut + "','" + EmpDetails.AttnDate + "','" + SessionCcode + "','" + SessionLcode + "')";
            ManualAttend.GetDetails(Query);


            //Add Attendance to LogTime IN and OUT
            MachineIDFunction(EmpDetails.AttnDate, EmpDetails.TimeIn, EmpDetails.AttnDateStr, EmpDetails.TimeOut);


            //Add Attendance to LogTime_Days
            NewTableFunction(EmpDetails.EmpNo, EmpDetails.AttnDate);

            return Json(status);
        }
        public void MachineIDFunction(string InDate, string TimeIN, string OUTDate, string TimeOUT)
        {
            DataTable dtLogTimeIN = new DataTable();
            DataTable dtLogTimeOUT = new DataTable();
            DataTable dtIPaddress = new DataTable();



            SSQL = "Select * from IPAddress_Mst where CompCode = '" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'";
            dtIPaddress = ManualAttend.GetDetails(SSQL);

            if (dtIPaddress.Rows.Count > 0)
            {
                for (int i = 0; i < dtIPaddress.Rows.Count; i++)
                {
                    if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                    {
                        mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                    else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                    {
                        mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                }
            }

            DateTime dt1 = DateTime.ParseExact(InDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string login = dt1.ToString("MM/dd/yyyy");
            DateTime dt2 = DateTime.ParseExact(OUTDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string login2 = dt2.ToString("MM/dd/yyyy");

            string logtimein = login + " " + TimeIN;
            string logtimeout = login2 + " " + TimeOUT;


            //Insert data into LogTimeIN
            SSQL = "";
            SSQL = SSQL + "insert into LogTime_IN(CompCode,LocCode,IPAddress,MachineID,TimeIN) values('" + SessionCcode + "','" + SessionLcode + "','" + mIpAddress_IN + "','" + Machine_ID_Encrypt + "','" + logtimein + "')";
            dtLogTimeIN = ManualAttend.GetDetails(SSQL);


            SSQL = "";
            SSQL = "insert into LogTime_OUT(CompCode,LocCode,IPAddress,MachineID,TimeOUT) values('" + SessionCcode + "','" + SessionLcode + "','" + mIpAddress_OUT + "','" + Machine_ID_Encrypt + "','" + logtimeout + "')";
            dtLogTimeOUT = ManualAttend.GetDetails(SSQL);

        }
        public void NewTableFunction(string MachineNo, string Date)
        {


            //Status = "";
            string[] iStr1;
            string[] iStr2;
            int intI;
            int intK;
            int intCol;
            string Fin_Year = "";
            string Months_Full_Str = "";
            string Date_Col_Str = "";
            int Month_Int = 1;
            string Spin_Machine_ID_Str = "";

            string[] Time_Minus_Value_Check;

            DataTable mLocalDS = new DataTable();

            DataTable mLocalDS1 = new DataTable();

            //Attn_Flex Col Add Var
            string[] Att_Date_Check;
            string Att_Already_Date_Check;
            string Att_Year_Check = "";
            int Month_Name_Change;
            string halfPresent = "0";
            int EPay_Total_Days_Get = 0;
            intCol = 4;
            Month_Name_Change = 1;
            int dayCount = 1;
            int daysAdded = 1;

            EPay_Total_Days_Get = 1;

            intI = 2;
            intK = 1;

            // string EmpNo = MachineNo;

            //string MachineID = UTF8Encryption(EmpNo);

            string SSQL = "";
            bool ErrFlag = false;

            DataTable Emp_DS = new DataTable();

            SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And IsActive='Yes'";
            if (MachineNo != "")
            {
                SSQL = SSQL + " And MachineID='" + MachineNo + "' ";

            }
            Emp_DS = ManualAttend.GetDetails(SSQL);

            if (Emp_DS.Rows.Count > 0)
            {
                ErrFlag = false;
            }
            else
            {
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                //DataTable DT_Chk = new DataTable();
                //SSQL = "Select * from LogTime_Days where Attn_Date_Str='" + Convert.ToDateTime(Date).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                //DT_Chk = ManualAttend.GetDetails(SSQL);
                //if (DT_Chk.Rows.Count > 0)
                //{
                //    ErrFlag = true;
                //}
                //else
                //{
                //    ErrFlag = false;
                //}
            }

            if (!ErrFlag)
            {
                for (int intRow = 0; intRow < Emp_DS.Rows.Count; intRow++)
                {

                    intK = 1;
                    string Emp_Total_Work_Time_1 = "00:00";
                    //Get Employee Week OF DAY
                    DataTable DS_WH = new DataTable();
                    string Emp_WH_Day;
                    string DOJ_Date_Str;
                    string MachineID1 = Emp_DS.Rows[intRow]["MachineID"].ToString();

                    string Query = " Select * from Employee_Mst where MachineID='" + MachineID1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' ";
                    //SSQL = "Select * from Employee_MST where MachineID='" + MachineID1 + "' And Compcode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'" +
                    DS_WH = ManualAttend.GetDetails(Query);
                    if (DS_WH.Rows.Count != 0)
                    {
                        Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                        DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();
                    }
                    else
                    {
                        Emp_WH_Day = "";
                        DOJ_Date_Str = "";
                    }
                    int colIndex = intK;
                    string DayofWeekName = "";
                    decimal Weekoff_Count = 0;
                    bool isPresent = false;
                    bool isWHPresent = false;
                    decimal Present_Count = 0;
                    decimal Present_WH_Count = 0;
                    int Appsent_Count = 0;
                    decimal Final_Count = 0;
                    decimal Total_Days_Count = 0;
                    string Already_Date_Check;
                    int Days_Insert_RowVal = 0;
                    decimal FullNight_Shift_Count = 0;
                    int NFH_Days_Count = 0;
                    decimal NFH_Days_Present_Count = 0;
                    Already_Date_Check = "";
                    string Year_Check = "";
                    string Time_IN_Str = "";
                    string Time_Out_Str = "";
                    int g = 1;

                    double OT_Time;

                    OT_Time = 0;

                    string INTime = "";
                    string OUTTime = "";
                    daysAdded = 1;

                    for (intCol = 0; (intCol <= (daysAdded - 1)); intCol++)
                    {
                        isPresent = false;
                        //string Emp_Total_Work_Time_1 = "00:00";
                        string Machine_ID_Str = "";
                        string OT_Week_OFF_Machine_No;
                        string Date_Value_Str = "";
                        string Total_Time_get = "";
                        string Final_OT_Work_Time_1 = "";
                        //DataSet mLocalDS = new DataSet();

                        //DataTable mLocalDS = new DataTable();


                        int j = 0;
                        double time_Check_dbl = 0;
                        string Date_Value_Str1 = "";
                        string Employee_Shift_Name_DB = "No Shift";
                        isPresent = false;


                        //Shift Change Check Variable Declaration Start
                        DateTime InTime_Check = new DateTime();
                        DateTime InToTime_Check = new DateTime();
                        TimeSpan InTime_TimeSpan;
                        string From_Time_Str = "";
                        string To_Time_Str = "";
                        //DataSet DS_InTime = new DataSet();
                        DataTable DS_Time = new DataTable();
                        //DataSet DS_Time = new DataSet();
                        DataTable DS_InTime = new DataTable();
                        DateTime EmpdateIN_Change = new DateTime();
                        DataTable InsertData = new DataTable();
                        string Final_InTime = "";
                        string Final_OutTime = "";
                        string Final_Shift = "";

                        int K = 0;
                        bool Shift_Check_blb = false;
                        string Shift_Start_Time_Change;
                        string Shift_End_Time_Change;
                        string Employee_Time_Change = "";
                        DateTime ShiftdateStartIN_Change = new DateTime();
                        DateTime ShiftdateEndIN_Change = new DateTime();

                        //Shift Change Check Variable Declaration End
                        string Employee_Punch = "";
                        Spin_Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID"].ToString();

                        if (Spin_Machine_ID_Str == "3")
                        {
                            Spin_Machine_ID_Str = "3";
                        }

                        // Machine_ID_Str = Encryption(Emp_DS.Rows[intRow]["MachineID"]);
                        Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID_Encrypt"].ToString();
                        OT_Week_OFF_Machine_No = Emp_DS.Rows[intRow]["MachineID"].ToString();
                        Date_Value_Str = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
                        Date_Value_Str1 = Convert.ToDateTime(Date_Value_Str).AddDays(1).ToShortDateString();

                        //DateTime dt = Convert.ToDateTime(Date.ToString());

                        //DayofWeekName = dt.DayOfWeek.ToString();

                        //TimeIN Get
                        if (OT_Week_OFF_Machine_No == "501")
                        {
                            OT_Week_OFF_Machine_No = "501";
                        }
                        SSQL = "";
                        SSQL = "Select distinct TimeIN,CONVERT(char(5), TimeIN, 108) as TimeIN24 from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                              " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";

                        mLocalDS = ManualAttend.GetDetails(SSQL);

                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";

                            INTime = "";

                        }
                        else
                        {
                            DateTime datetime1 = Convert.ToDateTime(mLocalDS.Rows[0][0].ToString());
                            INTime = datetime1.ToString("hh:mm tt");
                            //INTime = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0].ToString());
                        }

                        //TimeOUT Get
                        SSQL = "";
                        SSQL = "Select TimeOUT,CONVERT(char(5), TimeOUT, 108) as TimeOUT24 from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                               " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                        mLocalDS1 = ManualAttend.GetDetails(SSQL);

                        if (mLocalDS1.Rows.Count <= 0)
                        {
                            Time_Out_Str = "";

                            OUTTime = "";
                        }
                        else
                        {
                            DateTime datetime = Convert.ToDateTime(mLocalDS1.Rows[0][0].ToString());
                            OUTTime = datetime.ToString("hh:mm tt");
                            //OUTTime = string.Format(Convert.ToString(datetime), "HH:mm:ss tt");
                            //OUTTime = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0].ToString());
                        }

                        //Shift Change Check Start
                        if (mLocalDS.Rows.Count != 0)
                        {
                            InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
                            InToTime_Check = InTime_Check.AddHours(2);



                            InTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InTime_Check), "HH:mm:ss"));
                            InToTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InToTime_Check), "HH:mm:ss"));




                            //Old Time check error for 24 format

                            string TimeIN = InTime_Check.ToString();
                            string[] TimeIN_Split = TimeIN.Split(' ');
                            string TimeIN1 = TimeIN_Split[1].ToString();
                            string[] final = TimeIN1.Split(':');
                            string Final_IN = "";
                            if (Convert.ToInt32(final[0]) >= 10)
                            {
                                Final_IN = final[0] + ":" + final[1];
                            }
                            else
                            {
                                Final_IN = "0" + final[0] + ":" + final[1];
                            }


                            string TimeOUT = InToTime_Check.ToString();
                            string[] TimeOUT_Split = TimeOUT.Split(' ');
                            string TimeOUT1 = TimeOUT_Split[1].ToString();
                            string[] final1 = TimeOUT1.Split(':');
                            string Final_IN1 = "";
                            if (Convert.ToInt32(final1[0]) >= 10)
                            {
                                Final_IN1 = final1[0] + ":" + final1[1];
                            }
                            else
                            {
                                Final_IN1 = "0" + final1[0] + ":" + final1[1];
                            }




                            //New Format 24 change manually

                            //string InTime1 = mLocalDS.Rows[0]["TimeIN24"].ToString();

                            ////string TimeIN = InTime1.ToString();
                            ////string[] TimeIN_Split = TimeIN.Split(' ');
                            ////string TimeIN1 = TimeIN_Split[1].ToString();
                            //string[] final = InTime1.Split(':');
                            //string Final_IN = "";

                            //Final_IN = final[0] + ":" + final[1];




                            //string InTime2 = (Convert.ToInt16(final[0]) + 2).ToString();
                            //InTime2= InTime2+":"+ final[1];

                            ////string TimeOUT = InTime2.ToString();
                            ////string[] TimeOUT_Split = TimeOUT.Split(' ');
                            ////string TimeOUT1 = TimeOUT_Split[1].ToString();
                            //string[] final1 = InTime2.Split(':');
                            //string Final_IN1 = "";

                            //Final_IN1 = final1[0] + ":" + final1[1];



                            //InTime_TimeSpan = TimeSpan.Parse(InTime_Check).ToString();
                            //From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;

                            //Two Hours OutTime Check
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";


                            DS_Time = ManualAttend.GetDetails(SSQL);
                            if (DS_Time.Rows.Count != 0)
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                DS_InTime = ManualAttend.GetDetails(SSQL);
                                if (DS_InTime.Rows.Count != 0)
                                {
                                    //DataSet Shift_DS_Change = new DataSet();

                                    DataTable Shift_DS_Change = new DataTable();

                                    Final_InTime = DS_InTime.Rows[0]["TimeIN"].ToString();
                                    //Check With IN Time Shift
                                    SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%'";
                                    SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    Shift_DS_Change = ManualAttend.GetDetails(SSQL);
                                    Shift_Check_blb = false;
                                    for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
                                    {
                                        Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
                                        if (Shift_DS_Change.Rows[k]["EndIN_Days"].ToString() == "1")
                                        {
                                            Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                        }
                                        else
                                        {
                                            Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                        }

                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);

                                        if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                            Shift_Check_blb = true;
                                            break;
                                        }
                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                        EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                        if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                            Shift_Check_blb = true;
                                            break;
                                        }
                                    }
                                    if (Shift_Check_blb == true)
                                    {

                                        //IN Time Query Update
                                        string Querys = "";
                                        Querys = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                        " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                        mLocalDS = ManualAttend.GetDetails(Querys);

                                        if (Final_Shift == "SHIFT1")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                            " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        }
                                        else if (Final_Shift == "SHIFT2")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                                   " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        }
                                        else if (Final_Shift == "SHIFT3")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

                                        }
                                        else
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

                                        }


                                        mLocalDS1 = ManualAttend.GetDetails(Querys);
                                    }
                                    else
                                    {
                                        //Get Employee In Time
                                        SSQL = "";
                                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                               " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                        mLocalDS = ManualAttend.GetDetails(SSQL);
                                        //TimeOUT Get
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                               " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        mLocalDS1 = ManualAttend.GetDetails(SSQL);
                                    }
                                }
                                else
                                {
                                    //Get Employee In Time
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                           " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                    mLocalDS = ManualAttend.GetDetails(SSQL);

                                    //TimeOUT Get
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                          " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                    mLocalDS1 = ManualAttend.GetDetails(SSQL);

                                }
                            }
                            else
                            {
                                //Get Employee In Time
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                       " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                mLocalDS = ManualAttend.GetDetails(SSQL);

                                //TimeOUT Get
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                      " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                mLocalDS1 = ManualAttend.GetDetails(SSQL);
                            }
                        }
                        //Shift Change Check End 
                        if (mLocalDS.Rows.Count == 0)
                        {
                            Employee_Punch = "";
                        }
                        else
                        {
                            Employee_Punch = mLocalDS.Rows[0][0].ToString();
                        }
                        if (mLocalDS.Rows.Count >= 1)
                        {
                            for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                if (mLocalDS1.Rows.Count > tin)
                                {
                                    Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                                }
                                else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                                {
                                    Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                                }
                                else
                                {
                                    Time_Out_Str = "";
                                }
                                TimeSpan ts4 = new TimeSpan();
                                ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                                if (mLocalDS.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "";
                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                }
                                if (Time_IN_Str == "" || Time_Out_Str == "")
                                {
                                    time_Check_dbl = time_Check_dbl;
                                }
                                else
                                {


                                    DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                    DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                    TimeSpan ts = new TimeSpan();
                                    ts = date2.Subtract(date1);


                                    //& ":" & Trim(ts.Minutes)
                                    Total_Time_get = (ts.Hours).ToString();
                                    ts4 = ts4.Add(ts);

                                    //OT Time Get
                                    Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                    Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);

                                    if (Left_Val(ts4.Minutes.ToString(), 1) == "-" || Left_Val(ts4.Hours.ToString(), 1) == "-" || Emp_Total_Work_Time_1 == "0:-1")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                    }
                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {

                                        date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                        ts = date2.Subtract(date1);
                                        ts = date2.Subtract(date1);
                                        Total_Time_get = (ts.Hours).ToString();
                                        time_Check_dbl = double.Parse(Total_Time_get);
                                        Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes).ToString();
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:1" || Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }

                                    }
                                    else
                                    {
                                        time_Check_dbl = time_Check_dbl + double.Parse(Total_Time_get);
                                    }
                                }

                                break;

                            }//For End
                        }
                        else
                        {
                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;

                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                            }
                            for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
                            {
                                if (mLocalDS1.Rows.Count <= 0)
                                {
                                    Time_Out_Str = "";
                                }
                                else
                                {
                                    Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                                }

                                break;

                            }
                            //Emp_Total_Work_Time
                            if (Time_IN_Str == "" || Time_Out_Str == "")
                            {
                                time_Check_dbl = 0;
                            }
                            else
                            {
                                DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();
                                ts = date2.Subtract(date1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = (ts.Hours).ToString();
                                //OT Time Get
                                Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {

                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = ts.Hours.ToString();
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                    Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes);
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                        time_Check_dbl = 0;
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                        time_Check_dbl = 0;
                                    }
                                }
                                else
                                {
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                }
                            }
                        }
                        //Emp_Total_Work_Time

                        string StdWorkingHrs = "0";

                        if (Emp_DS.Rows[intRow]["StdWrkHrs"].ToString() != "" && Emp_DS.Rows[intRow]["StdWrkHrs"].ToString() != "0")
                        {
                            StdWorkingHrs = Emp_DS.Rows[intRow]["StdWrkHrs"].ToString();

                            string StdHrs = (Convert.ToDecimal(StdWorkingHrs) + Convert.ToDecimal(1)).ToString();

                            if (Convert.ToDouble(time_Check_dbl) >= Convert.ToDouble(StdHrs))
                            {
                                OT_Time = OT_Time + (time_Check_dbl - Convert.ToDouble(StdWorkingHrs));

                            }
                            else
                            {
                                OT_Time = 0;
                            }
                        }
                        else
                        {

                            if (time_Check_dbl >= 9)
                            {
                                OT_Time = OT_Time + (time_Check_dbl - 8);

                            }
                            else
                            {
                                OT_Time = 0;
                            }
                        }


                        string Emp_Total_Work_Time = "";
                        string Final_OT_Work_Time = "00:00";
                        string Final_OT_Work_Time_Val = "00:00";
                        Emp_Total_Work_Time = Emp_Total_Work_Time_1;
                        //Find Week OFF
                        string Employee_Week_Name = "";
                        string Assign_Week_Name = "";
                        mLocalDS = null;
                        Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();

                        SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "' and  Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "'";
                        mLocalDS = ManualAttend.GetDetails(SSQL);
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Assign_Week_Name = "";
                        }
                        else
                        {
                            //Assign_Week_Name = mLocalDS.Tables(0).Rows(0)(0)
                            Assign_Week_Name = "";
                        }

                        Boolean NFH_Day_Check = false;
                        DateTime NFH_Date;
                        DateTime DOJ_Date_Format;
                        DateTime Date_Value = new DateTime();
                        DataTable DT_NFH = new DataTable();
                        Date_Value = Convert.ToDateTime(Date_Value_Str.ToString());
                        string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value.ToString("yyyy/MM/dd") + "'";
                        DT_NFH = ManualAttend.GetDetails(qry_nfh);

                        if (DT_NFH.Rows.Count != 0)
                        {
                            NFH_Day_Check = true;
                            NFH_Days_Count = NFH_Days_Count + 1;
                        }

                        if (Employee_Week_Name == Assign_Week_Name)
                        {
                            //Get Employee Work Time
                            double Calculate_Attd_Work_Time = 8;
                            double Calculate_Attd_Work_Time_half = 0;


                            Calculate_Attd_Work_Time_half = 4.0;

                            halfPresent = "0";//Half Days Calculate Start
                            if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            {
                                Weekoff_Count = Weekoff_Count + 1;
                                isWHPresent = true;
                                halfPresent = "1";

                            }
                            //Half Days Calculate End
                            if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            {
                                Weekoff_Count = Weekoff_Count + 1;
                                isWHPresent = true;
                                halfPresent = "0";

                            }
                        }
                        else if (Employee_Week_Name == Emp_WH_Day)
                        {
                            //Get Employee Work Time
                            double Calculate_Attd_Work_Time = 8;
                            double Calculate_Attd_Work_Time_half = 0;


                            Calculate_Attd_Work_Time_half = 4.0;

                            halfPresent = "0";//Half Days Calculate Start
                            if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            {
                                isWHPresent = true;
                                halfPresent = "1";
                                Weekoff_Count = Weekoff_Count + 1;


                            }
                            //Half Days Calculate End
                            if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            {
                                isWHPresent = true;
                                halfPresent = "0";
                                Weekoff_Count = Weekoff_Count + 1;


                            }

                        }
                        else
                        {
                            //Get Employee Work Time
                            double Calculate_Attd_Work_Time = 8;
                            double Calculate_Attd_Work_Time_half = 0;


                            Calculate_Attd_Work_Time_half = 4.0;

                            halfPresent = "0";//Half Days Calculate Start
                            if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            {
                                isPresent = true;
                                halfPresent = "1";
                            }
                            //Half Days Calculate End
                            if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            {
                                isPresent = true;
                                halfPresent = "0";
                            }
                        }
                        //Shift Check Code Start
                        //DataSet Shift_Ds = new DataSet();

                        DataTable Shift_Ds = new DataTable();

                        string Start_IN = "";
                        string End_In = "";
                        string Shift_Name_Store = "";
                        bool Shift_Check_blb_Check = false;
                        DateTime ShiftdateStartIN_Check = new DateTime();
                        DateTime ShiftdateEndIN_Check = new DateTime();
                        DateTime EmpdateIN_Check = new DateTime();
                        string Wages = "";
                        if (INTime != "")
                        {

                            Shift_Check_blb_Check = false;
                            string SS = "GENERAL";
                            Wages = Emp_DS.Rows[intRow]["ShiftType"].ToString();

                            //if (Wages.ToUpper() == SS.ToUpper())
                            //{
                            //    Employee_Shift_Name_DB = "GENERAL";
                            //}
                            //else
                            //{
                            //Shift Master Check Code Start
                            SSQL = "Select * from Shift_Mst where  ShiftDesc <> 'GENERAL' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' Order by ShiftDesc Asc";
                            //  "CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'";
                            Shift_Ds = ManualAttend.GetDetails(SSQL);
                            if (Shift_Ds.Rows.Count != 0)
                            {
                                SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%' ";
                                SSQL = SSQL + "And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                //CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "' And 
                                Shift_Ds = ManualAttend.GetDetails(SSQL);
                                Shift_Check_blb = false;
                                for (int f = 0; f < Shift_Ds.Rows.Count; f++)
                                {

                                    int StartIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["StartIN_Days"]);
                                    int EndIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["EndIN_Days"]);
                                    Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(StartIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["StartIN"].ToString();
                                    End_In = Convert.ToDateTime(Date_Value_Str).AddDays(EndIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["EndIN"].ToString();
                                    ShiftdateStartIN_Check = System.Convert.ToDateTime(Start_IN);
                                    ShiftdateEndIN_Check = System.Convert.ToDateTime(End_In);
                                    EmpdateIN_Check = System.Convert.ToDateTime(Employee_Punch);
                                    if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                                    {
                                        Employee_Shift_Name_DB = Shift_Ds.Rows[f]["ShiftDesc"].ToString();
                                        Shift_Check_blb_Check = true;
                                    }

                                }
                                if (Shift_Check_blb_Check == false)
                                {
                                    Employee_Shift_Name_DB = "No Shift";
                                }
                            }
                            else
                            {
                                Employee_Shift_Name_DB = "No Shift";
                            }
                            //Shift Master Check Code End  
                            // }


                        }
                        else
                        {
                            Employee_Shift_Name_DB = "No Shift";
                        }
                        //Shift Check Code End
                        string ShiftType = "";
                        string Pre_Abs = "";
                        if (isPresent == true)
                        {

                            if (Employee_Shift_Name_DB != "No Shift")
                            {
                                ShiftType = "Proper";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }
                            else
                            {
                                if (Wages == "GENERAL")
                                {
                                    ShiftType = "GENERAL Mismatch";
                                }
                                else
                                {
                                    ShiftType = "Mismatch";
                                }

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }




                            if (halfPresent == "1")
                            {
                                Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_Count = Present_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
                                }
                            }
                            else if (halfPresent == "0")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                                }
                            }

                        }
                        else if (isWHPresent == true)
                        {

                            if (Employee_Shift_Name_DB != "No Shift")
                            {
                                ShiftType = "Proper";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }
                            else
                            {
                                if (Wages == "GENERAL")
                                {
                                    ShiftType = "GENERAL Mismatch";
                                }
                                else
                                {
                                    ShiftType = "Mismatch";
                                }

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }

                            if (halfPresent == "1")
                            {
                                Present_WH_Count = Convert.ToDecimal(Present_WH_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_WH_Count = Present_WH_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_Count = Present_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
                                }
                            }
                            else if (halfPresent == "0")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                                }
                            }
                        }
                        else
                        {

                            if (INTime == "" && OUTTime == "")
                            {
                                ShiftType = "Leave";
                                Employee_Shift_Name_DB = "Leave";
                                Pre_Abs = "Leave";

                            }
                            else if (INTime == "")
                            {
                                if (OUTTime != "")
                                {
                                    ShiftType = "Improper";
                                }
                                Pre_Abs = "Absent";

                            }
                            else if (OUTTime == "")
                            {
                                if (INTime != "")
                                {
                                    ShiftType = "Improper";
                                }
                                Pre_Abs = "Absent";

                            }
                            else if (INTime != "" && OUTTime != "")
                            {
                                ShiftType = "Absent";
                                Pre_Abs = "Absent";

                            }

                            Appsent_Count = Appsent_Count + 1;
                        }

                        intK += 1;




                        //Update Employee Worked_Days
                        //DataSet mDataSet = new DataSet();
                        DataTable mDataSet = new DataTable();
                        string Queryss = "";
                        DataSet Del_ds = new DataSet();
                        Queryss = "Select * from LogTime_Days where MachineID='" + MachineID1 + "' and  Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";

                        mDataSet = ManualAttend.GetDetails(Queryss);

                        //mDataSet = objdata.read(Queryss);  Emp_WH_Day
                        if (mDataSet.Rows.Count != 0)
                        {
                            Queryss = "Delete from LogTime_Days where MachineID='" + MachineID1 + "' and Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                            ManualAttend.GetDetails(Queryss);
                        }

                        DataSet Insert_Ds = new DataSet();

                        Queryss = "";
                        Queryss = "Insert Into LogTime_Days(CompCode,LocCode,MachineID,ExistingCode,FirstName,LastName,DeptName," +
                        "Designation,DOJ,Wages,CatName,SubCatName,Grade,BasicSalary,Present,Wh_Check,Wh_Count,Wh_Present_Count,NFH_Count,NFH_Present_Count,Shift,TimeIN,TimeOUT,Total_Hrs,OTHours,Attn_Date_Str,Attn_Date,MachineID_Encrypt,Total_Hrs1,TypeName,Present_Absent)" +
                         " Values('" + SessionCcode + "','" + SessionLcode + "','" + Emp_DS.Rows[intRow]["MachineID"] + "','" + Emp_DS.Rows[intRow]["ExistingCode"] + "'," +
                        "'" + Emp_DS.Rows[intRow]["FirstName"] + "','" + Emp_DS.Rows[intRow]["MiddleInitial"] + "'," +
                         "'" + Emp_DS.Rows[intRow]["DeptName"] + "','" + Emp_DS.Rows[intRow]["Designation"] + "'," +
                         "'" + Emp_DS.Rows[intRow]["DOJ"] + "','" + Emp_DS.Rows[intRow]["Wages"] + "','" + Emp_DS.Rows[intRow]["CatName"] + "','" + Emp_DS.Rows[intRow]["SubCatName"] + "','" + Emp_DS.Rows[intRow]["Grade"].ToString() + "','" + Emp_DS.Rows[intRow]["BasicSalary"] + "','" + Present_Count + "'," +
                         "'" + Emp_WH_Day + "','" + Weekoff_Count + "','" + Present_WH_Count + "','" + NFH_Days_Count + "','" + NFH_Days_Present_Count + "'," +
                         "'" + Employee_Shift_Name_DB + "','" + INTime + "','" + OUTTime + "','" + time_Check_dbl + "','" + OT_Time + "','" + Convert.ToDateTime(Date).ToString("yyyy/MM/dd") + "',Convert(datetime,'" + Convert.ToDateTime(Date).ToString("yyyy/MM/dd") + "',120),'" + UTF8Encryption(MachineID1) + "','" + Emp_Total_Work_Time_1 + "','" + ShiftType + "','" + Pre_Abs + "')";

                        ManualAttend.GetDetails(Queryss);

                        Present_Count = 0;
                        Present_WH_Count = 0;
                        Employee_Shift_Name_DB = "No Shift";
                        INTime = "";
                        OUTTime = "";
                        time_Check_dbl = 0;
                        OT_Time = 0;
                        Weekoff_Count = 0;
                        Emp_WH_Day = "";

                    }

                    intI += 1; ;
                    Total_Days_Count = 0;
                    Final_Count = 0;
                    Appsent_Count = 0;
                    Present_Count = 0;
                    Present_WH_Count = 0;
                    NFH_Days_Count = 0;
                    NFH_Days_Present_Count = 0;
                    INTime = "";
                    OUTTime = "";
                    Weekoff_Count = 0;
                    Emp_WH_Day = "";
                }



                //Calculation for Employee not register in Employee Mst
                DataTable DT_TempEmp = new DataTable();

                SSQL = "Select EmpNo from Temp_Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                DT_TempEmp = ManualAttend.GetDetails(SSQL);

                for (int Empk = 0; Empk < DT_TempEmp.Rows.Count; Empk++)
                {
                    intK = 1;
                    string Emp_Total_Work_Time_1 = "00:00";
                    //Get Employee Week OF DAY
                    DataTable DS_WH = new DataTable();
                    string Emp_WH_Day = "";
                    string DOJ_Date_Str = "";
                    string MachineID1 = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();


                    int colIndex = intK;
                    string DayofWeekName = "";
                    decimal Weekoff_Count = 0;
                    bool isPresent = false;
                    bool isWHPresent = false;
                    decimal Present_Count = 0;
                    decimal Present_WH_Count = 0;
                    int Appsent_Count = 0;
                    decimal Final_Count = 0;
                    decimal Total_Days_Count = 0;
                    string Already_Date_Check;
                    int Days_Insert_RowVal = 0;
                    decimal FullNight_Shift_Count = 0;
                    int NFH_Days_Count = 0;
                    decimal NFH_Days_Present_Count = 0;
                    Already_Date_Check = "";
                    string Year_Check = "";
                    string Time_IN_Str = "";
                    string Time_Out_Str = "";
                    int g = 1;

                    double OT_Time;

                    OT_Time = 0;

                    string INTime = "";
                    string OUTTime = "";
                    daysAdded = 1;

                    for (intCol = 0; (intCol <= (daysAdded - 1)); intCol++)
                    {
                        isPresent = false;
                        //string Emp_Total_Work_Time_1 = "00:00";
                        string Machine_ID_Str = "";
                        string OT_Week_OFF_Machine_No;
                        string Date_Value_Str = "";
                        string Total_Time_get = "";
                        string Final_OT_Work_Time_1 = "";
                        //DataSet mLocalDS = new DataSet();

                        //DataTable mLocalDS = new DataTable();


                        int j = 0;
                        double time_Check_dbl = 0;
                        string Date_Value_Str1 = "";
                        string Employee_Shift_Name_DB = "No Shift";
                        isPresent = false;


                        //Shift Change Check Variable Declaration Start
                        DateTime InTime_Check = new DateTime();
                        DateTime InToTime_Check = new DateTime();
                        TimeSpan InTime_TimeSpan;
                        string From_Time_Str = "";
                        string To_Time_Str = "";
                        //DataSet DS_InTime = new DataSet();
                        DataTable DS_Time = new DataTable();
                        //DataSet DS_Time = new DataSet();
                        DataTable DS_InTime = new DataTable();
                        DateTime EmpdateIN_Change = new DateTime();
                        DataTable InsertData = new DataTable();
                        string Final_InTime = "";
                        string Final_OutTime = "";
                        string Final_Shift = "";

                        int K = 0;
                        bool Shift_Check_blb = false;
                        string Shift_Start_Time_Change;
                        string Shift_End_Time_Change;
                        string Employee_Time_Change = "";
                        DateTime ShiftdateStartIN_Change = new DateTime();
                        DateTime ShiftdateEndIN_Change = new DateTime();

                        //Shift Change Check Variable Declaration End
                        string Employee_Punch = "";
                        Spin_Machine_ID_Str = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();

                        if (Spin_Machine_ID_Str == "603033")
                        {
                            Spin_Machine_ID_Str = "603033";
                        }

                        // Machine_ID_Str = Encryption(Emp_DS.Rows[intRow]["MachineID"]);
                        Machine_ID_Str = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();
                        OT_Week_OFF_Machine_No = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();
                        Date_Value_Str = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
                        Date_Value_Str1 = Convert.ToDateTime(Date_Value_Str).AddDays(1).ToShortDateString();

                        //DateTime dt = Convert.ToDateTime(Date.ToString());

                        //DayofWeekName = dt.DayOfWeek.ToString();

                        //TimeIN Get

                        SSQL = "";
                        SSQL = "Select distinct TimeIN,CONVERT(char(5), TimeIN, 108) as TimeIN24 from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                              " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";

                        mLocalDS = ManualAttend.GetDetails(SSQL);

                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";

                            INTime = "";

                        }
                        else
                        {
                            DateTime datetime1 = Convert.ToDateTime(mLocalDS.Rows[0][0].ToString());
                            INTime = datetime1.ToString("hh:mm tt");
                            //INTime = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0].ToString());
                        }

                        //TimeOUT Get
                        SSQL = "";
                        SSQL = "Select TimeOUT,CONVERT(char(5), TimeOUT, 108) as TimeOUT24 from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                               " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                        mLocalDS1 = ManualAttend.GetDetails(SSQL);

                        if (mLocalDS1.Rows.Count <= 0)
                        {
                            Time_Out_Str = "";

                            OUTTime = "";
                        }
                        else
                        {
                            DateTime datetime = Convert.ToDateTime(mLocalDS1.Rows[0][0].ToString());
                            OUTTime = datetime.ToString("hh:mm tt");
                            //OUTTime = string.Format(Convert.ToString(datetime), "HH:mm:ss tt");
                            //OUTTime = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0].ToString());
                        }

                        //Shift Change Check Start
                        if (mLocalDS.Rows.Count != 0)
                        {
                            InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
                            InToTime_Check = InTime_Check.AddHours(2);



                            InTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InTime_Check), "HH:mm:ss"));
                            InToTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InToTime_Check), "HH:mm:ss"));




                            //Old Time check error for 24 format

                            string TimeIN = InTime_Check.ToString();
                            string[] TimeIN_Split = TimeIN.Split(' ');
                            string TimeIN1 = TimeIN_Split[1].ToString();
                            string[] final = TimeIN1.Split(':');
                            string Final_IN = "";
                            if (Convert.ToInt32(final[0]) >= 10)
                            {
                                Final_IN = final[0] + ":" + final[1];
                            }
                            else
                            {
                                Final_IN = "0" + final[0] + ":" + final[1];
                            }


                            string TimeOUT = InToTime_Check.ToString();
                            string[] TimeOUT_Split = TimeOUT.Split(' ');
                            string TimeOUT1 = TimeOUT_Split[1].ToString();
                            string[] final1 = TimeOUT1.Split(':');
                            string Final_IN1 = "";
                            if (Convert.ToInt32(final1[0]) >= 10)
                            {
                                Final_IN1 = final1[0] + ":" + final1[1];
                            }
                            else
                            {
                                Final_IN1 = "0" + final1[0] + ":" + final1[1];
                            }



                            //Two Hours OutTime Check
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";


                            DS_Time = ManualAttend.GetDetails(SSQL);
                            if (DS_Time.Rows.Count != 0)
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                DS_InTime = ManualAttend.GetDetails(SSQL);
                                if (DS_InTime.Rows.Count != 0)
                                {
                                    //DataSet Shift_DS_Change = new DataSet();

                                    DataTable Shift_DS_Change = new DataTable();

                                    Final_InTime = DS_InTime.Rows[0]["TimeIN"].ToString();
                                    //Check With IN Time Shift
                                    SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%'";
                                    SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    Shift_DS_Change = ManualAttend.GetDetails(SSQL);
                                    Shift_Check_blb = false;
                                    for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
                                    {
                                        Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
                                        if (Shift_DS_Change.Rows[k]["EndIN_Days"].ToString() == "1")
                                        {
                                            Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                        }
                                        else
                                        {
                                            Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                        }

                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);

                                        if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                            Shift_Check_blb = true;
                                            break;
                                        }
                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                        EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                        if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                            Shift_Check_blb = true;
                                            break;
                                        }
                                    }
                                    if (Shift_Check_blb == true)
                                    {

                                        //IN Time Query Update
                                        string Querys = "";
                                        Querys = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                        " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                        mLocalDS = ManualAttend.GetDetails(Querys);

                                        if (Final_Shift == "SHIFT1")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                            " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        }
                                        else if (Final_Shift == "SHIFT2")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                                   " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        }
                                        else if (Final_Shift == "SHIFT3")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

                                        }
                                        else
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

                                        }


                                        mLocalDS1 = ManualAttend.GetDetails(Querys);
                                    }
                                    else
                                    {
                                        //Get Employee In Time
                                        SSQL = "";
                                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                               " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                        mLocalDS = ManualAttend.GetDetails(SSQL);
                                        //TimeOUT Get
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                               " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        mLocalDS1 = ManualAttend.GetDetails(SSQL);
                                    }
                                }
                                else
                                {
                                    //Get Employee In Time
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                           " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                    mLocalDS = ManualAttend.GetDetails(SSQL);

                                    //TimeOUT Get
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                          " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                    mLocalDS1 = ManualAttend.GetDetails(SSQL);

                                }
                            }
                            else
                            {
                                //Get Employee In Time
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                       " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                mLocalDS = ManualAttend.GetDetails(SSQL);

                                //TimeOUT Get
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                      " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                mLocalDS1 = ManualAttend.GetDetails(SSQL);
                            }
                        }
                        //Shift Change Check End 
                        if (mLocalDS.Rows.Count == 0)
                        {
                            Employee_Punch = "";
                        }
                        else
                        {
                            Employee_Punch = mLocalDS.Rows[0][0].ToString();
                        }
                        if (mLocalDS.Rows.Count >= 1)
                        {
                            for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                if (mLocalDS1.Rows.Count > tin)
                                {
                                    Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                                }
                                else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                                {
                                    Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                                }
                                else
                                {
                                    Time_Out_Str = "";
                                }
                                TimeSpan ts4 = new TimeSpan();
                                ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                                if (mLocalDS.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "";
                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                }
                                if (Time_IN_Str == "" || Time_Out_Str == "")
                                {
                                    time_Check_dbl = time_Check_dbl;
                                }
                                else
                                {


                                    DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                    DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                    TimeSpan ts = new TimeSpan();
                                    ts = date2.Subtract(date1);


                                    //& ":" & Trim(ts.Minutes)
                                    Total_Time_get = (ts.Hours).ToString();
                                    ts4 = ts4.Add(ts);

                                    //OT Time Get
                                    Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                    Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);

                                    if (Left_Val(ts4.Minutes.ToString(), 1) == "-" || Left_Val(ts4.Hours.ToString(), 1) == "-" || Emp_Total_Work_Time_1 == "0:-1")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                    }
                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {

                                        date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                        ts = date2.Subtract(date1);
                                        ts = date2.Subtract(date1);
                                        Total_Time_get = (ts.Hours).ToString();
                                        time_Check_dbl = double.Parse(Total_Time_get);
                                        Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes).ToString();
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:1" || Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }

                                    }
                                    else
                                    {
                                        time_Check_dbl = time_Check_dbl + double.Parse(Total_Time_get);
                                    }
                                }

                                break;

                            }//For End
                        }
                        else
                        {
                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;

                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                            }
                            for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
                            {
                                if (mLocalDS1.Rows.Count <= 0)
                                {
                                    Time_Out_Str = "";
                                }
                                else
                                {
                                    Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                                }

                                break;

                            }
                            //Emp_Total_Work_Time
                            if (Time_IN_Str == "" || Time_Out_Str == "")
                            {
                                time_Check_dbl = 0;
                            }
                            else
                            {
                                DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();
                                ts = date2.Subtract(date1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = (ts.Hours).ToString();
                                //OT Time Get
                                Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {

                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = ts.Hours.ToString();
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                    Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes);
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                        time_Check_dbl = 0;
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                        time_Check_dbl = 0;
                                    }
                                }
                                else
                                {
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                }
                            }
                        }
                        //Emp_Total_Work_Time

                        string StdWorkingHrs = "0";

                        if (time_Check_dbl >= 9)
                        {
                            OT_Time = OT_Time + (time_Check_dbl - 8);

                        }
                        else
                        {
                            OT_Time = 0;
                        }



                        string Emp_Total_Work_Time = "";
                        string Final_OT_Work_Time = "00:00";
                        string Final_OT_Work_Time_Val = "00:00";
                        Emp_Total_Work_Time = Emp_Total_Work_Time_1;
                        //Find Week OFF
                        string Employee_Week_Name = "";
                        string Assign_Week_Name = "";
                        mLocalDS = null;
                        Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();

                        SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "' and  Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "'";
                        mLocalDS = ManualAttend.GetDetails(SSQL);
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Assign_Week_Name = "";
                        }
                        else
                        {
                            //Assign_Week_Name = mLocalDS.Tables(0).Rows(0)(0)
                            Assign_Week_Name = "";
                        }

                        Boolean NFH_Day_Check = false;
                        DateTime NFH_Date;
                        DateTime DOJ_Date_Format;
                        DateTime Date_Value = new DateTime();
                        DataTable DT_NFH = new DataTable();
                        Date_Value = Convert.ToDateTime(Date_Value_Str.ToString());


                        if (Employee_Week_Name == Assign_Week_Name)
                        {
                            //Get Employee Work Time
                            double Calculate_Attd_Work_Time = 8;
                            double Calculate_Attd_Work_Time_half = 0;


                            Calculate_Attd_Work_Time_half = 4.0;

                            halfPresent = "0";//Half Days Calculate Start
                            if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            {
                                Weekoff_Count = Weekoff_Count + 1;
                                isWHPresent = true;
                                halfPresent = "1";

                            }
                            //Half Days Calculate End
                            if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            {
                                Weekoff_Count = Weekoff_Count + 1;
                                isWHPresent = true;
                                halfPresent = "0";

                            }
                        }
                        else if (Employee_Week_Name == Emp_WH_Day)
                        {
                            //Get Employee Work Time
                            double Calculate_Attd_Work_Time = 8;
                            double Calculate_Attd_Work_Time_half = 0;


                            Calculate_Attd_Work_Time_half = 4.0;

                            halfPresent = "0";//Half Days Calculate Start
                            if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            {
                                isWHPresent = true;
                                halfPresent = "1";
                                Weekoff_Count = Weekoff_Count + 1;


                            }
                            //Half Days Calculate End
                            if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            {
                                isWHPresent = true;
                                halfPresent = "0";
                                Weekoff_Count = Weekoff_Count + 1;


                            }

                        }
                        else
                        {
                            //Get Employee Work Time
                            double Calculate_Attd_Work_Time = 8;
                            double Calculate_Attd_Work_Time_half = 0;


                            Calculate_Attd_Work_Time_half = 4.0;

                            halfPresent = "0";//Half Days Calculate Start
                            if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            {
                                isPresent = true;
                                halfPresent = "1";
                            }
                            //Half Days Calculate End
                            if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            {
                                isPresent = true;
                                halfPresent = "0";
                            }
                        }
                        //Shift Check Code Start
                        //DataSet Shift_Ds = new DataSet();

                        DataTable Shift_Ds = new DataTable();

                        string Start_IN = "";
                        string End_In = "";
                        string Shift_Name_Store = "";
                        bool Shift_Check_blb_Check = false;
                        DateTime ShiftdateStartIN_Check = new DateTime();
                        DateTime ShiftdateEndIN_Check = new DateTime();
                        DateTime EmpdateIN_Check = new DateTime();
                        if (isPresent == true || isWHPresent == true)
                        {
                            Shift_Check_blb_Check = false;


                            //Shift Master Check Code Start
                            SSQL = "Select * from Shift_Mst where  ShiftDesc <> 'GENERAL' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' Order by ShiftDesc Asc";
                            //  "CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'";
                            Shift_Ds = ManualAttend.GetDetails(SSQL);
                            if (Shift_Ds.Rows.Count != 0)
                            {
                                SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%' ";
                                SSQL = SSQL + "And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                //CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "' And 
                                Shift_Ds = ManualAttend.GetDetails(SSQL);
                                Shift_Check_blb = false;
                                for (int f = 0; f < Shift_Ds.Rows.Count; f++)
                                {

                                    int StartIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["StartIN_Days"]);
                                    int EndIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["EndIN_Days"]);
                                    Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(StartIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["StartIN"].ToString();
                                    End_In = Convert.ToDateTime(Date_Value_Str).AddDays(EndIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["EndIN"].ToString();
                                    ShiftdateStartIN_Check = System.Convert.ToDateTime(Start_IN);
                                    ShiftdateEndIN_Check = System.Convert.ToDateTime(End_In);
                                    EmpdateIN_Check = System.Convert.ToDateTime(Employee_Punch);
                                    if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                                    {
                                        Employee_Shift_Name_DB = Shift_Ds.Rows[f]["ShiftDesc"].ToString();
                                        Shift_Check_blb_Check = true;
                                    }

                                }
                                if (Shift_Check_blb_Check == false)
                                {
                                    Employee_Shift_Name_DB = "No Shift";
                                }
                            }
                            else
                            {
                                Employee_Shift_Name_DB = "No Shift";
                            }
                            //Shift Master Check Code End  

                        }
                        else
                        {
                            Employee_Shift_Name_DB = "No Shift";

                        }
                        //Shift Check Code End
                        string ShiftType = "";
                        string Pre_Abs = "";


                        if (isPresent == true)
                        {

                            if (Employee_Shift_Name_DB != "No Shift")
                            {
                                ShiftType = "Proper";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }
                            else
                            {

                                ShiftType = "Mismatch";


                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }








                            if (halfPresent == "1")
                            {
                                Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_Count = Present_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
                                }
                            }
                            else if (halfPresent == "0")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                                }
                            }

                        }
                        else if (isWHPresent == true)
                        {

                            if (Employee_Shift_Name_DB != "No Shift")
                            {
                                ShiftType = "Proper";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }
                            else
                            {


                                ShiftType = "Mismatch";


                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }



                            if (halfPresent == "1")
                            {
                                Present_WH_Count = Convert.ToDecimal(Present_WH_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_WH_Count = Present_WH_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_Count = Present_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
                                }
                            }
                            else if (halfPresent == "0")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                                }
                            }
                        }
                        else
                        {

                            if (INTime == "" && OUTTime == "")
                            {
                                ShiftType = "Leave";
                                Employee_Shift_Name_DB = "Leave";
                                Pre_Abs = "Leave";

                            }
                            else if (INTime == "")
                            {
                                if (OUTTime != "")
                                {
                                    ShiftType = "Improper";
                                }
                                Pre_Abs = "Absent";

                            }
                            else if (OUTTime == "")
                            {
                                if (INTime != "")
                                {
                                    ShiftType = "Improper";
                                }
                                Pre_Abs = "Absent";

                            }
                            else if (INTime != "" && OUTTime != "")
                            {
                                ShiftType = "Absent";
                                Pre_Abs = "Absent";

                            }
                            Appsent_Count = Appsent_Count + 1;
                        }

                        intK += 1;




                        //Update Employee Worked_Days
                        //DataSet mDataSet = new DataSet();
                        DataTable mDataSet = new DataTable();
                        string Queryss = "";
                        DataSet Del_ds = new DataSet();
                        Queryss = "Select * from LogTime_Days where MachineID='" + MachineID1 + "' and  Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";

                        mDataSet = ManualAttend.GetDetails(Queryss);

                        //mDataSet = objdata.read(Queryss);  Emp_WH_Day
                        if (mDataSet.Rows.Count != 0)
                        {
                            Queryss = "Delete from LogTime_Days where MachineID='" + MachineID1 + "' and Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                            ManualAttend.GetDetails(Queryss);
                        }
                        DataSet Insert_Ds = new DataSet();

                        Queryss = "";
                        Queryss = "Insert Into LogTime_Days(CompCode,LocCode,MachineID,ExistingCode,Present,Wh_Check,Wh_Count,";
                        Queryss = Queryss + "Wh_Present_Count,NFH_Count,NFH_Present_Count,Shift,TimeIN,TimeOUT,Total_Hrs,OTHours,";
                        Queryss = Queryss + "Attn_Date_Str,Attn_Date,MachineID_Encrypt,Total_Hrs1,TypeName,Present_Absent)" +
                         " Values('" + SessionCcode + "','" + SessionLcode + "','" + MachineID1 + "','" + MachineID1 + "','" + Present_Count + "'," +
                         "'" + Emp_WH_Day + "','" + Weekoff_Count + "','" + Present_WH_Count + "','" + NFH_Days_Count + "','" + NFH_Days_Present_Count + "'," +
                         "'" + Employee_Shift_Name_DB + "','" + INTime + "','" + OUTTime + "','" + time_Check_dbl + "','" + OT_Time + "','" + Convert.ToDateTime(Date).ToString("yyyy/MM/dd") + "',Convert(datetime,'" + Convert.ToDateTime(Date).ToString("yyyy/MM/dd") + "',120),'" + UTF8Encryption(MachineID1) + "','" + Emp_Total_Work_Time_1 + "','" + ShiftType + "','" + Pre_Abs + "')";

                        ManualAttend.GetDetails(Queryss);

                        Present_Count = 0;
                        Present_WH_Count = 0;
                        Employee_Shift_Name_DB = "No Shift";
                        INTime = "";
                        OUTTime = "";
                        time_Check_dbl = 0;
                        OT_Time = 0;
                        Weekoff_Count = 0;
                        Emp_WH_Day = "";

                    }

                    intI += 1; ;
                    Total_Days_Count = 0;
                    Final_Count = 0;
                    Appsent_Count = 0;
                    Present_Count = 0;
                    Present_WH_Count = 0;
                    NFH_Days_Count = 0;
                    NFH_Days_Present_Count = 0;
                    INTime = "";
                    OUTTime = "";
                    Weekoff_Count = 0;
                    Emp_WH_Day = "";
                }



            }



        }


        private static string UTF8Encryption(string password)
        {
            string strmsg = string.Empty;
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            strmsg = Convert.ToBase64String(encode);
            return strmsg;
        }

        public JsonResult GetEmpID()
        {
            DataTable dt = new DataTable();
            string SQL = "Select EM.EmpNo,EM.ExistingCode,EM.FirstName,DM.DeptName from Employee_Mst EM inner join Department_Mst DM on EM.DeptName=DM.DeptCode where EM.IsActive='Yes'";
            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> PartyList = new List<ManualAttend>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend Qtylist = new ManualAttend();
                Qtylist.EmpNo = dt.Rows[i]["EmpNo"].ToString();
                Qtylist.ExistingCode = dt.Rows[i]["ExistingCode"].ToString();
                Qtylist.EmpName = dt.Rows[i]["FirstName"].ToString();
                Qtylist.DeptName = dt.Rows[i]["DeptName"].ToString();
                PartyList.Add(Qtylist);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLeaveType()
        {
            DataTable dt = new DataTable();
            string SQL = "Select LeaveCode,LeaveType from ManLeave_Details where  CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> PartyList = new List<ManualAttend>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend Qtylist = new ManualAttend();
                Qtylist.CodeVal = dt.Rows[i]["LeaveCode"].ToString();
                Qtylist.TypeVal = dt.Rows[i]["LeaveType"].ToString();

                PartyList.Add(Qtylist);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOTLoad()
        {
            DataTable dt = new DataTable();
            string SQL = "Select EmpNo,ExistingCode,EmpName,AttnDate,OThours from ManOT_Details where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> PartyList = new List<ManualAttend>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend Qtylist = new ManualAttend();
                Qtylist.EmpNo = dt.Rows[i]["EmpNo"].ToString();
                Qtylist.ExistingCode = dt.Rows[i]["ExistingCode"].ToString();
                Qtylist.EmpName = dt.Rows[i]["EmpName"].ToString();
                Qtylist.AttnDate = dt.Rows[i]["AttnDate"].ToString();
                Qtylist.TimeIn = dt.Rows[i]["OThours"].ToString();
                PartyList.Add(Qtylist);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllLeaveLoad()
        {
            DataTable dt = new DataTable();
            string SQL = "Select EmpNo,ExistingCode,EmpName,FromDate,ToDate,LeaveDesc from ManLeave_Entry where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> PartyList = new List<ManualAttend>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend Qtylist = new ManualAttend();
                Qtylist.EmpNo = dt.Rows[i]["EmpNo"].ToString();
                Qtylist.ExistingCode = dt.Rows[i]["ExistingCode"].ToString();
                Qtylist.EmpName = dt.Rows[i]["EmpName"].ToString();
                Qtylist.AttnDate = dt.Rows[i]["FromDate"].ToString();
                Qtylist.AttnDateStr = dt.Rows[i]["ToDate"].ToString();
                Qtylist.LeaveDesc = dt.Rows[i]["LeaveDesc"].ToString();
                PartyList.Add(Qtylist);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAttendanceLoad()
        {
            DataTable dt = new DataTable();
            string SQL = "Select EmpNo,ExistingCode,EmpName,AttnDate,LogTimeIn,LogTimeOut from ManAttn_Details where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> PartyList = new List<ManualAttend>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend Qtylist = new ManualAttend();
                Qtylist.EmpNo = dt.Rows[i]["EmpNo"].ToString();
                Qtylist.ExistingCode = dt.Rows[i]["ExistingCode"].ToString();
                Qtylist.EmpName = dt.Rows[i]["EmpName"].ToString();
                Qtylist.AttnDate = dt.Rows[i]["AttnDate"].ToString();
                Qtylist.TimeIn = dt.Rows[i]["LogTimeIn"].ToString();
                Qtylist.TimeOut = dt.Rows[i]["LogTimeOut"].ToString();
                PartyList.Add(Qtylist);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLeaveTypeLoad()
        {
            DataTable dt = new DataTable();
            string SQL = "Select LeaveType from ManLeave_Details where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> PartyList = new List<ManualAttend>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend Qtylist = new ManualAttend();
                Qtylist.UserType = dt.Rows[i]["LeaveType"].ToString();

                PartyList.Add(Qtylist);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTimeDeleteIN(string EmpNo, string TimeINdate)
        {

            DataTable dt_TimeIN = new DataTable();
            DateTime AtteDate;

            Machine_ID_Encrypt = UTF8Encryption(EmpNo);
            string attdate = TimeINdate;
            AtteDate = Convert.ToDateTime(attdate.ToString());

            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Encrypt + "'";
            SSQL = SSQL + "And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + "And TimeIN >='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01" + "'";
            SSQL = SSQL + "And TimeIN <='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59" + "' Order by TimeIN ASC";

            dt_TimeIN = ManualAttend.GetDetails(SSQL);

            List<ManualAttend> empList = new List<ManualAttend>();
            for (int i = 0; i < dt_TimeIN.Rows.Count; i++)
            {
                ManualAttend student = new ManualAttend();

                student.TimeIn = dt_TimeIN.Rows[i]["TimeIN"].ToString();


                empList.Add(student);
            }


            return Json(empList, JsonRequestBehavior.AllowGet);




            //return JsonResult();
        }

        public JsonResult GetTimeDeleteOUT(string EmpNo, string TimeOUTdate)
        {
            DataTable dt_TimeIN = new DataTable();
            DateTime AtteDate;

            Machine_ID_Encrypt = UTF8Encryption(EmpNo);
            string attdate = TimeOUTdate;
            AtteDate = Convert.ToDateTime(attdate.ToString());

            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Encrypt + "'";
            SSQL = SSQL + "And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + "And TimeOUT >='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01" + "'";
            SSQL = SSQL + "And TimeOUT <='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59" + "' Order by TimeOUT ASC";

            dt_TimeIN = ManualAttend.GetDetails(SSQL);

            List<ManualAttend> empList = new List<ManualAttend>();
            for (int i = 0; i < dt_TimeIN.Rows.Count; i++)
            {
                ManualAttend student = new ManualAttend();

                student.TimeIn = dt_TimeIN.Rows[i]["TimeOUT"].ToString();


                empList.Add(student);
            }


            return Json(empList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteTimeIN(string EmpNo, string TimeINdate)
        {
            DataTable dt_TimeIN = new DataTable();
            DataTable dt_Manual = new DataTable();
            DataTable dt_Temp = new DataTable();
            DateTime AtteDate;

            Machine_ID_Encrypt = UTF8Encryption(EmpNo);
            string attdate = TimeINdate;
            AtteDate = Convert.ToDateTime(attdate.ToString());


            //Delete LogTime_IN
            SSQL = "delete from LogTime_IN where MachineID='" + Machine_ID_Encrypt + "'";
            SSQL = SSQL + "And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + "And TimeIN >='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01" + "'";
            SSQL = SSQL + "And TimeIN <='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59" + "'";

            ManualAttend.GetDetails(SSQL);


            //Get Manual Attedance 
            SSQL = "";
            SSQL = SSQL + "select *  from ManAttn_Details where EmpNo='" + EmpNo + "' and convert(datetime,AttnDate,103)=convert(datetime,'" + TimeINdate + "',103)";
            SSQL = SSQL + "And CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dt_Manual = ManualAttend.GetDetails(SSQL);

            if (dt_Manual.Rows.Count != 0)
            {
                //Delete Manual Attendance 
                SSQL = "";
                SSQL = SSQL + "delete from ManAttn_Details where EmpNo='" + EmpNo + "' and convert(datetime,AttnDate,103)=convert(datetime,'" + TimeINdate + "',103)";
                SSQL = SSQL + "And CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                ManualAttend.GetDetails(SSQL);
            }

            SSQL = "";
            SSQL = SSQL + "select * from LogTime_Days where MachineID='" + EmpNo + "' and convert(datetime,Attn_Date_Str,103)=convert(datetime,'" + TimeINdate + "',103) and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dt_Temp = ManualAttend.GetDetails(SSQL);

            if (dt_Temp.Rows.Count != 0)
            {
                SSQL = SSQL + "delete from LogTime_Days where MachineID='" + EmpNo + "' and convert(datetime,Attn_Date_Str,103)=convert(datetime,'" + TimeINdate + "',103) and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                ManualAttend.GetDetails(SSQL);
            }



            //Get LogTime_IN
            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Encrypt + "'";
            SSQL = SSQL + "And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + "And TimeIN >='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01" + "'";
            SSQL = SSQL + "And TimeIN <='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59" + "' Order by TimeIN ASC";

            dt_TimeIN = ManualAttend.GetDetails(SSQL);

            List<ManualAttend> empList = new List<ManualAttend>();
            for (int i = 0; i < dt_TimeIN.Rows.Count; i++)
            {
                ManualAttend student = new ManualAttend();

                student.TimeIn = dt_TimeIN.Rows[i]["TimeIN"].ToString();

                empList.Add(student);
            }


            return Json(empList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteTimeOUT(string EmpNo, string TimeOUTdate)
        {
            DataTable dt_TimeIN = new DataTable();
            DataTable dt_Manual = new DataTable();
            DataTable dt_Temp = new DataTable();
            DateTime AtteDate;

            Machine_ID_Encrypt = UTF8Encryption(EmpNo);
            string attdate = TimeOUTdate;
            AtteDate = Convert.ToDateTime(attdate.ToString());

            SSQL = "delete from LogTime_OUT where MachineID='" + Machine_ID_Encrypt + "'";
            SSQL = SSQL + "And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + "And TimeOUT >='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01" + "'";
            SSQL = SSQL + "And TimeOUT <='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59" + "'";

            ManualAttend.GetDetails(SSQL);

            //Get Manual Attedance 
            SSQL = "";
            SSQL = SSQL + "select *  from ManAttn_Details where EmpNo='" + EmpNo + "' and convert(datetime,AttnDate,103)=convert(datetime,'" + TimeOUTdate + "',103)";
            SSQL = SSQL + "And CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dt_Manual = ManualAttend.GetDetails(SSQL);

            if (dt_Manual.Rows.Count != 0)
            {
                //Delete Manual Attendance 
                SSQL = "";
                SSQL = SSQL + "delete from ManAttn_Details where EmpNo='" + EmpNo + "' and convert(datetime,AttnDate,103)=convert(datetime,'" + TimeOUTdate + "',103)";
                SSQL = SSQL + "And CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                ManualAttend.GetDetails(SSQL);
            }

            SSQL = "";
            SSQL = SSQL + "select * from LogTime_Days where MachineID='" + EmpNo + "' and convert(datetime,Attn_Date_Str,103)=convert(datetime,'" + TimeOUTdate + "',103) and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dt_Temp = ManualAttend.GetDetails(SSQL);

            if (dt_Temp.Rows.Count != 0)
            {
                SSQL = SSQL + "delete from LogTime_Days where MachineID='" + EmpNo + "' and convert(datetime,Attn_Date_Str,103)=convert(datetime,'" + TimeOUTdate + "',103) and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                ManualAttend.GetDetails(SSQL);
            }



            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Encrypt + "'";
            SSQL = SSQL + "And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + "And TimeOUT >='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01" + "'";
            SSQL = SSQL + "And TimeOUT <='" + AtteDate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59" + "' Order by TimeOUT ASC";

            dt_TimeIN = ManualAttend.GetDetails(SSQL);

            List<ManualAttend> empList = new List<ManualAttend>();
            for (int i = 0; i < dt_TimeIN.Rows.Count; i++)
            {
                ManualAttend student = new ManualAttend();

                student.TimeIn = dt_TimeIN.Rows[i]["TimeOUT"].ToString();

                empList.Add(student);
            }


            return Json(empList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetEditEmpID(string EmpNo)
        {
            DataTable employees = new DataTable();

            string query = "select EmpNo,ExistingCode,(FirstName + '.' + MiddleInitial) as EmpName,DM.DeptName from Employee_Mst EM inner join Department_Mst DM on EM.DeptName=DM.DeptCode where EM.IsActive='Yes' and EmpNo='" + EmpNo + "'";

            employees = ManualAttend.GetDetails(query);

            List<ManualAttend> empList = new List<ManualAttend>();
            for (int i = 0; i < employees.Rows.Count; i++)
            {
                ManualAttend student = new ManualAttend();
                student.EmpNo = employees.Rows[i]["EmpNo"].ToString();
                student.ExistingCode = employees.Rows[i]["ExistingCode"].ToString();
                student.EmpName = employees.Rows[i]["EmpName"].ToString();
                student.DeptName = employees.Rows[i]["DeptName"].ToString();


                empList.Add(student);
            }


            return Json(empList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEditEmpidOT(string EmpNo)
        {
            DataTable employees = new DataTable();

            string query = "select EmpNo,ExistingCode,EmpName,AttnDate,OThours from ManOT_Details where  EmpNo='" + EmpNo + "'";

            employees = ManualAttend.GetDetails(query);

            List<ManualAttend> empList = new List<ManualAttend>();
            for (int i = 0; i < employees.Rows.Count; i++)
            {
                ManualAttend student = new ManualAttend();
                student.EmpNo = employees.Rows[i]["EmpNo"].ToString();
                student.ExistingCode = employees.Rows[i]["ExistingCode"].ToString();
                student.EmpName = employees.Rows[i]["EmpName"].ToString();
                student.AttnDate = employees.Rows[i]["AttnDate"].ToString();
                student.TimeIn = employees.Rows[i]["OThours"].ToString();


                empList.Add(student);
            }


            return Json(empList, JsonRequestBehavior.AllowGet);
        }

        private static string UTF8Decryption(string encryptpwd)
        {
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }


        public string Left_Val(string Value, int Length)
        {

            if (Value.Length >= Length)
            {
                return Value.Substring(0, Length);
            }
            else
            {
                return Value;
            }
        }

        public string Right_Val(string Value, int Length)
        {

            int i = 0;
            i = 0;
            if (Value.Length >= Length)
            {
                //i = Value.Length - Length
                return Value.Substring(Value.Length - Length, Length);
            }
            else
            {
                return Value;
            }
        }
    }
}