﻿using AttendanceMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AttendanceMVC.Controllers
{
    public class MasterController : Controller
    {
        static string SessionCcode = "";
        static string SessionLcode = "";
        static string SessionUserName;
        static string SessionUserID;
        static string SessionIsAdmin;
        // GET: Master
        public ActionResult UserCreation()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult HolidayMaster()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult RouteMaster()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
       


 	public ActionResult LabourAllotment()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult UserRights()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();
                SessionIsAdmin = Session["Isadmin"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public JsonResult InsertUserCreation(ManualAttend UserDet)
        {
            string query = "";
            string message = "Success";
            DataTable Dept_check = new DataTable();

            if (UserDet.UserName != "")
            {
               
                    query = "select * from MstUsers where UserName='" + UserDet.UserName + "'";
                    query = query + " And CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "'";

                    Dept_check = MasterDate.GetDetails(query);

               
                    if (Dept_check.Rows.Count != 0)
                    {
                        query = "Delete from MstUsers where UserName='" + UserDet.UserName + "'";
                        query = query + " And CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "'";
                        MasterDate.GetDetails(query);

                    query = "Delete from [Mvc_Rights]..MstUsers where UserName='" + UserDet.UserName + "'";
                    query = query + " And CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "'";
                    MasterDate.GetDetails(query);


                    message = "Update";
                    }

                    //Inert into MstUsers
                    query = "insert into MstUsers(UserCode,UserName,Password,IsAdmin,CompCode,LocationCode)values";
                    query = query + "('" + UserDet.UserName + "','" + UserDet.UserName + "','" + UTF8Encryption(UserDet.Password) + "','" + UserDet.UserType + "','" + SessionCcode + "','" + SessionLcode + "')";
                    MasterDate.GetDetails(query);

                query = "insert into [Mvc_Rights]..MstUsers(UserCode,UserName,Password,IsAdmin,CompCode,LocationCode)values";
                query = query + "('" + UserDet.UserName + "','" + UserDet.UserName + "','" + UTF8Encryption(UserDet.Password) + "','" + UserDet.UserType + "','" + SessionCcode + "','" + SessionLcode + "')";
                MasterDate.GetDetails(query);

            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        public JsonResult GetUserDetails()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<ManualAttend> PartyList = new List<ManualAttend>();


            string SQL = "Select * from MstUsers where";
            SQL = SQL + " CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "'";
            dt = ManualAttend.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend list = new ManualAttend();

                list.DeptName = dt.Rows[i]["CompCode"].ToString();
                list.EmpName = dt.Rows[i]["LocationCode"].ToString();
                list.UserName = dt.Rows[i]["UserName"].ToString();
                
                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDeleteUser(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";
            string SQL = "select * from MstUsers where UserName='" + ID + "'";
            SQL = SQL + " And CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "'";

            dt = MasterDate.GetDetails(SQL);

            if (dt.Rows.Count != 0)
            {
                SQL = "Delete from MstUsers where UserName='" + ID + "'";
                SQL = SQL + " And CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "'";
                MasterDate.GetDetails(SQL);

                message = "Delete";
            }
            else
            {
                message = "Already";
            }
            

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        public JsonResult GetDesignation()
        {
          
            DataTable dt = new DataTable();

            string SQL = "Select DE.DesignNo,DE.DesignName from Designation_Mst DE inner join Department_Mst DM";
            SQL = SQL + " on DE.DeptCode=DM.DeptCode";
            SQL = SQL + " where DE.CompCode='" + SessionCcode + "' And DE.LocCode='" + SessionLcode + "'";
            SQL = SQL + " And DM.CompCode='" + SessionCcode + "' And DM.LocCode='" + SessionLcode + "'";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Design = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["DesignNo"].ToString();
                List.Name = dt.Rows[i]["DesignName"].ToString();
               
                Design.Add(List);
            }

            return Json(Design, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetWorkerCode(string DesignID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";
            string SQL = "select * from Designation_Mst where DesignNo='" + DesignID + "'";
            SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            dt = MasterDate.GetDetails(SQL);

            if (dt.Rows.Count != 0)
            {
                message = dt.Rows[0]["WorkerCode"].ToString();
            }
            else
            {
                message = "";
            }


            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public JsonResult InsertLabourAllotment(MasterDate Labour)
        {
            string query = "";
            string message = "Success";
            DataTable Design_check = new DataTable();
           
            DataTable DT = new DataTable();
            string Designation = "";

            if (Labour.ID != "")
            {
                query = "select * from Designation_Mst where DesignNo='" + Labour.ID + "'";
                query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                DT = MasterDate.GetDetails(query);
                if (DT.Rows.Count != 0)
                {
                    Designation = DT.Rows[0]["DesignName"].ToString();
                }

                if (Labour.ID != "")
                {
                    query = "select * from LabourAllotment_Mst where DesignNo='" + Labour.ID + "'";
                    query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                }
                Design_check = MasterDate.GetDetails(query);

                if (Design_check.Rows.Count != 0)
                {
                    query = "Delete from LabourAllotment_Mst where DesignNo='" + Labour.ID + "'";
                    query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    MasterDate.GetDetails(query);

                    message = "Update";
                }

                //Inert into INWard_Sub
                query = "insert into LabourAllotment_Mst(DesignNo,DesignName,WorkerCode,GENERAL,";
                query = query + "SHIFT1,SHIFT2,SHIFT3,Total,CompCode,LocCode)values";
                query = query + "('" + Labour.ID + "','" + Designation + "','" + Labour.Name + "','" + Labour.Category + "',";
                query = query + "'" + Labour.Shift + "','" + Labour.SubCatName + "','" + Labour.MachineID + "','" + Labour.LastName + "',";
                query = query + "'" + SessionCcode + "','" + SessionLcode + "')";
                MasterDate.GetDetails(query);


            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetLabourAllotment()
        {

            DataTable dt = new DataTable();

            string SQL = "Select * from LabourAllotment_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
           
            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Design = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["DesignNo"].ToString();
                List.Name = dt.Rows[i]["DesignName"].ToString();
                List.Category = dt.Rows[i]["WorkerCode"].ToString();
                List.Shift = dt.Rows[i]["GENERAL"].ToString();
                List.SubCatName = dt.Rows[i]["SHIFT1"].ToString();
                List.MachineID = dt.Rows[i]["SHIFT2"].ToString();
                List.LastName = dt.Rows[i]["SHIFT3"].ToString();
                List.DeptName = dt.Rows[i]["Total"].ToString();

                Design.Add(List);
            }

            return Json(Design, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEditLabourAllotment(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();

            string SQL = "Select *from LabourAllotment_Mst where DesignNo='" + ID + "'";
            SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Design = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["DesignNo"].ToString();
                List.Name = dt.Rows[i]["DesignName"].ToString();
                List.Category = dt.Rows[i]["WorkerCode"].ToString();
                List.Shift = dt.Rows[i]["GENERAL"].ToString();
                List.SubCatName = dt.Rows[i]["SHIFT1"].ToString();
                List.MachineID = dt.Rows[i]["SHIFT2"].ToString();
                List.LastName = dt.Rows[i]["SHIFT3"].ToString();
                List.DeptName = dt.Rows[i]["Total"].ToString();

                Design.Add(List);
            }

            return Json(Design, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDeleteLabourAllotment(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";
          
            string SQL = "Select *from LabourAllotment_Mst where DesignNo='" + ID + "'";
            SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            if (dt.Rows.Count != 0)
            {
                SQL = "Delete from LabourAllotment_Mst where DesignNo='" + ID + "'";
                SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
                MasterDate.GetDetails(SQL);

                message = "Delete";
            }
            else
            {
                message = "No Data";
            }

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }



        private static string UTF8Encryption(string password)
        {
            string strmsg = string.Empty;
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            strmsg = Convert.ToBase64String(encode);
            return strmsg;
        }

        private static string UTF8Decryption(string encryptpwd)
        {
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }
        public JsonResult GetUserName()
        {
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();

            List<ManualAttend> PartyList = new List<ManualAttend>();


            string SQL = "Select * from Mvc_Rights..MstUsers where CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "' And IsAdmin='" + SessionIsAdmin + "' And UserCode!='Scoto' ";
            dt = ManualAttend.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend list = new ManualAttend();

                list.UserName = dt.Rows[i]["UserCode"].ToString();

                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ModulaName()
        {
            DataTable dt = new DataTable();
            string SQL = "Select * from Mvc_Rights..Module_List";
            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> Modulname = new List<ManualAttend>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend Mlist = new ManualAttend();

                Mlist.ModuleName = dt.Rows[i]["ModuleName"].ToString();

                Modulname.Add(Mlist);
            }
            return Json(Modulname, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MenuName()
        {
            DataTable dt = new DataTable();
            string SQL = "Select * from Mvc_Rights..Module_MenuHead_List";
            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> Menuname = new List<ManualAttend>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend MNlist = new ManualAttend();

                MNlist.MenuName = dt.Rows[i]["MenuName"].ToString();

                Menuname.Add(MNlist);
            }
            return Json(Menuname, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OnChanageModuleName(string ModuleName)
        {
            DataTable dt = new DataTable();
            string SQL = "Select * from Mvc_Rights..Module_MenuHead_List where ModuleName='" + ModuleName + "' ";
            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> OnMenuname = new List<ManualAttend>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend ONlist = new ManualAttend();

                ONlist.FormName = dt.Rows[i]["MenuName"].ToString();

                OnMenuname.Add(ONlist);
            }

            return Json(OnMenuname, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OnChanageMenuNameOne(string ModuleName, string MenuName, string UserName)
        {
            DataTable dt_user = new DataTable();
            DataTable dt = new DataTable();
            string SQL = "Select * from Mvc_Rights..Module_Menu_Form_List where ModuleName='" + ModuleName + "' And MenuName ='" + MenuName + "' ";

            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> OnMenuname = new List<ManualAttend>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend ONlist = new ManualAttend();

                string select = "0";

                SQL = "Select *from Mvc_Rights..Module_User_Rights where ModuleName='" + ModuleName + "' And MenuName='" + MenuName + "' And FormName='" + dt.Rows[i]["FormName"].ToString() + "' And UserName='" + UserName + "' ";
                SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                dt_user = ManualAttend.GetDetails(SQL);
                if (dt_user.Rows.Count != 0)
                {
                    select = dt_user.Rows[0]["ViewStatus"].ToString();
                }
                else
                {
                    select = "0";
                }

                ONlist.FormName = dt.Rows[i]["FormName"].ToString();
                ONlist.View = select;
                OnMenuname.Add(ONlist);
            }

            return Json(OnMenuname, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveUserRight(ManualAttend RoomDet)
        {
            string status = "Insert";

            string ModuleID = "";
            string MenuID = "";
            string FormID = "";
            string ModuleIDEncrypt = "";
            string MenuIDEncrypt = "";
            string FormIDEncrypt = "";
            string Form_LI_ID = "";
            string Menu_LI_ID = "";

            string query = "";
            DataTable Loc_check = new DataTable();

            List<ManualAttend> PalletDet = RoomDet.GetItemDet;

            for (int i = 0; i < PalletDet.Count; i++)
            {
                string select = PalletDet[i].View.ToString();
                //string ItemName = PalletDet[i].ItemName.ToString();
                //string View = PalletDet[i].PaleetName.ToString();
                //string PartyName = RoomDet.PartyName.ToString();
                //string PartyID = RoomDet.PartyID.ToString();
                //string Manucode = RoomDet.ItemCode.ToString();

                ModuleIDEncrypt = "";
                MenuIDEncrypt = "";
                FormIDEncrypt = "";

                if (select != "")
                {
                    query = "Select * from Mvc_Rights..Module_Menu_Form_List where ModuleName='" + RoomDet.ModuleName + "' ";
                    Loc_check = ManualAttend.GetDetails(query);
                    ModuleID = Loc_check.Rows[0]["ModuleID"].ToString();
                    ModuleIDEncrypt = UTF8Encryption(ModuleID).ToString();

                    query = "Select * from Mvc_Rights..Module_Menu_Form_List where ModuleName='" + RoomDet.ModuleName + "' And MenuName='" + RoomDet.MenuName + "' ";
                    Loc_check = ManualAttend.GetDetails(query);
                    MenuID = Loc_check.Rows[0]["MenuID"].ToString();
                    Menu_LI_ID = Loc_check.Rows[0]["Menu_LI_ID"].ToString();
                    MenuIDEncrypt = UTF8Encryption(MenuID).ToString();

                    query = "Select * from Mvc_Rights..Module_Menu_Form_List where ModuleName='" + RoomDet.ModuleName + "' And MenuName='" + RoomDet.MenuName + "' And FormName='" + PalletDet[i].FormName + "' ";
                    Loc_check = ManualAttend.GetDetails(query);
                    FormID = Loc_check.Rows[0]["FormID"].ToString();
                    Form_LI_ID = Loc_check.Rows[0]["Form_LI_ID"].ToString();
                    FormIDEncrypt = UTF8Encryption(FormID).ToString();

                    query = "Select * from Mvc_Rights..Module_User_Rights where ModuleName='" + RoomDet.ModuleName + "' And MenuName='" + RoomDet.MenuName + "' And FormName='" + PalletDet[i].FormName + "' And UserName='" + RoomDet.UserName + "' ";
                    query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    Loc_check = ManualAttend.GetDetails(query);

                    if (Loc_check.Rows.Count > 0)
                    {
                        query = "Delete from Mvc_Rights..Module_User_Rights where ModuleName='" + RoomDet.ModuleName + "' And MenuName='" + RoomDet.MenuName + "' And FormName='" + PalletDet[i].FormName + "' And UserName='" + RoomDet.UserName + "' ";
                        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        ManualAttend.GetDetails(query);

                        status = "Update";

                    }

                    query = "Insert into Mvc_Rights..Module_User_Rights(CompCode,LocCode,ModuleID,ModuleName,MenuID,MenuName,Menu_LI_ID,FormID,FormName,Form_LI_ID,UserName,ViewStatus)";
                    query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + ModuleIDEncrypt + "','" + RoomDet.ModuleName + "','" + MenuIDEncrypt + "','" + RoomDet.MenuName + "',";
                    query = query + "'" + Menu_LI_ID + "','" + FormIDEncrypt + "','" + PalletDet[i].FormName + "','" + Form_LI_ID + "','" + RoomDet.UserName + "','" + PalletDet[i].View + "')";
                    Loc_check = ManualAttend.GetDetails(query);




                }
                else if (select == "0")
                {

                }
                else

                {
                    status = "No Row";

                }

            }

            return Json(status);
        }

        public ActionResult SalaryDetails()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }


        public ActionResult PFESIMaster()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult IncentiveMaster()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public JsonResult GetEmployeeType()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();


            string SQL = "Select EmpTypeCd,EmpType,CASE when EmpCategory='1' then 'Staff' else 'Labour' END as EmpCategory from MstEmployeeType ";
            dt = MasterDate.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.ID = dt.Rows[i]["EmpTypeCd"].ToString();
                list.Name = dt.Rows[i]["EmpType"].ToString();
                list.Category = dt.Rows[i]["EmpCategory"].ToString();
                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetEmpTypeDetails(string EmpType)
        {
            DataTable dt = new DataTable();
            string SQL = "Select * from SalaryDetails_Mst where Emp_Type='" + EmpType + "' ";
            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> OnMenuname = new List<ManualAttend>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend ONlist = new ManualAttend();

                ONlist.BasicSalary = (Convert.ToDouble(dt.Rows[i]["Basic_Percentage"].ToString())).ToString();
                ONlist.Convey = (Convert.ToDouble(dt.Rows[i]["Convey_Percentage"].ToString())).ToString();
                ONlist.HRA = (Convert.ToDouble(dt.Rows[i]["HRA_Percentage"].ToString())).ToString();
                OnMenuname.Add(ONlist);
            }

            return Json(OnMenuname, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveSalaryDetails(ManualAttend RoomDet)
        {
            string status = "Insert";

            string query = "";
            DataTable Loc_check = new DataTable();

            query = "select * from SalaryDetails_Mst Where Emp_Type='" + RoomDet.UserType + "' ";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            Loc_check = ManualAttend.GetDetails(query);

            if (Loc_check.Rows.Count > 0)
            {
                query = "delete from SalaryDetails_Mst Where Emp_Type='" + RoomDet.UserType + "' ";
                query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                ManualAttend.GetDetails(query);

                status = "Update";

            }

            query = "Insert into SalaryDetails_Mst(CompCode,LocCode,Emp_Type,Basic_Percentage,Convey_Percentage,HRA_Percentage)";
            query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + RoomDet.UserType + "','" + RoomDet.BasicSalary + "'";
            query = query + ",'" + RoomDet.Convey + "','" + RoomDet.HRA + "')";
            Loc_check = ManualAttend.GetDetails(query);


            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmpTypeIncentive(string EmpType)
        {
            DataTable dt = new DataTable();
            string SQL = "Select * from Incentive_Mst where Employee_Type='" + EmpType + "' ";
            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> OnMenuname = new List<ManualAttend>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend ONlist = new ManualAttend();

                ONlist.Days = (Convert.ToDouble(dt.Rows[i]["Days"].ToString())).ToString();
                ONlist.Amount = (Convert.ToDouble(dt.Rows[i]["Amount"].ToString())).ToString();

                OnMenuname.Add(ONlist);
            }

            return Json(OnMenuname, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveIncentiveDetails(ManualAttend RoomDet)
        {
            string status = "Insert";

            string query = "";
            DataTable Loc_check = new DataTable();

            query = "select * from Incentive_Mst Where Employee_Type='" + RoomDet.UserType + "' ";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            Loc_check = ManualAttend.GetDetails(query);

            if (Loc_check.Rows.Count > 0)
            {
                query = "delete from Incentive_Mst Where Employee_Type='" + RoomDet.UserType + "' ";
                query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                ManualAttend.GetDetails(query);

                status = "Update";

            }

            query = "Insert into Incentive_Mst(CompCode,LocCode,Employee_Type,Days,Amount)";
            query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + RoomDet.UserType + "','" + RoomDet.Days + "'";
            query = query + ",'" + RoomDet.Amount + "')";
            Loc_check = ManualAttend.GetDetails(query);


            return Json(status, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SavePFESIDetails(ManualAttend RoomDet)
        {
            string status = "Insert";

            string query = "";
            DataTable Loc_check = new DataTable();

            query = "select * from PFESI_Mst";
            query = query + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            Loc_check = ManualAttend.GetDetails(query);

            if (Loc_check.Rows.Count > 0)
            {
                query = "delete from PFESI_Mst";
                query = query + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                ManualAttend.GetDetails(query);

                status = "Update";

            }

            query = "Insert into PFESI_Mst(CompCode,LocCode,PF_Per,ESI_Per,PF_Salary,Emp_ESI,EmpPF_AC1,EmpESI_AC2)";
            query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + RoomDet.UserType + "','" + RoomDet.BasicSalary + "'";
            query = query + ",'" + RoomDet.Convey + "','" + RoomDet.CodeVal + "','" + RoomDet.TypeVal + "'";
            query = query + ",'" + RoomDet.LeaveType + "')";
            Loc_check = ManualAttend.GetDetails(query);


            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveESICodeDetails(ManualAttend RoomDet)
        {
            string status = "Insert";

            string query = "";
            DataTable Loc_check = new DataTable();

            //query = "select * from ESICode_Mst Where ESICode='" + RoomDet.UserType + "' ";
            query = query + " select * from ESICode_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            Loc_check = ManualAttend.GetDetails(query);

            if (Loc_check.Rows.Count > 0)
            {
                //query = "delete from ESICode_Mst Where ESICode='" + RoomDet.UserType + "' ";
                query = query + " delete from ESICode_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                ManualAttend.GetDetails(query);

                status = "Update";

            }

            query = "Insert into ESICode_Mst(CompCode,LocCode,ESICode)";
            query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + RoomDet.HRA + "')";

            Loc_check = ManualAttend.GetDetails(query);


            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetESICode()
        {
            DataTable dt = new DataTable();
            string SQL = "select * from ESICode_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> OnMenuname = new List<ManualAttend>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend ONlist = new ManualAttend();

                ONlist.Days = dt.Rows[i]["ESICode"].ToString();

                OnMenuname.Add(ONlist);
            }

            return Json(OnMenuname, JsonRequestBehavior.AllowGet);
        }

        //DeleteESICode

        public JsonResult DeleteESICode(string ESICode)
        {
            string Status = "Insert";
            DataTable dt = new DataTable();
            string SQL = "select * from ESICode_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ESICode='" + ESICode + "'";
            dt = ManualAttend.GetDetails(SQL);

            if (dt.Rows.Count > 0)
            {
                SQL = "Delete from ESICode_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ESICode='" + ESICode + "'";
                ManualAttend.GetDetails(SQL);
                Status = "Deleted";
            }


            return Json(Status, JsonRequestBehavior.AllowGet);
        }
    }
}