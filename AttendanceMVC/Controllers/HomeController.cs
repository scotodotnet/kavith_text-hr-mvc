﻿using AttendanceMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AttendanceMVC.Controllers
{
    public class HomeController : Controller
    {
        static string SessionCcode = "";
        static string SessionLcode = "";
        static string SessionUserName;
        static string SessionUserID;
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        public ActionResult Dashboard()
        {


            ViewBag.Title = "Home Page";
            if (Session["SessionCcode"] != null)
            {
            SessionCcode = Session["SessionCcode"].ToString();
            SessionLcode = Session["SessionLcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();

            return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Payroll()
        {


            ViewBag.Title = "Home Page";
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Employee()
        {
            //string path = Server.MapPath("~/assets/img/user-1.png");
            //byte[] imageByteData = System.IO.File.ReadAllBytes(path);
            //string imageBase64Data = Convert.ToBase64String(imageByteData);
            //string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
            //ViewBag.ImageData = imageDataURL;
            return View();
        }

        public ActionResult DownLoadClear()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public JsonResult GetCompany()
        {
            DataTable dt = new DataTable();
            string SQL = "Select Distinct CompCode,CompCode + ' - ' + CompName as Cname from Company_Mst";
            dt = MasterDate.GetDetails(SQL);

            List<CompanyMaster> compList = new List<CompanyMaster>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CompanyMaster comp = new CompanyMaster();
                comp.CompCode = dt.Rows[i]["CompCode"].ToString();
                comp.CompName = dt.Rows[i]["Cname"].ToString();

                compList.Add(comp);
            }

            return Json(compList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLocation(string Ccode)
        {
            DataTable dt = new DataTable();
            string SQL = "Select Distinct LocCode,LocCode + ' - ' + LocName as Lname from Location_Mst where CompCode='" + Ccode + "'";
            dt = MasterDate.GetDetails(SQL);

            List<CompanyMaster> locList = new List<CompanyMaster>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CompanyMaster loc = new CompanyMaster();
                loc.LocCode = dt.Rows[i]["LocCode"].ToString();
                loc.LocName = dt.Rows[i]["Lname"].ToString();

                locList.Add(loc);
            }

            return Json(locList, JsonRequestBehavior.AllowGet);
        }



        public ActionResult GetUserNameList()
        {
            List<ManualAttend> UserList = new List<ManualAttend>();

            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();

            string UserName = Session["UserId"].ToString();

            if (UserName != "Scoto")
            {
                string SQL = "select UserCode from [Mvc_Rights]..MstUsers where CompCode ='" + Session["SessionCcode"].ToString() + "' And LocationCode='" + Session["SessionLcode"].ToString() + "' And UserCode='" + UserName + "'";
                dt = ManualAttend.GetDetails(SQL);


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ManualAttend ONlist = new ManualAttend();
                    ONlist.UserName = dt.Rows[i]["UserCode"].ToString();
                    UserList.Add(ONlist);


                }
            }
            else
            {
                ManualAttend ONlist = new ManualAttend();
                ONlist.UserName = "Scoto";
                UserList.Add(ONlist);
            }


            return Json(UserList, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetHideAndShow()
        {
            string SeesionUserID = Session["UserId"].ToString();

            List<ManualAttend> OnMenuname = new List<ManualAttend>();

            DataTable dt = new DataTable();

            string SQL = "select Menu_LI_ID,Form_LI_ID from [Mvc_Rights]..Module_User_Rights where UserName='" + SeesionUserID + "' And ViewStatus='1'";
            SQL = SQL + " And CompCode='" + Session["SessionCcode"].ToString() + "' And LocCode='" + Session["SessionLcode"].ToString() + "'";
            dt = ManualAttend.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend ONlist = new ManualAttend();

                ONlist.MenuName = dt.Rows[i]["Menu_LI_ID"].ToString();
                ONlist.FormName = dt.Rows[i]["Form_LI_ID"].ToString();

                OnMenuname.Add(ONlist);
            }

            return Json(OnMenuname, JsonRequestBehavior.AllowGet);
        }




        public ActionResult LoginUser(string Username, string PWD, string Ccode, string Lcode)
        {
            bool ErrFlag = false;
            string query = "";
            DataTable Dept_check = new DataTable();
            UserRegistrationClass objuser = new UserRegistrationClass();
            bool status = false;
            List<MasterDate> UserList = new List<MasterDate>();

            if (!ErrFlag)
            {
                string Findate_1 = "";
                string dtserver_Fin = "";
                string FinYearCode = "";
                string FinYear = "";
                DataTable DT_Server = new DataTable();
                DataTable Chk_Fin = new DataTable();

                string Query1 = "";


                Query1 = "Select convert(varchar,getdate(),105) as ServerDate";
                DT_Server = MasterDate.GetDetails(Query1);

                dtserver_Fin = DT_Server.Rows[0]["ServerDate"].ToString();


                //Query1 = "Select *from [Mvc_Rights]..Value_Mst";
                //Chk_Fin = MasterDate.GetDetails(Query1);

                //if (Chk_Fin.Rows.Count != 0)
                //{
                //    Findate_1 = Chk_Fin.Rows[0]["No_dVal"].ToString()+"/"+ Chk_Fin.Rows[0]["No_MVal"].ToString() + "/" + Chk_Fin.Rows[0]["No_YVal"].ToString();
                //    //Findate_2 = Chk_Fin.Rows[0]["EndingPeriod"].ToString();
                //    //FinYearCode = Chk_Fin.Rows[0]["YearCode"].ToString();
                //    //FinYear = Chk_Fin.Rows[0]["FinacialYear"].ToString();
                //}
                //DateTime MyDate1;
                //DateTime MyDate2;
                //DateTime dfrom = Convert.ToDateTime(Findate_1);
                //MyDate1 = DateTime.ParseExact(Findate_1, "dd-MM-yyyy", null);
                
                //DateTime dt_New = Convert.ToDateTime(dtserver_Fin);
                //if ((dfrom <= dt_New))
                //{




                    DataTable dt_v = new DataTable();
                    //Session["Rights"] = "ERP_Rights";
                    if (Username.Trim() == "Scoto")
                    {
                        Session["UserId"] = Username.Trim();
                        objuser.UserCode = Username.Trim();
                        objuser.Password = UTF8Encryption(PWD.Trim());
                        string pwd = UTF8Encryption(PWD.Trim());
                        DataTable dt = new DataTable();

                        query = "Select UserCode,UserName,Password,IsAdmin,LocationCode as Lcode,CompCode as Ccode,FormID,FormName from [Mvc_Rights]..MstUsers where UserCode='" + objuser.UserCode + "' and Password='" + objuser.Password + "'";
                        dt = MasterDate.GetDetails(query);
                        if (dt.Rows.Count > 0)
                        {
                            if ((dt.Rows[0]["UserCode"].ToString().Trim() == Username.Trim()) && (dt.Rows[0]["Password"].ToString().Trim() == pwd))
                            {
                                Session["Isadmin"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                                Session["Usernmdisplay"] = Username.Trim();
                                Session["SessionCcode"] = Ccode.Trim();
                                Session["SessionLcode"] = Lcode.Trim();

                                if (Session["Isadmin"].ToString() == "1")
                                {
                                    Session["RoleCode"] = "1";
                                }
                                else
                                {
                                    Session["RoleCode"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                                }



                            DataTable Com_DT = new DataTable();
                            query = "Select *from Company_Mst where CompCode='" + Ccode + "'";
                            Com_DT = MasterDate.GetDetails(query);
                            if(Com_DT.Rows.Count!=0)
                            {
                                Session["CmpName"] = Com_DT.Rows[0]["CompName"].ToString();

                            }

                            status = true;

                                //User default page    
                                MasterDate ONlist = new MasterDate();
                                ONlist.Name = status.ToString();
                                ONlist.ID = '/' + "Home" + '/' + "Dashboard";
                                UserList.Add(ONlist);




                            }
                            else
                            {
                                status = false;
                                //User default page    
                                MasterDate ONlist = new MasterDate();
                                ONlist.Name = status.ToString();
                                ONlist.ID = '/' + "Home" + '/' + "Index";
                                UserList.Add(ONlist);
                            }
                        }
                        else
                        {
                            status = false;
                            //User default page    
                            MasterDate ONlist = new MasterDate();
                            ONlist.Name = status.ToString();
                            ONlist.ID = '/' + "Home" + '/' + "Index";
                            UserList.Add(ONlist);
                        }

                    }
                    else
                    {
                        string date_1 = "";
                        string dtserver = "";

                        date_1 = (16 + "-" + 08 + "-" + 2021).ToString();

                        string Query = "";
                        Query = "Select convert(varchar,getdate(),105) as ServerDate";
                        DT_Server = MasterDate.GetDetails(Query);

                        dtserver = DT_Server.Rows[0]["ServerDate"].ToString();



                        if (Convert.ToDateTime(dtserver) < Convert.ToDateTime(date_1))
                        {
                            if (!ErrFlag)
                            {
                                Session["UserId"] = Username.Trim();
                                objuser.UserCode = Username.Trim();
                                objuser.Password = UTF8Encryption(PWD.Trim());
                                string pwd = UTF8Encryption(PWD.Trim());
                            
                                //string pwd1 = UTF8Decryption(PWD);
                                objuser.Ccode = Ccode;
                                objuser.Lcode = Lcode;
                                DataTable dt = new DataTable();

                                query = "Select UserCode,UserName,Password,IsAdmin,LocationCode as Lcode,CompCode as Ccode,FormID,FormName,Link_Url from [Mvc_Rights]..MstUsers where UserCode='" + objuser.UserCode + "' and Password='" + objuser.Password + "'";
                                query = query + " And CompCode='" + objuser.Ccode + "' And LocationCode='" + objuser.Lcode + "'";
                                dt = MasterDate.GetDetails(query);

                                if (dt.Rows.Count > 0)
                                {

                                    if ((dt.Rows[0]["UserCode"].ToString().Trim() == Username.Trim()) && (dt.Rows[0]["Password"].ToString().Trim() == pwd) && (dt.Rows[0]["Ccode"].ToString().Trim() == Ccode) && (dt.Rows[0]["Lcode"].ToString().Trim() == Lcode))
                                    {
                                        Session["Isadmin"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                                        Session["Usernmdisplay"] = Username.Trim();
                                        Session["SessionCcode"] = dt.Rows[0]["Ccode"].ToString().Trim();
                                        Session["SessionLcode"] = dt.Rows[0]["Lcode"].ToString().Trim();
                                        //Session["FinYearCode"] = FinYearCode;
                                        //Session["FinYear"] = FinYear;

                                        if (Session["Isadmin"].ToString() == "1")
                                        {
                                            Session["RoleCode"] = "1";
                                        }
                                        else
                                        {
                                            Session["RoleCode"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                                        }



                                    DataTable Com_DT = new DataTable();
                                    query = "Select *from Company_Mst where CompCode='" + dt.Rows[0]["Ccode"].ToString().Trim() + "'";
                                    Com_DT = MasterDate.GetDetails(query);
                                    if (Com_DT.Rows.Count != 0)
                                    {
                                        Session["CmpName"] = Com_DT.Rows[0]["CompName"].ToString();

                                    }

                                    status = true;




                                        //USer Default page
                                        DataTable dt1 = new DataTable();

                                        string UserName = Session["UserId"].ToString();

                                        string SQL = "select UserCode from [Mvc_Rights]..MstUsers where UserCode='" + UserName + "'";
                                        dt = MasterDate.GetDetails(SQL);

                                        SQL = "";
                                        SQL = "Select * from [Mvc_Rights]..Company_user_default_page where CompCode='" + Ccode + "' And LocCode='" + Lcode + "' And UserName='" + UserName + "' ";
                                        dt1 = MasterDate.GetDetails(SQL);
                                        //if (dt1.Rows.Count != 0)
                                        //{
                                        for (int i = 0; i < dt.Rows.Count; i++)
                                        {
                                            MasterDate ONlist = new MasterDate();

                                            ONlist.AgentName = dt.Rows[i]["UserCode"].ToString();
                                            ONlist.Name = status.ToString();
                                            if (dt1.Rows.Count != 0)
                                            {
                                                for (int j = 0; j < dt1.Rows.Count; j++)
                                                {

                                                    string Name = dt1.Rows[j]["Controller_Link"].ToString();
                                                    string Pass = dt1.Rows[j]["View_Link"].ToString();

                                                    ONlist.ID = '/' + Name + '/' + Pass;

                                                    UserList.Add(ONlist);
                                                }
                                            }
                                            else
                                            {
                                                string Name = "Home";
                                                string Pass = "Dashboard";

                                                ONlist.ID = '/' + Name + '/' + Pass;

                                                UserList.Add(ONlist);

                                            }

                                            UserList.Add(ONlist);

                                        }
                                        //}


                                    }
                                    else
                                    {
                                        status = false;
                                        //User default page    
                                        MasterDate ONlist = new MasterDate();
                                        ONlist.Name = status.ToString();
                                        ONlist.ID = '/' + "Home" + '/' + "Index";
                                        UserList.Add(ONlist);
                                    }
                                }
                                else
                                {
                                    status = false;
                                    //User default page    
                                    MasterDate ONlist = new MasterDate();
                                    ONlist.Name = status.ToString();
                                    ONlist.ID = '/' + "Home" + '/' + "Index";
                                    UserList.Add(ONlist);
                                }
                            }
                        }
                        else
                        {
                            status = false;
                            //User default page    
                            MasterDate ONlist = new MasterDate();
                            ONlist.Name = status.ToString();
                            ONlist.ID = '/' + "Home" + '/' + "Index";
                            UserList.Add(ONlist);
                        }
                    }
                //}
                //else
                //{
                //    status = false;
                //    //User default page    
                //    CommonParamerts ONlist = new CommonParamerts();
                //    ONlist.BillParty = status.ToString();
                //    ONlist.TransID = '/' + "Home" + '/' + "Index";
                //    UserList.Add(ONlist);
                //}
            }


            //return RedirectToAction("Employee","Employee");
            // return new JsonResult { Data = new { status = status } };
            return Json(UserList, JsonRequestBehavior.AllowGet);
        }

        private static string UTF8Encryption(string password)
        {
            string strmsg = string.Empty;
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            strmsg = Convert.ToBase64String(encode);
            return strmsg;
        }

        private static string UTF8Decryption(string encryptpwd)
        {
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }
    }
}
