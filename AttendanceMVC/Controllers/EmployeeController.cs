﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttendanceMVC.Models;
using AttendanceMVC.Controllers;
using System.Data;
using System.Text;

namespace AttendanceMVC.Controllers
{
    public class EmployeeController : Controller
    {
        static string SessionCcode = "";
        static string SessionLcode = "";
        static string SessionUserName;
        static string SessionUserID;
        // GET: Employee
        public ActionResult Index()
        {
            return View();
        }
        //public ActionResult EmployeeProfile()
        //{
        //    if (Session["SessionCcode"] != null)
        //    {
        //        SessionCcode = Session["SessionCcode"].ToString();
        //        SessionLcode = Session["SessionLcode"].ToString();
        //        SessionUserName = Session["Usernmdisplay"].ToString();
        //        SessionUserID = Session["UserId"].ToString();

        //        return View();
        //    }
        //    else
        //    {
        //        return RedirectToAction("Index", "Home");
        //    }
        //}

        public ActionResult EmployeeProfile()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                if (Session["EmpID"] != null && Session["EmpID"].ToString() != "")
                {
                    string macineid = Session["EmpID"].ToString();

                    string path = "E:/Scoto/Photo/" + macineid + ".jpg";
                    string dpath = "D:/Scoto/Photo/" + macineid + ".jpg";
                    string fpath = "F:/Scoto/Photo/" + macineid + ".jpg";
                    if (System.IO.File.Exists(path))
                    {
                        byte[] imageByteData = System.IO.File.ReadAllBytes(path);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.ImageData = imageDataURL;
                    }

                    else if (System.IO.File.Exists(dpath))
                    {
                        byte[] imageByteData = System.IO.File.ReadAllBytes(dpath);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.ImageData = imageDataURL;
                    }

                    else if (System.IO.File.Exists(fpath))
                    {
                        byte[] imageByteData = System.IO.File.ReadAllBytes(fpath);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.ImageData = imageDataURL;
                    }



                    else

                    {
                        string filepath = HttpContext.Server.MapPath("~/assets/img/no-img.jpg");
                        byte[] imageByteData = System.IO.File.ReadAllBytes(filepath);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.ImageData = imageDataURL;

                    }

                }
                else
                {

                    string path = "E:/Scoto/Photo/no -img.jpg";
                    string dpath = "D:/Scoto/Photo/no-img.jpg";
                    string fpath = "F:/Scoto/Photo/no-img.jpg";
                    // var absolutePath = HttpContext.Server.MapPath(path);

                    if (System.IO.File.Exists(path))
                    {

                        byte[] imageByteData = System.IO.File.ReadAllBytes(path);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.ImageData = imageDataURL;
                    }

                    else if (System.IO.File.Exists(dpath))
                    {

                        byte[] imageByteData = System.IO.File.ReadAllBytes(dpath);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.ImageData = imageDataURL;
                    }

                    else if (System.IO.File.Exists(fpath))
                    {

                        byte[] imageByteData = System.IO.File.ReadAllBytes(fpath);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.ImageData = imageDataURL;
                    }

                    else

                    {
                        string filepath = HttpContext.Server.MapPath("~/assets/img/no-img.jpg");
                        byte[] imageByteData = System.IO.File.ReadAllBytes(filepath);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.ImageData = imageDataURL;

                    }

                }
                if (Session["EmpID"] != null && Session["EmpID"].ToString() != "")
                {
                    string macineid = Session["EmpID"].ToString();

                    string path = "E:/Scoto/Document/" + macineid + ".jpg";
                    string dpath = "D:/Scoto/Document/" + macineid + ".jpg";
                    string fpath = "F:/Scoto/Document/" + macineid + ".jpg";
                    if (System.IO.File.Exists(path))
                    {
                        byte[] imageByteData = System.IO.File.ReadAllBytes(path);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.Image = imageDataURL;
                    }
                    else if (System.IO.File.Exists(dpath))
                    {
                        byte[] imageByteData = System.IO.File.ReadAllBytes(dpath);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.Image = imageDataURL;
                    }
                    else if (System.IO.File.Exists(fpath))
                    {
                        byte[] imageByteData = System.IO.File.ReadAllBytes(fpath);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.Image = imageDataURL;
                    }

                    else

                    {
                        string filepath = HttpContext.Server.MapPath("~/assets/img/no-img.jpg");
                        byte[] imageByteData = System.IO.File.ReadAllBytes(filepath);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.ImageData = imageDataURL;

                    }

                }
                else
                {

                    string path = "E:/Scoto/Document/card.jpg";
                    string dpath = "D:/Scoto/Document/card.jpg";
                    string fpath = "F:/Scoto/Document/card.jpg";
                    if (System.IO.File.Exists(path))
                    {
                        byte[] imageByteData = System.IO.File.ReadAllBytes(path);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.Image = imageDataURL;
                    }
                    else if (System.IO.File.Exists(dpath))
                    {
                        byte[] imageByteData = System.IO.File.ReadAllBytes(dpath);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.Image = imageDataURL;
                    }
                    else if (System.IO.File.Exists(fpath))
                    {
                        byte[] imageByteData = System.IO.File.ReadAllBytes(fpath);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.Image = imageDataURL;
                    }

                    else

                    {
                        string filepath = HttpContext.Server.MapPath("~/assets/img/no-img.jpg");
                        byte[] imageByteData = System.IO.File.ReadAllBytes(filepath);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                        ViewBag.ImageData = imageDataURL;

                    }


                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult EmployeeProfileMain()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Experience()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult EmpApproval()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult EmpStatus()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //Get Department Using Angular
        //HR_ModuleEntities db_Entity = new HR_ModuleEntities();
        public JsonResult GetDepartment()
        {
            //var Dept = db_Entity.Department_Mst.ToList();


            DataTable dt = new DataTable();

            string SQL = "Select * from Department_Mst  ";
            SQL = SQL + "where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["DeptCode"].ToString();
                List.Name = dt.Rows[i]["DeptName"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetGradeName()
        {
            //var Dept = db_Entity.Department_Mst.ToList();


            DataTable dt = new DataTable();

            string SQL = "Select * from MstGrade   ";
            //SQL = SQL + "where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["GradeID"].ToString();
                List.Name = dt.Rows[i]["GradeName"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCateName()
        {
            //var Dept = db_Entity.Department_Mst.ToList();


            DataTable dt = new DataTable();

            string SQL = "Select * from MstCategory  ";
            //SQL = SQL + "where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["CateID"].ToString();
                List.Name = dt.Rows[i]["CateName"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAgentName()
        {
            //var Dept = db_Entity.Department_Mst.ToList();


            DataTable dt = new DataTable();

            string SQL = "Select * from AgentMst   ";
            //SQL = SQL + "where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["AgentID"].ToString();
                List.Name = dt.Rows[i]["AgentName"].ToString();
                List.Deduction1 = dt.Rows[i]["Shift8"].ToString();
                List.Deduction2 = dt.Rows[i]["Shift12"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCanteenName()
        {
            //var Dept = db_Entity.Department_Mst.ToList();


            DataTable dt = new DataTable();

            string SQL = "Select * from MstCanteen   ";
            //SQL = SQL + "where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["CanID"].ToString();
                List.Name = dt.Rows[i]["CanteenName"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCanteenNameOpt()
        {
            //var Dept = db_Entity.Department_Mst.ToList();


            DataTable dt = new DataTable();

            string SQL = "Select CanOptID,CanteenNameOpt from MstCanteenOpt  ";
            //SQL = SQL + "where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["CanOptID"].ToString();
                List.Name = dt.Rows[i]["CanteenNameOpt"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult GetCateName()
        //{
        //    //var Dept = db_Entity.Department_Mst.ToList();


        //    DataTable dt = new DataTable();

        //    string SQL = "Select * from MstCategory where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'  ";
        //    //SQL = SQL + "where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";

        //    dt = MasterDate.GetDetails(SQL);

        //    List<MasterDate> Dept = new List<MasterDate>();

        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        MasterDate List = new MasterDate();
        //        List.ID = dt.Rows[i]["CateID"].ToString();
        //        List.Name = dt.Rows[i]["CateName"].ToString();

        //        Dept.Add(List);
        //    }

        //    return Json(Dept, JsonRequestBehavior.AllowGet);
        //}

        //public JsonResult GetCanteenName()
        //{
        //    //var Dept = db_Entity.Department_Mst.ToList();


        //    DataTable dt = new DataTable();

        //    string SQL = "Select * from MstCanteen where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'  ";
        //    //SQL = SQL + "where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";

        //    dt = MasterDate.GetDetails(SQL);

        //    List<MasterDate> Dept = new List<MasterDate>();

        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        MasterDate List = new MasterDate();
        //        List.ID = dt.Rows[i]["CanID"].ToString();
        //        List.Name = dt.Rows[i]["CanteenName"].ToString();

        //        Dept.Add(List);
        //    }

        //    return Json(Dept, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult GetGrade()
        {
            //var Dept = db_Entity.Department_Mst.ToList();


            DataTable dt = new DataTable();

            string SQL = "Select GradeID,GradeName from MstGrade   ";
            //SQL = SQL + "where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["GradeID"].ToString();
                List.Name = dt.Rows[i]["GradeName"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDesignation()
        {
            //var Dept = db_Entity.Department_Mst.ToList();


            DataTable dt = new DataTable();

            string SQL = "Select DE.DesignNo,DE.DesignName,DM.DeptName as DeptCode,DE.WorkerCode from Designation_Mst DE inner join Department_Mst DM";
            SQL = SQL + " on DE.DeptCode=DM.DeptCode";
            SQL = SQL + " where DE.CompCode='" + SessionCcode + "' And DE.LocCode='" + SessionLcode + "'";
            SQL = SQL + " And DM.CompCode='" + SessionCcode + "' And DM.LocCode='" + SessionLcode + "'";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Design = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["DesignNo"].ToString();
                List.Name = dt.Rows[i]["DesignName"].ToString();
                List.DeptName = dt.Rows[i]["DeptCode"].ToString();
                List.AgentName = dt.Rows[i]["WorkerCode"].ToString();

                Design.Add(List);
            }

            return Json(Design, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InsertDepartment(MasterDate Dept)
        {
            string query = "";
            string message = "Success";
            DataTable Dept_check = new DataTable();

            if (Dept.Name != "")
            {
                if (Dept.ID == "" || Dept.ID == null)
                {
                    query = "select * from Department_Mst where DeptName='" + Dept.Name + "'";
                    query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                }
                else
                {
                    query = "select * from Department_Mst where DeptCode='" + Dept.ID + "'";
                    query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                }
                Dept_check = MasterDate.GetDetails(query);

                if (Dept.ID == "" || Dept.ID == null)
                {
                    if (Dept_check.Rows.Count != 0)
                    {
                        //query = "Delete from Department_Mst where DeptName='" + Dept.Name + "'";
                        //query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        //MasterDate.GetDetails(query);

                        message = "Already";
                    }
                    else
                    {
                        //Inert into INWard_Sub
                        query = "insert into Department_Mst(DeptName,CompCode,LocCode)values";
                        query = query + "('" + Dept.Name + "','" + SessionCcode + "','" + SessionLcode + "')";
                        MasterDate.GetDetails(query);
                    }

                }
                else
                {
                    if (Dept_check.Rows.Count != 0)
                    {
                        query = "Update Department_Mst set DeptName='" + Dept.Name + "' where DeptCode='" + Dept.ID + "'";
                        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }
                }

            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        public JsonResult InsertGrade(MasterDate Dept)
        {
            string query = "";
            string message = "Success";
            DataTable Dept_check = new DataTable();

            if (Dept.Name != "")
            {
                if (Dept.ID == "" || Dept.ID == null)
                {
                    query = "select * from MstGrade where GradeName='" + Dept.Name + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";

                }
                else
                {
                    query = "select * from MstGrade where GradeID='" + Dept.ID + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                    //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                }
                Dept_check = MasterDate.GetDetails(query);

                if (Dept.ID == "" || Dept.ID == null)
                {
                    if (Dept_check.Rows.Count != 0)
                    {
                        query = "Delete from MstGrade where GradeName='" + Dept.Name + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }

                    //Inert into INWard_Sub
                    query = "insert into MstGrade(GradeName,CompCode,LocCode)values";
                    query = query + "('" + Dept.Name + "','" + SessionCcode + "','" + SessionLcode + "')";
                    MasterDate.GetDetails(query);
                }
                else
                {
                    if (Dept_check.Rows.Count != 0)
                    {
                        query = "Update MstGrade set GradeName='" + Dept.Name + "' where GradeID='" + Dept.ID + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }
                }

            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult InsertCate(MasterDate Dept)
        {
            string query = "";
            string message = "Success";
            DataTable Dept_check = new DataTable();

            if (Dept.Name != "")
            {
                if (Dept.ID == "" || Dept.ID == null)
                {
                    query = "select * from MstCategory where CateName='" + Dept.Name + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";

                }
                else
                {
                    query = "select * from MstCategory where CateID='" + Dept.ID + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
                    //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                }
                Dept_check = MasterDate.GetDetails(query);

                if (Dept.ID == "" || Dept.ID == null)
                {
                    if (Dept_check.Rows.Count != 0)
                    {
                        query = "Delete from MstCategory where CateName='" + Dept.Name + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }

                    //Inert into INWard_Sub
                    query = "insert into MstCategory(CateName,CompCode,LocCode)values";
                    query = query + "('" + Dept.Name + "','" + SessionCcode + "','" + SessionLcode + "')";
                    MasterDate.GetDetails(query);
                }
                else
                {
                    if (Dept_check.Rows.Count != 0)
                    {
                        query = "Update MstCategory set CateName='" + Dept.Name + "' where CateID='" + Dept.ID + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }
                }

            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult InsertCanteen(MasterDate Dept)
        {
            string query = "";
            string message = "Success";
            DataTable Dept_check = new DataTable();

            if (Dept.Name != "")
            {
                if (Dept.ID == "" || Dept.ID == null)
                {
                    query = "select * from MstCanteen where CanteenName='" + Dept.Name + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";

                }
                else
                {
                    query = "select * from MstCanteen where CanID='" + Dept.ID + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
                    //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                }
                Dept_check = MasterDate.GetDetails(query);

                if (Dept.ID == "" || Dept.ID == null)
                {
                    if (Dept_check.Rows.Count != 0)
                    {
                        query = "Delete from MstCanteen where CanteenName='" + Dept.Name + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }

                    //Inert into INWard_Sub
                    query = "insert into MstCanteen(CanteenName,CompCode,LocCode)values";
                    query = query + "('" + Dept.Name + "','" + SessionCcode + "','" + SessionLcode + "')";
                    MasterDate.GetDetails(query);
                }
                else
                {
                    if (Dept_check.Rows.Count != 0)
                    {
                        query = "Update MstCanteen set CanteenName='" + Dept.Name + "' where CanID='" + Dept.ID + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }
                }

            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult InsertCanteenOpt(MasterDate Dept)
        {
            string query = "";
            string message = "Success";
            DataTable Dept_check = new DataTable();

            if (Dept.Name != "")
            {
                if (Dept.ID == "" || Dept.ID == null)
                {
                    query = "select * from MstCanteenOpt where CanteenNameOpt='" + Dept.Name + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";

                }
                else
                {
                    query = "select * from MstCanteenOpt where CanOptID='" + Dept.ID + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
                    //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                }
                Dept_check = MasterDate.GetDetails(query);

                if (Dept.ID == "" || Dept.ID == null)
                {
                    if (Dept_check.Rows.Count != 0)
                    {
                        query = "Delete from MstCanteenOpt where CanteenNameOpt='" + Dept.Name + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }

                    //Inert into INWard_Sub
                    query = "insert into MstCanteenOpt(CanteenNameOpt,CompCode,LocCode)values";
                    query = query + "('" + Dept.Name + "','" + SessionCcode + "','" + SessionLcode + "')";
                    MasterDate.GetDetails(query);
                }
                else
                {
                    if (Dept_check.Rows.Count != 0)
                    {
                        query = "Update MstCanteenOpt set CanteenNameOpt='" + Dept.Name + "' where CanOptID='" + Dept.ID + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }
                }

            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult InsertAGent(MasterDate Dept)
        {
            string query = "";
            string message = "Success";
            DataTable Dept_check = new DataTable();

            if (Dept.Name != "")
            {
                if (Dept.ID == "" || Dept.ID == null)
                {
                    query = "select * from AgentMst where AgentName='" + Dept.Name + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";

                }
                else
                {
                    query = "select * from AgentMst where AgentID='" + Dept.ID + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                    //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                }
                Dept_check = MasterDate.GetDetails(query);

                if (Dept.ID == "" || Dept.ID == null)
                {
                    if (Dept_check.Rows.Count != 0)
                    {
                        query = "Delete from AgentMst where AgentName='" + Dept.Name + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }

                    //Inert into INWard_Sub
                    query = "insert into AgentMst(AgentName,Shift8,Shift12,CompCode,LocCode)values";
                    query = query + "('" + Dept.Name + "','" + Dept.Deduction1 + "','" + Dept.Deduction2 + "','" + SessionCcode + "','" + SessionLcode + "')";
                    MasterDate.GetDetails(query);
                }
                else
                {
                    if (Dept_check.Rows.Count != 0)
                    {
                        query = "Update AgentMst set AgentName='" + Dept.Name + "' where AgentID='" + Dept.ID + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }
                }

            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult InsertDesignation(MasterDate Design)
        {
            string query = "";
            string message = "Success";
            DataTable Design_check = new DataTable();

            if (Design.Name != "")
            {
                if (Design.ID == "" || Design.ID == null)
                {
                    query = "select * from Designation_Mst where DesignName='" + Design.Name + "' And DeptCode='" + Design.DeptName + "'";
                    query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                }
                else
                {
                    query = "select * from Designation_Mst where DesignNo='" + Design.ID + "'";
                    query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                }

                //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                Design_check = MasterDate.GetDetails(query);
                if (Design.ID == "" || Design.ID == null)
                {
                    if (Design_check.Rows.Count != 0)
                    {
                        //query = "Delete from Designation_Mst where DesignName='" + Design.Name + "' And DeptCode='" + Design.DeptName + "'";
                        //query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        //MasterDate.GetDetails(query);

                        message = "Already";
                    }
                    else
                    {
                        //Inert into INWard_Sub
                        query = "insert into Designation_Mst(DesignName,DeptCode,WorkerCode,CompCode,LocCode)values";
                        query = query + "('" + Design.Name + "','" + Design.DeptName + "','" + Design.AgentName + "','" + SessionCcode + "','" + SessionLcode + "')";
                        MasterDate.GetDetails(query);
                    }

                }
                else
                {
                    if (Design_check.Rows.Count != 0)
                    {
                        query = "Update Designation_Mst set DesignName='" + Design.Name + "',DeptCode='" + Design.DeptName + "',WorkerCode='" + Design.AgentName + "'";
                        query = query + " where DesignNo='" + Design.ID + "'";
                        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }
                }
            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public JsonResult GetDeptDesignation(string DeptID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();

            string SQL = "";
            DataTable dt = new DataTable();

            //if (Session["EmpID"] != null && Session["EmpID"].ToString() != "")
            //{
            //    SQL = "Select DM.DesignNo,DM.DesignName from Designation_Mst DM inner join Employee_Mst EM on DM.DeptCode=EM.DeptName";
            //    SQL = SQL + " where DM.CompCode='" + SessionCcode + "' And DM.LocCode='" + SessionLcode + "'";
            //    SQL = SQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            //    SQL = SQL + " And EM.EmpNo='" + Session["EmpID"].ToString() + "'";
            //}
            //else
            //{
            SQL = "Select DesignNo,DesignName from Designation_Mst";
            //  SQL = SQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            // SQL = SQL + " And DeptCode='" + DeptID + "'";
            //}



            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Design = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["DesignNo"].ToString();
                List.Name = dt.Rows[i]["DesignName"].ToString();

                Design.Add(List);
            }

            return Json(Design, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetQulification()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();


            string SQL = "Select * from MstQualification ";
            dt = MasterDate.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.ID = dt.Rows[i]["QualificationCd"].ToString();
                list.Name = dt.Rows[i]["QualificationNm"].ToString();

                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InsertQualification(MasterDate Qualify)
        {
            string query = "";
            string message = "Success";
            DataTable Design_check = new DataTable();

            if (Qualify.Name != "")
            {
                if (Qualify.ID == "" || Qualify.ID == null)
                {
                    query = "select * from MstQualification where QualificationNm='" + Qualify.Name + "'";

                }
                else
                {
                    query = "select * from dbo.MstQualification where QualificationCd='" + Qualify.ID + "'";
                    //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                }

                Design_check = MasterDate.GetDetails(query);
                if (Qualify.ID == "" || Qualify.ID == null)
                {
                    if (Design_check.Rows.Count != 0)
                    {
                        query = "Delete from dbo.MstQualification where QualificationNm='" + Qualify.Name + "'";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }
                    //Inert into INWard_Sub
                    query = "insert into dbo.MstQualification(QualificationNm)values";
                    query = query + "('" + Qualify.Name + "')";
                    MasterDate.GetDetails(query);
                }
                else
                {
                    if (Design_check.Rows.Count != 0)
                    {
                        query = "Update MstQualification set QualificationNm='" + Qualify.Name + "' where QualificationCd='" + Qualify.ID + "'";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }
                }

            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetEmployeeType()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();


            string SQL = "Select EmpTypeCd,EmpType,CASE when EmpCategory='1' then 'Staff' else 'Labour' END as EmpCategory from MstEmployeeType ";
            dt = MasterDate.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.ID = dt.Rows[i]["EmpTypeCd"].ToString();
                list.Name = dt.Rows[i]["EmpType"].ToString();
                list.Category = dt.Rows[i]["EmpCategory"].ToString();
                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult InsertEmployeeType(MasterDate EmpType)
        {
            string query = "";
            string message = "Success";
            DataTable Design_check = new DataTable();
            string Cate_Type = "";

            if (EmpType.Name != "")
            {

                if (EmpType.Category == "Staff")
                {
                    Cate_Type = "1";
                }
                else if (EmpType.Category == "Labour")
                {
                    Cate_Type = "2";
                }

                if (EmpType.ID == "" || EmpType.ID == null)
                {
                    query = "select * from MstEmployeeType where EmpType='" + EmpType.Name + "'";

                }
                else
                {
                    query = "select * from MstEmployeeType where EmpTypeCd='" + EmpType.ID + "'";
                    //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                }

                Design_check = MasterDate.GetDetails(query);
                if (EmpType.ID == "" || EmpType.ID == null)
                {
                    if (Design_check.Rows.Count != 0)
                    {
                        query = "Delete from MstEmployeeType where EmpType='" + EmpType.Name + "'";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }

                    //Inert into INWard_Sub
                    query = "insert into MstEmployeeType(EmpType,EmpCategory)values";
                    query = query + "('" + EmpType.Name + "','" + EmpType.Category + "')";
                    MasterDate.GetDetails(query);
                }
                else
                {
                    if (Design_check.Rows.Count != 0)
                    {
                        query = "Update MstEmployeeType set EmpType='" + EmpType.Name + "',EmpCategory='" + EmpType.Category + "' where EmpTypeCd='" + EmpType.ID + "'";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }
                }

            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult InsertBank(MasterDate Bank)
        {
            string query = "";
            string message = "Success";
            DataTable Design_check = new DataTable();
            string Cate_Type = "";

            if (Bank.BankName != "")
            {

                if (Bank.ID == "" || Bank.ID == null)
                {
                    query = "select * from MstBank where BankName='" + Bank.BankName + "' And IFCcode='" + Bank.Branch + "'";
                    //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";

                }
                else
                {
                    query = "select * from MstBank where BankCode='" + Bank.ID + "'";
                    //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                }

                Design_check = MasterDate.GetDetails(query);
                if (Bank.ID == "" || Bank.ID == null)
                {
                    if (Design_check.Rows.Count != 0)
                    {
                        query = "Delete from MstBank where BankName='" + Bank.BankName + "' And IFCcode='" + Bank.Branch + "'";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }

                    //Inert into INWard_Sub
                    query = "insert into MstBank(BankName,IFCcode)values";
                    query = query + "('" + Bank.BankName + "','" + Bank.Branch + "')";
                    MasterDate.GetDetails(query);
                }
                else
                {
                    if (Design_check.Rows.Count != 0)
                    {
                        query = "Update MstBank set BankName='" + Bank.BankName + "',IFCcode='" + Bank.Branch + "' where BankCode='" + Bank.ID + "'";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }
                }
            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult InsertCaste(PersonalDetails Caste1)
        {
            string query = "";
            string message = "Success";
            DataTable Design_check = new DataTable();

            if (Caste1.Caste != "")
            {

                if (Caste1.ID == "" || Caste1.ID == null)
                {
                    query = "select * from MstCaste where CasteName='" + Caste1.Caste + "'";
                    //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                }
                else
                {
                    query = "select * from MstCaste where CasteCode='" + Caste1.ID + "'";
                    //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                }


                Design_check = MasterDate.GetDetails(query);
                if (Caste1.ID == "" || Caste1.ID == null)
                {
                    if (Design_check.Rows.Count != 0)
                    {
                        query = "Delete from MstCaste where CasteName='" + Caste1.Caste + "'";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }

                    //Inert into INWard_Sub
                    query = "insert into MstCaste(CasteName)values";
                    query = query + "('" + Caste1.Caste + "')";
                    MasterDate.GetDetails(query);
                }
                else
                {
                    if (Design_check.Rows.Count != 0)
                    {
                        query = "Update MstCaste set CasteName='" + Caste1.Caste + "' where CasteCode='" + Caste1.ID + "'";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }
                }
            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult InsertCommunity(PersonalDetails Community1)
        {
            string query = "";
            string message = "Success";
            DataTable Design_check = new DataTable();

            if (Community1.Community != "")
            {
                if (Community1.ID == "" || Community1.ID == null)
                {
                    query = "select * from MstCommunity where CommunityName='" + Community1.Community + "'";
                    //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                }
                else
                {
                    query = "select * from MstCommunity where CommunityCode='" + Community1.ID + "'";
                    //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                }

                Design_check = MasterDate.GetDetails(query);
                if (Community1.ID == "" || Community1.ID == null)
                {
                    if (Design_check.Rows.Count != 0)
                    {
                        query = "Delete from MstCommunity where CommunityName='" + Community1.Community + "'";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }



                    //Inert into INWard_Sub
                    query = "insert into MstCommunity(CommunityName)values";
                    query = query + "('" + Community1.Community + "')";
                    MasterDate.GetDetails(query);
                }
                else
                {
                    if (Design_check.Rows.Count != 0)
                    {
                        query = "Update MstCommunity set CommunityName='" + Community1.Community + "' where CommunityCode='" + Community1.ID + "'";
                        //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                        MasterDate.GetDetails(query);

                        message = "Update";
                    }
                }
            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetBank()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();


            string SQL = "Select * from MstBank ";
            dt = MasterDate.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.ID = dt.Rows[i]["BankCode"].ToString();
                list.Name = dt.Rows[i]["BankName"].ToString();
                list.Branch = dt.Rows[i]["IFCcode"].ToString();

                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCaste()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();


            string SQL = "Select * from MstCaste ";
            dt = MasterDate.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.ID = dt.Rows[i]["CasteCode"].ToString();
                list.Name = dt.Rows[i]["CasteName"].ToString();

                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCommunity()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();


            string SQL = "Select * from MstCommunity ";
            dt = MasterDate.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.ID = dt.Rows[i]["CommunityCode"].ToString();
                list.Name = dt.Rows[i]["CommunityName"].ToString();

                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAgent()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();


            string SQL = "Select AgentID,AgentName from AgentMst ";
            SQL = SQL + "where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";

            dt = MasterDate.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.ID = dt.Rows[i]["AgentID"].ToString();
                list.Name = dt.Rows[i]["AgentName"].ToString();

                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult InsertEmpProfile(MasterDate EmpDet)
        {
            string query = "";
            string message = "Success";
            DataTable Emp_check = new DataTable();
            string MachineID_Encrypt = "";
            DataTable DT_Date = new DataTable();
            string SysDate = "";
            query = "SELECT GetDate() as CurDate";
            DT_Date = MasterDate.GetDetails(query);
            SysDate = "GetDate()";



            ///Encrty

            //query = "select *from Employee_Mst";
            //Emp_check = MasterDate.GetDetails(query);
            //for (int i = 0; i < Emp_check.Rows.Count; i++)
            //{
            //    string password = Emp_check.Rows[i]["MachineID"].ToString();

            //    string strmsg = string.Empty;
            //    byte[] encode = new byte[password.Length];
            //    encode = Encoding.UTF8.GetBytes(password);
            //    strmsg = Convert.ToBase64String(encode);

            //    query = "Update Employee_Mst set MachineID_Encrypt='" + strmsg + "' where MachineID='" + password + "'";
            //    MasterDate.GetDetails(query);
            //    //return strmsg;
            //}




            if (EmpDet.Name != "")
            {
                query = "select * from Employee_Mst where EmpNo='" + EmpDet.MachineID + "'";
                query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                Emp_check = MasterDate.GetDetails(query);

                if (Emp_check.Rows.Count != 0)
                {
                    query = "Delete from Employee_Mst where EmpNo='" + EmpDet.MachineID + "'";
                    query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    MasterDate.GetDetails(query);

                    MachineID_Encrypt = UTF8Encryption(EmpDet.MachineID);
                    //Inert into Employee_Mst
                    query = "insert into Employee_Mst(CompCode,LocCode,ShiftType,TypeName,EmpPrefix,EmpNo,CatName,SubCatName,";
                    query = query + "ExistingCode,MachineID,FirstName,LastName,MiddleInitial,Gender,BirthDate,Age,MaritalStatus,";
                    query = query + "DOJ,DeptName,Designation,PayPeriod_Desc,BaseSalary,PFNo,Nominee,ESINo,StdWrkHrs,";
                    query = query + "OTEligible,Nationality,Qualification,RecuritmentThro,IDMark1,IDMark2,BloodGroup,";
                    query = query + "Handicapped,Height,Weight,Address1,Address2,BankName,IsActive,Created_By,Created_Date,";
                    query = query + "IsNonAdmin,MachineID_Encrypt,Working_Hours,Calculate_Work_Hours,OT_Hours,Wages,";
                    query = query + "RecutersMob,parentsMobile,ParentsPhone,EmployeeMobile,Religion,BusNo,";
                    query = query + "Permanent_Dist,Permanent_Taluk,Present_Dist,Present_Taluk,WeekOff,";
                    query = query + "ESICode,PFDOJ,ESIDOJ,BranchCode,IFSC_Code,Eligible_PF,Eligible_ESI,BusRoute,";
                    query = query + "RoomNo,BasicSalary,OTSal,Deduction1amt,Deduction2amt,Allowance1amt,Allowance2amt,IncentAmt,";
                    query = query + "PFS,Mother,FatherName,GuardianName,SalaryThro,BrokerName,BrokerType,Community,Caste,PhysicalRemarks,";
                    query = query + "RejoinDate,RelieveDate,VPFSal,Others1,Others2,State,DocType,DocNo,DocDesc,";
                    query = query + "ConvenyAmt,SplAmt,Grade,WageCategoty,WorkCode,EligibleCateen,CanteenName,CanteenOperator,";
                    query = query + "LabourAmt,CanManageAmt,EligibleSnacks,SnacksOperator,SnacksAmt,EligibleIncentive,LeaveFrom1,";
                    query = query + "LeaveTo1,LeaveFrom2,LeaveTo2,LevDesign1,LevDesign2,Residance";
                    query = query + ")values";
                    query = query + "('" + SessionCcode + "','" + SessionLcode + "','" + EmpDet.Shift + "','GENERAL','A','" + EmpDet.MachineID + "','" + EmpDet.Category + "',";
                    query = query + "'" + EmpDet.SubCatName + "','" + EmpDet.ExistingCode + "','" + EmpDet.MachineID + "',";
                    query = query + "'" + EmpDet.Name + "','" + EmpDet.LastName + "','" + EmpDet.LastName + "','" + EmpDet.Gender + "','" + EmpDet.DOB + "',";
                    query = query + "'" + EmpDet.Age + "','" + EmpDet.MartialStatus + "','" + EmpDet.DOJ + "','" + EmpDet.DeptName + "',";
                    query = query + "'" + EmpDet.Designation + "','0.0','" + EmpDet.BasicSal + "','" + EmpDet.PFNo + "','" + EmpDet.Nominee + "',";
                    query = query + "'" + EmpDet.ESINo + "','" + EmpDet.StdWorkingHrs + "','" + EmpDet.OTEligible + "','" + EmpDet.Nationality + "',";
                    query = query + "'" + EmpDet.Qualification + "','" + EmpDet.RecruitThrg + "','" + EmpDet.IdenMark1 + "','" + EmpDet.IdenMark2 + "',";
                    query = query + "'" + EmpDet.BloodGrp + "','" + EmpDet.PhyChallenged + "','" + EmpDet.Height + "','" + EmpDet.Weight + "',";
                    query = query + "'" + EmpDet.PermAddr + "','" + EmpDet.TempAddr + "','" + EmpDet.BankName + "','" + EmpDet.ActiveMode + "',";
                    query = query + "'',GetDate(),'" + EmpDet.IsNonAdmin + "','" + MachineID_Encrypt + "','0','0','0',";
                    query = query + "'" + EmpDet.EmpType + "','" + EmpDet.RecruitMobile + "','" + EmpDet.ParentMob1 + "','" + EmpDet.ParentMob2 + "',";
                    query = query + "'" + EmpDet.EmpMobileNo + "','" + EmpDet.Religion + "','" + EmpDet.BusNo + "','" + EmpDet.PermDist + "','" + EmpDet.PermTaluk + "',";
                    query = query + "'" + EmpDet.TempDist + "','" + EmpDet.TempTaluk + "','" + EmpDet.WeekOff + "','" + EmpDet.ESINo + "',";
                    query = query + "'" + EmpDet.PFDate + "','" + EmpDet.ESIDate + "','" + EmpDet.Branch + "','" + EmpDet.IFSCCode + "','" + EmpDet.PFEligible + "','" + EmpDet.ESIEligible + "',";
                    query = query + "'" + EmpDet.BusRoute + "','" + EmpDet.HostelRoom + "','" + EmpDet.BasicSal + "','" + EmpDet.OTSal + "','" + EmpDet.Deduction1 + "','" + EmpDet.Deduction2 + "',";
                    query = query + "'" + EmpDet.Allowance1 + "','" + EmpDet.Allowance2 + "','" + EmpDet.IncentAmt + "','" + EmpDet.PFSal + "','" + EmpDet.MotherName + "','" + EmpDet.FatherName + "','" + EmpDet.GuardianName + "',";
                    query = query + "'" + EmpDet.SalaryThrough + "','" + EmpDet.AgentName + "','" + EmpDet.AgentType + "','" + EmpDet.Community + "','" + EmpDet.Caste + "',";
                    query = query + "'" + EmpDet.PhyReason + "','" + EmpDet.RejoinDate + "','" + EmpDet.ReleaveDate + "','" + EmpDet.VPFSal + "','" + EmpDet.Others1 + "','" + EmpDet.Others2 + "',";
                    query = query + "'" + EmpDet.State + "','" + EmpDet.DocType + "','" + EmpDet.DocNo + "','" + EmpDet.DocDesc + "','" + EmpDet.ConvenyAmt + "','" + EmpDet.SplAmt + "',";
                    query = query + "'" + EmpDet.Grade + "','" + EmpDet.WageCategoty + "','" + EmpDet.WorkCode + "','" + EmpDet.EligibleCateen + "','" + EmpDet.CanteenName + "',";
                    query = query + "'" + EmpDet.CanteenOperator + "','" + EmpDet.LabourAmt + "','" + EmpDet.CanManageAmt + "','" + EmpDet.EligibleSnacks + "','" + EmpDet.SnacksOperator + "',";
                    query = query + "'" + EmpDet.SnacksAmt + "','" + EmpDet.EligibleIncentive + "','" + EmpDet.LeaveFrom1 + "','" + EmpDet.LeaveTo1 + "','" + EmpDet.LeaveFrom2 + "',";
                    query = query + "'" + EmpDet.LeaveTo2 + "','" + EmpDet.LevDesign1 + "','" + EmpDet.LevDesign2 + "','" + EmpDet.Residance + "'";

                    query = query + ")";
                    MasterDate.GetDetails(query);


                    message = "Update";
                }
                else
                {



                    query = "Delete from Employee_Mst where EmpNo='" + EmpDet.MachineID + "'";
                    query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    MasterDate.GetDetails(query);

                    MachineID_Encrypt = UTF8Encryption(EmpDet.MachineID);
                    //Inert into Employee_Mst
                    query = "insert into Employee_Mst(CompCode,LocCode,ShiftType,TypeName,EmpPrefix,EmpNo,CatName,SubCatName,";
                    query = query + "ExistingCode,MachineID,FirstName,LastName,MiddleInitial,Gender,BirthDate,Age,MaritalStatus,";
                    query = query + "DOJ,DeptName,Designation,PayPeriod_Desc,BaseSalary,PFNo,Nominee,ESINo,StdWrkHrs,";
                    query = query + "OTEligible,Nationality,Qualification,RecuritmentThro,IDMark1,IDMark2,BloodGroup,";
                    query = query + "Handicapped,Height,Weight,Address1,Address2,BankName,IsActive,Created_By,Created_Date,";
                    query = query + "IsNonAdmin,MachineID_Encrypt,Working_Hours,Calculate_Work_Hours,OT_Hours,Wages,";
                    query = query + "RecutersMob,parentsMobile,ParentsPhone,EmployeeMobile,Religion,BusNo,";
                    query = query + "Permanent_Dist,Permanent_Taluk,Present_Dist,Present_Taluk,WeekOff,";
                    query = query + "ESICode,PFDOJ,ESIDOJ,BranchCode,IFSC_Code,Eligible_PF,Eligible_ESI,BusRoute,";
                    query = query + "RoomNo,BasicSalary,OTSal,Deduction1amt,Deduction2amt,Allowance1amt,Allowance2amt,IncentAmt,";
                    query = query + "PFS,Mother,FatherName,GuardianName,SalaryThro,BrokerName,BrokerType,Community,Caste,PhysicalRemarks,";
                    query = query + "RejoinDate,RelieveDate,VPFSal,Others1,Others2,State,DocType,DocNo,DocDesc,";
                    query = query + "ConvenyAmt,SplAmt,Grade,WageCategoty,WorkCode,EligibleCateen,CanteenName,CanteenOperator,";
                    query = query + "LabourAmt,CanManageAmt,EligibleSnacks,SnacksOperator,SnacksAmt,EligibleIncentive,LeaveFrom1,";
                    query = query + "LeaveTo1,LeaveFrom2,LeaveTo2,LevDesign1,LevDesign2,Residance";
                    query = query + ")values";
                    query = query + "('" + SessionCcode + "','" + SessionLcode + "','" + EmpDet.Shift + "','GENERAL','A','" + EmpDet.MachineID + "','" + EmpDet.Category + "',";
                    query = query + "'" + EmpDet.SubCatName + "','" + EmpDet.ExistingCode + "','" + EmpDet.MachineID + "',";
                    query = query + "'" + EmpDet.Name + "','" + EmpDet.LastName + "','" + EmpDet.LastName + "','" + EmpDet.Gender + "','" + EmpDet.DOB + "',";
                    query = query + "'" + EmpDet.Age + "','" + EmpDet.MartialStatus + "','" + EmpDet.DOJ + "','" + EmpDet.DeptName + "',";
                    query = query + "'" + EmpDet.Designation + "','0.0','" + EmpDet.BasicSal + "','" + EmpDet.PFNo + "','" + EmpDet.Nominee + "',";
                    query = query + "'" + EmpDet.ESINo + "','" + EmpDet.StdWorkingHrs + "','" + EmpDet.OTEligible + "','" + EmpDet.Nationality + "',";
                    query = query + "'" + EmpDet.Qualification + "','" + EmpDet.RecruitThrg + "','" + EmpDet.IdenMark1 + "','" + EmpDet.IdenMark2 + "',";
                    query = query + "'" + EmpDet.BloodGrp + "','" + EmpDet.PhyChallenged + "','" + EmpDet.Height + "','" + EmpDet.Weight + "',";
                    query = query + "'" + EmpDet.PermAddr + "','" + EmpDet.TempAddr + "','" + EmpDet.BankName + "','" + EmpDet.ActiveMode + "',";
                    query = query + "'',GetDate(),'" + EmpDet.IsNonAdmin + "','" + MachineID_Encrypt + "','0','0','0',";
                    query = query + "'" + EmpDet.EmpType + "','" + EmpDet.RecruitMobile + "','" + EmpDet.ParentMob1 + "','" + EmpDet.ParentMob2 + "',";
                    query = query + "'" + EmpDet.EmpMobileNo + "','" + EmpDet.Religion + "','" + EmpDet.BusNo + "','" + EmpDet.PermDist + "','" + EmpDet.PermTaluk + "',";
                    query = query + "'" + EmpDet.TempDist + "','" + EmpDet.TempTaluk + "','" + EmpDet.WeekOff + "','" + EmpDet.ESINo + "',";
                    query = query + "'" + EmpDet.PFDate + "','" + EmpDet.ESIDate + "','" + EmpDet.Branch + "','" + EmpDet.IFSCCode + "','" + EmpDet.PFEligible + "','" + EmpDet.ESIEligible + "',";
                    query = query + "'" + EmpDet.BusRoute + "','" + EmpDet.HostelRoom + "','" + EmpDet.BasicSal + "','" + EmpDet.OTSal + "','" + EmpDet.Deduction1 + "','" + EmpDet.Deduction2 + "',";
                    query = query + "'" + EmpDet.Allowance1 + "','" + EmpDet.Allowance2 + "','" + EmpDet.IncentAmt + "','" + EmpDet.PFSal + "','" + EmpDet.MotherName + "','" + EmpDet.FatherName + "','" + EmpDet.GuardianName + "',";
                    query = query + "'" + EmpDet.SalaryThrough + "','" + EmpDet.AgentName + "','" + EmpDet.AgentType + "','" + EmpDet.Community + "','" + EmpDet.Caste + "',";
                    query = query + "'" + EmpDet.PhyReason + "','" + EmpDet.RejoinDate + "','" + EmpDet.ReleaveDate + "','" + EmpDet.VPFSal + "','" + EmpDet.Others1 + "','" + EmpDet.Others2 + "',";
                    query = query + "'" + EmpDet.State + "','" + EmpDet.DocType + "','" + EmpDet.DocNo + "','" + EmpDet.DocDesc + "','" + EmpDet.ConvenyAmt + "','" + EmpDet.SplAmt + "',";
                    query = query + "'" + EmpDet.Grade + "','" + EmpDet.WageCategoty + "','" + EmpDet.WorkCode + "','" + EmpDet.EligibleCateen + "','" + EmpDet.CanteenName + "',";
                    query = query + "'" + EmpDet.CanteenOperator + "','" + EmpDet.LabourAmt + "','" + EmpDet.CanManageAmt + "','" + EmpDet.EligibleSnacks + "','" + EmpDet.SnacksOperator + "',";
                    query = query + "'" + EmpDet.SnacksAmt + "','" + EmpDet.EligibleIncentive + "','" + EmpDet.LeaveFrom1 + "','" + EmpDet.LeaveTo1 + "','" + EmpDet.LeaveFrom2 + "',";
                    query = query + "'" + EmpDet.LeaveTo2 + "','" + EmpDet.LevDesign1 + "','" + EmpDet.LevDesign2 + "','" + EmpDet.Residance + "'";

                    query = query + ")";
                    MasterDate.GetDetails(query);
                    message = "Update";

                    //MachineID_Encrypt = UTF8Encryption(EmpDet.MachineID);
                    ////Inert into Employee_Mst
                    //query = "insert into Employee_Approval(CompCode,LocCode,ShiftType,TypeName,EmpPrefix,EmpNo,CatName,SubCatName,";
                    //query = query + "ExistingCode,MachineID,FirstName,LastName,MiddleInitial,Gender,BirthDate,Age,MaritalStatus,";
                    //query = query + "DOJ,DeptName,Designation,PayPeriod_Desc,BaseSalary,PFNo,Nominee,ESINo,StdWrkHrs,";
                    //query = query + "OTEligible,Nationality,Qualification,RecuritmentThro,IDMark1,IDMark2,BloodGroup,";
                    //query = query + "Handicapped,Height,Weight,Address1,Address2,BankName,IsActive,Created_By,Created_Date,";
                    //query = query + "IsNonAdmin,MachineID_Encrypt,Working_Hours,Calculate_Work_Hours,OT_Hours,Wages,";
                    //query = query + "RecutersMob,parentsMobile,ParentsPhone,EmployeeMobile,Religion,BusNo,";
                    //query = query + "Permanent_Dist,Permanent_Taluk,Present_Dist,Present_Taluk,WeekOff,";
                    //query = query + "ESICode,PFDOJ,ESIDOJ,BranchCode,IFSC_Code,Eligible_PF,Eligible_ESI,BusRoute,";
                    //query = query + "RoomNo,BasicSalary,OTSal,Deduction1amt,Deduction2amt,Allowance1amt,Allowance2amt,IncentAmt,";
                    //query = query + "PFS,Mother,FatherName,GuardianName,SalaryThro,BrokerName,BrokerType,Community,Caste,PhysicalRemarks,";
                    //query = query + "RejoinDate,RelieveDate,VPFSal,Others1,Others2,State,DocType,DocNo,DocDesc,";
                    //query = query + "ConvenyAmt,SplAmt,Grade,WageCategoty,WorkCode,EligibleCateen,CanteenName,CanteenOperator,";
                    //query = query + "LabourAmt,CanManageAmt,EligibleSnacks,SnacksOperator,SnacksAmt,EligibleIncentive,LeaveFrom1,";
                    //query = query + "LeaveTo1,LeaveFrom2,LeaveTo2,LevDesign1,LevDesign2,Residance";
                    //query = query + ")values";
                    //query = query + "('" + SessionCcode + "','" + SessionLcode + "','" + EmpDet.Shift + "','GENERAL','A','" + EmpDet.MachineID + "','" + EmpDet.Category + "',";
                    //query = query + "'" + EmpDet.SubCatName + "','" + EmpDet.ExistingCode + "','" + EmpDet.MachineID + "',";
                    //query = query + "'" + EmpDet.Name + "','" + EmpDet.LastName + "','" + EmpDet.LastName + "','" + EmpDet.Gender + "','" + EmpDet.DOB + "',";
                    //query = query + "'" + EmpDet.Age + "','" + EmpDet.MartialStatus + "','" + EmpDet.DOJ + "','" + EmpDet.DeptName + "',";
                    //query = query + "'" + EmpDet.Designation + "','0.0','" + EmpDet.BasicSal + "','" + EmpDet.PFNo + "','" + EmpDet.Nominee + "',";
                    //query = query + "'" + EmpDet.ESINo + "','" + EmpDet.StdWorkingHrs + "','" + EmpDet.OTEligible + "','" + EmpDet.Nationality + "',";
                    //query = query + "'" + EmpDet.Qualification + "','" + EmpDet.RecruitThrg + "','" + EmpDet.IdenMark1 + "','" + EmpDet.IdenMark2 + "',";
                    //query = query + "'" + EmpDet.BloodGrp + "','" + EmpDet.PhyChallenged + "','" + EmpDet.Height + "','" + EmpDet.Weight + "',";
                    //query = query + "'" + EmpDet.PermAddr + "','" + EmpDet.TempAddr + "','" + EmpDet.BankName + "','" + EmpDet.ActiveMode + "',";
                    //query = query + "'',GetDate(),'" + EmpDet.IsNonAdmin + "','" + MachineID_Encrypt + "','0','0','0',";
                    //query = query + "'" + EmpDet.EmpType + "','" + EmpDet.RecruitMobile + "','" + EmpDet.ParentMob1 + "','" + EmpDet.ParentMob2 + "',";
                    //query = query + "'" + EmpDet.EmpMobileNo + "','" + EmpDet.Religion + "','" + EmpDet.BusNo + "','" + EmpDet.PermDist + "','" + EmpDet.PermTaluk + "',";
                    //query = query + "'" + EmpDet.TempDist + "','" + EmpDet.TempTaluk + "','" + EmpDet.WeekOff + "','" + EmpDet.ESINo + "',";
                    //query = query + "'" + EmpDet.PFDate + "','" + EmpDet.ESIDate + "','" + EmpDet.Branch + "','" + EmpDet.IFSCCode + "','" + EmpDet.PFEligible + "','" + EmpDet.ESIEligible + "',";
                    //query = query + "'" + EmpDet.BusRoute + "','" + EmpDet.HostelRoom + "','" + EmpDet.BasicSal + "','" + EmpDet.OTSal + "','" + EmpDet.Deduction1 + "','" + EmpDet.Deduction2 + "',";
                    //query = query + "'" + EmpDet.Allowance1 + "','" + EmpDet.Allowance2 + "','" + EmpDet.IncentAmt + "','" + EmpDet.PFSal + "','" + EmpDet.MotherName + "','" + EmpDet.FatherName + "','" + EmpDet.GuardianName + "',";
                    //query = query + "'" + EmpDet.SalaryThrough + "','" + EmpDet.AgentName + "','" + EmpDet.AgentType + "','" + EmpDet.Community + "','" + EmpDet.Caste + "',";
                    //query = query + "'" + EmpDet.PhyReason + "','" + EmpDet.RejoinDate + "','" + EmpDet.ReleaveDate + "','" + EmpDet.VPFSal + "','" + EmpDet.Others1 + "','" + EmpDet.Others2 + "',";
                    //query = query + "'" + EmpDet.State + "','" + EmpDet.DocType + "','" + EmpDet.DocNo + "','" + EmpDet.DocDesc + "','" + EmpDet.ConvenyAmt + "','" + EmpDet.SplAmt + "',";
                    //query = query + "'" + EmpDet.Grade + "','" + EmpDet.WageCategoty + "','" + EmpDet.WorkCode + "','" + EmpDet.EligibleCateen + "','" + EmpDet.CanteenName + "',";
                    //query = query + "'" + EmpDet.CanteenOperator + "','" + EmpDet.LabourAmt + "','" + EmpDet.CanManageAmt + "','" + EmpDet.EligibleSnacks + "','" + EmpDet.SnacksOperator + "',";
                    //query = query + "'" + EmpDet.SnacksAmt + "','" + EmpDet.EligibleIncentive + "','" + EmpDet.LeaveFrom1 + "','" + EmpDet.LeaveTo1 + "','" + EmpDet.LeaveFrom2 + "',";
                    //query = query + "'" + EmpDet.LeaveTo2 + "','" + EmpDet.LevDesign1 + "','" + EmpDet.LevDesign2 + "','" + EmpDet.Residance + "'";
                    //query = query + ")";
                    //MasterDate.GetDetails(query);
                }
            }
            else
            {
                message = "No Data";
            }

            //db.Employees.Add(emp);
            //db.SaveChanges();

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetEmployee(string ActiveMode)
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();

            if (SessionUserName == "IF")

            {
                string SQL = "select EM.MachineID,EM.ExistingCode,(EM.FirstName + '' + EM.MiddleInitial) as FirstName,DM.DeptName,(MT.EmpType) as Wages from Employee_Mst EM inner join Department_Mst DM on EM.DeptName=DM.DeptCode inner join MstEmployeeType MT on EM.Wages=MT.EmpTypeCd where EM.IsActive='" + ActiveMode + "'";
                SQL = SQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' and (EM.Wages='1' or EM.Wages='2') ";
                dt = MasterDate.GetDetails(SQL);
            }
            else
            {

                string SQL = "select EM.MachineID,EM.ExistingCode,(EM.FirstName + '' + EM.MiddleInitial) as FirstName,DM.DeptName,(MT.EmpType) as Wages from Employee_Mst EM inner join Department_Mst DM on EM.DeptName=DM.DeptCode inner join MstEmployeeType MT on EM.Wages=MT.EmpTypeCd where EM.IsActive='" + ActiveMode + "'";
                SQL = SQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
                dt = MasterDate.GetDetails(SQL);
            }



            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.MachineID = dt.Rows[i]["MachineID"].ToString();
                list.ExistingCode = dt.Rows[i]["ExistingCode"].ToString();
                list.Name = dt.Rows[i]["FirstName"].ToString();
                list.DeptName = dt.Rows[i]["DeptName"].ToString();
                list.EmpType = dt.Rows[i]["Wages"].ToString();

                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetEditDepartment(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();

            string SQL = "Select * from Department_Mst where DeptCode='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["DeptCode"].ToString();
                List.Name = dt.Rows[i]["DeptName"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEditGrade(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();

            string SQL = "Select * from MstGrade where GradeID='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["GradeID"].ToString();
                List.Name = dt.Rows[i]["GradeName"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetEditCanteenOperator(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();

            string SQL = "Select CanOptID,CanteenNameOpt from MstCanteenOpt where CanOptID='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["CanOptID"].ToString();
                List.Name = dt.Rows[i]["CanteenNameOpt"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetEditAgent(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();

            string SQL = "Select * from AgentMst where AgentID='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["AgentID"].ToString();
                List.Name = dt.Rows[i]["AgentName"].ToString();
                List.Deduction1 = dt.Rows[i]["Shift8"].ToString();
                List.Deduction2 = dt.Rows[i]["Shift12"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDeleteGrade(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";
            string SQL = "Select * from Employee_Mst where Grade='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            if (dt.Rows.Count == 0)
            {
                SQL = "Delete from MstGrade where GradeID='" + ID + "'";
                //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                MasterDate.GetDetails(SQL);

                message = "Delete";
            }
            else
            {
                message = "Already";
            }

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public JsonResult GetDeleteCanteenOperator(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";
            string SQL = "Select * from Employee_Mst where CanteenOperator='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            if (dt.Rows.Count == 0)
            {
                SQL = "Delete from MstCanteenOpt where CanOptID='" + ID + "'";
                //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                MasterDate.GetDetails(SQL);

                message = "Delete";
            }
            else
            {
                message = "Already";
            }

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public JsonResult GetDeleteAgent(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";
            string SQL = "Select * from Employee_Mst where BrokerName='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            if (dt.Rows.Count == 0)
            {
                SQL = "Delete from AgentMst where AgentID='" + ID + "'";
                //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                MasterDate.GetDetails(SQL);

                message = "Delete";
            }
            else
            {
                message = "Already";
            }

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetDeleteDepartment(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";
            string SQL = "Select *from Employee_Mst where DeptName='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            if (dt.Rows.Count == 0)
            {
                SQL = "Delete from Department_Mst where DeptCode='" + ID + "'";
                //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                MasterDate.GetDetails(SQL);

                message = "Delete";
            }
            else
            {
                message = "Already";
            }

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetEditDesignation(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();

            string SQL = "Select *from Designation_Mst where DesignNo='" + ID + "'";
            SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["DesignNo"].ToString();
                List.Name = dt.Rows[i]["DesignName"].ToString();
                List.DeptName = dt.Rows[i]["DeptCode"].ToString();
                List.AgentName = dt.Rows[i]["WorkerCode"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetEditQualification(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();

            string SQL = "Select *from MstQualification where QualificationCd='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["QualificationCd"].ToString();
                List.Name = dt.Rows[i]["QualificationNm"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDeleteQualification(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";
            string SQL = "Select *from Employee_Mst where Qualification='" + ID + "'";
            SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            dt = MasterDate.GetDetails(SQL);

            if (dt.Rows.Count == 0)
            {
                SQL = "Delete from MstQualification where QualificationCd='" + ID + "'";
                //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                MasterDate.GetDetails(SQL);

                message = "Delete";
            }
            else
            {
                message = "Already";
            }

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetEditEmpType(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();

            string SQL = "Select *from MstEmployeeType where EmpTypeCd='" + ID + "'";
            SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["EmpTypeCd"].ToString();
                List.Name = dt.Rows[i]["EmpType"].ToString();
                List.Category = dt.Rows[i]["EmpCategory"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDeleteDesignation(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";
            string SQL = "Select *from Employee_Mst where Designation='" + ID + "'";
            SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            dt = MasterDate.GetDetails(SQL);

            if (dt.Rows.Count == 0)
            {
                SQL = "Delete from Designation_Mst where DesignNo='" + ID + "'";
                //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                MasterDate.GetDetails(SQL);

                message = "Delete";
            }
            else
            {
                message = "Already";
            }

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetEditBank(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();

            string SQL = "Select *from MstBank where BankCode='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["BankCode"].ToString();
                List.Name = dt.Rows[i]["BankName"].ToString();
                List.Branch = dt.Rows[i]["IFCcode"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDeleteBank(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";
            string SQL = "Select *from Employee_Mst where BankName='" + ID + "'";
            SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            dt = MasterDate.GetDetails(SQL);

            if (dt.Rows.Count == 0)
            {
                SQL = "Delete from MstBank where BankCode='" + ID + "'";
                //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                MasterDate.GetDetails(SQL);

                message = "Delete";
            }
            else
            {
                message = "Already";
            }

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetDeleteEmpType(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";
            string SQL = "Select *from Employee_Mst where Wages='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            if (dt.Rows.Count == 0)
            {
                SQL = "Delete from MstEmployeeType where EmpTypeCd='" + ID + "'";
                //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                MasterDate.GetDetails(SQL);

                message = "Delete";
            }
            else
            {
                message = "Already";
            }

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetEditCaste(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();

            string SQL = "Select *from MstCaste where CasteCode='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["CasteCode"].ToString();
                List.Name = dt.Rows[i]["CasteName"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDeleteCaste(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";
            string SQL = "Select *from Employee_Mst where Caste='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            if (dt.Rows.Count == 0)
            {
                SQL = "Delete from MstCaste where CasteCode='" + ID + "'";
                //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                MasterDate.GetDetails(SQL);

                message = "Delete";
            }
            else
            {
                message = "Already";
            }

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetEditCommunity(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();

            string SQL = "Select *from MstCommunity where CommunityCode='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["CommunityCode"].ToString();
                List.Name = dt.Rows[i]["CommunityName"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDeleteCommunity(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";
            string SQL = "Select *from Employee_Mst where Community='" + ID + "'";
            //SQL = SQL + "where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            if (dt.Rows.Count == 0)
            {
                SQL = "Delete from MstCommunity where CommunityCode='" + ID + "'";
                //query = query + " And Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
                MasterDate.GetDetails(SQL);

                message = "Delete";
            }
            else
            {
                message = "Already";
            }

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public void GetEditEmployee(string TransID)
        {
            Session["EmpID"] = TransID;
        }
        public void AddEmployee()
        {
            Session["EmpID"] = "";
        }

        public JsonResult EmployeeEditList()
        {
            DataTable dt = new DataTable();
            List<MasterDate> TransList = new List<MasterDate>();
            if (Session["EmpID"] != null && Session["EmpID"].ToString() != "")
            {
                string SQL = "Select * from Employee_Mst where  EmpNo='" + Session["EmpID"] + "'";
                SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                dt = MasterDate.GetDetails(SQL);
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate Emplist = new MasterDate();
                Emplist.Category = dt.Rows[i]["CatName"].ToString();
                Emplist.SubCatName = dt.Rows[i]["SubCatName"].ToString();
                Emplist.Shift = dt.Rows[i]["ShiftType"].ToString();
                Emplist.MachineID = dt.Rows[i]["EmpNo"].ToString();
                Emplist.ExistingCode = dt.Rows[i]["ExistingCode"].ToString();
                Emplist.MachineID = dt.Rows[i]["MachineID"].ToString();
                Emplist.Name = dt.Rows[i]["FirstName"].ToString();
                Emplist.LastName = dt.Rows[i]["LastName"].ToString();
                Emplist.DOB = dt.Rows[i]["BirthDate"].ToString();
                Emplist.Age = dt.Rows[i]["Age"].ToString();
                Emplist.Gender = dt.Rows[i]["Gender"].ToString();

                Emplist.DOJ = dt.Rows[i]["DOJ"].ToString();
                Emplist.RejoinDate = dt.Rows[i]["RejoinDate"].ToString();
                Emplist.DeptName = dt.Rows[i]["DeptName"].ToString();
                Emplist.Designation = dt.Rows[i]["Designation"].ToString();
                Emplist.EmpType = dt.Rows[i]["Wages"].ToString();
                Emplist.Qualification = dt.Rows[i]["Qualification"].ToString();
                Emplist.EmpMobileNo = dt.Rows[i]["EmployeeMobile"].ToString();
                Emplist.OTEligible = dt.Rows[i]["OTEligible"].ToString();
                Emplist.PFEligible = dt.Rows[i]["Eligible_PF"].ToString();
                Emplist.PFNo = dt.Rows[i]["PFNo"].ToString();
                Emplist.PFDate = dt.Rows[i]["PFDOJ"].ToString();
                Emplist.ESIEligible = dt.Rows[i]["Eligible_ESI"].ToString();
                Emplist.ESINo = dt.Rows[i]["ESINo"].ToString();
                Emplist.ESIDate = dt.Rows[i]["ESIDOJ"].ToString();
                Emplist.HostelRoom = dt.Rows[i]["RoomNo"].ToString();
                Emplist.BusRoute = dt.Rows[i]["BusRoute"].ToString();
                Emplist.BusNo = dt.Rows[i]["BusNo"].ToString();
                Emplist.ActiveMode = dt.Rows[i]["IsActive"].ToString();
                Emplist.ReleaveDate = dt.Rows[i]["RelieveDate"].ToString();
                Emplist.IsNonAdmin = dt.Rows[i]["IsActive"].ToString();
                Emplist.SalaryThrough = dt.Rows[i]["SalaryThro"].ToString();
                Emplist.BankName = dt.Rows[i]["BankName"].ToString();
                Emplist.IFSCCode = dt.Rows[i]["IFSC_Code"].ToString();
                Emplist.Branch = dt.Rows[i]["BranchCode"].ToString();
                Emplist.BasicSal = dt.Rows[i]["BasicSalary"].ToString();
                Emplist.OTSal = dt.Rows[i]["OTSal"].ToString();
                Emplist.Allowance1 = dt.Rows[i]["Allowance1amt"].ToString();
                Emplist.Allowance2 = dt.Rows[i]["Allowance2amt"].ToString();
                Emplist.Deduction1 = dt.Rows[i]["Deduction1amt"].ToString();
                Emplist.Deduction2 = dt.Rows[i]["Deduction2amt"].ToString();
                Emplist.PFSal = dt.Rows[i]["PFS"].ToString();
                Emplist.VPFSal = dt.Rows[i]["VPFSal"].ToString();
                Emplist.Others1 = dt.Rows[i]["Others1"].ToString();
                Emplist.Others2 = dt.Rows[i]["Others2"].ToString();
                Emplist.MartialStatus = dt.Rows[i]["MaritalStatus"].ToString();
                Emplist.Nationality = dt.Rows[i]["Nationality"].ToString();
                Emplist.Religion = dt.Rows[i]["Religion"].ToString();
                Emplist.Height = dt.Rows[i]["Height"].ToString();
                Emplist.Weight = dt.Rows[i]["Weight"].ToString();
                Emplist.PhyChallenged = dt.Rows[i]["Handicapped"].ToString();
                Emplist.PhyReason = dt.Rows[i]["PhysicalRemarks"].ToString();
                Emplist.StdWorkingHrs = dt.Rows[i]["StdWrkHrs"].ToString();
                Emplist.IncentAmt = dt.Rows[i]["IncentAmt"].ToString();
                Emplist.BloodGrp = dt.Rows[i]["BloodGroup"].ToString();
                Emplist.Caste = dt.Rows[i]["Caste"].ToString();
                Emplist.Community = dt.Rows[i]["Community"].ToString();
                Emplist.AgentName = dt.Rows[i]["BrokerName"].ToString();
                Emplist.AgentType = dt.Rows[i]["BrokerType"].ToString();
                Emplist.RecruitThrg = dt.Rows[i]["RecuritmentThro"].ToString();
                Emplist.RecruitMobile = dt.Rows[i]["RecutersMob"].ToString();
                Emplist.WeekOff = dt.Rows[i]["WeekOff"].ToString();
                Emplist.Nominee = dt.Rows[i]["Nominee"].ToString();
                Emplist.FatherName = dt.Rows[i]["FatherName"].ToString();
                Emplist.MotherName = dt.Rows[i]["Mother"].ToString();
                Emplist.GuardianName = dt.Rows[i]["GuardianName"].ToString();
                Emplist.PermAddr = dt.Rows[i]["Address1"].ToString();
                Emplist.PermTaluk = dt.Rows[i]["Permanent_Taluk"].ToString();
                Emplist.PermDist = dt.Rows[i]["Permanent_Dist"].ToString();
                Emplist.TempAddr = dt.Rows[i]["Address2"].ToString();
                Emplist.TempTaluk = dt.Rows[i]["Present_Taluk"].ToString();
                Emplist.TempDist = dt.Rows[i]["Present_Dist"].ToString();
                Emplist.State = dt.Rows[i]["State"].ToString();
                Emplist.IdenMark1 = dt.Rows[i]["IDMark1"].ToString();
                Emplist.IdenMark2 = dt.Rows[i]["IDMark2"].ToString();
                Emplist.ParentMob1 = dt.Rows[i]["parentsMobile"].ToString();
                Emplist.ParentMob2 = dt.Rows[i]["ParentsPhone"].ToString();
                Emplist.DocType = dt.Rows[i]["DocType"].ToString();
                Emplist.DocNo = dt.Rows[i]["DocNo"].ToString();
                Emplist.DocDesc = dt.Rows[i]["DocDesc"].ToString();

                TransList.Add(Emplist);
            }

            return Json(TransList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmployeeApproval()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();


            string SQL = "Select * from Employee_Approval where (Status is null or Status='2')";
            SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            dt = MasterDate.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.MachineID = dt.Rows[i]["MachineID"].ToString();
                list.ExistingCode = dt.Rows[i]["ExistingCode"].ToString();
                list.Name = dt.Rows[i]["FirstName"].ToString() + " " + dt.Rows[i]["LastName"].ToString();
                list.DeptName = dt.Rows[i]["DeptName"].ToString();
                list.EmpType = dt.Rows[i]["Wages"].ToString();

                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmployeeStatus()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();


            string SQL = "Select MachineID,ExistingCode,FirstName,LastName,DeptName,Wages,";
            SQL = SQL + "CASE when Status='2' then 'Pending' when Status='3' then 'Cancel' END as Status from Employee_Approval where (Status='2' or Status='3')";
            SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            dt = MasterDate.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.MachineID = dt.Rows[i]["MachineID"].ToString();
                list.ExistingCode = dt.Rows[i]["ExistingCode"].ToString();
                list.Name = dt.Rows[i]["FirstName"].ToString() + " " + dt.Rows[i]["LastName"].ToString();
                list.DeptName = dt.Rows[i]["DeptName"].ToString();
                list.EmpType = dt.Rows[i]["Wages"].ToString();
                list.AgentName = dt.Rows[i]["Status"].ToString();

                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetApproveEmployee(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";

            string SQL = "Select *from Employee_Approval where EmpNo='" + ID + "'";
            SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            dt = MasterDate.GetDetails(SQL);


            if (dt.Rows.Count != 0)
            {

                SQL = "Insert into Employee_Mst(CompCode,LocCode,ShiftType,TypeName,EmpPrefix,";
                SQL = SQL + "EmpNo,CatName,SubCatName,ExistingCode,MachineID,";
                SQL = SQL + "FirstName,LastName,MiddleInitial,Gender,BirthDate,";
                SQL = SQL + "Age,MaritalStatus,DOJ,DeptName,Designation,PayPeriod_Desc,";
                SQL = SQL + "BaseSalary,PFNo,Nominee,ESINo,StdWrkHrs,OTEligible,Nationality,";
                SQL = SQL + "Qualification,Certificate,FamilyDetails,RecuritmentThro,";
                SQL = SQL + "IDMark1,IDMark2,BloodGroup,Handicapped,Height,Weight,";
                SQL = SQL + "Address1,Address2,BankName,BranchCode,AccountNo,";
                SQL = SQL + "EmpStatus,IsActive,Created_By,Created_Date,IsNonAdmin,";
                SQL = SQL + "MachineID_Encrypt,Working_Hours,Calculate_Work_Hours,";
                SQL = SQL + "OT_Hours,Wages,RecutersMob,parentsMobile,ParentsPhone,";
                SQL = SQL + "EmployeeMobile,SamepresentAddress,Religion,EmpLeft,BusNo,";
                SQL = SQL + "Refrence,Permanent_Dist,Permanent_Taluk,Present_Dist,";
                SQL = SQL + "Present_Taluk,WeekOff,Adolescent,Adolescent_Status,";
                SQL = SQL + "ESICode,PFDOJ,ESIDOJ,IFSC_Code,Eligible_PF,Eligible_ESI,";
                SQL = SQL + "BusRoute,RoomNo,BasicSalary,Deduction1amt,Deduction2amt,";
                SQL = SQL + "Allowance1amt,Allowance2amt,PFS,Mother,SalaryThro,";
                SQL = SQL + "BrokerName,BrokerType,BrokerAgent,Community,Caste,";
                SQL = SQL + "PhysicalRemarks,RejoinDate,RelieveDate,FatherName,";
                SQL = SQL + "VPFSal,Others1,Others2,OTSal,IncentAmt,GuardianName,";
                SQL = SQL + "State,DocType,DocNo,DocDesc,";
                SQL = SQL + "ConvenyAmt,SplAmt,Grade,WageCategoty,WorkCode,EligibleCateen,CanteenName,CanteenOperator,";
                SQL = SQL + "LabourAmt,CanManageAmt,EligibleSnacks,SnacksOperator,SnacksAmt,EligibleIncentive,LeaveFrom1,";
                SQL = SQL + "LeaveTo1,LeaveFrom2,LeaveTo2,LevDesign1,LevDesign2,Residance";

                SQL = SQL + ") ";
                SQL = SQL + "Select CompCode,LocCode,ShiftType,TypeName,EmpPrefix,";
                SQL = SQL + "EmpNo,CatName,SubCatName,ExistingCode,MachineID,";
                SQL = SQL + "FirstName,LastName,MiddleInitial,Gender,BirthDate,";
                SQL = SQL + "Age,MaritalStatus,DOJ,DeptName,Designation,PayPeriod_Desc,";
                SQL = SQL + "BaseSalary,PFNo,Nominee,ESINo,StdWrkHrs,OTEligible,Nationality,";
                SQL = SQL + "Qualification,Certificate,FamilyDetails,RecuritmentThro,";
                SQL = SQL + "IDMark1,IDMark2,BloodGroup,Handicapped,Height,Weight,";
                SQL = SQL + "Address1,Address2,BankName,BranchCode,AccountNo,";
                SQL = SQL + "EmpStatus,IsActive,Created_By,Created_Date,IsNonAdmin,";
                SQL = SQL + "MachineID_Encrypt,Working_Hours,Calculate_Work_Hours,";
                SQL = SQL + "OT_Hours,Wages,RecutersMob,parentsMobile,ParentsPhone,";
                SQL = SQL + "EmployeeMobile,SamepresentAddress,Religion,EmpLeft,BusNo,";
                SQL = SQL + "Refrence,Permanent_Dist,Permanent_Taluk,Present_Dist,";
                SQL = SQL + "Present_Taluk,WeekOff,Adolescent,Adolescent_Status,";
                SQL = SQL + "ESICode,PFDOJ,ESIDOJ,IFSC_Code,Eligible_PF,Eligible_ESI,";
                SQL = SQL + "BusRoute,RoomNo,BasicSalary,Deduction1amt,Deduction2amt,";
                SQL = SQL + "Allowance1amt,Allowance2amt,PFS,Mother,SalaryThro,";
                SQL = SQL + "BrokerName,BrokerType,BrokerAgent,Community,Caste,";
                SQL = SQL + "PhysicalRemarks,RejoinDate,RelieveDate,FatherName,";
                SQL = SQL + "VPFSal,Others1,Others2,OTSal,IncentAmt,GuardianName,";
                SQL = SQL + "State,DocType,DocNo,DocDesc";
                SQL = SQL + " from Employee_Approval where EmpNo='" + ID + "'";
                MasterDate.GetDetails(SQL);

                SQL = "Update Employee_Approval set Status='1' where EmpNo='" + ID + "'";
                MasterDate.GetDetails(SQL);



                message = "Approved";
            }

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetPendingEmployee(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";

            string SQL = "Select *from Employee_Approval where EmpNo='" + ID + "'";
            SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            dt = MasterDate.GetDetails(SQL);


            if (dt.Rows.Count != 0)
            {

                SQL = "Update Employee_Approval set Status='2' where EmpNo='" + ID + "'";
                SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                MasterDate.GetDetails(SQL);

                message = "Pending";
            }

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetCancelEmployee(string ID)
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            DataTable dt = new DataTable();
            string message = "";

            string SQL = "Select *from Employee_Approval where EmpNo='" + ID + "'";
            SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            dt = MasterDate.GetDetails(SQL);


            if (dt.Rows.Count != 0)
            {

                SQL = "Update Employee_Approval set Status='3' where EmpNo='" + ID + "'";
                SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                MasterDate.GetDetails(SQL);

                message = "Cancel";
            }

            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        private static string UTF8Encryption(string password)
        {
            string strmsg = string.Empty;
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            strmsg = Convert.ToBase64String(encode);
            return strmsg;
        }



    }
}