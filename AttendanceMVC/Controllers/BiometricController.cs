﻿using AttendanceMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AttendanceMVC.Controllers
{
    public class BiometricController : Controller
    {
        static string SessionCcode = "";
        static string SessionLcode = "";
        static string SessionUserName;
        static string SessionUserID;

        public zkemkeeper.CZKEMClass axCZKEM1 = new zkemkeeper.CZKEMClass();
        string[] Time_Minus_Value_Check;

        private int iMachineNumber = 1;
        private bool bIsConnected = false;



        string[] Time_Split_Str;
        string Time_IN_Get;
        string Time_Out_Update;
        Double Totol_Hours_Check = 0;
        int Random_No_Get;
        int Random_No_Fixed;
        Double Totol_Hours_Check_1 = 8.2;
        string Totaltime8above = "";


        string IPAddress;
        string IPMde;
        SqlConnection con;
        string Status;
        string LocCode;
        bool Errflag;
        bool Berrflag;
        bool Connect = false;
        bool Check_Download_Clear_Error = false;
        DataTable mLocalDS = new DataTable();

        // GET: Biometric
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Download()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult DeleteMachineID()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult UpdateMachineID()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public JsonResult GetUserID()
        {
            string Status = SessionUserID;

            return Json(Status, JsonRequestBehavior.AllowGet);
        }
        public JsonResult IDConvert()
        {
            string Status = "";
            string SSQL = "";
            SSQL = "";
            SSQL = "Select * from Employee_mst where Compcode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            DataTable dt = new DataTable();
            dt = MasterDate.GetDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SSQL = "Update Employee_Mst set MachineID_Encrypt='" + UTF8Encryption(dt.Rows[i]["MachineID"].ToString()) + "' where CompCode='" + SessionCcode + "' and LocCOde='" + SessionLcode + "'";
                    SSQL = SSQL + " and MachineID='"+ dt.Rows[i]["MachineID"].ToString() + "'";
                    MasterDate.GetDetails(SSQL);
                    Status = "ID CONVERTION COMPLETED";
                }
            }else
            {
                Status = "Error on ID Convert";
            }
            return Json(Status, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLastDate()
        {
            //var Dept = db_Entity.Department_Mst.ToList();
            string Status = "";

            DataTable dt = new DataTable();

            string SQL = "Select distinct Attn_Date_Str,convert(varchar(10),Attn_Date,103) as LastDate,Attn_Date";
            SQL = SQL + " from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' order by Attn_Date Desc";
            dt = MasterDate.GetDetails(SQL);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Status = dt.Rows[0]["LastDate"].ToString();
            }

            return Json(Status, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetIPAddress()
        {
            //var Dept = db_Entity.Department_Mst.ToList();


            DataTable dt = new DataTable();

            string SQL = "Select *from IPAddress_Mst  ";
            SQL = SQL + "where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["IPAddress"].ToString();
                List.Name = dt.Rows[i]["IPAddress"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DownloadDataWithoutManulAttend(string IPAddress)
        {
            string SSQL;
            DataTable Emp_DS = new DataTable();

            //SSQL = "Delete From LogTime_Days where Compcode='" + Ccode + "' And LocCode='" + Lcode + "'";
            //Emp_DS = MasterDate.GetDetails(SSQL);

            Errflag = true;

            if (bIsConnected == false)
            {
                //MessageBox.Show("Please connect the device first", "Error");
                //return
            }

            //string Attn_Date_Str = "";
            //string Attn_Date_Str1 = "";
            string sdwEnrollNumber = "";
            int idwVerifyMode = 0;
            int idwInOutMode = 0;
            int idwYear = 0;
            int idwMonth = 0;
            int idwDay = 0;
            int idwHour = 0;
            int idwMinute = 0;
            int idwSecond = 0;
            int idwWorkcode = 0;
            int idwErrorCode = 0;

            Status = "";

            //string[] Array = IpMode.Split('/');

            //IPAddress = "";
            //IPMde = "";

            //IPAddress = Array[0].ToString();
            //IPMde = Array[1].ToString();


            if (IPAddress == "")
            {
                // MessageBox.Show("IP and Port cannot be null", "Error");
                Json(Status, JsonRequestBehavior.AllowGet);
            }

            idwErrorCode = 0;


            int port = 4370;


            if (Errflag == true)
            {
                Status = "DOWNLOAD PROCESSING.......";

                Errflag = true;
            }



            bIsConnected = axCZKEM1.Connect_Net(IPAddress, Convert.ToInt32(port));

            if (bIsConnected == true)
            {

                iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                axCZKEM1.RegEvent(iMachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                // MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");

                Status = "Not Connected";
                Connect = true;


            }

            string Mode = "";

            string Attn_Date_Str;

            DataTable CheckManAttend = new DataTable();

            DataTable TBDataset = new DataTable();

            string MachineNo = "";
            axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
            if (axCZKEM1.ReadGeneralLogData(iMachineNumber))//read all the attendance records to the memory
            {
                Attn_Date_Str = "";

                while (axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, out sdwEnrollNumber, out idwVerifyMode,
                           out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
                {
                    if (sdwEnrollNumber == "2005")
                    {
                        string SS = "Sathish";
                    }

                    Mode = "";

                    DataTable IP_Da = new DataTable();
                    string IP_check = "select IPMode from IPAddress_Mst where IpAddress='" + IPAddress + "' and  CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";

                    IP_Da = MasterDate.GetDetails(IP_check);

                    if (IP_Da.Rows.Count == 0)
                    {
                    }
                    else
                    {
                        Mode = IP_Da.Rows[0]["IPMode"].ToString();
                    }

                    DateTime date = new DateTime();
                    date = Convert.ToDateTime(idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString() + ":" + idwSecond.ToString());
                    string Query = "";
                    string Chck_Date = idwYear.ToString() + "/" + idwMonth.ToString() + "/" + idwDay.ToString();
                    string Chck_Date1 = idwDay.ToString() + "/" + idwMonth.ToString() + "/" + idwYear.ToString();

                    //Encry Code
                    string strmsg = string.Empty;
                    byte[] encode = new byte[sdwEnrollNumber.Length];
                    encode = Encoding.UTF8.GetBytes(sdwEnrollNumber);
                    sdwEnrollNumber = Convert.ToBase64String(encode);

                    MachineNo = UTF8Decryption(sdwEnrollNumber);

                    //Query = "select * from ManAttn_Details where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                    //if (sdwEnrollNumber != "")
                    //{
                    //    Query = Query + " And Machine_No='" + MachineNo + "'";
                    //}
                    //if (Chck_Date != "")
                    //{
                    //    Query = Query + " And Convert(datetime,AttnDateStr,103)= Convert(datetime,'" + Chck_Date1 + "',103)";
                    //}

                    //CheckManAttend = MasterDate.GetDetails(Query);


                    if (Mode == "IN")
                    {
                        Query = "select * from ManAttn_Details where CompCode='" + SessionCcode + "' and Machine_No='" + MachineNo + "' And LocCode='" + SessionLcode + "' ";
                        Query = Query + " And LogTimeIn >='" + Chck_Date1 + " " + "00:00' And LogTimeIn <='" + Chck_Date1 + " " + "23:59' ";
                        CheckManAttend = MasterDate.GetDetails(Query);
                    }
                    else
                    {
                        Query = "select * from ManAttn_Details where CompCode='" + SessionCcode + "' and Machine_No='" + MachineNo + "' And LocCode='" + SessionLcode + "' ";
                        Query = Query + " And LogTimeOut >='" + Chck_Date1 + " " + "00:00' And LogTimeOut <='" + Chck_Date1 + " " + "23:59' ";
                        CheckManAttend = MasterDate.GetDetails(Query);
                    }


                    if (CheckManAttend.Rows.Count == 0)
                    {

                        Query = "select * from ManAttn_Details where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                        if (sdwEnrollNumber != "")
                        {
                            Query = Query + " And Machine_No='" + MachineNo + "'";
                        }
                        if (Chck_Date != "")
                        {
                            Query = Query + " And Convert(datetime,AttnDateStr,103)= Convert(datetime,'" + Chck_Date1 + "',103)";
                        }

                        CheckManAttend = MasterDate.GetDetails(Query);
                    }



                    DataTable DT_Emp = new DataTable();
                   DataTable DT_TempEmp = new DataTable();
                   Query = "Select *from Employee_Mst where MachineID='" + MachineNo + "'";
                    Query = Query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    DT_Emp= MasterDate.GetDetails(Query);
                    if(DT_Emp.Rows.Count==0)
                    {
                        Query = "Select *from Temp_Employee_Mst where EmpNo='" + MachineNo + "'";
                        Query = Query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        DT_TempEmp = MasterDate.GetDetails(Query);
                        if(DT_TempEmp.Rows.Count==0)
                        {
                            Query = "insert into Temp_Employee_Mst(CompCode,LocCode,EmpNo)values('" + SessionCcode + "','" + SessionLcode + "','" + MachineNo + "')";
                            MasterDate.GetDetails(Query);
                        }
                     }







                    if (CheckManAttend.Rows.Count == 0)
                    {
                        if (Mode == "IN")
                        {
                            Query = "delete from LogTime_IN where MachineID='" + sdwEnrollNumber + "' and TimeIN='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                            MasterDate.GetDetails(Query);
                            //SqlCommand cmd_del = new SqlCommand(Query, con);
                            //con.Open();
                            //cmd_del.ExecuteNonQuery();
                            //con.Close();

                            Query = "insert into LogTime_IN(CompCode,LocCode,IPAddress,MachineID,TimeIN)values('" + SessionCcode + "','" + SessionLcode + "','" + IPAddress + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                            MasterDate.GetDetails(Query);
                            //SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                            //con.Open();
                            //cmd_upd1.ExecuteNonQuery();
                            //con.Close();

                            //DataTable Emp_DT = new DataTable();
                            //DataTable mDataSet = new DataTable();
                            //string Queryss = "";
                            //Queryss = "Select * from LogTime_Days where MachineID='" + UTF8Decryption(sdwEnrollNumber) + "' and  Attn_Date_Str='" + Convert.ToDateTime(date).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                            //mDataSet = ManualAttend.GetDetails(Queryss);
                            //if (mDataSet.Rows.Count == 0)
                            //{

                            //    SSQL = "Select * from Employee_Mst where  MachineID_Encrypt='" + sdwEnrollNumber + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' ";
                            //    Emp_DT = ManualAttend.GetDetails(SSQL);

                            //    if (Emp_DT.Rows.Count != 0)
                            //    {
                            //        Queryss = "";
                            //        Queryss = "Insert Into LogTime_Days(CompCode,LocCode,MachineID,ExistingCode,FirstName,LastName,DeptName," +
                            //        "Designation,DOJ,Wages,CatName,SubCatName,BasicSalary,TimeIN,Attn_Date_Str,Attn_Date,MachineID_Encrypt)" +
                            //         " Values('" + SessionCcode + "','" + SessionLcode + "','" + Emp_DT.Rows[0]["MachineID"] + "','" + Emp_DT.Rows[0]["ExistingCode"] + "'," +
                            //        "'" + Emp_DT.Rows[0]["FirstName"] + "','" + Emp_DT.Rows[0]["MiddleInitial"] + "'," +
                            //         "'" + Emp_DT.Rows[0]["DeptName"] + "','" + Emp_DT.Rows[0]["Designation"] + "'," +
                            //         "'" + Emp_DT.Rows[0]["DOJ"] + "','" + Emp_DT.Rows[0]["Wages"] + "','" + Emp_DT.Rows[0]["CatName"] + "','" + Emp_DT.Rows[0]["SubCatName"] + "','" + Emp_DT.Rows[0]["BasicSalary"] + "'," +
                            //         "'" + date.ToString("HH:mm:ss") + "','" + Convert.ToDateTime(date).AddDays(0).ToString("yyyy/MM/dd") + "',Convert(datetime,'" + Convert.ToDateTime(date).AddDays(0).ToString("yyyy/MM/dd") + "',120),'" + sdwEnrollNumber + "')";

                            //        ManualAttend.GetDetails(Queryss);
                            //    }

                            //}

                        }

                        if (Mode == "OUT")
                        {
                            Query = "delete from LogTime_OUT where MachineID='" + sdwEnrollNumber + "' and TimeOUT='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                            MasterDate.GetDetails(Query);
                            //SqlCommand cmd_del = new SqlCommand(Query, con);
                            //con.Open();
                            //cmd_del.ExecuteNonQuery();
                            //con.Close();

                            Query = "insert into LogTime_OUT(CompCode,LocCode,IPAddress,MachineID,TimeOUT)values('" + SessionCcode + "','" + SessionLcode + "','" + IPAddress + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                            MasterDate.GetDetails(Query);
                            //SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                            //con.Open();
                            //cmd_upd1.ExecuteNonQuery();
                            //con.Close();


                            //DataTable Emp_DT = new DataTable();
                            //DataTable mDataSet = new DataTable();
                            //string Queryss = "";
                            //Queryss = "Select * from LogTime_Days where MachineID='" + UTF8Decryption(sdwEnrollNumber) + "' and  Attn_Date_Str='" + Convert.ToDateTime(date).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                            //mDataSet = ManualAttend.GetDetails(Queryss);
                            //if (mDataSet.Rows.Count != 0)
                            //{

                            //    string Date = "";

                            //    Date = date.ToString("yyyy/MM/dd");

                            //    //if (idwHour >= 0 && idwHour <= 10)
                            //    //{
                            //    //    Attn_Date_Str = String.Format("{0:yyyy/MM/dd}", Date);

                            //    //    DateTime dt = Convert.ToDateTime(Attn_Date_Str).AddDays(-1);

                            //    //    Attn_Date_Str = String.Format(dt.ToString("yyyy/MM/dd"));
                            //    //}
                            //    //else
                            //    //{
                            //        Attn_Date_Str = date.ToString("yyyy/MM/dd");
                            //    //}


                            //    if (Attn_Date_Str != "")
                            //    {
                            //        Get_Working_Days_Save_DB(sdwEnrollNumber, Attn_Date_Str);
                            //    }


                            //}
                            //else
                            //{

                            //    SSQL = "Select * from Employee_Mst where  MachineID_Encrypt='" + sdwEnrollNumber + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' ";
                            //    Emp_DT = ManualAttend.GetDetails(SSQL);

                            //    if (Emp_DT.Rows.Count != 0)
                            //    {
                            //        Queryss = "";
                            //        Queryss = "Insert Into LogTime_Days(CompCode,LocCode,MachineID,ExistingCode,FirstName,LastName,DeptName," +
                            //        "Designation,DOJ,Wages,CatName,SubCatName,BasicSalary,TimeOUT,Attn_Date_Str,Attn_Date,MachineID_Encrypt)" +
                            //         " Values('" + SessionCcode + "','" + SessionLcode + "','" + Emp_DT.Rows[0]["MachineID"] + "','" + Emp_DT.Rows[0]["ExistingCode"] + "'," +
                            //        "'" + Emp_DT.Rows[0]["FirstName"] + "','" + Emp_DT.Rows[0]["MiddleInitial"] + "'," +
                            //         "'" + Emp_DT.Rows[0]["DeptName"] + "','" + Emp_DT.Rows[0]["Designation"] + "'," +
                            //         "'" + Emp_DT.Rows[0]["DOJ"] + "','" + Emp_DT.Rows[0]["Wages"] + "','" + Emp_DT.Rows[0]["CatName"] + "','" + Emp_DT.Rows[0]["SubCatName"] + "','" + Emp_DT.Rows[0]["BasicSalary"] + "'," +
                            //         "'" + date.ToString("HH:mm:ss") + "','" + Convert.ToDateTime(date).AddDays(0).ToString("yyyy/MM/dd") + "',Convert(datetime,'" + Convert.ToDateTime(date).AddDays(0).ToString("yyyy/MM/dd") + "',120),'" + sdwEnrollNumber + "')";

                            //        ManualAttend.GetDetails(Queryss);
                            //    }

                            //}



                        }
                        if (Mode == "LUNCH")
                        {
                            Query = "delete from LogTime_Lunch where MachineID='" + sdwEnrollNumber + "' and TimeIN='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                            TBDataset = MasterDate.GetDetails(Query);
                            //SqlCommand cmd_del = new SqlCommand(Query, con);
                            //con.Open();
                            //cmd_del.ExecuteNonQuery();
                            //con.Close();

                            Query = "insert into LogTime_Lunch(CompCode,LocCode,IPAddress,MachineID,TimeIN)values('" + SessionCcode + "','" + SessionLcode + "','" + IPAddress + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                            TBDataset = MasterDate.GetDetails(Query);
                            //SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                            //con.Open();
                            //cmd_upd1.ExecuteNonQuery();
                            //con.Close();


                        }


                    }
                    else
                    {
                        //Skip
                    }

                    //if (Mode == "OUT")
                    //{
                    //    string Date = "";

                    //    Date = idwDay + "/" + idwMonth + "/" + idwYear;

                    //    if (idwHour >= 0 && idwHour <= 10)
                    //    {
                    //        Attn_Date_Str = String.Format("{0:dd/MM/yyyy}", Date);

                    //        DateTime dt = Convert.ToDateTime(Attn_Date_Str).AddDays(-1);

                    //        Attn_Date_Str = String.Format(dt.ToString("dd/MM/yyyy"));
                    //    }
                    //    else
                    //    {
                    //        Attn_Date_Str = String.Format("{0:dd/MM/yyyy}", Date);
                    //    }


                    //    if (Attn_Date_Str != "")
                    //    {
                    //        Get_Working_Days_Save_DB(sdwEnrollNumber, Attn_Date_Str);
                    //    }

                    //}

                }
            }
            else
            {
                Check_Download_Clear_Error = true;
                // Cursor = Cursors.Default;
                axCZKEM1.GetLastError(ref idwErrorCode);
                if (idwErrorCode != 0)
                {
                    //MessageBox.Show("Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString(), "Error");
                }
                else
                {
                    //MessageBox.Show("No data from terminal returns!", "Error");
                }
            }
            axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device

            if (Errflag == true && Connect == false)
            {

                Status = "DOWNLOAD COMPLETED";
                Errflag = true;
            }
            else
            {
                Check_Download_Clear_Error = true;
                //lblDwnCmpltd.Text = "Machine Can't Ping";
                Status = "Not Connected";
                Errflag = true;
            }

            //SSQL = "";           

            //if (Mode == "IN" || Mode == "OUT")
            //{

            //    SSQL = "Delete From LogTime_Days where Compcode='" + Ccode + "' And LocCode='" + Lcode + "'";
            //    Emp_DS = MasterDate.GetDetails(SSQL);

            //    SSQL = "Select * from Employee_Mst where Compcode='" + Ccode +"' And LocCode='"+ Lcode +"' And IsActive='Yes'";
            //    Emp_DS = MasterDate.GetDetails(SSQL);

            //    for(int i = 0; i < Emp_DS.Rows.Count;i++)
            //    {
            //        string Machine_ID = Emp_DS.Rows[i]["MachineID"].ToString();

            //        Get_Working_Days_Save_DB(Machine_ID , Date);

            //    }




            //}





            return Json(Status, JsonRequestBehavior.AllowGet);
        }

        private static string UTF8Encryption(string password)
        {
            string strmsg = string.Empty;
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            strmsg = Convert.ToBase64String(encode);
            return strmsg;
        }

        private static string UTF8Decryption(string encryptpwd)
        {
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }

        public int GetRandom(int min, int max)
        {
            Random random = new Random(); return random.Next(min, max);
        }
        public string Left_Val(string Value, int Length)
        {

            if (Value.Length >= Length)
            {
                return Value.Substring(0, Length);
            }
            else
            {
                return Value;
            }
        }

        public string Right_Val(string Value, int Length)
        {

            int i = 0;
            i = 0;
            if (Value.Length >= Length)
            {
                //i = Value.Length - Length
                return Value.Substring(Value.Length - Length, Length);
            }
            else
            {
                return Value;
            }
        }
        public JsonResult GetWorkingDaysSaveDB(string FromDate, string ToDate)
        {

            Status = "";
            string[] iStr1;
            string[] iStr2;
            int intI;
            int intK;
            int intCol;
            string Fin_Year = "";
            string Months_Full_Str = "";
            string Date_Col_Str = "";
            int Month_Int = 1;
            string Spin_Machine_ID_Str = "";

            string[] Time_Minus_Value_Check;

            DataTable mLocalDS = new DataTable();

            DataTable mLocalDS1 = new DataTable();

            //Attn_Flex Col Add Var
            string[] Att_Date_Check;
            string Att_Already_Date_Check;
            string Att_Year_Check = "";
            int Month_Name_Change;
            string halfPresent = "0";
            int EPay_Total_Days_Get = 0;
            intCol = 4;
            Month_Name_Change = 1;
            int dayCount = 1;
            int daysAdded = 1;

            EPay_Total_Days_Get = 1;

            intI = 2;
            intK = 1;

            // string EmpNo = MachineNo;

            //string MachineID = UTF8Encryption(EmpNo);

            string SSQL = "";
            bool ErrFlag = false;

            DataTable Emp_DS = new DataTable();

            SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And IsActive='Yes'";
            Emp_DS = ManualAttend.GetDetails(SSQL);

            if (Emp_DS.Rows.Count > 0)
            {
                ErrFlag = false;
            }
            else
            {
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                //DataTable DT_Chk = new DataTable();
                //SSQL = "Select * from LogTime_Days where Attn_Date_Str='" + Convert.ToDateTime(Date).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                //DT_Chk = ManualAttend.GetDetails(SSQL);
                //if (DT_Chk.Rows.Count > 0)
                //{
                //    ErrFlag = true;
                //}
                //else
                //{
                //    ErrFlag = false;
                //}
            }

            if (!ErrFlag)
            {
                for (int intRow = 0; intRow < Emp_DS.Rows.Count; intRow++)
                {

                    intK = 1;
                    string Emp_Total_Work_Time_1 = "00:00";
                    //Get Employee Week OF DAY
                    DataTable DS_WH = new DataTable();
                    string Emp_WH_Day;
                    string DOJ_Date_Str;
                    string MachineID1 = Emp_DS.Rows[intRow]["MachineID"].ToString();

                    string Query = " Select * from Employee_Mst where MachineID='" + MachineID1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' ";
                    //SSQL = "Select * from Employee_MST where MachineID='" + MachineID1 + "' And Compcode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'" +
                    DS_WH = ManualAttend.GetDetails(Query);
                    if (DS_WH.Rows.Count != 0)
                    {
                        Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                        DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();
                    }
                    else
                    {
                        Emp_WH_Day = "";
                        DOJ_Date_Str = "";
                    }
                    int colIndex = intK;
                    string DayofWeekName = "";
                    decimal Weekoff_Count = 0;
                    bool isPresent = false;
                    bool isWHPresent = false;
                    decimal Present_Count = 0;
                    decimal Present_WH_Count = 0;
                    int Appsent_Count = 0;
                    decimal Final_Count = 0;
                    decimal Total_Days_Count = 0;
                    string Already_Date_Check;
                    int Days_Insert_RowVal = 0;
                    decimal FullNight_Shift_Count = 0;
                    int NFH_Days_Count = 0;
                    decimal NFH_Days_Present_Count = 0;
                    Already_Date_Check = "";
                    string Year_Check = "";
                    string Time_IN_Str = "";
                    string Time_Out_Str = "";
                    int g = 1;

                    double OT_Time;

                    OT_Time = 0;

                    string INTime = "";
                    string OUTTime = "";

                    DateTime Date11 = Convert.ToDateTime(FromDate);
                    string dat = ToDate;
                    DateTime Date12 = Convert.ToDateTime(dat);

                    int daycount = (int)((Date12 - Date11).TotalDays);

                    daysAdded = daycount + 1;

                    for (intCol = 0; (intCol <= (daysAdded - 1)); intCol++)
                    {
                        isPresent = false;
                        isWHPresent = false;
                        //string Emp_Total_Work_Time_1 = "00:00";
                        string Machine_ID_Str = "";
                        string OT_Week_OFF_Machine_No;
                        string Date_Value_Str = "";
                        string Total_Time_get = "";
                        string Final_OT_Work_Time_1 = "";
                        //DataSet mLocalDS = new DataSet();

                        //DataTable mLocalDS = new DataTable();


                        int j = 0;
                        double time_Check_dbl = 0;
                        string Date_Value_Str1 = "";
                        string Employee_Shift_Name_DB = "No Shift";
                        isPresent = false;


                        //Shift Change Check Variable Declaration Start
                        DateTime InTime_Check = new DateTime();
                        DateTime InToTime_Check = new DateTime();
                        TimeSpan InTime_TimeSpan;
                        string From_Time_Str = "";
                        string To_Time_Str = "";
                        //DataSet DS_InTime = new DataSet();
                        DataTable DS_Time = new DataTable();
                        //DataSet DS_Time = new DataSet();
                        DataTable DS_InTime = new DataTable();
                        DateTime EmpdateIN_Change = new DateTime();
                        DataTable InsertData = new DataTable();
                        string Final_InTime = "";
                        string Final_OutTime = "";
                        string Final_Shift = "";

                        int K = 0;
                        bool Shift_Check_blb = false;
                        string Shift_Start_Time_Change;
                       
                        string Shift_End_Time_Change;
                        string Employee_Time_Change = "";
                        DateTime ShiftdateStartIN_Change = new DateTime();
                        DateTime ShiftdateEndIN_Change = new DateTime();

                        //Shift Change Check Variable Declaration End
                        string Employee_Punch = "";
                        Spin_Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID"].ToString();

                        if (Spin_Machine_ID_Str == "204")
                        {
                            Spin_Machine_ID_Str = "204";
                        }

                       

                        // Machine_ID_Str = Encryption(Emp_DS.Rows[intRow]["MachineID"]);
                        Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID_Encrypt"].ToString();
                        OT_Week_OFF_Machine_No = Emp_DS.Rows[intRow]["MachineID"].ToString();
                        Date_Value_Str = Convert.ToDateTime(FromDate).AddDays(intCol).ToShortDateString();
                        Date_Value_Str1 = Convert.ToDateTime(Date_Value_Str).AddDays(1).ToShortDateString();
                        DataTable da_Shift_Type = new DataTable();
                        SSQL = "select * from Employee_Mst where MachineID='"+ OT_Week_OFF_Machine_No + "'";
                        da_Shift_Type = ManualAttend.GetDetails(SSQL);
                        if (da_Shift_Type.Rows.Count != 0)
                        {
                            Final_Shift = da_Shift_Type.Rows[0]["ShiftType"].ToString();
                        }
                        

                        //DateTime dt = Convert.ToDateTime(Date.ToString());

                        //DayofWeekName = dt.DayOfWeek.ToString();

                        //TimeIN Get

                        SSQL = "";
                        SSQL = "Select distinct TimeIN,CONVERT(char(5), TimeIN, 108) as TimeIN24 from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                              " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";

                        mLocalDS = ManualAttend.GetDetails(SSQL);

                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";

                            INTime = "";

                        }
                        else
                        {
                            DateTime datetime1 = Convert.ToDateTime(mLocalDS.Rows[0][0].ToString());
                            INTime = datetime1.ToString("hh:mm tt");
                            //INTime = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0].ToString());
                        }

                        //TimeOUT Get
                        SSQL = "";
                        SSQL = "Select TimeOUT,CONVERT(char(5), TimeOUT, 108) as TimeOUT24 from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                               " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                        mLocalDS1 = ManualAttend.GetDetails(SSQL);

                        if (mLocalDS1.Rows.Count <= 0)
                        {
                            Time_Out_Str = "";

                            OUTTime = "";
                        }
                        else
                        {
                            DateTime datetime = Convert.ToDateTime(mLocalDS1.Rows[0][0].ToString());
                            OUTTime = datetime.ToString("hh:mm tt");
                            //OUTTime = string.Format(Convert.ToString(datetime), "HH:mm:ss tt");
                            //OUTTime = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0].ToString());
                        }

                        //Shift Change Check Start
                        if (mLocalDS.Rows.Count != 0)
                        {
                            InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
                            InToTime_Check = InTime_Check.AddHours(2);



                            InTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InTime_Check), "HH:mm:ss"));
                            InToTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InToTime_Check), "HH:mm:ss"));




                            //Old Time check error for 24 format

                            string TimeIN = InTime_Check.ToString();
                            string[] TimeIN_Split = TimeIN.Split(' ');
                            string TimeIN1 = TimeIN_Split[1].ToString();
                            string[] final = TimeIN1.Split(':');
                            string Final_IN = "";
                            if (Convert.ToInt32(final[0]) >= 10)
                            {
                                Final_IN = final[0] + ":" + final[1];
                            }
                            else
                            {
                                Final_IN = "0" + final[0] + ":" + final[1];
                            }


                            string TimeOUT = InToTime_Check.ToString();
                            string[] TimeOUT_Split = TimeOUT.Split(' ');
                            string TimeOUT1 = TimeOUT_Split[1].ToString();
                            string[] final1 = TimeOUT1.Split(':');
                            string Final_IN1 = "";
                            if (Convert.ToInt32(final1[0]) >= 10)
                            {
                                Final_IN1 = final1[0] + ":" + final1[1];
                            }
                            else
                            {
                                Final_IN1 = "0" + final1[0] + ":" + final1[1];
                            }




                            //New Format 24 change manually

                            //string InTime1 = mLocalDS.Rows[0]["TimeIN24"].ToString();

                            ////string TimeIN = InTime1.ToString();
                            ////string[] TimeIN_Split = TimeIN.Split(' ');
                            ////string TimeIN1 = TimeIN_Split[1].ToString();
                            //string[] final = InTime1.Split(':');
                            //string Final_IN = "";

                            //Final_IN = final[0] + ":" + final[1];




                            //string InTime2 = (Convert.ToInt16(final[0]) + 2).ToString();
                            //InTime2= InTime2+":"+ final[1];

                            ////string TimeOUT = InTime2.ToString();
                            ////string[] TimeOUT_Split = TimeOUT.Split(' ');
                            ////string TimeOUT1 = TimeOUT_Split[1].ToString();
                            //string[] final1 = InTime2.Split(':');
                            //string Final_IN1 = "";

                            //Final_IN1 = final1[0] + ":" + final1[1];



                            //InTime_TimeSpan = TimeSpan.Parse(InTime_Check).ToString();
                            //From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;

                            //Two Hours OutTime Check
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";


                            DS_Time = ManualAttend.GetDetails(SSQL);
                            if (DS_Time.Rows.Count != 0)
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                DS_InTime = ManualAttend.GetDetails(SSQL);
                                if (DS_InTime.Rows.Count != 0)
                                {
                                    //DataSet Shift_DS_Change = new DataSet();

                                    DataTable Shift_DS_Change = new DataTable();

                                    Final_InTime = mLocalDS.Rows[0]["TimeIN"].ToString();
                                    //Check With IN Time Shift
                                    SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%'";
                                    SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    Shift_DS_Change = ManualAttend.GetDetails(SSQL);
                                    Shift_Check_blb = false;
                                    for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
                                    {
                                        Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
                                        if (Shift_DS_Change.Rows[k]["EndIN_Days"].ToString() == "1")
                                        {
                                            Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                        }
                                        else
                                        {
                                            Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                        }

                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);

                                        if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                            Shift_Check_blb = true;
                                            break;
                                        }
                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                        EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                        if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                            Shift_Check_blb = true;
                                            break;
                                        }
                                    }
                                    if (Shift_Check_blb == true)
                                    {

                                        //IN Time Query Update
                                        string Querys = "";
                                        Querys = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                        " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                        mLocalDS = ManualAttend.GetDetails(Querys);

                                        if (Final_Shift == "SHIFT1")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                            " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        }
                                        else if (Final_Shift == "SHIFT2")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                                   " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        }
                                        else if (Final_Shift == "SHIFT3")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

                                        }
                                        else
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

                                        }


                                        mLocalDS1 = ManualAttend.GetDetails(Querys);
                                    }
                                    else
                                    {
                                        //Get Employee In Time
                                        SSQL = "";
                                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                               " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                        mLocalDS = ManualAttend.GetDetails(SSQL);
                                        //TimeOUT Get
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                               " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        mLocalDS1 = ManualAttend.GetDetails(SSQL);
                                    }
                                }
                                else
                                {
                                    //Get Employee In Time
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                           " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                    mLocalDS = ManualAttend.GetDetails(SSQL);

                                    //TimeOUT Get
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                          " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                    mLocalDS1 = ManualAttend.GetDetails(SSQL);

                                }
                            }
                            else
                            {
                                //Get Employee In Time
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                       " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                mLocalDS = ManualAttend.GetDetails(SSQL);

                                //TimeOUT Get
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                      " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                mLocalDS1 = ManualAttend.GetDetails(SSQL);
                            }
                        }




                        if(mLocalDS.Rows.Count != 0)
                        {
                            DataTable Shift_DS_Change = new DataTable();

                            Final_InTime = mLocalDS.Rows[0]["TimeIN"].ToString();
                            //Check With IN Time Shift
                            SSQL = "Select * from Shift_Mst";
                            SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            Shift_DS_Change = ManualAttend.GetDetails(SSQL);
                            Shift_Check_blb = false;
                            for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
                            {
                                Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[k]["StartIN"].ToString();
                                if (Shift_DS_Change.Rows[k]["EndIN_Days"].ToString() == "1")
                                {
                                    Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[k]["EndIN"].ToString();
                                }
                                else
                                {
                                    Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[k]["EndIN"].ToString();
                                }

                                ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);

                                if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                {
                                    if (Final_Shift == "GENERAL")
                                    {
                                        Final_Shift = "GENERAL";
                                        break;
                                    }
                                    else
                                    {
                                        Final_Shift = Shift_DS_Change.Rows[k]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                        break;
                                    }
                                }
                                ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                {
                                    if (Final_Shift == "GENERAL")
                                    {
                                        Final_Shift = "GENERAL";
                                        break;
                                    }
                                    else
                                    {
                                        Final_Shift = Shift_DS_Change.Rows[k]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                        break;
                                    }
                                }
                            }

                        }
                        //Shift Change Check End 
                        if (mLocalDS.Rows.Count == 0)
                        {
                            Employee_Punch = "";
                        }
                        else
                        {
                            Employee_Punch = mLocalDS.Rows[0][0].ToString();
                        }
                        if (mLocalDS.Rows.Count >= 1)
                        {
                            bool Errflg = false;
                            if(Final_Shift== "GENERAL")
                            {
                                Errflg = true;
                            }
                            if(Final_Shift== "SHIFT1")
                            {
                                Errflg = true;
                            }
                            if (Final_Shift == "SHIFT2")
                            {
                                Errflg = true;
                            }
                            if (Final_Shift == "SHIFT6")
                            {
                                Errflg = true;
                            }

                            for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                            {
                                if (Errflg == true)
                                {
                                    DataTable  da_Final = new DataTable();
                                    SSQL = "select StartTime,StartIN,EndIN  from GraceTime_Shift_Mst  where ShiftDesc='" + Final_Shift + "'";
                                    da_Final = ManualAttend.GetDetails(SSQL);
                                    if(da_Final.Rows.Count!=0)
                                    {
                                        EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                        Shift_Start_Time_Change = Date_Value_Str + " " + da_Final.Rows[0]["StartIN"].ToString();
                                        Shift_End_Time_Change= Date_Value_Str + " " + da_Final.Rows[0]["EndIN"].ToString();

                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);

                                       
                                        if(EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Time_IN_Str = Date_Value_Str + " " + da_Final.Rows[0]["StartTime"].ToString();
                                        }
                                        else
                                        {
                                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                        }

                                    }


                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS.Rows[tin][0].ToString();

                                }

                                
                                if (mLocalDS1.Rows.Count > tin)
                                {
                                    Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                                }
                                else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                                {
                                    Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                                }
                                else
                                {
                                    Time_Out_Str = "";
                                }
                                TimeSpan ts4 = new TimeSpan();
                                ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                                if (mLocalDS.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "";
                                }
                                else
                                {
                                    //Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                }
                                if (Time_IN_Str == "" || Time_Out_Str == "")
                                {
                                    time_Check_dbl = time_Check_dbl;
                                }
                                else
                                {


                                    DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                    DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                    TimeSpan ts = new TimeSpan();
                                    ts = date2.Subtract(date1);


                                    //& ":" & Trim(ts.Minutes)
                                    Total_Time_get = (ts.Hours).ToString();
                                    ts4 = ts4.Add(ts);

                                    //OT Time Get
                                    Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                    Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);

                                    if (Left_Val(ts4.Minutes.ToString(), 1) == "-" || Left_Val(ts4.Hours.ToString(), 1) == "-" || Emp_Total_Work_Time_1 == "0:-1")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                    }
                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {

                                        date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                        ts = date2.Subtract(date1);
                                        ts = date2.Subtract(date1);
                                        Total_Time_get = (ts.Hours).ToString();
                                        time_Check_dbl = double.Parse(Total_Time_get);
                                        Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes).ToString();
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:1" || Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }

                                    }
                                    else
                                    {
                                        time_Check_dbl = time_Check_dbl + double.Parse(Total_Time_get);
                                    }
                                }

                                break;

                            }//For End
                        }
                        else
                        {

                            bool Errflg = false;
                            if (Final_Shift == "GENERAL")
                            {
                                Errflg = true;
                            }
                            if (Final_Shift == "SHIFT1")
                            {
                                Errflg = true;
                            }
                            if (Final_Shift == "SHIFT2")
                            {
                                Errflg = true;
                            }

                            if (mLocalDS.Rows.Count != 0)
                            {
                                if (Errflg == true)
                                {
                                    DataTable da_Final = new DataTable();
                                    SSQL = "select StartTime,StartIN,EndIN  from GraceTime_Shift_Mst  where ShiftDesc='" + Final_Shift + "'";
                                    da_Final = ManualAttend.GetDetails(SSQL);
                                    if (da_Final.Rows.Count != 0)
                                    {
                                        Final_InTime = mLocalDS.Rows[0]["TimeIN"].ToString();
                                        EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                        Shift_Start_Time_Change = Date_Value_Str + " " + da_Final.Rows[0]["StartIN"].ToString();
                                        Shift_End_Time_Change = Date_Value_Str + " " + da_Final.Rows[0]["EndIN"].ToString();

                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);


                                        if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Time_IN_Str = Date_Value_Str + " " + da_Final.Rows[0]["StartTime"].ToString();
                                        }
                                        else
                                        {
                                            Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                                        }

                                    }


                                }
                                else
                                {
                                    if (mLocalDS.Rows.Count != 0)
                                    {
                                        Time_IN_Str = mLocalDS.Rows[0][0].ToString();

                                    }
                                    else
                                    {
                                        Time_IN_Str = "";
                                    }
                                }
                            }


                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;

                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                            }
                            for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
                            {
                                if (mLocalDS1.Rows.Count <= 0)
                                {
                                    Time_Out_Str = "";
                                }
                                else
                                {
                                    Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                                }

                                break;

                            }
                            //Emp_Total_Work_Time
                            if (Time_IN_Str == "" || Time_Out_Str == "")
                            {
                                time_Check_dbl = 0;
                            }
                            else
                            {
                                DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();
                                ts = date2.Subtract(date1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = (ts.Hours).ToString();
                                //OT Time Get
                                Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {

                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = ts.Hours.ToString();
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                    Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes);
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                        time_Check_dbl = 0;
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                        time_Check_dbl = 0;
                                    }
                                }
                                else
                                {
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                }
                            }
                        }
                        //Emp_Total_Work_Time

                        string StdWorkingHrs = "0";

                        if (Emp_DS.Rows[intRow]["StdWrkHrs"].ToString() != "" && Emp_DS.Rows[intRow]["StdWrkHrs"].ToString() != "0")
                        {
                            StdWorkingHrs = Emp_DS.Rows[intRow]["StdWrkHrs"].ToString();

                            string StdHrs = (Convert.ToDecimal(StdWorkingHrs) + Convert.ToDecimal(1)).ToString();

                            if (Convert.ToDouble(time_Check_dbl) >= Convert.ToDouble(StdHrs))
                            {
                                OT_Time = OT_Time + (time_Check_dbl - Convert.ToDouble(StdWorkingHrs));

                            }
                            else
                            {
                                OT_Time = 0;
                            }
                        }
                        else
                        {

                            if (time_Check_dbl >= 9)
                            {
                                OT_Time = OT_Time + (time_Check_dbl - 8);

                            }
                            else
                            {
                                OT_Time = 0;
                            }
                        }


                        string Emp_Total_Work_Time = "";
                        string Final_OT_Work_Time = "00:00";
                        string Final_OT_Work_Time_Val = "00:00";
                        Emp_Total_Work_Time = Emp_Total_Work_Time_1;
                        //Find Week OFF
                        string Employee_Week_Name = "";
                        string Assign_Week_Name = "";
                        mLocalDS = null;
                        Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                        Assign_Week_Name = Emp_WH_Day;

                        //SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "' and  Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "'";
                        //mLocalDS = ManualAttend.GetDetails(SSQL);
                        //if (mLocalDS.Rows.Count <= 0)
                        //{
                        //    Assign_Week_Name = "";
                        //}
                        //else
                        //{
                        //    //Assign_Week_Name = mLocalDS.Tables(0).Rows(0)(0)
                        //    Assign_Week_Name = "";
                        //}


                        Boolean NFH_Day_Check = false;
                        DateTime NFH_Date;
                        DateTime DOJ_Date_Format;
                        DateTime Date_Value = new DateTime();
                        DataTable DT_NFH = new DataTable();
                        Date_Value = Convert.ToDateTime(Date_Value_Str.ToString());
                        string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value.ToString("yyyy/MM/dd") + "'";
                        DT_NFH = ManualAttend.GetDetails(qry_nfh);

                        if (DT_NFH.Rows.Count != 0)
                        {
                            NFH_Day_Check = true;
                            NFH_Days_Count = NFH_Days_Count + 1;
                        }

                        if (Employee_Week_Name == Assign_Week_Name)
                        {
                            //Get Employee Work Time
                            double Calculate_Attd_Work_Time = 8;
                            double Calculate_Attd_Work_Time_half = 0;
                            Weekoff_Count = Weekoff_Count + 1;

                            Calculate_Attd_Work_Time_half = 4.0;

                            halfPresent = "0";//Half Days Calculate Start
                            if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            {
                                // Weekoff_Count = Weekoff_Count + 1;
                                isWHPresent = true;
                                halfPresent = "1";

                            }
                            //Half Days Calculate End
                            if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            {
                                //Weekoff_Count = Weekoff_Count + 1;
                                isWHPresent = true;
                                halfPresent = "0";

                            }
                        }
                        else if (Employee_Week_Name == Emp_WH_Day)
                        {
                            //Get Employee Work Time
                            double Calculate_Attd_Work_Time = 8;
                            double Calculate_Attd_Work_Time_half = 0;


                            Calculate_Attd_Work_Time_half = 4.0;

                            Weekoff_Count = Weekoff_Count + 1;

                            halfPresent = "0";//Half Days Calculate Start
                            if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            {
                                isWHPresent = true;
                                halfPresent = "1";
                                //Weekoff_Count = Weekoff_Count + 1;


                            }
                            //Half Days Calculate End
                            if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            {
                                isWHPresent = true;
                                halfPresent = "0";
                                //Weekoff_Count = Weekoff_Count + 1;


                            }

                        }
                        else
                        {
                            //Get Employee Work Time
                            double Calculate_Attd_Work_Time = 8;
                            double Calculate_Attd_Work_Time_half = 0;


                            Calculate_Attd_Work_Time_half = 4.0;

                            halfPresent = "0";//Half Days Calculate Start
                            if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            {
                                isPresent = true;
                                halfPresent = "1";
                            }
                            //Half Days Calculate End
                            if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            {
                                isPresent = true;
                                halfPresent = "0";
                            }
                        }
                        //Shift Check Code Start
                        //DataSet Shift_Ds = new DataSet();

                        DataTable Shift_Ds = new DataTable();

                        string Start_IN = "";
                        string End_In = "";
                        string Shift_Name_Store = "";
                        bool Shift_Check_blb_Check = false;
                        DateTime ShiftdateStartIN_Check = new DateTime();
                        DateTime ShiftdateEndIN_Check = new DateTime();
                        DateTime EmpdateIN_Check = new DateTime();
                        string Wages = "";
                        if (INTime != "")
                        {
                            Shift_Check_blb_Check = false;

                            string SS = "GENERAL";
                            Wages = Emp_DS.Rows[intRow]["ShiftType"].ToString();

                            if (Wages.ToUpper() == SS.ToUpper())
                            {
                                Employee_Shift_Name_DB = "GENERAL";
                            }
                            else
                            {
                                //Shift Master Check Code Start
                                SSQL = "Select * from Shift_Mst where  ShiftDesc <> 'GENERAL' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' Order by ShiftDesc Asc";
                                //  "CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'";
                                Shift_Ds = ManualAttend.GetDetails(SSQL);
                                if (Shift_Ds.Rows.Count != 0)
                                {
                                    SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%' ";
                                    SSQL = SSQL + "And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    //CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "' And 
                                    Shift_Ds = ManualAttend.GetDetails(SSQL);
                                    Shift_Check_blb = false;
                                    for (int f = 0; f < Shift_Ds.Rows.Count; f++)
                                    {

                                        int StartIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["StartIN_Days"]);
                                        int EndIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["EndIN_Days"]);
                                        Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(StartIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["StartIN"].ToString();
                                        End_In = Convert.ToDateTime(Date_Value_Str).AddDays(EndIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["EndIN"].ToString();
                                        ShiftdateStartIN_Check = System.Convert.ToDateTime(Start_IN);
                                        ShiftdateEndIN_Check = System.Convert.ToDateTime(End_In);
                                        EmpdateIN_Check = System.Convert.ToDateTime(Employee_Punch);
                                        if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                                        {
                                            Employee_Shift_Name_DB = Shift_Ds.Rows[f]["ShiftDesc"].ToString();
                                            Shift_Check_blb_Check = true;
                                        }

                                    }
                                    if (Shift_Check_blb_Check == false)
                                    {
                                        Employee_Shift_Name_DB = "No Shift";
                                    }

                                }
                                else
                                {
                                    Employee_Shift_Name_DB = "No Shift";
                                }
                                //Shift Master Check Code End  
                            }
                        }
                        else
                        {
                            Employee_Shift_Name_DB = "No Shift";
                        }
                        //Shift Check Code End


                        string ShiftType = "";
                        string Pre_Abs = "";
                        if (isPresent == true)
                        {

                            if (Employee_Shift_Name_DB != "No Shift")
                            {
                                ShiftType = "Proper";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }
                            else
                            {
                                if (Wages == "GENERAL")
                                {
                                    ShiftType = "GENERAL Mismatch";
                                }
                                else
                                {
                                    ShiftType = "Mismatch";
                                }

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }


                            if (halfPresent == "1")
                            {
                                Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_Count = Present_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
                                }
                            }
                            else if (halfPresent == "0")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                                }
                            }

                        }
                        else if (isWHPresent == true)
                        {
                            if (Employee_Shift_Name_DB != "No Shift")
                            {
                                ShiftType = "Proper";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }
                            else
                            {
                                if (Wages == "GENERAL")
                                {
                                    ShiftType = "GENERAL Mismatch";
                                }
                                else
                                {
                                    ShiftType = "Mismatch";
                                }

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }


                            }

                            if (halfPresent == "1")
                            {
                                Present_WH_Count = Convert.ToDecimal(Present_WH_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_WH_Count = Present_WH_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_Count = Present_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
                                }
                            }
                            else if (halfPresent == "0")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                                }
                            }
                        }
                        else
                        {
                            Appsent_Count = Appsent_Count + 1;


                            if (INTime == "" && OUTTime == "")
                            {
                                ShiftType = "Leave";
                                Employee_Shift_Name_DB = "Leave";
                                Pre_Abs = "Leave";

                            }
                            else if (INTime == "")
                            {
                                if (OUTTime != "")
                                {
                                    ShiftType = "Improper";
                                }
                                Pre_Abs = "Absent";

                            }
                            else if (OUTTime == "")
                            {
                                if (INTime != "")
                                {
                                    ShiftType = "Improper";
                                }
                                Pre_Abs = "Absent";

                            }
                            else if (INTime != "" && OUTTime != "")
                            {
                                ShiftType = "Absent";
                                Pre_Abs = "Absent";

                            }


                        }

                        intK += 1;




                        //Update Employee Worked_Days
                        //DataSet mDataSet = new DataSet();
                        DataTable mDataSet = new DataTable();
                        string Queryss = "";
                        DataSet Del_ds = new DataSet();
                        Queryss = "Select * from LogTime_Days where MachineID='" + MachineID1 + "' and  Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";

                        mDataSet = ManualAttend.GetDetails(Queryss);

                        //mDataSet = objdata.read(Queryss);  Emp_WH_Day
                        if (mDataSet.Rows.Count != 0)
                        {
                            Queryss = "Delete from LogTime_Days where MachineID='" + MachineID1 + "' and Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                            ManualAttend.GetDetails(Queryss);
                        }

                        DataSet Insert_Ds = new DataSet();

                        Queryss = "";
                        Queryss = "Insert Into LogTime_Days(CompCode,LocCode,MachineID,ExistingCode,FirstName,LastName,DeptName," +
                        "Designation,DOJ,Wages,CatName,SubCatName,Grade,BasicSalary,Present,Wh_Check,Wh_Count,Wh_Present_Count,NFH_Count,NFH_Present_Count,Shift,TimeIN,TimeOUT,Total_Hrs,OTHours,Attn_Date_Str,Attn_Date,MachineID_Encrypt,Total_Hrs1,TypeName,Present_Absent)" +
                         " Values('" + SessionCcode + "','" + SessionLcode + "','" + Emp_DS.Rows[intRow]["MachineID"] + "','" + Emp_DS.Rows[intRow]["ExistingCode"] + "'," +
                        "'" + Emp_DS.Rows[intRow]["FirstName"] + "','" + Emp_DS.Rows[intRow]["MiddleInitial"] + "'," +
                         "'" + Emp_DS.Rows[intRow]["DeptName"] + "','" + Emp_DS.Rows[intRow]["Designation"] + "'," +
                         "'" + Emp_DS.Rows[intRow]["DOJ"] + "','" + Emp_DS.Rows[intRow]["Wages"] + "','" + Emp_DS.Rows[intRow]["CatName"] + "','" + Emp_DS.Rows[intRow]["SubCatName"] + "','" + Emp_DS.Rows[intRow]["Grade"].ToString() + "','" + Emp_DS.Rows[intRow]["BasicSalary"] + "','" + Present_Count + "'," +
                         "'" + Emp_WH_Day + "','" + Weekoff_Count + "','" + Present_WH_Count + "','" + NFH_Days_Count + "','" + NFH_Days_Present_Count + "'," +
                         "'" + Employee_Shift_Name_DB + "','" + INTime + "','" + OUTTime + "','" + time_Check_dbl + "','" + OT_Time + "','" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "',Convert(datetime,'" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "',120),'" + UTF8Encryption(MachineID1) + "','" + Emp_Total_Work_Time_1 + "','" + ShiftType + "','" + Pre_Abs + "')";

                        ManualAttend.GetDetails(Queryss);

                        Present_Count = 0;
                        Present_WH_Count = 0;
                        Employee_Shift_Name_DB = "No Shift";
                        INTime = "";
                        OUTTime = "";
                        time_Check_dbl = 0;
                        OT_Time = 0;
                        Weekoff_Count = 0;
                        //Emp_WH_Day = "";
                        Emp_Total_Work_Time_1 = "00:00";
                        NFH_Days_Count = 0;
                        NFH_Days_Present_Count = 0;
                    }

                    intI += 1; ;
                    Total_Days_Count = 0;
                    Final_Count = 0;
                    Appsent_Count = 0;
                    Present_Count = 0;
                    Present_WH_Count = 0;
                    NFH_Days_Count = 0;
                    NFH_Days_Present_Count = 0;
                    INTime = "";
                    OUTTime = "";
                    Weekoff_Count = 0;
                    Emp_WH_Day = "";
                }



                //Calculation for Employee not register in Employee Mst
                DataTable DT_TempEmp = new DataTable();

                SSQL = "Select EmpNo from Temp_Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                DT_TempEmp = ManualAttend.GetDetails(SSQL);

                for (int Empk = 0; Empk < DT_TempEmp.Rows.Count; Empk++)
                {
                    intK = 1;
                    string Emp_Total_Work_Time_1 = "00:00";
                    //Get Employee Week OF DAY
                    DataTable DS_WH = new DataTable();
                    string Emp_WH_Day = "";
                    string DOJ_Date_Str = "";
                    string MachineID1 = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();


                    int colIndex = intK;
                    string DayofWeekName = "";
                    decimal Weekoff_Count = 0;
                    bool isPresent = false;
                    bool isWHPresent = false;
                    decimal Present_Count = 0;
                    decimal Present_WH_Count = 0;
                    int Appsent_Count = 0;
                    decimal Final_Count = 0;
                    decimal Total_Days_Count = 0;
                    string Already_Date_Check;
                    int Days_Insert_RowVal = 0;
                    decimal FullNight_Shift_Count = 0;
                    int NFH_Days_Count = 0;
                    decimal NFH_Days_Present_Count = 0;
                    Already_Date_Check = "";
                    string Year_Check = "";
                    string Time_IN_Str = "";
                    string Time_Out_Str = "";
                    int g = 1;

                    double OT_Time;

                    OT_Time = 0;

                    string INTime = "";
                    string OUTTime = "";

                    DateTime Date11 = Convert.ToDateTime(FromDate);
                    string dat = ToDate;
                    DateTime Date12 = Convert.ToDateTime(dat);

                    int daycount = (int)((Date12 - Date11).TotalDays);

                    daysAdded = daycount + 1;

                    //daysAdded = 1;

                    for (intCol = 0; (intCol <= (daysAdded - 1)); intCol++)
                    {
                        isPresent = false;
                        isWHPresent = false;
                        //string Emp_Total_Work_Time_1 = "00:00";
                        string Machine_ID_Str = "";
                        string OT_Week_OFF_Machine_No;
                        string Date_Value_Str = "";
                        string Total_Time_get = "";
                        string Final_OT_Work_Time_1 = "";
                        //DataSet mLocalDS = new DataSet();

                        //DataTable mLocalDS = new DataTable();


                        int j = 0;
                        double time_Check_dbl = 0;
                        string Date_Value_Str1 = "";
                        string Employee_Shift_Name_DB = "No Shift";
                        isPresent = false;


                        //Shift Change Check Variable Declaration Start
                        DateTime InTime_Check = new DateTime();
                        DateTime InToTime_Check = new DateTime();
                        TimeSpan InTime_TimeSpan;
                        string From_Time_Str = "";
                        string To_Time_Str = "";
                        //DataSet DS_InTime = new DataSet();
                        DataTable DS_Time = new DataTable();
                        //DataSet DS_Time = new DataSet();
                        DataTable DS_InTime = new DataTable();
                        DateTime EmpdateIN_Change = new DateTime();
                        DataTable InsertData = new DataTable();
                        string Final_InTime = "";
                        string Final_OutTime = "";
                        string Final_Shift = "";

                        int K = 0;
                        bool Shift_Check_blb = false;
                        string Shift_Start_Time_Change;
                        string Shift_End_Time_Change;
                        string Employee_Time_Change = "";
                        DateTime ShiftdateStartIN_Change = new DateTime();
                        DateTime ShiftdateEndIN_Change = new DateTime();

                        //Shift Change Check Variable Declaration End
                        string Employee_Punch = "";
                        Spin_Machine_ID_Str = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();

                        if (Spin_Machine_ID_Str == "106272")
                        {
                            Spin_Machine_ID_Str = "106272";
                        }

                        // Machine_ID_Str = Encryption(Emp_DS.Rows[intRow]["MachineID"]);
                        Machine_ID_Str = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();
                        OT_Week_OFF_Machine_No = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();
                        Date_Value_Str = Convert.ToDateTime(FromDate).AddDays(intCol).ToShortDateString();
                        Date_Value_Str1 = Convert.ToDateTime(Date_Value_Str).AddDays(1).ToShortDateString();

                        //DateTime dt = Convert.ToDateTime(Date.ToString());

                        //DayofWeekName = dt.DayOfWeek.ToString();

                        //TimeIN Get

                        SSQL = "";
                        SSQL = "Select distinct TimeIN,CONVERT(char(5), TimeIN, 108) as TimeIN24 from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                              " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";

                        mLocalDS = ManualAttend.GetDetails(SSQL);

                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";

                            INTime = "";

                        }
                        else
                        {
                            DateTime datetime1 = Convert.ToDateTime(mLocalDS.Rows[0][0].ToString());
                            INTime = datetime1.ToString("hh:mm tt");
                            //INTime = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0].ToString());
                        }

                        //TimeOUT Get
                        SSQL = "";
                        SSQL = "Select TimeOUT,CONVERT(char(5), TimeOUT, 108) as TimeOUT24 from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                               " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                        mLocalDS1 = ManualAttend.GetDetails(SSQL);

                        if (mLocalDS1.Rows.Count <= 0)
                        {
                            Time_Out_Str = "";

                            OUTTime = "";
                        }
                        else
                        {
                            DateTime datetime = Convert.ToDateTime(mLocalDS1.Rows[0][0].ToString());
                            OUTTime = datetime.ToString("hh:mm tt");
                            //OUTTime = string.Format(Convert.ToString(datetime), "HH:mm:ss tt");
                            //OUTTime = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0].ToString());
                        }

                        //Shift Change Check Start
                        if (mLocalDS.Rows.Count != 0)
                        {
                            InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
                            InToTime_Check = InTime_Check.AddHours(2);



                            InTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InTime_Check), "HH:mm:ss"));
                            InToTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InToTime_Check), "HH:mm:ss"));




                            //Old Time check error for 24 format

                            string TimeIN = InTime_Check.ToString();
                            string[] TimeIN_Split = TimeIN.Split(' ');
                            string TimeIN1 = TimeIN_Split[1].ToString();
                            string[] final = TimeIN1.Split(':');
                            string Final_IN = "";
                            if (Convert.ToInt32(final[0]) >= 10)
                            {
                                Final_IN = final[0] + ":" + final[1];
                            }
                            else
                            {
                                Final_IN = "0" + final[0] + ":" + final[1];
                            }


                            string TimeOUT = InToTime_Check.ToString();
                            string[] TimeOUT_Split = TimeOUT.Split(' ');
                            string TimeOUT1 = TimeOUT_Split[1].ToString();
                            string[] final1 = TimeOUT1.Split(':');
                            string Final_IN1 = "";
                            if (Convert.ToInt32(final1[0]) >= 10)
                            {
                                Final_IN1 = final1[0] + ":" + final1[1];
                            }
                            else
                            {
                                Final_IN1 = "0" + final1[0] + ":" + final1[1];
                            }



                            //Two Hours OutTime Check
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";


                            DS_Time = ManualAttend.GetDetails(SSQL);
                            if (DS_Time.Rows.Count != 0)
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                DS_InTime = ManualAttend.GetDetails(SSQL);
                                if (DS_InTime.Rows.Count != 0)
                                {
                                    //DataSet Shift_DS_Change = new DataSet();

                                    DataTable Shift_DS_Change = new DataTable();

                                    Final_InTime = DS_InTime.Rows[0]["TimeIN"].ToString();
                                    //Check With IN Time Shift
                                    SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%'";
                                    SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    Shift_DS_Change = ManualAttend.GetDetails(SSQL);
                                    Shift_Check_blb = false;
                                    for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
                                    {
                                        Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
                                        if (Shift_DS_Change.Rows[k]["EndIN_Days"].ToString() == "1")
                                        {
                                            Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                        }
                                        else
                                        {
                                            Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                        }

                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);

                                        if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                            Shift_Check_blb = true;
                                            break;
                                        }
                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                        EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                        if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                            Shift_Check_blb = true;
                                            break;
                                        }
                                    }
                                    if (Shift_Check_blb == true)
                                    {

                                        //IN Time Query Update
                                        string Querys = "";
                                        Querys = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                        " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                        mLocalDS = ManualAttend.GetDetails(Querys);

                                        if (Final_Shift == "SHIFT1")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                            " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        }
                                        else if (Final_Shift == "SHIFT2")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                                   " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        }
                                        else if (Final_Shift == "SHIFT3")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

                                        }
                                        else
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

                                        }


                                        mLocalDS1 = ManualAttend.GetDetails(Querys);
                                    }
                                    else
                                    {
                                        //Get Employee In Time
                                        SSQL = "";
                                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                               " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                        mLocalDS = ManualAttend.GetDetails(SSQL);
                                        //TimeOUT Get
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                               " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        mLocalDS1 = ManualAttend.GetDetails(SSQL);
                                    }
                                }
                                else
                                {
                                    //Get Employee In Time
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                           " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                    mLocalDS = ManualAttend.GetDetails(SSQL);

                                    //TimeOUT Get
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                          " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                    mLocalDS1 = ManualAttend.GetDetails(SSQL);

                                }
                            }
                            else
                            {
                                //Get Employee In Time
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                       " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                mLocalDS = ManualAttend.GetDetails(SSQL);

                                //TimeOUT Get
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                      " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                mLocalDS1 = ManualAttend.GetDetails(SSQL);
                            }
                        }
                        //Shift Change Check End 
                        if (mLocalDS.Rows.Count == 0)
                        {
                            Employee_Punch = "";
                        }
                        else
                        {
                            Employee_Punch = mLocalDS.Rows[0][0].ToString();
                        }
                        if (mLocalDS.Rows.Count >= 1)
                        {
                            for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                if (mLocalDS1.Rows.Count > tin)
                                {
                                    Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                                }
                                else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                                {
                                    Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                                }
                                else
                                {
                                    Time_Out_Str = "";
                                }
                                TimeSpan ts4 = new TimeSpan();
                                ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                                if (mLocalDS.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "";
                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                }
                                if (Time_IN_Str == "" || Time_Out_Str == "")
                                {
                                    time_Check_dbl = time_Check_dbl;
                                }
                                else
                                {


                                    DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                    DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                    TimeSpan ts = new TimeSpan();
                                    ts = date2.Subtract(date1);


                                    //& ":" & Trim(ts.Minutes)
                                    Total_Time_get = (ts.Hours).ToString();
                                    ts4 = ts4.Add(ts);

                                    //OT Time Get
                                    Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                    Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);

                                    if (Left_Val(ts4.Minutes.ToString(), 1) == "-" || Left_Val(ts4.Hours.ToString(), 1) == "-" || Emp_Total_Work_Time_1 == "0:-1")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                    }
                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {

                                        date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                        ts = date2.Subtract(date1);
                                        ts = date2.Subtract(date1);
                                        Total_Time_get = (ts.Hours).ToString();
                                        time_Check_dbl = double.Parse(Total_Time_get);
                                        Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes).ToString();
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:1" || Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }

                                    }
                                    else
                                    {
                                        time_Check_dbl = time_Check_dbl + double.Parse(Total_Time_get);
                                    }
                                }

                                break;

                            }//For End
                        }
                        else
                        {
                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;

                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                            }
                            for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
                            {
                                if (mLocalDS1.Rows.Count <= 0)
                                {
                                    Time_Out_Str = "";
                                }
                                else
                                {
                                    Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                                }

                                break;

                            }
                            //Emp_Total_Work_Time
                            if (Time_IN_Str == "" || Time_Out_Str == "")
                            {
                                time_Check_dbl = 0;
                            }
                            else
                            {
                                DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();
                                ts = date2.Subtract(date1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = (ts.Hours).ToString();
                                //OT Time Get
                                Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {

                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = ts.Hours.ToString();
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                    Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes);
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                        time_Check_dbl = 0;
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                        time_Check_dbl = 0;
                                    }
                                }
                                else
                                {
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                }
                            }
                        }
                        //Emp_Total_Work_Time

                        string StdWorkingHrs = "0";

                        if (time_Check_dbl >= 9)
                        {
                            OT_Time = OT_Time + (time_Check_dbl - 8);

                        }
                        else
                        {
                            OT_Time = 0;
                        }



                        string Emp_Total_Work_Time = "";
                        string Final_OT_Work_Time = "00:00";
                        string Final_OT_Work_Time_Val = "00:00";
                        Emp_Total_Work_Time = Emp_Total_Work_Time_1;
                        //Find Week OFF
                        string Employee_Week_Name = "";
                        string Assign_Week_Name = "";
                        mLocalDS = null;
                        Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();

                        //SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "' and  Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "'";
                        //mLocalDS = ManualAttend.GetDetails(SSQL);
                        //if (mLocalDS.Rows.Count <= 0)
                        //{
                        //    Assign_Week_Name = "";
                        //}
                        //else
                        //{
                        //    //Assign_Week_Name = mLocalDS.Tables(0).Rows(0)(0)
                        //    Assign_Week_Name = "";
                        //}

                        Boolean NFH_Day_Check = false;
                        DateTime NFH_Date;
                        DateTime DOJ_Date_Format;
                        DateTime Date_Value = new DateTime();
                        DataTable DT_NFH = new DataTable();
                        Date_Value = Convert.ToDateTime(Date_Value_Str.ToString());


                        if (Employee_Week_Name == Assign_Week_Name)
                        {
                            //Get Employee Work Time
                            double Calculate_Attd_Work_Time = 8;
                            double Calculate_Attd_Work_Time_half = 0;


                            Calculate_Attd_Work_Time_half = 4.0;

                            halfPresent = "0";//Half Days Calculate Start
                            if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            {
                                Weekoff_Count = Weekoff_Count + 1;
                                isWHPresent = true;
                                halfPresent = "1";

                            }
                            //Half Days Calculate End
                            if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            {
                                Weekoff_Count = Weekoff_Count + 1;
                                isWHPresent = true;
                                halfPresent = "0";

                            }
                        }
                        else if (Employee_Week_Name == Emp_WH_Day)
                        {
                            //Get Employee Work Time
                            double Calculate_Attd_Work_Time = 8;
                            double Calculate_Attd_Work_Time_half = 0;


                            Calculate_Attd_Work_Time_half = 4.0;

                            halfPresent = "0";//Half Days Calculate Start
                            if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            {
                                isWHPresent = true;
                                halfPresent = "1";
                                Weekoff_Count = Weekoff_Count + 1;


                            }
                            //Half Days Calculate End
                            if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            {
                                isWHPresent = true;
                                halfPresent = "0";
                                Weekoff_Count = Weekoff_Count + 1;


                            }

                        }
                        else
                        {
                            //Get Employee Work Time
                            double Calculate_Attd_Work_Time = 8;
                            double Calculate_Attd_Work_Time_half = 0;


                            Calculate_Attd_Work_Time_half = 4.0;

                            halfPresent = "0";//Half Days Calculate Start
                            if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            {
                                isPresent = true;
                                halfPresent = "1";
                            }
                            //Half Days Calculate End
                            if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            {
                                isPresent = true;
                                halfPresent = "0";
                            }
                        }
                        //Shift Check Code Start
                        //DataSet Shift_Ds = new DataSet();

                        DataTable Shift_Ds = new DataTable();

                        string Start_IN = "";
                        string End_In = "";
                        string Shift_Name_Store = "";
                        bool Shift_Check_blb_Check = false;
                        DateTime ShiftdateStartIN_Check = new DateTime();
                        DateTime ShiftdateEndIN_Check = new DateTime();
                        DateTime EmpdateIN_Check = new DateTime();
                        if (INTime != "")
                        {
                            Shift_Check_blb_Check = false;

                            //Shift Master Check Code Start
                            SSQL = "Select * from Shift_Mst where  ShiftDesc <> 'GENERAL' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' Order by ShiftDesc Asc";
                            //  "CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'";
                            Shift_Ds = ManualAttend.GetDetails(SSQL);
                            if (Shift_Ds.Rows.Count != 0)
                            {
                                SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%' ";
                                SSQL = SSQL + "And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                //CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "' And 
                                Shift_Ds = ManualAttend.GetDetails(SSQL);
                                Shift_Check_blb = false;
                                for (int f = 0; f < Shift_Ds.Rows.Count; f++)
                                {

                                    int StartIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["StartIN_Days"]);
                                    int EndIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["EndIN_Days"]);
                                    Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(StartIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["StartIN"].ToString();
                                    End_In = Convert.ToDateTime(Date_Value_Str).AddDays(EndIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["EndIN"].ToString();
                                    ShiftdateStartIN_Check = System.Convert.ToDateTime(Start_IN);
                                    ShiftdateEndIN_Check = System.Convert.ToDateTime(End_In);
                                    EmpdateIN_Check = System.Convert.ToDateTime(Employee_Punch);
                                    if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                                    {
                                        Employee_Shift_Name_DB = Shift_Ds.Rows[f]["ShiftDesc"].ToString();
                                        Shift_Check_blb_Check = true;
                                    }

                                }
                                if (Shift_Check_blb_Check == false)
                                {
                                    Employee_Shift_Name_DB = "No Shift";
                                }

                            }
                            else
                            {
                                Employee_Shift_Name_DB = "No Shift";
                            }
                            //Shift Master Check Code End  

                        }
                        else
                        {
                            Employee_Shift_Name_DB = "No Shift";
                        }
                        //Shift Check Code End
                        string ShiftType = "";
                        string Pre_Abs = "";
                        if (isPresent == true)
                        {

                            if (Employee_Shift_Name_DB != "No Shift")
                            {
                                ShiftType = "Proper";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }
                            else
                            {
                                ShiftType = "Mismatch";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }

                            if (halfPresent == "1")
                            {
                                Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_Count = Present_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
                                }
                            }
                            else if (halfPresent == "0")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                                }
                            }

                        }
                        else if (isWHPresent == true)
                        {

                            if (Employee_Shift_Name_DB != "No Shift")
                            {
                                ShiftType = "Proper";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }
                            else
                            {

                                ShiftType = "Mismatch";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }

                            if (halfPresent == "1")
                            {
                                Present_WH_Count = Convert.ToDecimal(Present_WH_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_WH_Count = Present_WH_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_Count = Present_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
                                }
                            }
                            else if (halfPresent == "0")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                                }
                            }
                        }
                        else
                        {
                            Appsent_Count = Appsent_Count + 1;

                            if (INTime == "" && OUTTime == "")
                            {
                                ShiftType = "Leave";
                                Employee_Shift_Name_DB = "Leave";
                                Pre_Abs = "Leave";

                            }
                            else if (INTime == "")
                            {
                                if (OUTTime != "")
                                {
                                    ShiftType = "Improper";
                                }
                                Pre_Abs = "Absent";

                            }
                            else if (OUTTime == "")
                            {
                                if (INTime != "")
                                {
                                    ShiftType = "Improper";
                                }
                                Pre_Abs = "Absent";

                            }
                            else if (INTime != "" && OUTTime != "")
                            {
                                ShiftType = "Absent";
                                Pre_Abs = "Absent";

                            }
                        }

                        intK += 1;




                        //Update Employee Worked_Days
                        //DataSet mDataSet = new DataSet();
                        DataTable mDataSet = new DataTable();
                        string Queryss = "";
                        DataSet Del_ds = new DataSet();
                        Queryss = "Select * from LogTime_Days where MachineID='" + MachineID1 + "' and  Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";

                        mDataSet = ManualAttend.GetDetails(Queryss);

                        //mDataSet = objdata.read(Queryss);  Emp_WH_Day
                        if (mDataSet.Rows.Count != 0)
                        {
                            Queryss = "Delete from LogTime_Days where MachineID='" + MachineID1 + "' and Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                            ManualAttend.GetDetails(Queryss);
                        }
                        DataSet Insert_Ds = new DataSet();

                        Queryss = "";
                        Queryss = "Insert Into LogTime_Days(CompCode,LocCode,MachineID,ExistingCode,Present,Wh_Check,Wh_Count,";
                        Queryss = Queryss + "Wh_Present_Count,NFH_Count,NFH_Present_Count,Shift,TimeIN,TimeOUT,Total_Hrs,OTHours,";
                        Queryss = Queryss + "Attn_Date_Str,Attn_Date,MachineID_Encrypt,Total_Hrs1,TypeName,Present_Absent)" +
                         " Values('" + SessionCcode + "','" + SessionLcode + "','" + MachineID1 + "','" + MachineID1 + "','" + Present_Count + "'," +
                         "'" + Emp_WH_Day + "','" + Weekoff_Count + "','" + Present_WH_Count + "','" + NFH_Days_Count + "','" + NFH_Days_Present_Count + "'," +
                         "'" + Employee_Shift_Name_DB + "','" + INTime + "','" + OUTTime + "','" + time_Check_dbl + "','" + OT_Time + "','" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "',Convert(datetime,'" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "',120),'" + UTF8Encryption(MachineID1) + "','" + Emp_Total_Work_Time_1 + "','" + ShiftType + "','" + Pre_Abs + "')";

                        ManualAttend.GetDetails(Queryss);

                        Present_Count = 0;
                        Present_WH_Count = 0;
                        Employee_Shift_Name_DB = "No Shift";
                        INTime = "";
                        OUTTime = "";
                        time_Check_dbl = 0;
                        OT_Time = 0;
                        Weekoff_Count = 0;
                        //Emp_WH_Day = "";
                        Emp_Total_Work_Time_1 = "00:00";
                        NFH_Days_Count = 0;
                        NFH_Days_Present_Count = 0;
                    }

                    intI += 1; ;
                    Total_Days_Count = 0;
                    Final_Count = 0;
                    Appsent_Count = 0;
                    Present_Count = 0;
                    Present_WH_Count = 0;
                    NFH_Days_Count = 0;
                    NFH_Days_Present_Count = 0;
                    INTime = "";
                    OUTTime = "";
                    Weekoff_Count = 0;
                    Emp_WH_Day = "";
                }

                IFllogin(FromDate, ToDate);
                Status = "Completed";
            }
            else
            {
                Status = "Already Exists";

            }

            return Json(Status, JsonRequestBehavior.AllowGet);
        }



        public void IFllogin(string FromDate, String ToDate)

        {

            Status = "";
            string[] iStr1;
            string[] iStr2;
            int intI;
            int intK;
            int intCol;
            string Fin_Year = "";
            string Months_Full_Str = "";
            string Date_Col_Str = "";
            int Month_Int = 1;
            string Spin_Machine_ID_Str = "";

            string[] Time_Minus_Value_Check;

            DataTable mLocalDS = new DataTable();

            DataTable mLocalDS1 = new DataTable();

            //Attn_Flex Col Add Var
            string[] Att_Date_Check;
            string Att_Already_Date_Check;
            string Att_Year_Check = "";
            int Month_Name_Change;
            string halfPresent = "0";
            int EPay_Total_Days_Get = 0;
            intCol = 4;
            Month_Name_Change = 1;
            int dayCount = 1;
            int daysAdded = 1;

            EPay_Total_Days_Get = 1;

            intI = 2;
            intK = 1;

            // string EmpNo = MachineNo;

            //string MachineID = UTF8Encryption(EmpNo);

            string SSQL = "";
            bool ErrFlag = false;

            DataTable Emp_DS = new DataTable();

            SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' and  (Wages='1'or Wages='2')";
            SSQL = SSQL + " And IsActive='Yes'";
            Emp_DS = ManualAttend.GetDetails(SSQL);

            if (Emp_DS.Rows.Count > 0)
            {
                ErrFlag = false;
            }
            else
            {
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                //DataTable DT_Chk = new DataTable();
                //SSQL = "Select * from LogTime_Days where Attn_Date_Str='" + Convert.ToDateTime(Date).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                //DT_Chk = ManualAttend.GetDetails(SSQL);
                //if (DT_Chk.Rows.Count > 0)
                //{
                //    ErrFlag = true;
                //}
                //else
                //{
                //    ErrFlag = false;
                //}
            }

            if (!ErrFlag)
            {
                for (int intRow = 0; intRow < Emp_DS.Rows.Count; intRow++)
                {

                    intK = 1;
                    string Emp_Total_Work_Time_1 = "00:00";
                    //Get Employee Week OF DAY
                    DataTable DS_WH = new DataTable();
                    string Emp_WH_Day;
                    string DOJ_Date_Str;
                    string MachineID1 = Emp_DS.Rows[intRow]["MachineID"].ToString();

                    string Query = " Select * from Employee_Mst where MachineID='" + MachineID1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' ";
                    //SSQL = "Select * from Employee_MST where MachineID='" + MachineID1 + "' And Compcode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'" +
                    DS_WH = ManualAttend.GetDetails(Query);
                    if (DS_WH.Rows.Count != 0)
                    {
                        Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                        DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();
                    }
                    else
                    {
                        Emp_WH_Day = "";
                        DOJ_Date_Str = "";
                    }
                    int colIndex = intK;
                    string DayofWeekName = "";
                    decimal Weekoff_Count = 0;
                    bool isPresent = false;
                    bool isWHPresent = false;
                    decimal Present_Count = 0;
                    decimal Present_WH_Count = 0;
                    int Appsent_Count = 0;
                    decimal Final_Count = 0;
                    decimal Total_Days_Count = 0;
                    string Already_Date_Check;
                    int Days_Insert_RowVal = 0;
                    decimal FullNight_Shift_Count = 0;
                    int NFH_Days_Count = 0;
                    decimal NFH_Days_Present_Count = 0;
                    Already_Date_Check = "";
                    string Year_Check = "";
                    string Time_IN_Str = "";
                    string Time_Out_Str = "";
                    int g = 1;

                    double OT_Time;

                    OT_Time = 0;

                    string INTime = "";
                    string OUTTime = "";

                    DateTime Date11 = Convert.ToDateTime(FromDate);
                    string dat = ToDate;
                    DateTime Date12 = Convert.ToDateTime(dat);

                    int daycount = (int)((Date12 - Date11).TotalDays);

                    daysAdded = daycount + 1;

                    for (intCol = 0; (intCol <= (daysAdded - 1)); intCol++)
                    {
                        isPresent = false;
                        isWHPresent = false;
                        //string Emp_Total_Work_Time_1 = "00:00";
                        string Machine_ID_Str = "";
                        string OT_Week_OFF_Machine_No;
                        string Date_Value_Str = "";
                        string Total_Time_get = "";
                        string Final_OT_Work_Time_1 = "";
                        //DataSet mLocalDS = new DataSet();

                        //DataTable mLocalDS = new DataTable();


                        int j = 0;
                        double time_Check_dbl = 0;
                        string Date_Value_Str1 = "";
                        string Employee_Shift_Name_DB = "No Shift";
                        isPresent = false;


                        //Shift Change Check Variable Declaration Start
                        DateTime InTime_Check = new DateTime();
                        DateTime InToTime_Check = new DateTime();
                        TimeSpan InTime_TimeSpan;
                        string From_Time_Str = "";
                        string To_Time_Str = "";
                        //DataSet DS_InTime = new DataSet();
                        DataTable DS_Time = new DataTable();
                        //DataSet DS_Time = new DataSet();
                        DataTable DS_InTime = new DataTable();
                        DateTime EmpdateIN_Change = new DateTime();
                        DataTable InsertData = new DataTable();
                        string Final_InTime = "";
                        string Final_OutTime = "";
                        string Final_Shift = "";

                        int K = 0;
                        bool Shift_Check_blb = false;
                        string Shift_Start_Time_Change;
                        string Shift_End_Time_Change;
                        string Employee_Time_Change = "";
                        DateTime ShiftdateStartIN_Change = new DateTime();
                        DateTime ShiftdateEndIN_Change = new DateTime();

                        //Shift Change Check Variable Declaration End
                        string Employee_Punch = "";
                        Spin_Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID"].ToString();

                        if (Spin_Machine_ID_Str == "206303")
                        {
                            Spin_Machine_ID_Str = "206303";
                        }


                        // Machine_ID_Str = Encryption(Emp_DS.Rows[intRow]["MachineID"]);
                        Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID_Encrypt"].ToString();
                        OT_Week_OFF_Machine_No = Emp_DS.Rows[intRow]["MachineID"].ToString();
                        Date_Value_Str = Convert.ToDateTime(FromDate).AddDays(intCol).ToShortDateString();
                        Date_Value_Str1 = Convert.ToDateTime(Date_Value_Str).AddDays(1).ToShortDateString();

                        //DateTime dt = Convert.ToDateTime(Date.ToString());

                        //DayofWeekName = dt.DayOfWeek.ToString();

                        //TimeIN Get

                        SSQL = "";
                        SSQL = "Select distinct TimeIN,CONVERT(char(5), TimeIN, 108) as TimeIN24 from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                              " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";

                        mLocalDS = ManualAttend.GetDetails(SSQL);

                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";

                            INTime = "";

                        }
                        else
                        {
                            DateTime datetime1 = Convert.ToDateTime(mLocalDS.Rows[0][0].ToString());
                            INTime = datetime1.ToString("hh:mm tt");
                            //INTime = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0].ToString());
                        }

                        //TimeOUT Get
                        SSQL = "";
                        SSQL = "Select TimeOUT,CONVERT(char(5), TimeOUT, 108) as TimeOUT24 from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                               " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                        mLocalDS1 = ManualAttend.GetDetails(SSQL);

                        if (mLocalDS1.Rows.Count <= 0)
                        {
                            Time_Out_Str = "";

                            OUTTime = "";
                        }
                        else
                        {
                            DateTime datetime = Convert.ToDateTime(mLocalDS1.Rows[0][0].ToString());
                            OUTTime = datetime.ToString("hh:mm tt");
                            //OUTTime = string.Format(Convert.ToString(datetime), "HH:mm:ss tt");
                            //OUTTime = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0].ToString());
                        }

                        //Shift Change Check Start
                        if (mLocalDS.Rows.Count != 0)
                        {
                            InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
                            InToTime_Check = InTime_Check.AddHours(2);



                            InTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InTime_Check), "HH:mm:ss"));
                            InToTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InToTime_Check), "HH:mm:ss"));




                            //Old Time check error for 24 format

                            string TimeIN = InTime_Check.ToString();
                            string[] TimeIN_Split = TimeIN.Split(' ');
                            string TimeIN1 = TimeIN_Split[1].ToString();
                            string[] final = TimeIN1.Split(':');
                            string Final_IN = "";
                            if (Convert.ToInt32(final[0]) >= 10)
                            {
                                Final_IN = final[0] + ":" + final[1];
                            }
                            else
                            {
                                Final_IN = "0" + final[0] + ":" + final[1];
                            }


                            string TimeOUT = InToTime_Check.ToString();
                            string[] TimeOUT_Split = TimeOUT.Split(' ');
                            string TimeOUT1 = TimeOUT_Split[1].ToString();
                            string[] final1 = TimeOUT1.Split(':');
                            string Final_IN1 = "";
                            if (Convert.ToInt32(final1[0]) >= 10)
                            {
                                Final_IN1 = final1[0] + ":" + final1[1];
                            }
                            else
                            {
                                Final_IN1 = "0" + final1[0] + ":" + final1[1];
                            }




                            //New Format 24 change manually

                            //string InTime1 = mLocalDS.Rows[0]["TimeIN24"].ToString();

                            ////string TimeIN = InTime1.ToString();
                            ////string[] TimeIN_Split = TimeIN.Split(' ');
                            ////string TimeIN1 = TimeIN_Split[1].ToString();
                            //string[] final = InTime1.Split(':');
                            //string Final_IN = "";

                            //Final_IN = final[0] + ":" + final[1];




                            //string InTime2 = (Convert.ToInt16(final[0]) + 2).ToString();
                            //InTime2= InTime2+":"+ final[1];

                            ////string TimeOUT = InTime2.ToString();
                            ////string[] TimeOUT_Split = TimeOUT.Split(' ');
                            ////string TimeOUT1 = TimeOUT_Split[1].ToString();
                            //string[] final1 = InTime2.Split(':');
                            //string Final_IN1 = "";

                            //Final_IN1 = final1[0] + ":" + final1[1];



                            //InTime_TimeSpan = TimeSpan.Parse(InTime_Check).ToString();
                            //From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;

                            //Two Hours OutTime Check
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";


                            DS_Time = ManualAttend.GetDetails(SSQL);
                            if (DS_Time.Rows.Count != 0)
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                DS_InTime = ManualAttend.GetDetails(SSQL);
                                if (DS_InTime.Rows.Count != 0)
                                {
                                    //DataSet Shift_DS_Change = new DataSet();

                                    DataTable Shift_DS_Change = new DataTable();

                                    Final_InTime = DS_InTime.Rows[0]["TimeIN"].ToString();
                                    //Check With IN Time Shift
                                    SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%'";
                                    SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    Shift_DS_Change = ManualAttend.GetDetails(SSQL);
                                    Shift_Check_blb = false;
                                    for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
                                    {
                                        Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
                                        if (Shift_DS_Change.Rows[k]["EndIN_Days"].ToString() == "1")
                                        {
                                            Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                        }
                                        else
                                        {
                                            Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                        }

                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);

                                        if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                            Shift_Check_blb = true;
                                            break;
                                        }
                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                        EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                        if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                            Shift_Check_blb = true;
                                            break;
                                        }
                                    }
                                    if (Shift_Check_blb == true)
                                    {

                                        //IN Time Query Update
                                        string Querys = "";
                                        Querys = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                        " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                        mLocalDS = ManualAttend.GetDetails(Querys);

                                        if (Final_Shift == "SHIFT1")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                            " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        }
                                        else if (Final_Shift == "SHIFT2")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                                   " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        }
                                        else if (Final_Shift == "SHIFT3")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

                                        }
                                        else
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

                                        }


                                        mLocalDS1 = ManualAttend.GetDetails(Querys);
                                    }
                                    else
                                    {
                                        //Get Employee In Time
                                        SSQL = "";
                                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                               " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                        mLocalDS = ManualAttend.GetDetails(SSQL);
                                        //TimeOUT Get
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                               " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        mLocalDS1 = ManualAttend.GetDetails(SSQL);
                                    }
                                }
                                else
                                {
                                    //Get Employee In Time
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                           " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                    mLocalDS = ManualAttend.GetDetails(SSQL);

                                    //TimeOUT Get
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                          " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                    mLocalDS1 = ManualAttend.GetDetails(SSQL);

                                }
                            }
                            else
                            {
                                //Get Employee In Time
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                       " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                mLocalDS = ManualAttend.GetDetails(SSQL);

                                //TimeOUT Get
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                      " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                mLocalDS1 = ManualAttend.GetDetails(SSQL);
                            }
                        }
                        //Shift Change Check End 
                        if (mLocalDS.Rows.Count == 0)
                        {
                            Employee_Punch = "";
                        }
                        else
                        {
                            Employee_Punch = mLocalDS.Rows[0][0].ToString();
                        }
                        if (mLocalDS.Rows.Count >= 1)
                        {
                            for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                if (mLocalDS1.Rows.Count > tin)
                                {
                                    Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                                }
                                else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                                {
                                    Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                                }
                                else
                                {
                                    Time_Out_Str = "";
                                }
                                TimeSpan ts4 = new TimeSpan();
                                ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                                if (mLocalDS.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "";
                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                }
                                if (Time_IN_Str == "" || Time_Out_Str == "")
                                {
                                    time_Check_dbl = time_Check_dbl;
                                }
                                else
                                {


                                    DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                    DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                    TimeSpan ts = new TimeSpan();
                                    ts = date2.Subtract(date1);


                                    //& ":" & Trim(ts.Minutes)
                                    Total_Time_get = (ts.Hours).ToString();
                                    ts4 = ts4.Add(ts);

                                    //OT Time Get
                                    Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                    Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);

                                    if (Left_Val(ts4.Minutes.ToString(), 1) == "-" || Left_Val(ts4.Hours.ToString(), 1) == "-" || Emp_Total_Work_Time_1 == "0:-1")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                    }
                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {

                                        date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                        ts = date2.Subtract(date1);
                                        ts = date2.Subtract(date1);
                                        Total_Time_get = (ts.Hours).ToString();
                                        time_Check_dbl = double.Parse(Total_Time_get);
                                        Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes).ToString();
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:1" || Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }

                                    }
                                    else
                                    {
                                        time_Check_dbl = time_Check_dbl + double.Parse(Total_Time_get);
                                    }


                                    //Working Hours 8 Fixed Total Hours Time_CHeck 8.20 Start

                                    Time_Split_Str = Emp_Total_Work_Time_1.Split(':');
                                    Time_IN_Get = Time_IN_Str.ToString();

                                    Time_Out_Update = "";
                                    string TimeCheck_Str = Time_Split_Str[0] + "." + Time_Split_Str[1];
                                    Totol_Hours_Check = Convert.ToDouble(TimeCheck_Str);

                                    if (Totol_Hours_Check_1 < Totol_Hours_Check)
                                    {
                                        Random_No_Get = GetRandom(Random_No_Fixed, 15);
                                        DateTime NewDT = Convert.ToDateTime(Time_IN_Get);
                                        NewDT = NewDT.AddHours(8);
                                        NewDT = NewDT.AddMinutes(Random_No_Get);

                                        Time_Out_Update = String.Format("{0:hh:mm tt}", NewDT);
                                        // AutoDTable.Rows[iRow2][8] = Time_Out_Update;
                                        DateTime MDate1 = Convert.ToDateTime(Time_IN_Get);
                                        DateTime MDate2 = Convert.ToDateTime(Time_Out_Update);

                                        TimeSpan ts11;
                                        if (MDate2 > MDate1)
                                        {
                                            ts11 = MDate2.Subtract(MDate1);
                                        }
                                        else
                                        {
                                            ts11 = NewDT.Subtract(MDate1);
                                        }

                                        string s2 = ts11.Minutes.ToString();
                                        if (s2.Length == 1)
                                        {
                                            string ts_str = "0" + ts11.Minutes;
                                            Emp_Total_Work_Time_1 = ts11.Hours + ":" + ts_str;

                                            Totaltime8above = ts11.Hours.ToString();

                                            time_Check_dbl = Convert.ToInt32(Totaltime8above);
                                        }
                                        else
                                        {
                                            Emp_Total_Work_Time_1 = ts11.Hours.ToString() + ":" + ts11.Minutes.ToString();


                                            Totaltime8above = ts11.Hours.ToString();

                                            time_Check_dbl = Convert.ToInt32(Totaltime8above);

                                        }
                                    }



                                }

                                break;

                            }//For End
                        }
                        else
                        {
                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;

                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                            }
                            for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
                            {
                                if (mLocalDS1.Rows.Count <= 0)
                                {
                                    Time_Out_Str = "";
                                }
                                else
                                {
                                    Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                                }

                                break;

                            }
                            //Emp_Total_Work_Time
                            if (Time_IN_Str == "" || Time_Out_Str == "")
                            {
                                time_Check_dbl = 0;
                            }
                            else
                            {
                                DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();
                                ts = date2.Subtract(date1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = (ts.Hours).ToString();
                                //OT Time Get
                                Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {

                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = ts.Hours.ToString();
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                    Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes);
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                        time_Check_dbl = 0;
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                        time_Check_dbl = 0;
                                    }
                                }
                                else
                                {
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                }


                                //Working Hours 8 Fixed Total Hours Time_CHeck 8.20 Start

                                Time_Split_Str = Emp_Total_Work_Time_1.Split(':');
                                Time_IN_Get = Time_IN_Str.ToString();

                                Time_Out_Update = "";
                                string TimeCheck_Str = Time_Split_Str[0] + "." + Time_Split_Str[1];
                                Totol_Hours_Check = Convert.ToDouble(TimeCheck_Str);

                                if (Totol_Hours_Check_1 < Totol_Hours_Check)
                                {
                                    Random_No_Get = GetRandom(Random_No_Fixed, 15);
                                    DateTime NewDT = Convert.ToDateTime(Time_IN_Get);
                                    NewDT = NewDT.AddHours(8);
                                    NewDT = NewDT.AddMinutes(Random_No_Get);

                                    Time_Out_Update = String.Format("{0:hh:mm tt}", NewDT);
                                    // AutoDTable.Rows[iRow2][8] = Time_Out_Update;
                                    DateTime MDate1 = Convert.ToDateTime(Time_IN_Get);
                                    DateTime MDate2 = Convert.ToDateTime(Time_Out_Update);

                                    TimeSpan ts11;
                                    if (MDate2 > MDate1)
                                    {
                                        ts11 = MDate2.Subtract(MDate1);
                                    }
                                    else
                                    {
                                        ts11 = NewDT.Subtract(MDate1);
                                    }

                                    string s2 = ts11.Minutes.ToString();

                                    if (s2.Length == 1)
                                    {
                                        string ts_str = "0" + ts11.Minutes;
                                        Emp_Total_Work_Time_1 = ts11.Hours + ":" + ts_str;

                                        Totaltime8above = ts11.Hours.ToString();

                                        time_Check_dbl = Convert.ToInt32(Totaltime8above);
                                    }
                                    else
                                    {
                                        Emp_Total_Work_Time_1 = ts11.Hours.ToString() + ":" + ts11.Minutes.ToString();


                                        Totaltime8above = ts11.Hours.ToString();

                                        time_Check_dbl = Convert.ToInt32(Totaltime8above);

                                    }
                                }



                            }
                        }
                        //Emp_Total_Work_Time

                        string StdWorkingHrs = "0";

                        if (Emp_DS.Rows[intRow]["StdWrkHrs"].ToString() != "" && Emp_DS.Rows[intRow]["StdWrkHrs"].ToString() != "0")
                        {
                            StdWorkingHrs = Emp_DS.Rows[intRow]["StdWrkHrs"].ToString();

                            string StdHrs = (Convert.ToDecimal(StdWorkingHrs) + Convert.ToDecimal(1)).ToString();

                            if (Convert.ToDouble(time_Check_dbl) >= Convert.ToDouble(StdHrs))
                            {
                                OT_Time = OT_Time + (time_Check_dbl - Convert.ToDouble(StdWorkingHrs));

                            }
                            else
                            {
                                OT_Time = 0;
                            }
                        }
                        else
                        {

                            if (time_Check_dbl >= 9)
                            {
                                OT_Time = OT_Time + (time_Check_dbl - 8);

                            }
                            else
                            {
                                OT_Time = 0;
                            }
                        }


                        string Emp_Total_Work_Time = "";
                        string Final_OT_Work_Time = "00:00";
                        string Final_OT_Work_Time_Val = "00:00";
                        Emp_Total_Work_Time = Emp_Total_Work_Time_1;
                        //Find Week OFF
                        string Employee_Week_Name = "";
                        string Assign_Week_Name = "";
                        mLocalDS = null;
                        Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                        Assign_Week_Name = Emp_WH_Day;

                        //SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "' and  Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "'";
                        //mLocalDS = ManualAttend.GetDetails(SSQL);
                        //if (mLocalDS.Rows.Count <= 0)
                        //{
                        //    Assign_Week_Name = "";
                        //}
                        //else
                        //{
                        //    //Assign_Week_Name = mLocalDS.Tables(0).Rows(0)(0)
                        //    Assign_Week_Name = "";
                        //}


                        Boolean NFH_Day_Check = false;
                        DateTime NFH_Date;
                        DateTime DOJ_Date_Format;
                        DateTime Date_Value = new DateTime();
                        DataTable DT_NFH = new DataTable();
                        Date_Value = Convert.ToDateTime(Date_Value_Str.ToString());
                        string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value.ToString("yyyy/MM/dd") + "'";
                        DT_NFH = ManualAttend.GetDetails(qry_nfh);

                        if (DT_NFH.Rows.Count != 0)
                        {
                            NFH_Day_Check = true;
                            NFH_Days_Count = NFH_Days_Count + 1;
                        }

                        if (Employee_Week_Name == Assign_Week_Name)
                        {
                            ////Get Employee Work Time
                            //double Calculate_Attd_Work_Time = 8;
                            //double Calculate_Attd_Work_Time_half = 0;
                            //Weekoff_Count = Weekoff_Count + 1;

                            //Calculate_Attd_Work_Time_half = 4.0;

                            //halfPresent = "0";//Half Days Calculate Start
                            //if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            //{
                            //    // Weekoff_Count = Weekoff_Count + 1;
                            //    isWHPresent = true;
                            //    halfPresent = "1";

                            //}
                            ////Half Days Calculate End
                            //if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            //{
                            //    //Weekoff_Count = Weekoff_Count + 1;
                            //    isWHPresent = true;
                            //    halfPresent = "0";

                            //}
                            INTime = "";
                            OUTTime = "";
                            time_Check_dbl = 0;
                            Emp_Total_Work_Time_1 = "00:00";


                        }
                        else if (Employee_Week_Name == Emp_WH_Day)
                        {
                            ////Get Employee Work Time
                            //double Calculate_Attd_Work_Time = 8;
                            //double Calculate_Attd_Work_Time_half = 0;


                            //Calculate_Attd_Work_Time_half = 4.0;

                            //Weekoff_Count = Weekoff_Count + 1;

                            //halfPresent = "0";//Half Days Calculate Start
                            //if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            //{
                            //    isWHPresent = true;
                            //    halfPresent = "1";
                            //    //Weekoff_Count = Weekoff_Count + 1;


                            //}
                            ////Half Days Calculate End
                            //if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            //{
                            //    isWHPresent = true;
                            //    halfPresent = "0";
                            //    //Weekoff_Count = Weekoff_Count + 1;


                            //}

                            INTime = "";
                            OUTTime = "";
                            time_Check_dbl = 0;
                            Emp_Total_Work_Time_1 = "00:00";

                        }
                        else
                        {
                            //Get Employee Work Time
                            double Calculate_Attd_Work_Time = 8;
                            double Calculate_Attd_Work_Time_half = 0;


                            Calculate_Attd_Work_Time_half = 4.0;

                            halfPresent = "0";//Half Days Calculate Start
                            if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            {
                                isPresent = true;
                                halfPresent = "1";
                            }
                            //Half Days Calculate End
                            if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            {
                                isPresent = true;
                                halfPresent = "0";
                            }
                        }
                        //Shift Check Code Start
                        //DataSet Shift_Ds = new DataSet();

                        DataTable Shift_Ds = new DataTable();

                        string Start_IN = "";
                        string End_In = "";
                        string Shift_Name_Store = "";
                        bool Shift_Check_blb_Check = false;
                        DateTime ShiftdateStartIN_Check = new DateTime();
                        DateTime ShiftdateEndIN_Check = new DateTime();
                        DateTime EmpdateIN_Check = new DateTime();
                        string Wages = "";
                        if (INTime != "")
                        {
                            Shift_Check_blb_Check = false;

                            string SS = "GENERAL";
                            Wages = Emp_DS.Rows[intRow]["ShiftType"].ToString();

                            if (Wages.ToUpper() == SS.ToUpper())
                            {
                                Employee_Shift_Name_DB = "GENERAL";
                            }
                            else
                            {
                                //Shift Master Check Code Start
                                SSQL = "Select * from Shift_Mst where  ShiftDesc <> 'GENERAL' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' Order by ShiftDesc Asc";
                                //  "CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'";
                                Shift_Ds = ManualAttend.GetDetails(SSQL);
                                if (Shift_Ds.Rows.Count != 0)
                                {
                                    SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%' ";
                                    SSQL = SSQL + "And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    //CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "' And 
                                    Shift_Ds = ManualAttend.GetDetails(SSQL);
                                    Shift_Check_blb = false;
                                    for (int f = 0; f < Shift_Ds.Rows.Count; f++)
                                    {

                                        int StartIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["StartIN_Days"]);
                                        int EndIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["EndIN_Days"]);
                                        Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(StartIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["StartIN"].ToString();
                                        End_In = Convert.ToDateTime(Date_Value_Str).AddDays(EndIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["EndIN"].ToString();
                                        ShiftdateStartIN_Check = System.Convert.ToDateTime(Start_IN);
                                        ShiftdateEndIN_Check = System.Convert.ToDateTime(End_In);
                                        EmpdateIN_Check = System.Convert.ToDateTime(Employee_Punch);
                                        if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                                        {
                                            Employee_Shift_Name_DB = Shift_Ds.Rows[f]["ShiftDesc"].ToString();
                                            Shift_Check_blb_Check = true;
                                        }

                                    }
                                    if (Shift_Check_blb_Check == false)
                                    {
                                        Employee_Shift_Name_DB = "No Shift";
                                    }

                                }
                                else
                                {
                                    Employee_Shift_Name_DB = "No Shift";
                                }
                                //Shift Master Check Code End  
                            }
                        }
                        else
                        {
                            Employee_Shift_Name_DB = "No Shift";
                        }
                        //Shift Check Code End


                        string ShiftType = "";
                        string Pre_Abs = "";
                        if (isPresent == true)
                        {

                            if (Employee_Shift_Name_DB != "No Shift")
                            {
                                ShiftType = "Proper";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }
                            else
                            {
                                if (Wages == "GENERAL")
                                {
                                    ShiftType = "GENERAL Mismatch";
                                }
                                else
                                {
                                    ShiftType = "Mismatch";
                                }

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }


                            if (halfPresent == "1")
                            {
                                Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_Count = Present_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
                                }
                            }
                            else if (halfPresent == "0")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                                }
                            }

                        }
                        else if (isWHPresent == true)
                        {
                            if (Employee_Shift_Name_DB != "No Shift")
                            {
                                ShiftType = "Proper";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }
                            else
                            {
                                if (Wages == "GENERAL")
                                {
                                    ShiftType = "GENERAL Mismatch";
                                }
                                else
                                {
                                    ShiftType = "Mismatch";
                                }

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }


                            }

                            if (halfPresent == "1")
                            {
                                Present_WH_Count = Convert.ToDecimal(Present_WH_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_WH_Count = Present_WH_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_Count = Present_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
                                }
                            }
                            else if (halfPresent == "0")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                                }
                            }
                        }
                        else
                        {
                            Appsent_Count = Appsent_Count + 1;


                            if (INTime == "" && OUTTime == "")
                            {
                                ShiftType = "Leave";
                                Employee_Shift_Name_DB = "Leave";
                                Pre_Abs = "Leave";

                            }
                            else if (INTime == "")
                            {
                                if (OUTTime != "")
                                {
                                    ShiftType = "Improper";
                                }
                                Pre_Abs = "Absent";

                            }
                            else if (OUTTime == "")
                            {
                                if (INTime != "")
                                {
                                    ShiftType = "Improper";
                                }
                                Pre_Abs = "Absent";

                            }
                            else if (INTime != "" && OUTTime != "")
                            {
                                ShiftType = "Absent";
                                Pre_Abs = "Absent";

                            }


                        }

                        intK += 1;




                        //Update Employee Worked_Days
                        //DataSet mDataSet = new DataSet();
                        DataTable mDataSet = new DataTable();
                        string Queryss = "";
                        DataSet Del_ds = new DataSet();
                        Queryss = "Select * from IFLogTime_Days where MachineID='" + MachineID1 + "' and  Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";

                        mDataSet = ManualAttend.GetDetails(Queryss);

                        //mDataSet = objdata.read(Queryss);  Emp_WH_Day
                        if (mDataSet.Rows.Count != 0)
                        {
                            Queryss = "Delete from IFLogTime_Days where MachineID='" + MachineID1 + "' and Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                            ManualAttend.GetDetails(Queryss);
                        }

                        DataSet Insert_Ds = new DataSet();

                        Queryss = "";
                        Queryss = "Insert Into IFLogTime_Days(CompCode,LocCode,MachineID,ExistingCode,FirstName,LastName,DeptName," +
                        "Designation,DOJ,Wages,CatName,SubCatName,Grade,BasicSalary,Present,Wh_Check,Wh_Count,Wh_Present_Count,NFH_Count,NFH_Present_Count,Shift,TimeIN,TimeOUT,Total_Hrs,OTHours,Attn_Date_Str,Attn_Date,MachineID_Encrypt,Total_Hrs1,TypeName,Present_Absent)" +
                         " Values('" + SessionCcode + "','" + SessionLcode + "','" + Emp_DS.Rows[intRow]["MachineID"] + "','" + Emp_DS.Rows[intRow]["ExistingCode"] + "'," +
                        "'" + Emp_DS.Rows[intRow]["FirstName"] + "','" + Emp_DS.Rows[intRow]["MiddleInitial"] + "'," +
                         "'" + Emp_DS.Rows[intRow]["DeptName"] + "','" + Emp_DS.Rows[intRow]["Designation"] + "'," +
                         "'" + Emp_DS.Rows[intRow]["DOJ"] + "','" + Emp_DS.Rows[intRow]["Wages"] + "','" + Emp_DS.Rows[intRow]["CatName"] + "','" + Emp_DS.Rows[intRow]["SubCatName"] + "','" + Emp_DS.Rows[intRow]["Grade"].ToString() + "','" + Emp_DS.Rows[intRow]["BasicSalary"] + "','" + Present_Count + "'," +
                         "'" + Emp_WH_Day + "','" + Weekoff_Count + "','" + Present_WH_Count + "','" + NFH_Days_Count + "','" + NFH_Days_Present_Count + "'," +
                         "'" + Employee_Shift_Name_DB + "','" + INTime + "','" + OUTTime + "','" + time_Check_dbl + "','" + OT_Time + "','" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "',Convert(datetime,'" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "',120),'" + UTF8Encryption(MachineID1) + "','" + Emp_Total_Work_Time_1 + "','" + ShiftType + "','" + Pre_Abs + "')";

                        ManualAttend.GetDetails(Queryss);

                        Present_Count = 0;
                        Present_WH_Count = 0;
                        Employee_Shift_Name_DB = "No Shift";
                        INTime = "";
                        OUTTime = "";
                        time_Check_dbl = 0;
                        OT_Time = 0;
                        Weekoff_Count = 0;
                        //Emp_WH_Day = "";
                        Emp_Total_Work_Time_1 = "00:00";
                        NFH_Days_Count = 0;
                        NFH_Days_Present_Count = 0;
                    }

                    intI += 1; ;
                    Total_Days_Count = 0;
                    Final_Count = 0;
                    Appsent_Count = 0;
                    Present_Count = 0;
                    Present_WH_Count = 0;
                    NFH_Days_Count = 0;
                    NFH_Days_Present_Count = 0;
                    INTime = "";
                    OUTTime = "";
                    Weekoff_Count = 0;
                    Emp_WH_Day = "";
                }



                //Calculation for Employee not register in Employee Mst
                DataTable DT_TempEmp = new DataTable();

                SSQL = "Select EmpNo from Temp_Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                DT_TempEmp = ManualAttend.GetDetails(SSQL);

                for (int Empk = 0; Empk < DT_TempEmp.Rows.Count; Empk++)
                {
                    intK = 1;
                    string Emp_Total_Work_Time_1 = "00:00";
                    //Get Employee Week OF DAY
                    DataTable DS_WH = new DataTable();
                    string Emp_WH_Day = "";
                    string DOJ_Date_Str = "";
                    string MachineID1 = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();


                    int colIndex = intK;
                    string DayofWeekName = "";
                    decimal Weekoff_Count = 0;
                    bool isPresent = false;
                    bool isWHPresent = false;
                    decimal Present_Count = 0;
                    decimal Present_WH_Count = 0;
                    int Appsent_Count = 0;
                    decimal Final_Count = 0;
                    decimal Total_Days_Count = 0;
                    string Already_Date_Check;
                    int Days_Insert_RowVal = 0;
                    decimal FullNight_Shift_Count = 0;
                    int NFH_Days_Count = 0;
                    decimal NFH_Days_Present_Count = 0;
                    Already_Date_Check = "";
                    string Year_Check = "";
                    string Time_IN_Str = "";
                    string Time_Out_Str = "";
                    int g = 1;

                    double OT_Time;

                    OT_Time = 0;

                    string INTime = "";
                    string OUTTime = "";

                    DateTime Date11 = Convert.ToDateTime(FromDate);
                    string dat = ToDate;
                    DateTime Date12 = Convert.ToDateTime(dat);

                    int daycount = (int)((Date12 - Date11).TotalDays);

                    daysAdded = daycount + 1;

                    //daysAdded = 1;

                    for (intCol = 0; (intCol <= (daysAdded - 1)); intCol++)
                    {
                        isPresent = false;
                        isWHPresent = false;
                        //string Emp_Total_Work_Time_1 = "00:00";
                        string Machine_ID_Str = "";
                        string OT_Week_OFF_Machine_No;
                        string Date_Value_Str = "";
                        string Total_Time_get = "";
                        string Final_OT_Work_Time_1 = "";
                        //DataSet mLocalDS = new DataSet();

                        //DataTable mLocalDS = new DataTable();


                        int j = 0;
                        double time_Check_dbl = 0;
                        string Date_Value_Str1 = "";
                        string Employee_Shift_Name_DB = "No Shift";
                        isPresent = false;


                        //Shift Change Check Variable Declaration Start
                        DateTime InTime_Check = new DateTime();
                        DateTime InToTime_Check = new DateTime();
                        TimeSpan InTime_TimeSpan;
                        string From_Time_Str = "";
                        string To_Time_Str = "";
                        //DataSet DS_InTime = new DataSet();
                        DataTable DS_Time = new DataTable();
                        //DataSet DS_Time = new DataSet();
                        DataTable DS_InTime = new DataTable();
                        DateTime EmpdateIN_Change = new DateTime();
                        DataTable InsertData = new DataTable();
                        string Final_InTime = "";
                        string Final_OutTime = "";
                        string Final_Shift = "";

                        int K = 0;
                        bool Shift_Check_blb = false;
                        string Shift_Start_Time_Change;
                        string Shift_End_Time_Change;
                        string Employee_Time_Change = "";
                        DateTime ShiftdateStartIN_Change = new DateTime();
                        DateTime ShiftdateEndIN_Change = new DateTime();

                        //Shift Change Check Variable Declaration End
                        string Employee_Punch = "";
                        Spin_Machine_ID_Str = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();

                        if (Spin_Machine_ID_Str == "106272")
                        {
                            Spin_Machine_ID_Str = "106272";
                        }

                        // Machine_ID_Str = Encryption(Emp_DS.Rows[intRow]["MachineID"]);
                        Machine_ID_Str = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();
                        OT_Week_OFF_Machine_No = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();
                        Date_Value_Str = Convert.ToDateTime(FromDate).AddDays(intCol).ToShortDateString();
                        Date_Value_Str1 = Convert.ToDateTime(Date_Value_Str).AddDays(1).ToShortDateString();

                        //DateTime dt = Convert.ToDateTime(Date.ToString());

                        //DayofWeekName = dt.DayOfWeek.ToString();

                        //TimeIN Get

                        SSQL = "";
                        SSQL = "Select distinct TimeIN,CONVERT(char(5), TimeIN, 108) as TimeIN24 from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                              " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";

                        mLocalDS = ManualAttend.GetDetails(SSQL);

                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";

                            INTime = "";

                        }
                        else
                        {
                            DateTime datetime1 = Convert.ToDateTime(mLocalDS.Rows[0][0].ToString());
                            INTime = datetime1.ToString("hh:mm tt");
                            //INTime = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0].ToString());
                        }

                        //TimeOUT Get
                        SSQL = "";
                        SSQL = "Select TimeOUT,CONVERT(char(5), TimeOUT, 108) as TimeOUT24 from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                               " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                        mLocalDS1 = ManualAttend.GetDetails(SSQL);

                        if (mLocalDS1.Rows.Count <= 0)
                        {
                            Time_Out_Str = "";

                            OUTTime = "";
                        }
                        else
                        {
                            DateTime datetime = Convert.ToDateTime(mLocalDS1.Rows[0][0].ToString());
                            OUTTime = datetime.ToString("hh:mm tt");
                            //OUTTime = string.Format(Convert.ToString(datetime), "HH:mm:ss tt");
                            //OUTTime = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0].ToString());
                        }

                        //Shift Change Check Start
                        if (mLocalDS.Rows.Count != 0)
                        {
                            InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
                            InToTime_Check = InTime_Check.AddHours(2);



                            InTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InTime_Check), "HH:mm:ss"));
                            InToTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InToTime_Check), "HH:mm:ss"));




                            //Old Time check error for 24 format

                            string TimeIN = InTime_Check.ToString();
                            string[] TimeIN_Split = TimeIN.Split(' ');
                            string TimeIN1 = TimeIN_Split[1].ToString();
                            string[] final = TimeIN1.Split(':');
                            string Final_IN = "";
                            if (Convert.ToInt32(final[0]) >= 10)
                            {
                                Final_IN = final[0] + ":" + final[1];
                            }
                            else
                            {
                                Final_IN = "0" + final[0] + ":" + final[1];
                            }


                            string TimeOUT = InToTime_Check.ToString();
                            string[] TimeOUT_Split = TimeOUT.Split(' ');
                            string TimeOUT1 = TimeOUT_Split[1].ToString();
                            string[] final1 = TimeOUT1.Split(':');
                            string Final_IN1 = "";
                            if (Convert.ToInt32(final1[0]) >= 10)
                            {
                                Final_IN1 = final1[0] + ":" + final1[1];
                            }
                            else
                            {
                                Final_IN1 = "0" + final1[0] + ":" + final1[1];
                            }



                            //Two Hours OutTime Check
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";


                            DS_Time = ManualAttend.GetDetails(SSQL);
                            if (DS_Time.Rows.Count != 0)
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                DS_InTime = ManualAttend.GetDetails(SSQL);
                                if (DS_InTime.Rows.Count != 0)
                                {
                                    //DataSet Shift_DS_Change = new DataSet();

                                    DataTable Shift_DS_Change = new DataTable();

                                    Final_InTime = DS_InTime.Rows[0]["TimeIN"].ToString();
                                    //Check With IN Time Shift
                                    SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%'";
                                    SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    Shift_DS_Change = ManualAttend.GetDetails(SSQL);
                                    Shift_Check_blb = false;
                                    for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
                                    {
                                        Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
                                        if (Shift_DS_Change.Rows[k]["EndIN_Days"].ToString() == "1")
                                        {
                                            Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                        }
                                        else
                                        {
                                            Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                                        }

                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);

                                        if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                            Shift_Check_blb = true;
                                            break;
                                        }
                                        ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                        ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                        EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                        if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                        {
                                            Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                            Shift_Check_blb = true;
                                            break;
                                        }
                                    }
                                    if (Shift_Check_blb == true)
                                    {

                                        //IN Time Query Update
                                        string Querys = "";
                                        Querys = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                        " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                        mLocalDS = ManualAttend.GetDetails(Querys);

                                        if (Final_Shift == "SHIFT1")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                            " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        }
                                        else if (Final_Shift == "SHIFT2")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                                   " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        }
                                        else if (Final_Shift == "SHIFT3")
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

                                        }
                                        else
                                        {
                                            Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

                                        }


                                        mLocalDS1 = ManualAttend.GetDetails(Querys);
                                    }
                                    else
                                    {
                                        //Get Employee In Time
                                        SSQL = "";
                                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                               " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                        mLocalDS = ManualAttend.GetDetails(SSQL);
                                        //TimeOUT Get
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                               " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                        mLocalDS1 = ManualAttend.GetDetails(SSQL);
                                    }
                                }
                                else
                                {
                                    //Get Employee In Time
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                           " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                    mLocalDS = ManualAttend.GetDetails(SSQL);

                                    //TimeOUT Get
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                          " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                    mLocalDS1 = ManualAttend.GetDetails(SSQL);

                                }
                            }
                            else
                            {
                                //Get Employee In Time
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                       " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                mLocalDS = ManualAttend.GetDetails(SSQL);

                                //TimeOUT Get
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                      " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
                                mLocalDS1 = ManualAttend.GetDetails(SSQL);
                            }
                        }
                        //Shift Change Check End 
                        if (mLocalDS.Rows.Count == 0)
                        {
                            Employee_Punch = "";
                        }
                        else
                        {
                            Employee_Punch = mLocalDS.Rows[0][0].ToString();
                        }
                        if (mLocalDS.Rows.Count >= 1)
                        {
                            for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                if (mLocalDS1.Rows.Count > tin)
                                {
                                    Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                                }
                                else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                                {
                                    Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                                }
                                else
                                {
                                    Time_Out_Str = "";
                                }
                                TimeSpan ts4 = new TimeSpan();
                                ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                                if (mLocalDS.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "";
                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                }
                                if (Time_IN_Str == "" || Time_Out_Str == "")
                                {
                                    time_Check_dbl = time_Check_dbl;
                                }
                                else
                                {


                                    DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                    DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                    TimeSpan ts = new TimeSpan();
                                    ts = date2.Subtract(date1);


                                    //& ":" & Trim(ts.Minutes)
                                    Total_Time_get = (ts.Hours).ToString();
                                    ts4 = ts4.Add(ts);

                                    //OT Time Get
                                    Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                    Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);

                                    if (Left_Val(ts4.Minutes.ToString(), 1) == "-" || Left_Val(ts4.Hours.ToString(), 1) == "-" || Emp_Total_Work_Time_1 == "0:-1")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                    }
                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {

                                        date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                        ts = date2.Subtract(date1);
                                        ts = date2.Subtract(date1);
                                        Total_Time_get = (ts.Hours).ToString();
                                        time_Check_dbl = double.Parse(Total_Time_get);
                                        Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes).ToString();
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:1" || Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }

                                    }
                                    else
                                    {
                                        time_Check_dbl = time_Check_dbl + double.Parse(Total_Time_get);
                                    }



                                    //Working Hours 8 Fixed Total Hours Time_CHeck 8.20 Start

                                    Time_Split_Str = Emp_Total_Work_Time_1.Split(':');
                                    Time_IN_Get = Time_IN_Str.ToString();

                                    Time_Out_Update = "";
                                    string TimeCheck_Str = Time_Split_Str[0] + "." + Time_Split_Str[1];
                                    Totol_Hours_Check = Convert.ToDouble(TimeCheck_Str);

                                    if (Totol_Hours_Check_1 < Totol_Hours_Check)
                                    {
                                        Random_No_Get = GetRandom(Random_No_Fixed, 15);
                                        DateTime NewDT = Convert.ToDateTime(Time_IN_Get);
                                        NewDT = NewDT.AddHours(8);
                                        NewDT = NewDT.AddMinutes(Random_No_Get);

                                        Time_Out_Update = String.Format("{0:hh:mm tt}", NewDT);
                                        // AutoDTable.Rows[iRow2][8] = Time_Out_Update;
                                        DateTime MDate1 = Convert.ToDateTime(Time_IN_Get);
                                        DateTime MDate2 = Convert.ToDateTime(Time_Out_Update);

                                        TimeSpan ts11;
                                        if (MDate2 > MDate1)
                                        {
                                            ts11 = MDate2.Subtract(MDate1);
                                        }
                                        else
                                        {
                                            ts11 = NewDT.Subtract(MDate1);
                                        }

                                        string s2 = ts11.Minutes.ToString();
                                        if (s2.Length == 1)
                                        {
                                            string ts_str = "0" + ts11.Minutes;
                                            Emp_Total_Work_Time_1 = ts11.Hours + ":" + ts_str;

                                            Totaltime8above = ts11.Hours.ToString();

                                            time_Check_dbl = Convert.ToInt32(Totaltime8above);
                                        }
                                        else
                                        {
                                            Emp_Total_Work_Time_1 = ts11.Hours.ToString() + ":" + ts11.Minutes.ToString();


                                            Totaltime8above = ts11.Hours.ToString();

                                            time_Check_dbl = Convert.ToInt32(Totaltime8above);

                                        }
                                    }

                                }

                                break;

                            }//For End
                        }
                        else
                        {
                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;

                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                            }
                            for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
                            {
                                if (mLocalDS1.Rows.Count <= 0)
                                {
                                    Time_Out_Str = "";
                                }
                                else
                                {
                                    Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                                }

                                break;

                            }
                            //Emp_Total_Work_Time
                            if (Time_IN_Str == "" || Time_Out_Str == "")
                            {
                                time_Check_dbl = 0;
                            }
                            else
                            {
                                DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();
                                ts = date2.Subtract(date1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = (ts.Hours).ToString();
                                //OT Time Get
                                Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {

                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = ts.Hours.ToString();
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                    Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes);
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                        time_Check_dbl = 0;
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                        time_Check_dbl = 0;
                                    }
                                }
                                else
                                {
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                }




                                //Working Hours 8 Fixed Total Hours Time_CHeck 8.20 Start

                                Time_Split_Str = Emp_Total_Work_Time_1.Split(':');
                                Time_IN_Get = Time_IN_Str.ToString();

                                Time_Out_Update = "";
                                string TimeCheck_Str = Time_Split_Str[0] + "." + Time_Split_Str[1];
                                Totol_Hours_Check = Convert.ToDouble(TimeCheck_Str);

                                if (Totol_Hours_Check_1 < Totol_Hours_Check)
                                {
                                    Random_No_Get = GetRandom(Random_No_Fixed, 15);
                                    DateTime NewDT = Convert.ToDateTime(Time_IN_Get);
                                    NewDT = NewDT.AddHours(8);
                                    NewDT = NewDT.AddMinutes(Random_No_Get);

                                    Time_Out_Update = String.Format("{0:hh:mm tt}", NewDT);
                                    // AutoDTable.Rows[iRow2][8] = Time_Out_Update;
                                    DateTime MDate1 = Convert.ToDateTime(Time_IN_Get);
                                    DateTime MDate2 = Convert.ToDateTime(Time_Out_Update);

                                    TimeSpan ts11;
                                    if (MDate2 > MDate1)
                                    {
                                        ts11 = MDate2.Subtract(MDate1);
                                    }
                                    else
                                    {
                                        ts11 = NewDT.Subtract(MDate1);
                                    }

                                    string s2 = ts11.Minutes.ToString();
                                    if (s2.Length == 1)
                                    {
                                        string ts_str = "0" + ts11.Minutes;
                                        Emp_Total_Work_Time_1 = ts11.Hours + ":" + ts_str;

                                        Totaltime8above = ts11.Hours.ToString();

                                        time_Check_dbl = Convert.ToInt32(Totaltime8above);
                                    }
                                    else
                                    {
                                        Emp_Total_Work_Time_1 = ts11.Hours.ToString() + ":" + ts11.Minutes.ToString();


                                        Totaltime8above = ts11.Hours.ToString();

                                        time_Check_dbl = Convert.ToInt32(Totaltime8above);

                                    }
                                }


                            }
                        }
                        //Emp_Total_Work_Time

                        string StdWorkingHrs = "0";

                        if (time_Check_dbl >= 9)
                        {
                            OT_Time = OT_Time + (time_Check_dbl - 8);

                        }
                        else
                        {
                            OT_Time = 0;
                        }



                        string Emp_Total_Work_Time = "";
                        string Final_OT_Work_Time = "00:00";
                        string Final_OT_Work_Time_Val = "00:00";
                        Emp_Total_Work_Time = Emp_Total_Work_Time_1;
                        //Find Week OFF
                        string Employee_Week_Name = "";
                        string Assign_Week_Name = "";
                        mLocalDS = null;
                        Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();

                        //SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "' and  Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "'";
                        //mLocalDS = ManualAttend.GetDetails(SSQL);
                        //if (mLocalDS.Rows.Count <= 0)
                        //{
                        //    Assign_Week_Name = "";
                        //}
                        //else
                        //{
                        //    //Assign_Week_Name = mLocalDS.Tables(0).Rows(0)(0)
                        //    Assign_Week_Name = "";
                        //}

                        Boolean NFH_Day_Check = false;
                        DateTime NFH_Date;
                        DateTime DOJ_Date_Format;
                        DateTime Date_Value = new DateTime();
                        DataTable DT_NFH = new DataTable();
                        Date_Value = Convert.ToDateTime(Date_Value_Str.ToString());


                        if (Employee_Week_Name == Assign_Week_Name)
                        {
                            ////Get Employee Work Time
                            //double Calculate_Attd_Work_Time = 8;
                            //double Calculate_Attd_Work_Time_half = 0;


                            //Calculate_Attd_Work_Time_half = 4.0;

                            //halfPresent = "0";//Half Days Calculate Start
                            //if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            //{
                            //    Weekoff_Count = Weekoff_Count + 1;
                            //    isWHPresent = true;
                            //    halfPresent = "1";

                            //}
                            ////Half Days Calculate End
                            //if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            //{
                            //    Weekoff_Count = Weekoff_Count + 1;
                            //    isWHPresent = true;
                            //    halfPresent = "0";

                            //}
                            INTime = "";
                            OUTTime = "";
                            time_Check_dbl = 0;
                            Emp_Total_Work_Time_1 = "00:00";

                        }
                        else if (Employee_Week_Name == Emp_WH_Day)
                        {
                            //    //Get Employee Work Time
                            //    double Calculate_Attd_Work_Time = 8;
                            //    double Calculate_Attd_Work_Time_half = 0;


                            //    Calculate_Attd_Work_Time_half = 4.0;

                            //    halfPresent = "0";//Half Days Calculate Start
                            //    if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            //    {
                            //        isWHPresent = true;
                            //        halfPresent = "1";
                            //        Weekoff_Count = Weekoff_Count + 1;


                            //    }
                            //    //Half Days Calculate End
                            //    if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            //    {
                            //        isWHPresent = true;
                            //        halfPresent = "0";
                            //        Weekoff_Count = Weekoff_Count + 1;


                            //    }

                            INTime = "";
                            OUTTime = "";
                            time_Check_dbl = 0;
                            Emp_Total_Work_Time_1 = "00:00";

                        }
                        else
                        {
                            //Get Employee Work Time
                            double Calculate_Attd_Work_Time = 8;
                            double Calculate_Attd_Work_Time_half = 0;


                            Calculate_Attd_Work_Time_half = 4.0;

                            halfPresent = "0";//Half Days Calculate Start
                            if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            {
                                isPresent = true;
                                halfPresent = "1";
                            }
                            //Half Days Calculate End
                            if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            {
                                isPresent = true;
                                halfPresent = "0";
                            }
                        }
                        //Shift Check Code Start
                        //DataSet Shift_Ds = new DataSet();

                        DataTable Shift_Ds = new DataTable();

                        string Start_IN = "";
                        string End_In = "";
                        string Shift_Name_Store = "";
                        bool Shift_Check_blb_Check = false;
                        DateTime ShiftdateStartIN_Check = new DateTime();
                        DateTime ShiftdateEndIN_Check = new DateTime();
                        DateTime EmpdateIN_Check = new DateTime();
                        if (INTime != "")
                        {
                            Shift_Check_blb_Check = false;

                            //Shift Master Check Code Start
                            SSQL = "Select * from Shift_Mst where  ShiftDesc <> 'GENERAL' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' Order by ShiftDesc Asc";
                            //  "CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'";
                            Shift_Ds = ManualAttend.GetDetails(SSQL);
                            if (Shift_Ds.Rows.Count != 0)
                            {
                                SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%' ";
                                SSQL = SSQL + "And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                //CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "' And 
                                Shift_Ds = ManualAttend.GetDetails(SSQL);
                                Shift_Check_blb = false;
                                for (int f = 0; f < Shift_Ds.Rows.Count; f++)
                                {

                                    int StartIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["StartIN_Days"]);
                                    int EndIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["EndIN_Days"]);
                                    Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(StartIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["StartIN"].ToString();
                                    End_In = Convert.ToDateTime(Date_Value_Str).AddDays(EndIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["EndIN"].ToString();
                                    ShiftdateStartIN_Check = System.Convert.ToDateTime(Start_IN);
                                    ShiftdateEndIN_Check = System.Convert.ToDateTime(End_In);
                                    EmpdateIN_Check = System.Convert.ToDateTime(Employee_Punch);
                                    if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                                    {
                                        Employee_Shift_Name_DB = Shift_Ds.Rows[f]["ShiftDesc"].ToString();
                                        Shift_Check_blb_Check = true;
                                    }

                                }
                                if (Shift_Check_blb_Check == false)
                                {
                                    Employee_Shift_Name_DB = "No Shift";
                                }

                            }
                            else
                            {
                                Employee_Shift_Name_DB = "No Shift";
                            }
                            //Shift Master Check Code End  

                        }
                        else
                        {
                            Employee_Shift_Name_DB = "No Shift";
                        }
                        //Shift Check Code End
                        string ShiftType = "";
                        string Pre_Abs = "";
                        if (isPresent == true)
                        {

                            if (Employee_Shift_Name_DB != "No Shift")
                            {
                                ShiftType = "Proper";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }
                            else
                            {
                                ShiftType = "Mismatch";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }

                            if (halfPresent == "1")
                            {
                                Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_Count = Present_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
                                }
                            }
                            else if (halfPresent == "0")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                                }
                            }

                        }
                        else if (isWHPresent == true)
                        {

                            if (Employee_Shift_Name_DB != "No Shift")
                            {
                                ShiftType = "Proper";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }
                            else
                            {

                                ShiftType = "Mismatch";

                                if (halfPresent == "1")
                                {
                                    Pre_Abs = "Half Day";
                                }
                                else
                                {
                                    Pre_Abs = "Present";
                                }

                            }

                            if (halfPresent == "1")
                            {
                                Present_WH_Count = Convert.ToDecimal(Present_WH_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_WH_Count = Present_WH_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_Count = Present_Count + 1;
                            }

                            if (halfPresent == "1")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
                                }
                            }
                            else if (halfPresent == "0")
                            {
                                if (NFH_Day_Check == true)
                                {
                                    NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                                }
                            }
                        }
                        else
                        {
                            Appsent_Count = Appsent_Count + 1;

                            if (INTime == "" && OUTTime == "")
                            {
                                ShiftType = "Leave";
                                Employee_Shift_Name_DB = "Leave";
                                Pre_Abs = "Leave";

                            }
                            else if (INTime == "")
                            {
                                if (OUTTime != "")
                                {
                                    ShiftType = "Improper";
                                }
                                Pre_Abs = "Absent";

                            }
                            else if (OUTTime == "")
                            {
                                if (INTime != "")
                                {
                                    ShiftType = "Improper";
                                }
                                Pre_Abs = "Absent";

                            }
                            else if (INTime != "" && OUTTime != "")
                            {
                                ShiftType = "Absent";
                                Pre_Abs = "Absent";

                            }
                        }

                        intK += 1;




                        //Update Employee Worked_Days
                        //DataSet mDataSet = new DataSet();
                        DataTable mDataSet = new DataTable();
                        string Queryss = "";
                        DataSet Del_ds = new DataSet();
                        Queryss = "Select * from IFLogTime_Days where MachineID='" + MachineID1 + "' and  Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";

                        mDataSet = ManualAttend.GetDetails(Queryss);

                        //mDataSet = objdata.read(Queryss);  Emp_WH_Day
                        if (mDataSet.Rows.Count != 0)
                        {
                            Queryss = "Delete from IFLogTime_Days where MachineID='" + MachineID1 + "' and Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                            ManualAttend.GetDetails(Queryss);
                        }
                        DataSet Insert_Ds = new DataSet();

                        Queryss = "";
                        Queryss = "Insert Into IFLogTime_Days(CompCode,LocCode,MachineID,ExistingCode,Present,Wh_Check,Wh_Count,";
                        Queryss = Queryss + "Wh_Present_Count,NFH_Count,NFH_Present_Count,Shift,TimeIN,TimeOUT,Total_Hrs,OTHours,";
                        Queryss = Queryss + "Attn_Date_Str,Attn_Date,MachineID_Encrypt,Total_Hrs1,TypeName,Present_Absent)" +
                         " Values('" + SessionCcode + "','" + SessionLcode + "','" + MachineID1 + "','" + MachineID1 + "','" + Present_Count + "'," +
                         "'" + Emp_WH_Day + "','" + Weekoff_Count + "','" + Present_WH_Count + "','" + NFH_Days_Count + "','" + NFH_Days_Present_Count + "'," +
                         "'" + Employee_Shift_Name_DB + "','" + INTime + "','" + OUTTime + "','" + time_Check_dbl + "','" + OT_Time + "','" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "',Convert(datetime,'" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "',120),'" + UTF8Encryption(MachineID1) + "','" + Emp_Total_Work_Time_1 + "','" + ShiftType + "','" + Pre_Abs + "')";

                        ManualAttend.GetDetails(Queryss);

                        Present_Count = 0;
                        Present_WH_Count = 0;
                        Employee_Shift_Name_DB = "No Shift";
                        INTime = "";
                        OUTTime = "";
                        time_Check_dbl = 0;
                        OT_Time = 0;
                        Weekoff_Count = 0;
                        //Emp_WH_Day = "";
                        Emp_Total_Work_Time_1 = "00:00";
                        NFH_Days_Count = 0;
                        NFH_Days_Present_Count = 0;
                    }

                    intI += 1; ;
                    Total_Days_Count = 0;
                    Final_Count = 0;
                    Appsent_Count = 0;
                    Present_Count = 0;
                    Present_WH_Count = 0;
                    NFH_Days_Count = 0;
                    NFH_Days_Present_Count = 0;
                    INTime = "";
                    OUTTime = "";
                    Weekoff_Count = 0;
                    Emp_WH_Day = "";
                }



            }

        }

        //public JsonResult GetWorkingDaysSaveDB(string Date)
        //{

        //    Status = "";
        //    string[] iStr1;
        //    string[] iStr2;
        //    int intI;
        //    int intK;
        //    int intCol;
        //    string Fin_Year = "";
        //    string Months_Full_Str = "";
        //    string Date_Col_Str = "";
        //    int Month_Int = 1;
        //    string Spin_Machine_ID_Str = "";

        //    string[] Time_Minus_Value_Check;

        //    DataTable mLocalDS = new DataTable();

        //    DataTable mLocalDS1 = new DataTable();

        //    //Attn_Flex Col Add Var
        //    string[] Att_Date_Check;
        //    string Att_Already_Date_Check;
        //    string Att_Year_Check = "";
        //    int Month_Name_Change;
        //    string halfPresent = "0";
        //    int EPay_Total_Days_Get = 0;
        //    intCol = 4;
        //    Month_Name_Change = 1;
        //    int dayCount = 1;
        //    int daysAdded = 1;

        //    EPay_Total_Days_Get = 1;

        //    intI = 2;
        //    intK = 1;

        //    // string EmpNo = MachineNo;

        //    //string MachineID = UTF8Encryption(EmpNo);

        //    string SSQL = "";
        //    bool ErrFlag = false;

        //    DataTable Emp_DS = new DataTable();

        //    SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
        //    SSQL = SSQL + " And IsActive='Yes'";
        //    Emp_DS = ManualAttend.GetDetails(SSQL);

        //    if (Emp_DS.Rows.Count > 0)
        //    {
        //        ErrFlag = false;
        //    }
        //    else
        //    {
        //        ErrFlag = true;
        //    }

        //    if (!ErrFlag)
        //    {
        //        DataTable DT_Chk = new DataTable();
        //        SSQL = "Select * from LogTime_Days where Attn_Date_Str='" + Convert.ToDateTime(Date).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
        //        DT_Chk = ManualAttend.GetDetails(SSQL);
        //        if (DT_Chk.Rows.Count > 0)
        //        {
        //            ErrFlag = true;
        //        }
        //        else
        //        {
        //            ErrFlag = false;
        //        }
        //    }

        //    if (!ErrFlag)
        //    {
        //        for (int intRow = 0; intRow < Emp_DS.Rows.Count; intRow++)
        //        {

        //            intK = 1;
        //            string Emp_Total_Work_Time_1 = "00:00";
        //            //Get Employee Week OF DAY
        //            DataTable DS_WH = new DataTable();
        //            string Emp_WH_Day;
        //            string DOJ_Date_Str;
        //            string MachineID1 = Emp_DS.Rows[intRow]["MachineID"].ToString();

        //            string Query = " Select * from Employee_Mst where MachineID='" + MachineID1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' ";
        //            //SSQL = "Select * from Employee_MST where MachineID='" + MachineID1 + "' And Compcode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'" +
        //            DS_WH = ManualAttend.GetDetails(Query);
        //            if (DS_WH.Rows.Count != 0)
        //            {
        //                Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
        //                DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();
        //            }
        //            else
        //            {
        //                Emp_WH_Day = "";
        //                DOJ_Date_Str = "";
        //            }
        //            int colIndex = intK;
        //            string DayofWeekName = "";
        //            decimal Weekoff_Count = 0;
        //            bool isPresent = false;
        //            bool isWHPresent = false;
        //            decimal Present_Count = 0;
        //            decimal Present_WH_Count = 0;
        //            int Appsent_Count = 0;
        //            decimal Final_Count = 0;
        //            decimal Total_Days_Count = 0;
        //            string Already_Date_Check;
        //            int Days_Insert_RowVal = 0;
        //            decimal FullNight_Shift_Count = 0;
        //            int NFH_Days_Count = 0;
        //            decimal NFH_Days_Present_Count = 0;
        //            Already_Date_Check = "";
        //            string Year_Check = "";
        //            string Time_IN_Str = "";
        //            string Time_Out_Str = "";
        //            int g = 1;

        //            double OT_Time;

        //            OT_Time = 0;

        //            string INTime = "";
        //            string OUTTime = "";
        //            daysAdded = 1;

        //            for (intCol = 0; (intCol <= (daysAdded - 1)); intCol++)
        //            {
        //                isPresent = false;
        //                //string Emp_Total_Work_Time_1 = "00:00";
        //                string Machine_ID_Str = "";
        //                string OT_Week_OFF_Machine_No;
        //                string Date_Value_Str = "";
        //                string Total_Time_get = "";
        //                string Final_OT_Work_Time_1 = "";
        //                //DataSet mLocalDS = new DataSet();

        //                //DataTable mLocalDS = new DataTable();


        //                int j = 0;
        //                double time_Check_dbl = 0;
        //                string Date_Value_Str1 = "";
        //                string Employee_Shift_Name_DB = "No Shift";
        //                isPresent = false;


        //                //Shift Change Check Variable Declaration Start
        //                DateTime InTime_Check = new DateTime();
        //                DateTime InToTime_Check = new DateTime();
        //                TimeSpan InTime_TimeSpan;
        //                string From_Time_Str = "";
        //                string To_Time_Str = "";
        //                //DataSet DS_InTime = new DataSet();
        //                DataTable DS_Time = new DataTable();
        //                //DataSet DS_Time = new DataSet();
        //                DataTable DS_InTime = new DataTable();
        //                DateTime EmpdateIN_Change = new DateTime();
        //                DataTable InsertData = new DataTable();
        //                string Final_InTime = "";
        //                string Final_OutTime = "";
        //                string Final_Shift = "";

        //                int K = 0;
        //                bool Shift_Check_blb = false;
        //                string Shift_Start_Time_Change;
        //                string Shift_End_Time_Change;
        //                string Employee_Time_Change = "";
        //                DateTime ShiftdateStartIN_Change = new DateTime();
        //                DateTime ShiftdateEndIN_Change = new DateTime();

        //                //Shift Change Check Variable Declaration End
        //                string Employee_Punch = "";
        //                Spin_Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID"].ToString();

        //                if (Spin_Machine_ID_Str == "209")
        //                {
        //                    Spin_Machine_ID_Str = "209";
        //                }

        //                // Machine_ID_Str = Encryption(Emp_DS.Rows[intRow]["MachineID"]);
        //                Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID_Encrypt"].ToString();
        //                OT_Week_OFF_Machine_No = Emp_DS.Rows[intRow]["MachineID"].ToString();
        //                Date_Value_Str = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
        //                Date_Value_Str1 = Convert.ToDateTime(Date_Value_Str).AddDays(1).ToShortDateString();

        //                //DateTime dt = Convert.ToDateTime(Date.ToString());

        //                //DayofWeekName = dt.DayOfWeek.ToString();

        //                //TimeIN Get
        //                if (OT_Week_OFF_Machine_No == "8")
        //                {
        //                    OT_Week_OFF_Machine_No = "8";
        //                }
        //                SSQL = "";
        //                SSQL = "Select distinct TimeIN,CONVERT(char(5), TimeIN, 108) as TimeIN24 from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                      " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";

        //                mLocalDS = ManualAttend.GetDetails(SSQL);

        //                if (mLocalDS.Rows.Count <= 0)
        //                {
        //                    Time_IN_Str = "";

        //                    INTime = "";

        //                }
        //                else
        //                {
        //                    DateTime datetime1 = Convert.ToDateTime(mLocalDS.Rows[0][0].ToString());
        //                    INTime = datetime1.ToString("hh:mm tt");
        //                    //INTime = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0].ToString());
        //                }

        //                //TimeOUT Get
        //                SSQL = "";
        //                SSQL = "Select TimeOUT,CONVERT(char(5), TimeOUT, 108) as TimeOUT24 from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                       " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
        //                mLocalDS1 = ManualAttend.GetDetails(SSQL);

        //                if (mLocalDS1.Rows.Count <= 0)
        //                {
        //                    Time_Out_Str = "";

        //                    OUTTime = "";
        //                }
        //                else
        //                {
        //                    DateTime datetime = Convert.ToDateTime(mLocalDS1.Rows[0][0].ToString());
        //                    OUTTime = datetime.ToString("hh:mm tt");
        //                    //OUTTime = string.Format(Convert.ToString(datetime), "HH:mm:ss tt");
        //                    //OUTTime = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0].ToString());
        //                }

        //                //Shift Change Check Start
        //                if (mLocalDS.Rows.Count != 0)
        //                {
        //                    InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
        //                    InToTime_Check = InTime_Check.AddHours(2);



        //                    InTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InTime_Check), "HH:mm:ss"));
        //                    InToTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InToTime_Check), "HH:mm:ss"));




        //                    //Old Time check error for 24 format

        //                    string TimeIN = InTime_Check.ToString();
        //                    string[] TimeIN_Split = TimeIN.Split(' ');
        //                    string TimeIN1 = TimeIN_Split[1].ToString();
        //                    string[] final = TimeIN1.Split(':');
        //                    string Final_IN = "";
        //                    if (Convert.ToInt32(final[0]) >= 10)
        //                    {
        //                        Final_IN = final[0] + ":" + final[1];
        //                    }
        //                    else
        //                    {
        //                        Final_IN = "0" + final[0] + ":" + final[1];
        //                    }


        //                    string TimeOUT = InToTime_Check.ToString();
        //                    string[] TimeOUT_Split = TimeOUT.Split(' ');
        //                    string TimeOUT1 = TimeOUT_Split[1].ToString();
        //                    string[] final1 = TimeOUT1.Split(':');
        //                    string Final_IN1 = "";
        //                    if (Convert.ToInt32(final1[0]) >= 10)
        //                    {
        //                        Final_IN1 = final1[0] + ":" + final1[1];
        //                    }
        //                    else
        //                    {
        //                        Final_IN1 = "0" + final1[0] + ":" + final1[1];
        //                    }




        //                    //New Format 24 change manually

        //                    //string InTime1 = mLocalDS.Rows[0]["TimeIN24"].ToString();

        //                    ////string TimeIN = InTime1.ToString();
        //                    ////string[] TimeIN_Split = TimeIN.Split(' ');
        //                    ////string TimeIN1 = TimeIN_Split[1].ToString();
        //                    //string[] final = InTime1.Split(':');
        //                    //string Final_IN = "";

        //                    //Final_IN = final[0] + ":" + final[1];




        //                    //string InTime2 = (Convert.ToInt16(final[0]) + 2).ToString();
        //                    //InTime2= InTime2+":"+ final[1];

        //                    ////string TimeOUT = InTime2.ToString();
        //                    ////string[] TimeOUT_Split = TimeOUT.Split(' ');
        //                    ////string TimeOUT1 = TimeOUT_Split[1].ToString();
        //                    //string[] final1 = InTime2.Split(':');
        //                    //string Final_IN1 = "";

        //                    //Final_IN1 = final1[0] + ":" + final1[1];



        //                    //InTime_TimeSpan = TimeSpan.Parse(InTime_Check).ToString();
        //                    //From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;

        //                    //Two Hours OutTime Check
        //                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                            " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";


        //                    DS_Time = ManualAttend.GetDetails(SSQL);
        //                    if (DS_Time.Rows.Count != 0)
        //                    {
        //                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                        " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
        //                        DS_InTime = ManualAttend.GetDetails(SSQL);
        //                        if (DS_InTime.Rows.Count != 0)
        //                        {
        //                            //DataSet Shift_DS_Change = new DataSet();

        //                            DataTable Shift_DS_Change = new DataTable();

        //                            Final_InTime = DS_InTime.Rows[0]["TimeIN"].ToString();
        //                            //Check With IN Time Shift
        //                            SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%'";
        //                            SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //                            Shift_DS_Change = ManualAttend.GetDetails(SSQL);
        //                            Shift_Check_blb = false;
        //                            for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
        //                            {
        //                                Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
        //                                if (Shift_DS_Change.Rows[k]["EndIN_Days"].ToString() == "1")
        //                                {
        //                                    Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
        //                                }
        //                                else
        //                                {
        //                                    Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
        //                                }

        //                                ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
        //                                ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);

        //                                if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
        //                                {
        //                                    Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
        //                                    Shift_Check_blb = true;
        //                                    break;
        //                                }
        //                                ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
        //                                ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
        //                                EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
        //                                if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
        //                                {
        //                                    Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
        //                                    Shift_Check_blb = true;
        //                                    break;
        //                                }
        //                            }
        //                            if (Shift_Check_blb == true)
        //                            {

        //                                //IN Time Query Update
        //                                string Querys = "";
        //                                Querys = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
        //                                " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
        //                                mLocalDS = ManualAttend.GetDetails(Querys);

        //                                if (Final_Shift == "SHIFT1")
        //                                {
        //                                    Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
        //                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
        //                                }
        //                                else if (Final_Shift == "SHIFT2")
        //                                {
        //                                    Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
        //                                           " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
        //                                }
        //                                else if (Final_Shift == "SHIFT3")
        //                                {
        //                                    Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
        //                                            " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

        //                                }
        //                                else
        //                                {
        //                                    Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
        //                                            " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

        //                                }


        //                                mLocalDS1 = ManualAttend.GetDetails(Querys);
        //                            }
        //                            else
        //                            {
        //                                //Get Employee In Time
        //                                SSQL = "";
        //                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
        //                                       " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
        //                                mLocalDS = ManualAttend.GetDetails(SSQL);
        //                                //TimeOUT Get
        //                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
        //                                       " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
        //                                mLocalDS1 = ManualAttend.GetDetails(SSQL);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            //Get Employee In Time
        //                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
        //                                   " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
        //                            mLocalDS = ManualAttend.GetDetails(SSQL);

        //                            //TimeOUT Get
        //                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
        //                                  " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
        //                            mLocalDS1 = ManualAttend.GetDetails(SSQL);

        //                        }
        //                    }
        //                    else
        //                    {
        //                        //Get Employee In Time
        //                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
        //                               " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
        //                        mLocalDS = ManualAttend.GetDetails(SSQL);

        //                        //TimeOUT Get
        //                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
        //                              " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
        //                        mLocalDS1 = ManualAttend.GetDetails(SSQL);
        //                    }
        //                }
        //                //Shift Change Check End 
        //                if (mLocalDS.Rows.Count == 0)
        //                {
        //                    Employee_Punch = "";
        //                }
        //                else
        //                {
        //                    Employee_Punch = mLocalDS.Rows[0][0].ToString();
        //                }
        //                if (mLocalDS.Rows.Count >= 1)
        //                {
        //                    for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
        //                    {
        //                        Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
        //                        if (mLocalDS1.Rows.Count > tin)
        //                        {
        //                            Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
        //                        }
        //                        else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
        //                        {
        //                            Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
        //                        }
        //                        else
        //                        {
        //                            Time_Out_Str = "";
        //                        }
        //                        TimeSpan ts4 = new TimeSpan();
        //                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
        //                        if (mLocalDS.Rows.Count <= 0)
        //                        {
        //                            Time_IN_Str = "";
        //                        }
        //                        else
        //                        {
        //                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
        //                        }
        //                        if (Time_IN_Str == "" || Time_Out_Str == "")
        //                        {
        //                            time_Check_dbl = time_Check_dbl;
        //                        }
        //                        else
        //                        {


        //                            DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
        //                            DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
        //                            TimeSpan ts = new TimeSpan();
        //                            ts = date2.Subtract(date1);


        //                            //& ":" & Trim(ts.Minutes)
        //                            Total_Time_get = (ts.Hours).ToString();
        //                            ts4 = ts4.Add(ts);

        //                            //OT Time Get
        //                            Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
        //                            Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);

        //                            if (Left_Val(ts4.Minutes.ToString(), 1) == "-" || Left_Val(ts4.Hours.ToString(), 1) == "-" || Emp_Total_Work_Time_1 == "0:-1")
        //                            {
        //                                Emp_Total_Work_Time_1 = "00:00";

        //                            }
        //                            if (Left_Val(Total_Time_get, 1) == "-")
        //                            {

        //                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
        //                                ts = date2.Subtract(date1);
        //                                ts = date2.Subtract(date1);
        //                                Total_Time_get = (ts.Hours).ToString();
        //                                time_Check_dbl = double.Parse(Total_Time_get);
        //                                Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes).ToString();
        //                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

        //                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
        //                                {
        //                                    Emp_Total_Work_Time_1 = "00:00";
        //                                }
        //                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:1" || Emp_Total_Work_Time_1 == "0:-3")
        //                                {
        //                                    Emp_Total_Work_Time_1 = "00:00";
        //                                }

        //                            }
        //                            else
        //                            {
        //                                time_Check_dbl = time_Check_dbl + double.Parse(Total_Time_get);
        //                            }
        //                        }

        //                        break;

        //                    }//For End
        //                }
        //                else
        //                {
        //                    TimeSpan ts4 = new TimeSpan();
        //                    ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;

        //                    if (mLocalDS.Rows.Count <= 0)
        //                    {
        //                        Time_IN_Str = "";
        //                    }
        //                    else
        //                    {
        //                        Time_IN_Str = mLocalDS.Rows[0][0].ToString();
        //                    }
        //                    for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
        //                    {
        //                        if (mLocalDS1.Rows.Count <= 0)
        //                        {
        //                            Time_Out_Str = "";
        //                        }
        //                        else
        //                        {
        //                            Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
        //                        }

        //                        break;

        //                    }
        //                    //Emp_Total_Work_Time
        //                    if (Time_IN_Str == "" || Time_Out_Str == "")
        //                    {
        //                        time_Check_dbl = 0;
        //                    }
        //                    else
        //                    {
        //                        DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
        //                        DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
        //                        TimeSpan ts = new TimeSpan();
        //                        ts = date2.Subtract(date1);
        //                        ts = date2.Subtract(date1);
        //                        Total_Time_get = (ts.Hours).ToString();
        //                        //OT Time Get
        //                        Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
        //                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

        //                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
        //                        {
        //                            Emp_Total_Work_Time_1 = "00:00";
        //                        }
        //                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
        //                        {
        //                            Emp_Total_Work_Time_1 = "00:00";
        //                        }
        //                        if (Left_Val(Total_Time_get, 1) == "-")
        //                        {

        //                            date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
        //                            ts = date2.Subtract(date1);
        //                            ts = date2.Subtract(date1);
        //                            Total_Time_get = ts.Hours.ToString();
        //                            time_Check_dbl = double.Parse(Total_Time_get);
        //                            Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes);
        //                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
        //                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
        //                            {
        //                                Emp_Total_Work_Time_1 = "00:00";

        //                                time_Check_dbl = 0;
        //                            }
        //                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
        //                            {
        //                                Emp_Total_Work_Time_1 = "00:00";

        //                                time_Check_dbl = 0;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            time_Check_dbl = double.Parse(Total_Time_get);
        //                        }
        //                    }
        //                }
        //                //Emp_Total_Work_Time

        //                string StdWorkingHrs = "0";

        //                if (Emp_DS.Rows[intRow]["StdWrkHrs"].ToString() != "" && Emp_DS.Rows[intRow]["StdWrkHrs"].ToString() != "0")
        //                {
        //                    StdWorkingHrs = Emp_DS.Rows[intRow]["StdWrkHrs"].ToString();

        //                    string StdHrs = (Convert.ToDecimal(StdWorkingHrs) + Convert.ToDecimal(1)).ToString();

        //                    if (Convert.ToDouble(time_Check_dbl) >= Convert.ToDouble(StdHrs))
        //                    {
        //                        OT_Time = OT_Time + (time_Check_dbl - Convert.ToDouble(StdWorkingHrs));

        //                    }
        //                    else
        //                    {
        //                        OT_Time = 0;
        //                    }
        //                }
        //                else
        //                {

        //                    if (time_Check_dbl >= 9)
        //                    {
        //                        OT_Time = OT_Time + (time_Check_dbl - 8);

        //                    }
        //                    else
        //                    {
        //                        OT_Time = 0;
        //                    }
        //                }


        //                string Emp_Total_Work_Time = "";
        //                string Final_OT_Work_Time = "00:00";
        //                string Final_OT_Work_Time_Val = "00:00";
        //                Emp_Total_Work_Time = Emp_Total_Work_Time_1;
        //                //Find Week OFF
        //                string Employee_Week_Name = "";
        //                string Assign_Week_Name = "";
        //                mLocalDS = null;
        //                Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();

        //                SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "' and  Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "'";
        //                mLocalDS = ManualAttend.GetDetails(SSQL);
        //                if (mLocalDS.Rows.Count <= 0)
        //                {
        //                    Assign_Week_Name = "";
        //                }
        //                else
        //                {
        //                    //Assign_Week_Name = mLocalDS.Tables(0).Rows(0)(0)
        //                    Assign_Week_Name = "";
        //                }

        //                Boolean NFH_Day_Check = false;
        //                DateTime NFH_Date;
        //                DateTime DOJ_Date_Format;
        //                DateTime Date_Value = new DateTime();
        //                DataTable DT_NFH = new DataTable();
        //                Date_Value = Convert.ToDateTime(Date_Value_Str.ToString());
        //                string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value.ToString("yyyy/MM/dd") + "'";
        //                DT_NFH = ManualAttend.GetDetails(qry_nfh);

        //                if (DT_NFH.Rows.Count != 0)
        //                {
        //                    NFH_Day_Check = true;
        //                    NFH_Days_Count = NFH_Days_Count + 1;
        //                }

        //                if (Employee_Week_Name == Assign_Week_Name)
        //                {
        //                    //Get Employee Work Time
        //                    double Calculate_Attd_Work_Time = 8;
        //                    double Calculate_Attd_Work_Time_half = 0;
        //                    Weekoff_Count = Weekoff_Count + 1;


        //                    Calculate_Attd_Work_Time_half = 4.0;

        //                    halfPresent = "0";//Half Days Calculate Start
        //                    if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
        //                    {
        //                        // Weekoff_Count = Weekoff_Count + 1;
        //                        isWHPresent = true;
        //                        halfPresent = "1";

        //                    }
        //                    //Half Days Calculate End
        //                    if (time_Check_dbl >= Calculate_Attd_Work_Time)
        //                    {
        //                        //Weekoff_Count = Weekoff_Count + 1;
        //                        isWHPresent = true;
        //                        halfPresent = "0";

        //                    }
        //                }
        //                else if (Employee_Week_Name == Emp_WH_Day)
        //                {
        //                    //Get Employee Work Time
        //                    double Calculate_Attd_Work_Time = 8;
        //                    double Calculate_Attd_Work_Time_half = 0;

        //                    Weekoff_Count = Weekoff_Count + 1;

        //                    Calculate_Attd_Work_Time_half = 4.0;

        //                    halfPresent = "0";//Half Days Calculate Start
        //                    if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
        //                    {
        //                        isWHPresent = true;
        //                        halfPresent = "1";
        //                        //Weekoff_Count = Weekoff_Count + 1;


        //                    }
        //                    //Half Days Calculate End
        //                    if (time_Check_dbl >= Calculate_Attd_Work_Time)
        //                    {
        //                        isWHPresent = true;
        //                        halfPresent = "0";
        //                        //Weekoff_Count = Weekoff_Count + 1;


        //                    }

        //                }
        //                else
        //                {
        //                    //Get Employee Work Time
        //                    double Calculate_Attd_Work_Time = 8;
        //                    double Calculate_Attd_Work_Time_half = 0;


        //                    Calculate_Attd_Work_Time_half = 4.0;

        //                    halfPresent = "0";//Half Days Calculate Start
        //                    if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
        //                    {
        //                        isPresent = true;
        //                        halfPresent = "1";
        //                    }
        //                    //Half Days Calculate End
        //                    if (time_Check_dbl >= Calculate_Attd_Work_Time)
        //                    {
        //                        isPresent = true;
        //                        halfPresent = "0";
        //                    }
        //                }
        //                //Shift Check Code Start
        //                //DataSet Shift_Ds = new DataSet();

        //                DataTable Shift_Ds = new DataTable();

        //                string Start_IN = "";
        //                string End_In = "";
        //                string Shift_Name_Store = "";
        //                bool Shift_Check_blb_Check = false;
        //                DateTime ShiftdateStartIN_Check = new DateTime();
        //                DateTime ShiftdateEndIN_Check = new DateTime();
        //                DateTime EmpdateIN_Check = new DateTime();
        //                string Wages = "";
        //                if (INTime != "")
        //                {

        //                    Shift_Check_blb_Check = false;
        //                    string SS = "GENERAL";
        //                    Wages = Emp_DS.Rows[intRow]["ShiftType"].ToString();

        //                    if (Wages.ToUpper() == SS.ToUpper())
        //                    {
        //                        Employee_Shift_Name_DB = "GENERAL";


        //                        SSQL = "Select * from Shift_Mst Where ShiftDesc ='" + Employee_Shift_Name_DB + "'and ";
        //                        // SSQL = "Select * from Shift_Mst Where  ";
        //                        SSQL = SSQL + "CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //                        //CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "' And 
        //                        Shift_Ds = ManualAttend.GetDetails(SSQL);
        //                        Shift_Check_blb = false;
        //                        for (int f = 0; f < Shift_Ds.Rows.Count; f++)
        //                        {

        //                            int StartIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["StartIN_Days"]);
        //                            int EndIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["EndIN_Days"]);
        //                            Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(StartIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["StartIN"].ToString();
        //                            End_In = Convert.ToDateTime(Date_Value_Str).AddDays(EndIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["EndIN"].ToString();
        //                            ShiftdateStartIN_Check = System.Convert.ToDateTime(Start_IN);
        //                            ShiftdateEndIN_Check = System.Convert.ToDateTime(End_In);
        //                            EmpdateIN_Check = System.Convert.ToDateTime(Employee_Punch);
        //                            if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
        //                            {
        //                                Employee_Shift_Name_DB = Shift_Ds.Rows[f]["ShiftDesc"].ToString();
        //                                Shift_Check_blb_Check = true;
        //                            }

        //                        }
        //                        if (Shift_Check_blb_Check == false)
        //                        {
        //                            Employee_Shift_Name_DB = "No Shift";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        //Shift Master Check Code Start
        //                        SSQL = "Select * from Shift_Mst where  ShiftDesc <> 'GENERAL' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' Order by ShiftDesc Asc";
        //                        // SSQL = "Select * from Shift_Mst where  CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' Order by ShiftDesc Asc";
        //                        //  "CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'";
        //                        Shift_Ds = ManualAttend.GetDetails(SSQL);
        //                        if (Shift_Ds.Rows.Count != 0)
        //                        {
        //                            SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%' ";
        //                            // SSQL = "Select * from Shift_Mst Where  ";
        //                            SSQL = SSQL + " and CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //                            //CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "' And 
        //                            Shift_Ds = ManualAttend.GetDetails(SSQL);
        //                            Shift_Check_blb = false;
        //                            for (int f = 0; f < Shift_Ds.Rows.Count; f++)
        //                            {

        //                                int StartIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["StartIN_Days"]);
        //                                int EndIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["EndIN_Days"]);
        //                                Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(StartIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["StartIN"].ToString();
        //                                End_In = Convert.ToDateTime(Date_Value_Str).AddDays(EndIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["EndIN"].ToString();
        //                                ShiftdateStartIN_Check = System.Convert.ToDateTime(Start_IN);
        //                                ShiftdateEndIN_Check = System.Convert.ToDateTime(End_In);
        //                                EmpdateIN_Check = System.Convert.ToDateTime(Employee_Punch);
        //                                if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
        //                                {
        //                                    Employee_Shift_Name_DB = Shift_Ds.Rows[f]["ShiftDesc"].ToString();
        //                                    Shift_Check_blb_Check = true;
        //                                }

        //                            }
        //                            if (Shift_Check_blb_Check == false)
        //                            {
        //                                Employee_Shift_Name_DB = "No Shift";
        //                            }
        //                        }
        //                        else
        //                        {
        //                            Employee_Shift_Name_DB = "No Shift";
        //                        }
        //                        //Shift Master Check Code End  
        //                    }


        //                }
        //                else
        //                {
        //                    Employee_Shift_Name_DB = "No Shift";
        //                }
        //                //Shift Check Code End
        //                string ShiftType = "";
        //                string Pre_Abs = "";
        //                if (isPresent == true)
        //                {

        //                    if (Employee_Shift_Name_DB != "No Shift")
        //                    {
        //                        ShiftType = "Proper";

        //                        if (halfPresent == "1")
        //                        {
        //                            Pre_Abs = "Half Day";
        //                        }
        //                        else
        //                        {
        //                            Pre_Abs = "Present";
        //                        }

        //                    }
        //                    else
        //                    {
        //                        if (Wages.ToUpper() == "GENERAL")
        //                        {
        //                            ShiftType = "GENERAL Mismatch";
        //                        }
        //                        else
        //                        {
        //                            ShiftType = "Mismatch";
        //                        }

        //                        if (halfPresent == "1")
        //                        {
        //                            Pre_Abs = "Half Day";
        //                        }
        //                        else
        //                        {
        //                            Pre_Abs = "Present";
        //                        }

        //                    }




        //                    if (halfPresent == "1")
        //                    {
        //                        Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
        //                    }
        //                    else if (halfPresent == "0")
        //                    {
        //                        Present_Count = Present_Count + 1;
        //                    }

        //                    if (halfPresent == "1")
        //                    {
        //                        if (NFH_Day_Check == true)
        //                        {
        //                            NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
        //                        }
        //                    }
        //                    else if (halfPresent == "0")
        //                    {
        //                        if (NFH_Day_Check == true)
        //                        {
        //                            NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
        //                        }
        //                    }

        //                }
        //                else if (isWHPresent == true)
        //                {

        //                    if (Employee_Shift_Name_DB != "No Shift")
        //                    {
        //                        ShiftType = "Proper";

        //                        if (halfPresent == "1")
        //                        {
        //                            Pre_Abs = "Half Day";
        //                        }
        //                        else
        //                        {
        //                            Pre_Abs = "Present";
        //                        }

        //                    }
        //                    else
        //                    {
        //                        if (Wages.ToUpper() == "GENERAL")
        //                        {
        //                            ShiftType = "GENERAL Mismatch";
        //                        }
        //                        else
        //                        {
        //                            ShiftType = "Mismatch";
        //                        }

        //                        if (halfPresent == "1")
        //                        {
        //                            Pre_Abs = "Half Day";
        //                        }
        //                        else
        //                        {
        //                            Pre_Abs = "Present";
        //                        }

        //                    }

        //                    if (halfPresent == "1")
        //                    {
        //                        Present_WH_Count = Convert.ToDecimal(Present_WH_Count) + Convert.ToDecimal(0.5);
        //                    }
        //                    else if (halfPresent == "0")
        //                    {
        //                        Present_WH_Count = Present_WH_Count + 1;
        //                    }

        //                    if (halfPresent == "1")
        //                    {
        //                        Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
        //                    }
        //                    else if (halfPresent == "0")
        //                    {
        //                        Present_Count = Present_Count + 1;
        //                    }

        //                    if (halfPresent == "1")
        //                    {
        //                        if (NFH_Day_Check == true)
        //                        {
        //                            NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
        //                        }
        //                    }
        //                    else if (halfPresent == "0")
        //                    {
        //                        if (NFH_Day_Check == true)
        //                        {
        //                            NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
        //                        }
        //                    }
        //                }
        //                else
        //                {

        //                    if (INTime == "" && OUTTime == "")
        //                    {
        //                        ShiftType = "Leave";
        //                        Employee_Shift_Name_DB = "Leave";
        //                        Pre_Abs = "Leave";

        //                    }
        //                    else if (INTime == "")
        //                    {
        //                        if (OUTTime != "")
        //                        {
        //                            ShiftType = "Improper";
        //                        }
        //                        Pre_Abs = "Absent";

        //                    }
        //                    else if (OUTTime == "")
        //                    {
        //                        if (INTime != "")
        //                        {
        //                            ShiftType = "Improper";
        //                        }
        //                        Pre_Abs = "Absent";

        //                    }
        //                    else if (INTime != "" && OUTTime != "")
        //                    {
        //                        ShiftType = "Absent";
        //                        Pre_Abs = "Absent";

        //                    }

        //                    Appsent_Count = Appsent_Count + 1;
        //                }

        //                intK += 1;




        //                //Update Employee Worked_Days
        //                //DataSet mDataSet = new DataSet();
        //                DataTable mDataSet = new DataTable();
        //                string Queryss = "";
        //                DataSet Del_ds = new DataSet();
        //                Queryss = "Select * from LogTime_Days where MachineID='" + MachineID1 + "' and  Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";

        //                mDataSet = ManualAttend.GetDetails(Queryss);

        //                //mDataSet = objdata.read(Queryss);  Emp_WH_Day
        //                if (mDataSet.Rows.Count != 0)
        //                {
        //                    Queryss = "Delete from LogTime_Days where MachineID='" + MachineID1 + "' and Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
        //                    ManualAttend.GetDetails(Queryss);
        //                }


        //                if (Wages.ToUpper() == "GENERAL")
        //                {
        //                    DataSet Insert_Ds = new DataSet();

        //                    Queryss = "";
        //                    Queryss = "Insert Into LogTime_Days(CompCode,LocCode,MachineID,ExistingCode,FirstName,LastName,DeptName," +
        //                    "Designation,DOJ,Wages,CatName,SubCatName,Grade,BasicSalary,Present,Wh_Check,Wh_Count,Wh_Present_Count,NFH_Count,NFH_Present_Count,Shift,TimeIN,TimeOUT,Total_Hrs,OTHours,Attn_Date_Str,Attn_Date,MachineID_Encrypt,Total_Hrs1,TypeName,Present_Absent)" +
        //                     " Values('" + SessionCcode + "','" + SessionLcode + "','" + Emp_DS.Rows[intRow]["MachineID"] + "','" + Emp_DS.Rows[intRow]["ExistingCode"] + "'," +
        //                    "'" + Emp_DS.Rows[intRow]["FirstName"] + "','" + Emp_DS.Rows[intRow]["MiddleInitial"] + "'," +
        //                     "'" + Emp_DS.Rows[intRow]["DeptName"] + "','" + Emp_DS.Rows[intRow]["Designation"] + "'," +
        //                     "'" + Emp_DS.Rows[intRow]["DOJ"] + "','" + Emp_DS.Rows[intRow]["Wages"] + "','" + Emp_DS.Rows[intRow]["CatName"] + "','" + Emp_DS.Rows[intRow]["SubCatName"] + "','" + Emp_DS.Rows[intRow]["Grade"].ToString() + "','" + Emp_DS.Rows[intRow]["BasicSalary"] + "','" + Present_Count + "'," +
        //                     "'" + Emp_WH_Day + "','" + Weekoff_Count + "','" + Present_WH_Count + "','" + NFH_Days_Count + "','" + NFH_Days_Present_Count + "'," +
        //                     "'GENERAL','" + INTime + "','" + OUTTime + "','" + time_Check_dbl + "','" + OT_Time + "','" + Convert.ToDateTime(Date).ToString("yyyy/MM/dd") + "',Convert(datetime,'" + Convert.ToDateTime(Date).ToString("yyyy/MM/dd") + "',120),'" + UTF8Encryption(MachineID1) + "','" + Emp_Total_Work_Time_1 + "','" + ShiftType + "','" + Pre_Abs + "')";

        //                    ManualAttend.GetDetails(Queryss);
        //                }

        //                else
        //                {
        //                    DataSet Insert_Ds = new DataSet();

        //                    Queryss = "";
        //                    Queryss = "Insert Into LogTime_Days(CompCode,LocCode,MachineID,ExistingCode,FirstName,LastName,DeptName," +
        //                    "Designation,DOJ,Wages,CatName,SubCatName,Grade,BasicSalary,Present,Wh_Check,Wh_Count,Wh_Present_Count,NFH_Count,NFH_Present_Count,Shift,TimeIN,TimeOUT,Total_Hrs,OTHours,Attn_Date_Str,Attn_Date,MachineID_Encrypt,Total_Hrs1,TypeName,Present_Absent)" +
        //                     " Values('" + SessionCcode + "','" + SessionLcode + "','" + Emp_DS.Rows[intRow]["MachineID"] + "','" + Emp_DS.Rows[intRow]["ExistingCode"] + "'," +
        //                    "'" + Emp_DS.Rows[intRow]["FirstName"] + "','" + Emp_DS.Rows[intRow]["MiddleInitial"] + "'," +
        //                     "'" + Emp_DS.Rows[intRow]["DeptName"] + "','" + Emp_DS.Rows[intRow]["Designation"] + "'," +
        //                     "'" + Emp_DS.Rows[intRow]["DOJ"] + "','" + Emp_DS.Rows[intRow]["Wages"] + "','" + Emp_DS.Rows[intRow]["CatName"] + "','" + Emp_DS.Rows[intRow]["SubCatName"] + "','" + Emp_DS.Rows[intRow]["Grade"].ToString() + "','" + Emp_DS.Rows[intRow]["BasicSalary"] + "','" + Present_Count + "'," +
        //                     "'" + Emp_WH_Day + "','" + Weekoff_Count + "','" + Present_WH_Count + "','" + NFH_Days_Count + "','" + NFH_Days_Present_Count + "'," +
        //                     "'" + Employee_Shift_Name_DB + "','" + INTime + "','" + OUTTime + "','" + time_Check_dbl + "','" + OT_Time + "','" + Convert.ToDateTime(Date).ToString("yyyy/MM/dd") + "',Convert(datetime,'" + Convert.ToDateTime(Date).ToString("yyyy/MM/dd") + "',120),'" + UTF8Encryption(MachineID1) + "','" + Emp_Total_Work_Time_1 + "','" + ShiftType + "','" + Pre_Abs + "')";

        //                    ManualAttend.GetDetails(Queryss);

        //                }


        //                Present_Count = 0;
        //                Present_WH_Count = 0;
        //                Employee_Shift_Name_DB = "No Shift";
        //                INTime = "";
        //                OUTTime = "";
        //                time_Check_dbl = 0;
        //                OT_Time = 0;
        //                Weekoff_Count = 0;
        //                Emp_WH_Day = "";

        //            }

        //            intI += 1; ;
        //            Total_Days_Count = 0;
        //            Final_Count = 0;
        //            Appsent_Count = 0;
        //            Present_Count = 0;
        //            Present_WH_Count = 0;
        //            NFH_Days_Count = 0;
        //            NFH_Days_Present_Count = 0;
        //            INTime = "";
        //            OUTTime = "";
        //            Weekoff_Count = 0;
        //            Emp_WH_Day = "";
        //        }



        //        //Calculation for Employee not register in Employee Mst
        //        DataTable DT_TempEmp = new DataTable();

        //        SSQL = "Select EmpNo from Temp_Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //        DT_TempEmp = ManualAttend.GetDetails(SSQL);

        //        for (int Empk = 0; Empk < DT_TempEmp.Rows.Count; Empk++)
        //        {
        //            intK = 1;
        //            string Emp_Total_Work_Time_1 = "00:00";
        //            //Get Employee Week OF DAY
        //            DataTable DS_WH = new DataTable();
        //            string Emp_WH_Day = "";
        //            string DOJ_Date_Str = "";
        //            string MachineID1 = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();


        //            int colIndex = intK;
        //            string DayofWeekName = "";
        //            decimal Weekoff_Count = 0;
        //            bool isPresent = false;
        //            bool isWHPresent = false;
        //            decimal Present_Count = 0;
        //            decimal Present_WH_Count = 0;
        //            int Appsent_Count = 0;
        //            decimal Final_Count = 0;
        //            decimal Total_Days_Count = 0;
        //            string Already_Date_Check;
        //            int Days_Insert_RowVal = 0;
        //            decimal FullNight_Shift_Count = 0;
        //            int NFH_Days_Count = 0;
        //            decimal NFH_Days_Present_Count = 0;
        //            Already_Date_Check = "";
        //            string Year_Check = "";
        //            string Time_IN_Str = "";
        //            string Time_Out_Str = "";
        //            int g = 1;

        //            double OT_Time;

        //            OT_Time = 0;

        //            string INTime = "";
        //            string OUTTime = "";
        //            daysAdded = 1;

        //            for (intCol = 0; (intCol <= (daysAdded - 1)); intCol++)
        //            {
        //                isPresent = false;
        //                //string Emp_Total_Work_Time_1 = "00:00";
        //                string Machine_ID_Str = "";
        //                string OT_Week_OFF_Machine_No;
        //                string Date_Value_Str = "";
        //                string Total_Time_get = "";
        //                string Final_OT_Work_Time_1 = "";
        //                //DataSet mLocalDS = new DataSet();

        //                //DataTable mLocalDS = new DataTable();


        //                int j = 0;
        //                double time_Check_dbl = 0;
        //                string Date_Value_Str1 = "";
        //                string Employee_Shift_Name_DB = "No Shift";
        //                isPresent = false;


        //                //Shift Change Check Variable Declaration Start
        //                DateTime InTime_Check = new DateTime();
        //                DateTime InToTime_Check = new DateTime();
        //                TimeSpan InTime_TimeSpan;
        //                string From_Time_Str = "";
        //                string To_Time_Str = "";
        //                //DataSet DS_InTime = new DataSet();
        //                DataTable DS_Time = new DataTable();
        //                //DataSet DS_Time = new DataSet();
        //                DataTable DS_InTime = new DataTable();
        //                DateTime EmpdateIN_Change = new DateTime();
        //                DataTable InsertData = new DataTable();
        //                string Final_InTime = "";
        //                string Final_OutTime = "";
        //                string Final_Shift = "";

        //                int K = 0;
        //                bool Shift_Check_blb = false;
        //                string Shift_Start_Time_Change;
        //                string Shift_End_Time_Change;
        //                string Employee_Time_Change = "";
        //                DateTime ShiftdateStartIN_Change = new DateTime();
        //                DateTime ShiftdateEndIN_Change = new DateTime();

        //                //Shift Change Check Variable Declaration End
        //                string Employee_Punch = "";
        //                Spin_Machine_ID_Str = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();

        //                if (Spin_Machine_ID_Str == "603033")
        //                {
        //                    Spin_Machine_ID_Str = "603033";
        //                }

        //                // Machine_ID_Str = Encryption(Emp_DS.Rows[intRow]["MachineID"]);
        //                Machine_ID_Str = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();
        //                OT_Week_OFF_Machine_No = DT_TempEmp.Rows[Empk]["EmpNo"].ToString();
        //                Date_Value_Str = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
        //                Date_Value_Str1 = Convert.ToDateTime(Date_Value_Str).AddDays(1).ToShortDateString();

        //                //DateTime dt = Convert.ToDateTime(Date.ToString());

        //                //DayofWeekName = dt.DayOfWeek.ToString();

        //                //TimeIN Get

        //                SSQL = "";
        //                SSQL = "Select distinct TimeIN,CONVERT(char(5), TimeIN, 108) as TimeIN24 from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                      " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";

        //                mLocalDS = ManualAttend.GetDetails(SSQL);

        //                if (mLocalDS.Rows.Count <= 0)
        //                {
        //                    Time_IN_Str = "";

        //                    INTime = "";

        //                }
        //                else
        //                {
        //                    DateTime datetime1 = Convert.ToDateTime(mLocalDS.Rows[0][0].ToString());
        //                    INTime = datetime1.ToString("hh:mm tt");
        //                    //INTime = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0].ToString());
        //                }

        //                //TimeOUT Get
        //                SSQL = "";
        //                SSQL = "Select TimeOUT,CONVERT(char(5), TimeOUT, 108) as TimeOUT24 from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                       " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
        //                mLocalDS1 = ManualAttend.GetDetails(SSQL);

        //                if (mLocalDS1.Rows.Count <= 0)
        //                {
        //                    Time_Out_Str = "";

        //                    OUTTime = "";
        //                }
        //                else
        //                {
        //                    DateTime datetime = Convert.ToDateTime(mLocalDS1.Rows[0][0].ToString());
        //                    OUTTime = datetime.ToString("hh:mm tt");
        //                    //OUTTime = string.Format(Convert.ToString(datetime), "HH:mm:ss tt");
        //                    //OUTTime = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0].ToString());
        //                }

        //                //Shift Change Check Start
        //                if (mLocalDS.Rows.Count != 0)
        //                {
        //                    InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
        //                    InToTime_Check = InTime_Check.AddHours(2);



        //                    InTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InTime_Check), "HH:mm:ss"));
        //                    InToTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InToTime_Check), "HH:mm:ss"));




        //                    //Old Time check error for 24 format

        //                    string TimeIN = InTime_Check.ToString();
        //                    string[] TimeIN_Split = TimeIN.Split(' ');
        //                    string TimeIN1 = TimeIN_Split[1].ToString();
        //                    string[] final = TimeIN1.Split(':');
        //                    string Final_IN = "";
        //                    if (Convert.ToInt32(final[0]) >= 10)
        //                    {
        //                        Final_IN = final[0] + ":" + final[1];
        //                    }
        //                    else
        //                    {
        //                        Final_IN = "0" + final[0] + ":" + final[1];
        //                    }


        //                    string TimeOUT = InToTime_Check.ToString();
        //                    string[] TimeOUT_Split = TimeOUT.Split(' ');
        //                    string TimeOUT1 = TimeOUT_Split[1].ToString();
        //                    string[] final1 = TimeOUT1.Split(':');
        //                    string Final_IN1 = "";
        //                    if (Convert.ToInt32(final1[0]) >= 10)
        //                    {
        //                        Final_IN1 = final1[0] + ":" + final1[1];
        //                    }
        //                    else
        //                    {
        //                        Final_IN1 = "0" + final1[0] + ":" + final1[1];
        //                    }



        //                    //Two Hours OutTime Check
        //                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                            " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";


        //                    DS_Time = ManualAttend.GetDetails(SSQL);
        //                    if (DS_Time.Rows.Count != 0)
        //                    {
        //                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                        " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
        //                        DS_InTime = ManualAttend.GetDetails(SSQL);
        //                        if (DS_InTime.Rows.Count != 0)
        //                        {
        //                            //DataSet Shift_DS_Change = new DataSet();

        //                            DataTable Shift_DS_Change = new DataTable();

        //                            Final_InTime = DS_InTime.Rows[0]["TimeIN"].ToString();
        //                            //Check With IN Time Shift
        //                            SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%'";
        //                            SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //                            Shift_DS_Change = ManualAttend.GetDetails(SSQL);
        //                            Shift_Check_blb = false;
        //                            for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
        //                            {
        //                                Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
        //                                if (Shift_DS_Change.Rows[k]["EndIN_Days"].ToString() == "1")
        //                                {
        //                                    Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
        //                                }
        //                                else
        //                                {
        //                                    Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
        //                                }

        //                                ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
        //                                ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);

        //                                if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
        //                                {
        //                                    Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
        //                                    Shift_Check_blb = true;
        //                                    break;
        //                                }
        //                                ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
        //                                ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
        //                                EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
        //                                if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
        //                                {
        //                                    Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
        //                                    Shift_Check_blb = true;
        //                                    break;
        //                                }
        //                            }
        //                            if (Shift_Check_blb == true)
        //                            {

        //                                //IN Time Query Update
        //                                string Querys = "";
        //                                Querys = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                                " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
        //                                mLocalDS = ManualAttend.GetDetails(Querys);

        //                                if (Final_Shift == "SHIFT1")
        //                                {
        //                                    Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
        //                                }
        //                                else if (Final_Shift == "SHIFT2")
        //                                {
        //                                    Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                                           " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
        //                                }
        //                                else if (Final_Shift == "SHIFT3")
        //                                {
        //                                    Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                                            " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

        //                                }
        //                                else
        //                                {
        //                                    Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                                            " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";

        //                                }


        //                                mLocalDS1 = ManualAttend.GetDetails(Querys);
        //                            }
        //                            else
        //                            {
        //                                //Get Employee In Time
        //                                SSQL = "";
        //                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                                       " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
        //                                mLocalDS = ManualAttend.GetDetails(SSQL);
        //                                //TimeOUT Get
        //                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                                       " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
        //                                mLocalDS1 = ManualAttend.GetDetails(SSQL);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            //Get Employee In Time
        //                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                                   " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
        //                            mLocalDS = ManualAttend.GetDetails(SSQL);

        //                            //TimeOUT Get
        //                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                                  " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
        //                            mLocalDS1 = ManualAttend.GetDetails(SSQL);

        //                        }
        //                    }
        //                    else
        //                    {
        //                        //Get Employee In Time
        //                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                               " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
        //                        mLocalDS = ManualAttend.GetDetails(SSQL);

        //                        //TimeOUT Get
        //                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
        //                              " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Desc";
        //                        mLocalDS1 = ManualAttend.GetDetails(SSQL);
        //                    }
        //                }
        //                //Shift Change Check End 
        //                if (mLocalDS.Rows.Count == 0)
        //                {
        //                    Employee_Punch = "";
        //                }
        //                else
        //                {
        //                    Employee_Punch = mLocalDS.Rows[0][0].ToString();
        //                }
        //                if (mLocalDS.Rows.Count >= 1)
        //                {
        //                    for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
        //                    {
        //                        Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
        //                        if (mLocalDS1.Rows.Count > tin)
        //                        {
        //                            Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
        //                        }
        //                        else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
        //                        {
        //                            Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
        //                        }
        //                        else
        //                        {
        //                            Time_Out_Str = "";
        //                        }
        //                        TimeSpan ts4 = new TimeSpan();
        //                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
        //                        if (mLocalDS.Rows.Count <= 0)
        //                        {
        //                            Time_IN_Str = "";
        //                        }
        //                        else
        //                        {
        //                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
        //                        }
        //                        if (Time_IN_Str == "" || Time_Out_Str == "")
        //                        {
        //                            time_Check_dbl = time_Check_dbl;
        //                        }
        //                        else
        //                        {


        //                            DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
        //                            DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
        //                            TimeSpan ts = new TimeSpan();
        //                            ts = date2.Subtract(date1);


        //                            //& ":" & Trim(ts.Minutes)
        //                            Total_Time_get = (ts.Hours).ToString();
        //                            ts4 = ts4.Add(ts);

        //                            //OT Time Get
        //                            Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
        //                            Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);

        //                            if (Left_Val(ts4.Minutes.ToString(), 1) == "-" || Left_Val(ts4.Hours.ToString(), 1) == "-" || Emp_Total_Work_Time_1 == "0:-1")
        //                            {
        //                                Emp_Total_Work_Time_1 = "00:00";

        //                            }
        //                            if (Left_Val(Total_Time_get, 1) == "-")
        //                            {

        //                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
        //                                ts = date2.Subtract(date1);
        //                                ts = date2.Subtract(date1);
        //                                Total_Time_get = (ts.Hours).ToString();
        //                                time_Check_dbl = double.Parse(Total_Time_get);
        //                                Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes).ToString();
        //                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

        //                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
        //                                {
        //                                    Emp_Total_Work_Time_1 = "00:00";
        //                                }
        //                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:1" || Emp_Total_Work_Time_1 == "0:-3")
        //                                {
        //                                    Emp_Total_Work_Time_1 = "00:00";
        //                                }

        //                            }
        //                            else
        //                            {
        //                                time_Check_dbl = time_Check_dbl + double.Parse(Total_Time_get);
        //                            }
        //                        }

        //                        break;

        //                    }//For End
        //                }
        //                else
        //                {
        //                    TimeSpan ts4 = new TimeSpan();
        //                    ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;

        //                    if (mLocalDS.Rows.Count <= 0)
        //                    {
        //                        Time_IN_Str = "";
        //                    }
        //                    else
        //                    {
        //                        Time_IN_Str = mLocalDS.Rows[0][0].ToString();
        //                    }
        //                    for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
        //                    {
        //                        if (mLocalDS1.Rows.Count <= 0)
        //                        {
        //                            Time_Out_Str = "";
        //                        }
        //                        else
        //                        {
        //                            Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
        //                        }

        //                        break;

        //                    }
        //                    //Emp_Total_Work_Time
        //                    if (Time_IN_Str == "" || Time_Out_Str == "")
        //                    {
        //                        time_Check_dbl = 0;
        //                    }
        //                    else
        //                    {
        //                        DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
        //                        DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
        //                        TimeSpan ts = new TimeSpan();
        //                        ts = date2.Subtract(date1);
        //                        ts = date2.Subtract(date1);
        //                        Total_Time_get = (ts.Hours).ToString();
        //                        //OT Time Get
        //                        Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
        //                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

        //                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
        //                        {
        //                            Emp_Total_Work_Time_1 = "00:00";
        //                        }
        //                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
        //                        {
        //                            Emp_Total_Work_Time_1 = "00:00";
        //                        }
        //                        if (Left_Val(Total_Time_get, 1) == "-")
        //                        {

        //                            date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
        //                            ts = date2.Subtract(date1);
        //                            ts = date2.Subtract(date1);
        //                            Total_Time_get = ts.Hours.ToString();
        //                            time_Check_dbl = double.Parse(Total_Time_get);
        //                            Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes);
        //                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
        //                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
        //                            {
        //                                Emp_Total_Work_Time_1 = "00:00";

        //                                time_Check_dbl = 0;
        //                            }
        //                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
        //                            {
        //                                Emp_Total_Work_Time_1 = "00:00";

        //                                time_Check_dbl = 0;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            time_Check_dbl = double.Parse(Total_Time_get);
        //                        }
        //                    }
        //                }
        //                //Emp_Total_Work_Time

        //                string StdWorkingHrs = "0";

        //                if (time_Check_dbl >= 9)
        //                {
        //                    OT_Time = OT_Time + (time_Check_dbl - 8);

        //                }
        //                else
        //                {
        //                    OT_Time = 0;
        //                }



        //                string Emp_Total_Work_Time = "";
        //                string Final_OT_Work_Time = "00:00";
        //                string Final_OT_Work_Time_Val = "00:00";
        //                Emp_Total_Work_Time = Emp_Total_Work_Time_1;
        //                //Find Week OFF
        //                string Employee_Week_Name = "";
        //                string Assign_Week_Name = "";
        //                mLocalDS = null;
        //                Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();

        //                SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "' and  Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "'";
        //                mLocalDS = ManualAttend.GetDetails(SSQL);
        //                if (mLocalDS.Rows.Count <= 0)
        //                {
        //                    Assign_Week_Name = "";
        //                }
        //                else
        //                {
        //                    //Assign_Week_Name = mLocalDS.Tables(0).Rows(0)(0)
        //                    Assign_Week_Name = "";
        //                }

        //                Boolean NFH_Day_Check = false;
        //                DateTime NFH_Date;
        //                DateTime DOJ_Date_Format;
        //                DateTime Date_Value = new DateTime();
        //                DataTable DT_NFH = new DataTable();
        //                Date_Value = Convert.ToDateTime(Date_Value_Str.ToString());


        //                if (Employee_Week_Name == Assign_Week_Name)
        //                {
        //                    //Get Employee Work Time
        //                    double Calculate_Attd_Work_Time = 8;
        //                    double Calculate_Attd_Work_Time_half = 0;


        //                    Calculate_Attd_Work_Time_half = 4.0;

        //                    halfPresent = "0";//Half Days Calculate Start
        //                    if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
        //                    {
        //                        Weekoff_Count = Weekoff_Count + 1;
        //                        isWHPresent = true;
        //                        halfPresent = "1";

        //                    }
        //                    //Half Days Calculate End
        //                    if (time_Check_dbl >= Calculate_Attd_Work_Time)
        //                    {
        //                        Weekoff_Count = Weekoff_Count + 1;
        //                        isWHPresent = true;
        //                        halfPresent = "0";

        //                    }
        //                }
        //                else if (Employee_Week_Name == Emp_WH_Day)
        //                {
        //                    //Get Employee Work Time
        //                    double Calculate_Attd_Work_Time = 8;
        //                    double Calculate_Attd_Work_Time_half = 0;


        //                    Calculate_Attd_Work_Time_half = 4.0;

        //                    halfPresent = "0";//Half Days Calculate Start
        //                    if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
        //                    {
        //                        isWHPresent = true;
        //                        halfPresent = "1";
        //                        Weekoff_Count = Weekoff_Count + 1;


        //                    }
        //                    //Half Days Calculate End
        //                    if (time_Check_dbl >= Calculate_Attd_Work_Time)
        //                    {
        //                        isWHPresent = true;
        //                        halfPresent = "0";
        //                        Weekoff_Count = Weekoff_Count + 1;


        //                    }

        //                }
        //                else
        //                {
        //                    //Get Employee Work Time
        //                    double Calculate_Attd_Work_Time = 8;
        //                    double Calculate_Attd_Work_Time_half = 0;


        //                    Calculate_Attd_Work_Time_half = 4.0;

        //                    halfPresent = "0";//Half Days Calculate Start
        //                    if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
        //                    {
        //                        isPresent = true;
        //                        halfPresent = "1";
        //                    }
        //                    //Half Days Calculate End
        //                    if (time_Check_dbl >= Calculate_Attd_Work_Time)
        //                    {
        //                        isPresent = true;
        //                        halfPresent = "0";
        //                    }
        //                }
        //                //Shift Check Code Start
        //                //DataSet Shift_Ds = new DataSet();

        //                DataTable Shift_Ds = new DataTable();

        //                string Start_IN = "";
        //                string End_In = "";
        //                string Shift_Name_Store = "";
        //                bool Shift_Check_blb_Check = false;
        //                DateTime ShiftdateStartIN_Check = new DateTime();
        //                DateTime ShiftdateEndIN_Check = new DateTime();
        //                DateTime EmpdateIN_Check = new DateTime();
        //                if (isPresent == true || isWHPresent == true)
        //                {
        //                    Shift_Check_blb_Check = false;


        //                    //Shift Master Check Code Start
        //                    SSQL = "Select * from Shift_Mst where  ShiftDesc <> 'GENERAL' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' Order by ShiftDesc Asc";
        //                    //  "CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "'";
        //                    Shift_Ds = ManualAttend.GetDetails(SSQL);
        //                    if (Shift_Ds.Rows.Count != 0)
        //                    {
        //                        SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%' ";
        //                        SSQL = SSQL + "And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //                        //CompCode='" + iStr1[0] + "' And LocCode='" + iStr2[0] + "' And 
        //                        Shift_Ds = ManualAttend.GetDetails(SSQL);
        //                        Shift_Check_blb = false;
        //                        for (int f = 0; f < Shift_Ds.Rows.Count; f++)
        //                        {

        //                            int StartIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["StartIN_Days"]);
        //                            int EndIN_Days = Convert.ToInt16(Shift_Ds.Rows[f]["EndIN_Days"]);
        //                            Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(StartIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["StartIN"].ToString();
        //                            End_In = Convert.ToDateTime(Date_Value_Str).AddDays(EndIN_Days).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[f]["EndIN"].ToString();
        //                            ShiftdateStartIN_Check = System.Convert.ToDateTime(Start_IN);
        //                            ShiftdateEndIN_Check = System.Convert.ToDateTime(End_In);
        //                            EmpdateIN_Check = System.Convert.ToDateTime(Employee_Punch);
        //                            if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
        //                            {
        //                                Employee_Shift_Name_DB = Shift_Ds.Rows[f]["ShiftDesc"].ToString();
        //                                Shift_Check_blb_Check = true;
        //                            }

        //                        }
        //                        if (Shift_Check_blb_Check == false)
        //                        {
        //                            Employee_Shift_Name_DB = "No Shift";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        Employee_Shift_Name_DB = "No Shift";
        //                    }
        //                    //Shift Master Check Code End  

        //                }
        //                else
        //                {
        //                    Employee_Shift_Name_DB = "No Shift";

        //                }
        //                //Shift Check Code End
        //                string ShiftType = "";
        //                string Pre_Abs = "";


        //                if (isPresent == true)
        //                {

        //                    if (Employee_Shift_Name_DB != "No Shift")
        //                    {
        //                        ShiftType = "Proper";

        //                        if (halfPresent == "1")
        //                        {
        //                            Pre_Abs = "Half Day";
        //                        }
        //                        else
        //                        {
        //                            Pre_Abs = "Present";
        //                        }

        //                    }
        //                    else
        //                    {

        //                        ShiftType = "Mismatch";


        //                        if (halfPresent == "1")
        //                        {
        //                            Pre_Abs = "Half Day";
        //                        }
        //                        else
        //                        {
        //                            Pre_Abs = "Present";
        //                        }

        //                    }








        //                    if (halfPresent == "1")
        //                    {
        //                        Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
        //                    }
        //                    else if (halfPresent == "0")
        //                    {
        //                        Present_Count = Present_Count + 1;
        //                    }

        //                    if (halfPresent == "1")
        //                    {
        //                        if (NFH_Day_Check == true)
        //                        {
        //                            NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
        //                        }
        //                    }
        //                    else if (halfPresent == "0")
        //                    {
        //                        if (NFH_Day_Check == true)
        //                        {
        //                            NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
        //                        }
        //                    }

        //                }
        //                else if (isWHPresent == true)
        //                {

        //                    if (Employee_Shift_Name_DB != "No Shift")
        //                    {
        //                        ShiftType = "Proper";

        //                        if (halfPresent == "1")
        //                        {
        //                            Pre_Abs = "Half Day";
        //                        }
        //                        else
        //                        {
        //                            Pre_Abs = "Present";
        //                        }

        //                    }
        //                    else
        //                    {


        //                        ShiftType = "Mismatch";


        //                        if (halfPresent == "1")
        //                        {
        //                            Pre_Abs = "Half Day";
        //                        }
        //                        else
        //                        {
        //                            Pre_Abs = "Present";
        //                        }

        //                    }



        //                    if (halfPresent == "1")
        //                    {
        //                        Present_WH_Count = Convert.ToDecimal(Present_WH_Count) + Convert.ToDecimal(0.5);
        //                    }
        //                    else if (halfPresent == "0")
        //                    {
        //                        Present_WH_Count = Present_WH_Count + 1;
        //                    }

        //                    if (halfPresent == "1")
        //                    {
        //                        Present_Count = Convert.ToDecimal(Present_Count) + Convert.ToDecimal(0.5);
        //                    }
        //                    else if (halfPresent == "0")
        //                    {
        //                        Present_Count = Present_Count + 1;
        //                    }

        //                    if (halfPresent == "1")
        //                    {
        //                        if (NFH_Day_Check == true)
        //                        {
        //                            NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(0.5);
        //                        }
        //                    }
        //                    else if (halfPresent == "0")
        //                    {
        //                        if (NFH_Day_Check == true)
        //                        {
        //                            NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
        //                        }
        //                    }
        //                }
        //                else
        //                {

        //                    if (INTime == "" && OUTTime == "")
        //                    {
        //                        ShiftType = "Leave";
        //                        Employee_Shift_Name_DB = "Leave";
        //                        Pre_Abs = "Leave";

        //                    }
        //                    else if (INTime == "")
        //                    {
        //                        if (OUTTime != "")
        //                        {
        //                            ShiftType = "Improper";
        //                        }
        //                        Pre_Abs = "Absent";

        //                    }
        //                    else if (OUTTime == "")
        //                    {
        //                        if (INTime != "")
        //                        {
        //                            ShiftType = "Improper";
        //                        }
        //                        Pre_Abs = "Absent";

        //                    }
        //                    else if (INTime != "" && OUTTime != "")
        //                    {
        //                        ShiftType = "Absent";
        //                        Pre_Abs = "Absent";

        //                    }
        //                    Appsent_Count = Appsent_Count + 1;
        //                }

        //                intK += 1;




        //                //Update Employee Worked_Days
        //                //DataSet mDataSet = new DataSet();
        //                DataTable mDataSet = new DataTable();
        //                string Queryss = "";
        //                DataSet Del_ds = new DataSet();
        //                Queryss = "Select * from LogTime_Days where MachineID='" + MachineID1 + "' and  Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";

        //                mDataSet = ManualAttend.GetDetails(Queryss);

        //                //mDataSet = objdata.read(Queryss);  Emp_WH_Day
        //                if (mDataSet.Rows.Count != 0)
        //                {
        //                    Queryss = "Delete from LogTime_Days where MachineID='" + MachineID1 + "' and Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
        //                    ManualAttend.GetDetails(Queryss);
        //                }
        //                DataSet Insert_Ds = new DataSet();

        //                Queryss = "";
        //                Queryss = "Insert Into LogTime_Days(CompCode,LocCode,MachineID,ExistingCode,Present,Wh_Check,Wh_Count,";
        //                Queryss = Queryss + "Wh_Present_Count,NFH_Count,NFH_Present_Count,Shift,TimeIN,TimeOUT,Total_Hrs,OTHours,";
        //                Queryss = Queryss + "Attn_Date_Str,Attn_Date,MachineID_Encrypt,Total_Hrs1,TypeName,Present_Absent)" +
        //                 " Values('" + SessionCcode + "','" + SessionLcode + "','" + MachineID1 + "','" + MachineID1 + "','" + Present_Count + "'," +
        //                 "'" + Emp_WH_Day + "','" + Weekoff_Count + "','" + Present_WH_Count + "','" + NFH_Days_Count + "','" + NFH_Days_Present_Count + "'," +
        //                 "'" + Employee_Shift_Name_DB + "','" + INTime + "','" + OUTTime + "','" + time_Check_dbl + "','" + OT_Time + "','" + Convert.ToDateTime(Date).ToString("yyyy/MM/dd") + "',Convert(datetime,'" + Convert.ToDateTime(Date).ToString("yyyy/MM/dd") + "',120),'" + UTF8Encryption(MachineID1) + "','" + Emp_Total_Work_Time_1 + "','" + ShiftType + "','" + Pre_Abs + "')";

        //                ManualAttend.GetDetails(Queryss);

        //                Present_Count = 0;
        //                Present_WH_Count = 0;
        //                Employee_Shift_Name_DB = "No Shift";
        //                INTime = "";
        //                OUTTime = "";
        //                time_Check_dbl = 0;
        //                OT_Time = 0;
        //                Weekoff_Count = 0;
        //                Emp_WH_Day = "";

        //            }

        //            intI += 1; ;
        //            Total_Days_Count = 0;
        //            Final_Count = 0;
        //            Appsent_Count = 0;
        //            Present_Count = 0;
        //            Present_WH_Count = 0;
        //            NFH_Days_Count = 0;
        //            NFH_Days_Present_Count = 0;
        //            INTime = "";
        //            OUTTime = "";
        //            Weekoff_Count = 0;
        //            Emp_WH_Day = "";
        //        }


        //        Status = "Completed";
        //    }
        //    else
        //    {
        //        Status = "Already Exists";

        //    }

        //    return Json(Status, JsonRequestBehavior.AllowGet);
        //}


    }
}
