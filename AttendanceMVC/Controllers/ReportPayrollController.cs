﻿using AttendanceMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace AttendanceMVC.Controllers
{
    public class ReportPayrollController : Controller
    {
        static string Ccode = "";
        static string Lcode = "";
        static string SessionCcode = "";
        static string SessionLcode = "";
        static string SessionUserName;
        static string SessionUserID;
        System.Web.UI.WebControls.DataGrid grid =
                          new System.Web.UI.WebControls.DataGrid();

        string SSQL = "";
        // GET: ReportPayroll
        public ActionResult Voucher()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult PaySlip()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public JsonResult GetEmployeeType()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();


            string SQL = "Select EmpTypeCd,EmpType,CASE when EmpCategory='1' then 'Staff' else 'Labour' END as EmpCategory from MstEmployeeType ";
            dt = MasterDate.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.ID = dt.Rows[i]["EmpTypeCd"].ToString();
                list.Name = dt.Rows[i]["EmpType"].ToString();
                list.Category = dt.Rows[i]["EmpCategory"].ToString();
                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FinancialYear1()
        {
            List<MasterDate> PartyList = new List<MasterDate>();
            int currentYear = DateTime.Now.Year;
            for (int i = 0; i < 10; i++)
            {
                MasterDate list = new MasterDate();
                list.ID = currentYear.ToString();
                list.Name = (currentYear.ToString() + "-" + Convert.ToString(currentYear + 1)).ToString();
                currentYear = currentYear - 1;
                PartyList.Add(list);
            }
            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VoucherDetails(string FromDate, string ToDate, string WagesType, string FinYear, string Months)
        {

            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                GetVoucherDetails(FromDate, ToDate, WagesType, FinYear, Months);

                return View();

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }


        }

        public void GetVoucherDetails(string FromDate, string ToDate, string WagesType, string FinYear, string Months)
        {
            DataTable DT = new DataTable();

            SSQL = "Select EM.EmpNo as EmpNo,EM.FirstName as EmpName,DM.DeptName as Dept,CM.CateName as Type,SD.PerDaySal as SalperDay,EM.BasicSalary as BaseSalary,";
            SSQL = SSQL + "SD.WorkedDays as WorkDays,SD.GrossEarnings as EarnedSalary,SD.OTHoursNew as OTHrs,SD.OTHoursAmtNew as OTSalary,(SD.GrossEarnings+SD.OTHoursAmtNew) as TotalEarnedSalary,";
            SSQL = SSQL + "SD.PfSalary as PF,SD.ESI,SD.Advance,SD.DayIncentive as Incentive,SD.Messdeduction as LessCanteen,SD.Conveyance,SD.TotalDeductions as TotDed,SD.NetPay as NetSalary,";
            SSQL = SSQL + "SD.RoundOffNetPay as NetAmt,SD.FromBankACno as AccNo from Employee_Mst EM inner join SalaryDetails SD on EM.EmpNo=SD.EmpNo inner join Department_Mst DM";
            SSQL = SSQL + " on EM.DeptName=DM.DeptCode inner join MstCategory CM on EM.WageCategoty=CM.CateID";
            SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And DM.CompCode='" + SessionCcode + "' And DM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CM.CompCode='" + SessionCcode + "' And CM.LocCode='" + SessionLcode + "'";
            if (WagesType != "0")
            {
                SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
                SSQL = SSQL + " And SD.Wagestype='" + WagesType + "'";
            }
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(datetime,SD.FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                SSQL = SSQL + " And CONVERT(datetime,SD.ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
            }
            if (FinYear != "0")
            {
                SSQL = SSQL + " And SD.FinancialYear='" + FinYear + "'";
            }
            if (Months != "0")
            {
                SSQL = SSQL + " And SD.Month='" + Months + "'";
            }

            DT = ManualAttend.GetDetails(SSQL);


            DataTable DT_Comp = new DataTable();
            string CompName = "";
            SSQL = "Select *from Company_Mst where CompCode='" + SessionCcode + "'";
            DT_Comp = ManualAttend.GetDetails(SSQL);

            if (DT_Comp.Rows.Count != 0)
            {
                CompName = DT_Comp.Rows[0]["CompName"].ToString();
            }

            grid.DataSource = DT;
            grid.DataBind();
            string attachment = "attachment;filename=SALARY LIST.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='21'>");
            Response.Write("<a style=\"font-weight:bold\">" + CompName + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='21'>");
            Response.Write("<a style=\"font-weight:bold\">SALARY LIST &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='21'>");
            Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            Response.Write("&nbsp;&nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }

        public ActionResult PaySlipDetails(string FromDate, string ToDate, string WagesType, string FinYear, string Months)
        {

            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                GetPaySlipDetails(FromDate, ToDate, WagesType, FinYear, Months);

                return View();

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }


        }

        public void GetPaySlipDetails(string FromDate, string ToDate, string WagesType, string FinYear, string Months)
        {

            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable AutoDTable = new DataTable();
            DateTime date1;
            DateTime date2;
            DateTime Date2 = new DateTime();
            if (WagesType == "3")
            {
                
                AutoDTable.Columns.Add("S.NO");
                AutoDTable.Columns.Add("ExistingCode");
                AutoDTable.Columns.Add("Name");
                AutoDTable.Columns.Add("DeptName");
                Double TotalDays;
                date1 = Convert.ToDateTime(FromDate);
                string dat = ToDate;
                Date2 = Convert.ToDateTime(dat);

                int daycount = (int)((Date2 - date1).TotalDays);
                int daysAdded = 0;

                while (daycount >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());

                    //string datt = Convert.ToString(dayy.ToShortDateString());

                    //string[] dat1 = datt.Split('/');
                    //AutoDTable.Columns.Add(dat1[0].ToString());
                    AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                    daycount -= 1;
                    daysAdded += 1;
                }
                AutoDTable.Columns.Add("Total_Hrs");
                AutoDTable.Columns.Add("Base");
                AutoDTable.Columns.Add("OTHoursNew");
                AutoDTable.Columns.Add("OverTime");
                AutoDTable.Columns.Add("Advance");
                AutoDTable.Columns.Add("TotalDeductions");
                AutoDTable.Columns.Add("Net-Salary");
                AutoDTable.Columns.Add("Signature");
                // ,MG.GradeName,MC.CateName
                string SSQL = "";
                SSQL = "Select EM.EmpNo,isnull(EM.FirstName,'') as FirstName,isnull(DM.DeptName,'') As DeptName,isnull(DS.DesignName,'') As DesignName,";
                SSQL = SSQL + "SD.Fixed_Work_Days,SD.BasicandDA,SD.ESI,SD.LeaveDays,SD.Advance,SD.MediAllow,SD.allowances3,SD.Month,SD.FinancialYear,";
                SSQL = SSQL + "SD.WorkedDays,EM.BaseSalary,SD.allowances4,SD.allowances5,SD.Deduction3,SD.Deduction4,SD.Deduction5,";
                SSQL = SSQL + "SD.Messdeduction,SD.FromBankACno,SD.GrossEarnings,SD.TotalDeductions,SD.RoundOffNetPay,SD.NetPay,SD.OTHoursNew,SD.Wagestype,SD.OverTime,";
                SSQL = SSQL + "EM.DOJ,SD.ProvidentFund,SD.HRA,SD.ConvAllow,isnull(SD.PerDaySal,'0') As PerDaySal";
                SSQL = SSQL + " from Employee_Mst EM inner join SalaryDetails SD on EM.EmpNo=SD.EmpNo";
                SSQL = SSQL + " inner join Department_Mst DM on EM.DeptName = DM.DeptCode";
                SSQL = SSQL + " inner join Designation_Mst DS on EM.Designation = DS.DesignNo";
                //SSQL = SSQL + " inner join AttenanceDetails AT on AT.EmpNo = EM.EmpNo";
                //  SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
                SSQL = SSQL + " where EM.CompCode = '" + SessionCcode + "' And EM.LocCode = '" + SessionLcode + "' And SD.Ccode = '" + SessionCcode + "' And SD.Lcode = '" + SessionLcode + "'";
                SSQL = SSQL + " And DM.CompCode = '" + SessionCcode + "' And DM.LocCode = '" + SessionLcode + "' ";
                // And MG.CompCode = '" + SessionCcode + "' And MG.LocCode = '" + SessionLcode + "' ";
                // SSQL = SSQL + " And MC.CompCode = '" + SessionCcode + "' And MC.LocCode = '" + SessionLcode + "'";
                if (FromDate != "")
                {
                    SSQL = SSQL + " And Convert(datetime,SD.FromDate,120) = Convert(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                }

                if (ToDate != "")
                {
                    SSQL = SSQL + " And Convert(datetime,SD.ToDate,120) = Convert(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                }

                if (WagesType != "0")
                {
                    SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
                }

                if (FinYear != "")
                {
                    SSQL = SSQL + " And SD.FinancialYear='" + FinYear + "'";
                }

                if (Months != "0")
                {
                    SSQL = SSQL + " And SD.Month='" + Months + "'";
                }

                SSQL = SSQL + "";

                dt1 = ManualAttend.GetDetails(SSQL);




                SSQL = "";
                SSQL = "select CompName from Company_Mst where CompCode = '" + SessionCcode + "'";
                dt = ManualAttend.GetDetails(SSQL);

                decimal grandPerDaySal = 0;
                decimal grandOTHoursNew = 0;
                decimal grandOverTime = 0;
                decimal grandadvance = 0;
                decimal grandTotalDeductions = 0;
                decimal grandNetpay = 0;
                int sno = 1;
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    // string wages = dt1.Rows[i]["Wagestype"].ToString();
                    string Machno = "";
                    Machno = dt1.Rows[i]["EmpNo"].ToString();
                    SSQL = "Select * from AttenanceDetails where EmpNo='" + Machno + "' ";
                    SSQL = SSQL + "";

                    if (FromDate != "")
                    {
                        SSQL = SSQL + " And Convert(datetime,FromDate,120) = Convert(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                    }

                    if (ToDate != "")
                    {
                        SSQL = SSQL + " And Convert(datetime,ToDate,120) = Convert(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                    }
                    dt2 = ManualAttend.GetDetails(SSQL);

                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();

                    decimal PerDaySal = Convert.ToDecimal(dt1.Rows[i]["PerDaySal"].ToString());
                    decimal OTHoursNew = Convert.ToDecimal(dt1.Rows[i]["OTHoursNew"].ToString());
                    decimal OverTime = Convert.ToDecimal(dt1.Rows[i]["OverTime"].ToString());
                    decimal Advance = Convert.ToDecimal(dt1.Rows[i]["Advance"].ToString());
                    decimal TotalDeductions = Convert.ToDecimal(dt1.Rows[i]["TotalDeductions"].ToString());
                    decimal NetPay = Convert.ToDecimal(dt1.Rows[i]["NetPay"].ToString());

                    AutoDTable.Rows[i]["S.No"] = sno;
                    AutoDTable.Rows[i]["ExistingCode"] = dt1.Rows[i]["EmpNo"].ToString();
                    AutoDTable.Rows[i]["Name"] = dt1.Rows[i]["FirstName"].ToString();
                    AutoDTable.Rows[i]["DeptName"] = dt1.Rows[i]["DeptName"].ToString();

                    AutoDTable.Rows[i][4] = dt2.Rows[0]["Date1"].ToString();
                    AutoDTable.Rows[i][5] = dt2.Rows[0]["Date2"].ToString();
                    AutoDTable.Rows[i][6] = dt2.Rows[0]["Date3"].ToString();
                    AutoDTable.Rows[i][7] = dt2.Rows[0]["Date4"].ToString();
                    AutoDTable.Rows[i][8] = dt2.Rows[0]["Date5"].ToString();
                    AutoDTable.Rows[i][9] = dt2.Rows[0]["Date6"].ToString();
                    AutoDTable.Rows[i][10] = dt2.Rows[0]["Date7"].ToString();

                    AutoDTable.Rows[i]["Total_Hrs"] = dt1.Rows[i]["WorkedDays"].ToString();
                    AutoDTable.Rows[i]["Base"] = dt1.Rows[i]["PerDaySal"].ToString();
                    AutoDTable.Rows[i]["OTHoursNew"] = dt1.Rows[i]["OTHoursNew"].ToString();
                    AutoDTable.Rows[i]["OverTime"] = dt1.Rows[i]["OverTime"].ToString();
                  
                    AutoDTable.Rows[i]["Advance"] = dt1.Rows[i]["Advance"].ToString();
                    AutoDTable.Rows[i]["TotalDeductions"] = dt1.Rows[i]["TotalDeductions"].ToString();
                    AutoDTable.Rows[i]["Net-Salary"] = dt1.Rows[i]["NetPay"].ToString();
                    AutoDTable.Rows[i]["Signature"] = "";

                    sno = sno + 1;

                    grandPerDaySal = grandPerDaySal + PerDaySal;
                    grandOTHoursNew = grandOTHoursNew + OTHoursNew;
                    grandOverTime = grandOverTime + OverTime;
                    grandadvance = grandadvance + Advance;
                    grandTotalDeductions = grandTotalDeductions + TotalDeductions;
                    grandNetpay = grandNetpay + NetPay;



                }
                int count = AutoDTable.Rows.Count;
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[count]["Total_Hrs"] = "<span  Font-Bold = 'true'><b>" + "Total" + "</b></span>";
                AutoDTable.Rows[count]["Base"] = grandPerDaySal;
                AutoDTable.Rows[count]["OTHoursNew"] = grandOTHoursNew;
                AutoDTable.Rows[count]["OverTime"] = grandOverTime;
                AutoDTable.Rows[count]["Advance"] = grandadvance;
                AutoDTable.Rows[count]["TotalDeductions"] = grandTotalDeductions;
                AutoDTable.Rows[count]["Net-Salary"] = grandNetpay;
            }

            if (WagesType == "2")
            {
                AutoDTable.Columns.Add("S.NO");
                AutoDTable.Columns.Add("ExistingCode");
                AutoDTable.Columns.Add("Name");
                AutoDTable.Columns.Add("DeptName");
                Double TotalDays;
                date1 = Convert.ToDateTime(FromDate);
                string dat = ToDate;
                Date2 = Convert.ToDateTime(dat);

                int daycount = (int)((Date2 - date1).TotalDays);
                int daysAdded = 0;

                while (daycount >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());

                    //string datt = Convert.ToString(dayy.ToShortDateString());

                    //string[] dat1 = datt.Split('/');
                    //AutoDTable.Columns.Add(dat1[0].ToString());
                    AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                    daycount -= 1;
                    daysAdded += 1;
                }
                AutoDTable.Columns.Add("Total_Hrs");
                AutoDTable.Columns.Add("Base");
                AutoDTable.Columns.Add("Advance");
                AutoDTable.Columns.Add("TotalDeductions");
                AutoDTable.Columns.Add("Net-Salary");
                AutoDTable.Columns.Add("Signature");
                // ,MG.GradeName,MC.CateName
                string SSQL = "";
                SSQL = "Select EM.EmpNo,isnull(EM.FirstName,'') as FirstName,isnull(DM.DeptName,'') As DeptName,isnull(DS.DesignName,'') As DesignName,";
                SSQL = SSQL + "SD.Fixed_Work_Days,SD.BasicandDA,SD.ESI,SD.LeaveDays,SD.Advance,SD.MediAllow,SD.allowances3,SD.Month,SD.FinancialYear,";
                SSQL = SSQL + "SD.WorkedDays,EM.BaseSalary,SD.allowances4,SD.allowances5,SD.Deduction3,SD.Deduction4,SD.Deduction5,";
                SSQL = SSQL + "SD.Messdeduction,SD.FromBankACno,SD.GrossEarnings,SD.TotalDeductions,SD.RoundOffNetPay,SD.NetPay,SD.OTHoursNew,SD.Wagestype,";
                SSQL = SSQL + "EM.DOJ,SD.ProvidentFund,SD.HRA,SD.ConvAllow,isnull(SD.PerDaySal,'0') As PerDaySal";
                SSQL = SSQL + " from Employee_Mst EM inner join SalaryDetails SD on EM.EmpNo=SD.EmpNo";
                SSQL = SSQL + " inner join Department_Mst DM on EM.DeptName = DM.DeptCode";
                SSQL = SSQL + " inner join Designation_Mst DS on EM.Designation = DS.DesignNo";
                // SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID";
                //  SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
                SSQL = SSQL + " where EM.CompCode = '" + SessionCcode + "' And EM.LocCode = '" + SessionLcode + "' And SD.Ccode = '" + SessionCcode + "' And SD.Lcode = '" + SessionLcode + "'";
                SSQL = SSQL + " And DM.CompCode = '" + SessionCcode + "' And DM.LocCode = '" + SessionLcode + "' ";
                // And MG.CompCode = '" + SessionCcode + "' And MG.LocCode = '" + SessionLcode + "' ";
                // SSQL = SSQL + " And MC.CompCode = '" + SessionCcode + "' And MC.LocCode = '" + SessionLcode + "'";
                if (FromDate != "")
                {
                    SSQL = SSQL + " And Convert(datetime,SD.FromDate,120) = Convert(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                }

                if (ToDate != "")
                {
                    SSQL = SSQL + " And Convert(datetime,SD.ToDate,120) = Convert(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                }

                if (WagesType != "0")
                {
                    SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
                }

                if (FinYear != "")
                {
                    SSQL = SSQL + " And SD.FinancialYear='" + FinYear + "'";
                }

                if (Months != "0")
                {
                    SSQL = SSQL + " And SD.Month='" + Months + "'";
                }

                SSQL = SSQL + "";

                dt1 = ManualAttend.GetDetails(SSQL);




                SSQL = "";
                SSQL = "select CompName from Company_Mst where CompCode = '" + SessionCcode + "'";
                dt = ManualAttend.GetDetails(SSQL);

                decimal grandbbasesalary = 0;
                decimal grandgrossearning = 0;
                decimal grandadvance = 0;
                decimal grandtotaldeduct = 0;
                decimal grandnetpay = 0;

                int sno = 1;
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    // string wages = dt1.Rows[i]["Wagestype"].ToString();
                    string Machno = "";
                    Machno = dt1.Rows[i]["EmpNo"].ToString();
                    SSQL = "Select * from AttenanceDetails where EmpNo='" + Machno + "' ";
                    SSQL = SSQL + "";

                    if (FromDate != "")
                    {
                        SSQL = SSQL + " And Convert(datetime,FromDate,120) = Convert(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                    }

                    if (ToDate != "")
                    {
                        SSQL = SSQL + " And Convert(datetime,ToDate,120) = Convert(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                    }
                    dt2 = ManualAttend.GetDetails(SSQL);

                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();

                    AutoDTable.Rows[i]["S.No"] = sno;
                    AutoDTable.Rows[i]["ExistingCode"] = dt1.Rows[i]["EmpNo"].ToString();
                    AutoDTable.Rows[i]["Name"] = dt1.Rows[i]["FirstName"].ToString();
                    AutoDTable.Rows[i]["DeptName"] = dt1.Rows[i]["DeptName"].ToString();

                    AutoDTable.Rows[i][4] = dt2.Rows[0]["Date1"].ToString();
                    AutoDTable.Rows[i][5] = dt2.Rows[0]["Date2"].ToString();
                    AutoDTable.Rows[i][6] = dt2.Rows[0]["Date3"].ToString();
                    AutoDTable.Rows[i][7] = dt2.Rows[0]["Date4"].ToString();
                    AutoDTable.Rows[i][8] = dt2.Rows[0]["Date5"].ToString();
                    AutoDTable.Rows[i][9] = dt2.Rows[0]["Date6"].ToString();
                    AutoDTable.Rows[i][10] = dt2.Rows[0]["Date7"].ToString();

                    decimal basesalary = Convert.ToDecimal(dt1.Rows[i]["PerDaySal"].ToString());
                    decimal grossearning = Convert.ToDecimal(dt1.Rows[i]["GrossEarnings"].ToString());
                    decimal Advance = Convert.ToDecimal(dt1.Rows[i]["Advance"].ToString());
                    decimal totalDeduct = Convert.ToDecimal(dt1.Rows[i]["TotalDeductions"].ToString());
                    decimal netpay = Convert.ToDecimal(dt1.Rows[i]["RoundOffNetPay"].ToString());

                    AutoDTable.Rows[i]["Total_Hrs"] = dt1.Rows[i]["WorkedDays"].ToString();
                    AutoDTable.Rows[i]["Base"] = dt1.Rows[i]["PerDaySal"].ToString();
                    AutoDTable.Rows[i]["Advance"] = dt1.Rows[i]["Advance"].ToString();
                    AutoDTable.Rows[i]["TotalDeductions"] = dt1.Rows[i]["TotalDeductions"].ToString();
                    AutoDTable.Rows[i]["Net-Salary"] = dt1.Rows[i]["NetPay"].ToString();
                    AutoDTable.Rows[i]["Signature"] = "";

                    sno = sno + 1;
                    grandbbasesalary = grandbbasesalary + basesalary;
                    grandgrossearning = grandgrossearning + grossearning;
                    grandadvance = grandadvance + Advance;
                    grandtotaldeduct = grandtotaldeduct + totalDeduct;
                    grandnetpay = grandnetpay + netpay;



                }
                int count = AutoDTable.Rows.Count;
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[count]["Total_Hrs"] = "<span  Font-Bold = 'true'><b>" + "Total" + "</b></span>";
                AutoDTable.Rows[count]["Base"] = grandbbasesalary;
               // AutoDTable.Rows[count]["GrossEarnings"] = grandgrossearning;
                AutoDTable.Rows[count]["Advance"] = grandadvance;
                AutoDTable.Rows[count]["TotalDeductions"] = grandtotaldeduct;
                AutoDTable.Rows[count]["Net-Salary"] = grandnetpay;
            }

            if (WagesType == "1")
            {
                AutoDTable.Columns.Add("S.NO");
                AutoDTable.Columns.Add("MachineID");
                AutoDTable.Columns.Add("ExistingCode");
                AutoDTable.Columns.Add("Name");
                AutoDTable.Columns.Add("DeptName");
                AutoDTable.Columns.Add("WorkingDays");
                AutoDTable.Columns.Add("Base");
                AutoDTable.Columns.Add("Ot_Days");
                AutoDTable.Columns.Add("Ot_Hrs_salary");
                AutoDTable.Columns.Add("allowances");
                AutoDTable.Columns.Add("GrossEarnings");
                AutoDTable.Columns.Add("Deduction");
                AutoDTable.Columns.Add("Advance");
                AutoDTable.Columns.Add("TotalDeduction");
                AutoDTable.Columns.Add("Netpay");
                AutoDTable.Columns.Add("Signature");

                // ,MG.GradeName,MC.CateName
                string SSQL = "";
                SSQL = "Select EM.EmpNo,EM.ExistingCode,isnull(EM.FirstName,'') as FirstName,isnull(DM.DeptName,'') As DeptName,isnull(DS.DesignName,'') As DesignName,";
                SSQL = SSQL + "SD.Fixed_Work_Days,SD.BasicandDA,SD.ESI,SD.LeaveDays,SD.Advance,SD.MediAllow,SD.allowances3,SD.Month,SD.FinancialYear,SD.ThreesidedAmt,";
                SSQL = SSQL + "SD.WorkedDays,EM.BaseSalary,SD.allowances4,SD.allowances5,SD.Deduction3,SD.Deduction4,SD.Deduction5,AT.Weekoff,";
                SSQL = SSQL + "SD.Messdeduction,SD.FromBankACno,SD.GrossEarnings,SD.TotalDeductions,SD.RoundOffNetPay,SD.NetPay,SD.OTHoursNew,SD.Wagestype,";
                SSQL = SSQL + "EM.DOJ,SD.ProvidentFund,SD.HRA,SD.ConvAllow,isnull(SD.PerDaySal,'0') As PerDaySal";
                SSQL = SSQL + " from Employee_Mst EM inner join SalaryDetails SD on EM.EmpNo=SD.EmpNo";
                SSQL = SSQL + " inner join Department_Mst DM on EM.DeptName = DM.DeptCode";
                SSQL = SSQL + " inner join Designation_Mst DS on EM.Designation = DS.DesignNo";
                SSQL = SSQL + " inner join AttenanceDetails AT on AT.EmpNo = EM.EmpNo And SD.Month=AT.Months And SD.FinancialYear=AT.FinancialYear";
                // SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID";
                //  SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
                SSQL = SSQL + " where EM.CompCode = '" + SessionCcode + "' And EM.LocCode = '" + SessionLcode + "' And SD.Ccode = '" + SessionCcode + "' And SD.Lcode = '" + SessionLcode + "'";
                SSQL = SSQL + " And DM.CompCode = '" + SessionCcode + "' And DM.LocCode = '" + SessionLcode + "' ";
                // And MG.CompCode = '" + SessionCcode + "' And MG.LocCode = '" + SessionLcode + "' ";
                // SSQL = SSQL + " And MC.CompCode = '" + SessionCcode + "' And MC.LocCode = '" + SessionLcode + "'";
                if (FromDate != "")
                {
                    SSQL = SSQL + " And Convert(datetime,SD.FromDate,120) = Convert(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                }

                if (ToDate != "")
                {
                    SSQL = SSQL + " And Convert(datetime,SD.ToDate,120) = Convert(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                }

                if (WagesType != "0")
                {
                    SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
                }

                if (FinYear != "")
                {
                    SSQL = SSQL + " And SD.FinancialYear='" + FinYear + "'";
                }

                if (Months != "0")
                {
                    SSQL = SSQL + " And SD.Month='" + Months + "'";
                }

                SSQL = SSQL + "";

                dt1 = ManualAttend.GetDetails(SSQL);




                SSQL = "";
                SSQL = "select CompName from Company_Mst where CompCode = '" + SessionCcode + "'";
                dt = ManualAttend.GetDetails(SSQL);
                decimal deduction = 0;
                string ded3 = "";
                string ded4 = "";
                string ded5 = "";
                int sno = 1;
                decimal grandbbasesalary = 0;
                decimal grandgrossearning = 0;
                decimal grandadvance = 0;
                decimal grandtotaldeduct = 0;
                decimal grandnetpay = 0;
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    ded3 = dt1.Rows[i]["Deduction3"].ToString();
                    ded4 = dt1.Rows[i]["Deduction4"].ToString();
                    ded5 = dt1.Rows[i]["Deduction5"].ToString();
                    deduction = Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5);


                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();

                    decimal basesalary = Convert.ToDecimal(dt1.Rows[i]["PerDaySal"].ToString());
                    decimal grossearning = Convert.ToDecimal(dt1.Rows[i]["GrossEarnings"].ToString());
                    decimal Advance = Convert.ToDecimal(dt1.Rows[i]["Advance"].ToString());
                    decimal totalDeduct = Convert.ToDecimal(dt1.Rows[i]["TotalDeductions"].ToString());
                    decimal netpay = Convert.ToDecimal(dt1.Rows[i]["RoundOffNetPay"].ToString());



                    AutoDTable.Rows[i]["S.No"] = sno;
                    AutoDTable.Rows[i]["MachineID"] = dt1.Rows[i]["EmpNo"].ToString();
                    AutoDTable.Rows[i]["ExistingCode"] = dt1.Rows[i]["ExistingCode"].ToString();
                    AutoDTable.Rows[i]["Name"] = dt1.Rows[i]["FirstName"].ToString();
                    AutoDTable.Rows[i]["DeptName"] = dt1.Rows[i]["DeptName"].ToString();
                    AutoDTable.Rows[i]["WorkingDays"] = dt1.Rows[i]["WorkedDays"].ToString();

                    AutoDTable.Rows[i]["Base"] = dt1.Rows[i]["PerDaySal"].ToString();
                    AutoDTable.Rows[i]["Ot_Days"] = dt1.Rows[i]["weekoff"].ToString();

                    AutoDTable.Rows[i]["Ot_Hrs_salary"] = dt1.Rows[i]["ThreesidedAmt"].ToString();
                    AutoDTable.Rows[i]["allowances"] = dt1.Rows[i]["allowances3"].ToString();
                    AutoDTable.Rows[i]["GrossEarnings"] = dt1.Rows[i]["GrossEarnings"].ToString();
                    AutoDTable.Rows[i]["Deduction"] = ded3;
                    AutoDTable.Rows[i]["Advance"] = dt1.Rows[i]["Advance"].ToString();
                    AutoDTable.Rows[i]["TotalDeduction"] = dt1.Rows[i]["TotalDeductions"].ToString();
                    AutoDTable.Rows[i]["Netpay"] = dt1.Rows[i]["RoundOffNetPay"].ToString();
                    AutoDTable.Rows[i]["Signature"] = "";

                    sno = sno + 1;

                    grandbbasesalary = grandbbasesalary + basesalary;
                    grandgrossearning = grandgrossearning + grossearning;
                    grandadvance = grandadvance + Advance;
                    grandtotaldeduct = grandtotaldeduct + totalDeduct;
                    grandnetpay = grandnetpay + netpay;

                }
                int count = AutoDTable.Rows.Count;
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[count]["WorkingDays"] = "<span  Font-Bold = 'true'><b>" + "Total" + "</b></span>";
                AutoDTable.Rows[count]["Base"] = grandbbasesalary;
                AutoDTable.Rows[count]["GrossEarnings"] = grandgrossearning;
                AutoDTable.Rows[count]["Advance"] = grandadvance;
                AutoDTable.Rows[count]["TotalDeduction"] = grandtotaldeduct;
                AutoDTable.Rows[count]["Netpay"] = grandnetpay;
            }

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=PAY SLIP.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' Font.Size ='20' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">NEW - PAYSLIP REPORT &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\"> Salary Month of " + "  " + FromDate + "-" + ToDate + "</a>");
            Response.Write("&nbsp;&nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\"> </a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();



        }


        private static void DrawLine(PdfWriter writer, float x1, float y1, float x2, float y2)
        {
            PdfContentByte contentByte = writer.DirectContent;
            //contentByte.SetColorStroke(color);
            contentByte.MoveTo(x1, y1);
            contentByte.LineTo(x2, y2);
            contentByte.Stroke();
        }

        private static PdfPCell PhraseCell(Phrase phrase, int align)
        {
            PdfPCell cell = new PdfPCell(phrase);
            //cell.BorderColor = Color.WHITE;
            //cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingBottom = 2f;
            cell.PaddingTop = 0f;
            return cell;
        }

    }
}