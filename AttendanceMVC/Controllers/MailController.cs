﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using AttendanceMVC.Models;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

using System.Net;

namespace AttendanceMVC.Controllers
{
    public class MailController : Controller
    {
        MailMessage mail = new MailMessage();
        string SessionCcode = "Kavitha";
        string SessionLcode = "UNIT I";
        string SessionCompanyName = "KAVITHA TEXTILES";
        // GET: Mail
        public ActionResult Index()
        {
            return View();
        }



        public ActionResult MailDayAttendanceDayWise()
        {


            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                //SessionUserName = Session["Usernmdisplay"].ToString();
                //SessionUserID = Session["UserId"].ToString();

                GetDayAttendanceDayWise_Mail();
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }


        public void GetDayAttendanceDayWise_Mail()
        {
            DateTime Date1;
            DataSet ds = new DataSet();
            DataTable AutoDTable = new DataTable();
            string SSQL = "";


            Date1 = DateTime.Now.AddDays(-1);
            DateTime FromDate = DateTime.Now.AddDays(-1);




            DataTable Shift_DS = new DataTable();


            Document document = new Document(PageSize.A4, 25, 25, 45, 40);
            Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            writer.PageEvent = new HeaderFooter();
            document.Open();


            PdfPTable table = new PdfPTable(5);
            float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
            table.SetWidths(widths);
            table.WidthPercentage = 100;

            PdfPTable table1 = new PdfPTable(7);
            float[] widths1 = new float[] { 10f, 15f, 15f, 35f, 15f, 15f, 15f };
            table1.SetWidths(widths1);
            table1.WidthPercentage = 100;

            PdfPTable table2 = new PdfPTable(4);
            float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            table2.SetWidths(widths2);
            table2.WidthPercentage = 100;

            PdfPCell cell;

            DataTable Depart = new DataTable();


            SSQL = "select MachineID,isnull(DeptName,'') As DeptName,Shift,isnull(FirstName,'') as FirstName,TimeIN,TimeOUT,";
            SSQL = SSQL + "SubCatName,Total_Hrs1 from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            SSQL = SSQL + "  And Shift !='No Shift'  And TimeIN!=''";

            AutoDTable = ManualAttend.GetDetails(SSQL);


            SSQL = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";


            //SSQL = " Group by ShiftDesc Asc ";

            Shift_DS = ManualAttend.GetDetails(SSQL);

            SSQL = "select distinct DeptCode, DeptName from Department_Mst order by DeptName Asc";
            Depart = ManualAttend.GetDetails(SSQL);

            DataTable SubCat = new DataTable();

            SSQL = "select SubCatName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' group by  SubCatName ";
            SubCat = ManualAttend.GetDetails(SSQL);

            //table1.TableEvent = new BorderEvent();

            if (AutoDTable.Rows.Count > 0)
            {
                cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("DAY ATTENDANCE DAY WISE", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Total No.of Employee: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.BackgroundColor = BaseColor.YELLOW;
                cell.Colspan = 2;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                //cell.Border = 0;
                //cell.Colspan = 7;
                ////cell.PaddingTop = 5f;
                ////cell.PaddingBottom = 5f;
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER;
                cell.Colspan = 7;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("EmpNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("GrandTOT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.BOTTOM_BORDER;
                cell.Colspan = 7;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                int Count = 0;
                int Count1 = 0;
                int Count2 = 0;
                int Count3 = 0;
                int SubInsider = 0;
                int SubOutSider = 0;

                int SNo = 1;

                int IntK = 0;


                for (int j = 0; j < Shift_DS.Rows.Count; j++)
                {
                    string Shift = Shift_DS.Rows[j]["ShiftDesc"].ToString();


                    cell = PhraseCell(new Phrase(" " + Shift_DS.Rows[j]["ShiftDesc"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 7;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    for (int In = 0; In < AutoDTable.Rows.Count; In++)
                    {
                        string EmpNames = AutoDTable.Rows[In]["FirstName"].ToString();
                        string Department = AutoDTable.Rows[In]["DeptName"].ToString();
                        string ShiftType = AutoDTable.Rows[In]["Shift"].ToString();

                        if (EmpNames == "" && Department == "" && Shift == ShiftType)
                        {
                            cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["SubCatName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["TimeOUT"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["Total_Hrs1"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            SNo = SNo + 1;
                        }

                    }

                    Count = 0;



                    for (int k = 0; k < Depart.Rows.Count; k++)
                    {
                        string Deprt = Depart.Rows[k]["DeptCode"].ToString();

                        cell = PhraseCell(new Phrase(" " + Depart.Rows[k]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.Colspan = 7;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        table1.AddCell(cell);
                        Count1 = 0;

                        Count2 = 0;

                        SubInsider = 0;
                        SubOutSider = 0;

                        //for (int m = 0; m < SubCat.Rows.Count; m++)
                        //{
                        //    string Sub = SubCat.Rows[m]["SubCatName"].ToString();

                        //    Count3 = 0;

                        for (int i = 0; i < AutoDTable.Rows.Count; i++)
                        {
                            string Deprt1 = AutoDTable.Rows[i]["DeptName"].ToString();
                            string Shift1 = AutoDTable.Rows[i]["Shift"].ToString();
                            string Sub1 = AutoDTable.Rows[i]["SubCatName"].ToString();

                            if (Deprt == Deprt1 && Shift == Shift1)
                            {
                                cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["SubCatName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOUT"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs1"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                Count = Count + 1;

                                Count2 = Count2 + 1;

                                //Count3 = Count3 + 1;

                                SNo = SNo + 1;

                            }
                        }

                        if (Count2 > 0)
                        {

                            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" Total: " + Count2, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                        }


                        if (Count2 == 0)
                        {
                            table1.DeleteLastRow();
                        }
                    }

                    if (Count == 0)
                    {
                        table1.DeleteLastRow();
                    }
                }

                //if (Count == 0)
                //{
                //    table1.DeleteLastRow();
                //}

            }
            else
            {

                cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("DAY ATTENDANCE DAY WISE", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" No Data Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 7;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

            }

            table.SpacingBefore = 10f;
            table.SpacingAfter = 10f;

            document.Add(table);

            table1.SpacingBefore = 5f;
            table1.SpacingAfter = 5f;

            document.Add(table1);


            document.Close();

            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=DAY_ATTENDANCE_DAY_WISE.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            mail.To.Add("kavithatextiless@gmail.com");
            mail.From = new MailAddress("aatm2005@gmail.com");
            mail.Subject = "SRI Kavitha Textiles - UNIT I";
            mail.Body = "Find the attachment for Daily Report";
            mail.IsBodyHtml = true;
            // Attachment data = new Attachment(document, MediaTypeNames.Application.Pdf);
            //mail.Attachments.Add(document);

            mail.Attachments.Add(new Attachment(new MemoryStream(bytes), "iTextSharpPDF.pdf"));


            //Attachment att = new Attachment(memoryStream,"Daywise");
           // Attachment att = new Attachment(memoryStream, "Daywise");
            //mail.Attachments.Add(att);

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
            smtp.Credentials = new System.Net.NetworkCredential("aatm2005@gmail.com", "Altius@2005");  //Or your Smtp Email ID and Password
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = true;
            smtp.Send(mail);

            Response.End();
            Response.Close();


        }

        private static PdfPCell PhraseCell(Phrase phrase, int align)
        {
            PdfPCell cell = new PdfPCell(phrase);
            //cell.BorderColor = Color.WHITE;
            //cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingBottom = 2f;
            cell.PaddingTop = 0f;
            return cell;
        }


    }
}