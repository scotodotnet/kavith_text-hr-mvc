﻿using AttendanceMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.UI;
using System.Globalization;

namespace AttendanceMVC.Controllers
{
    public class ReportController : Controller
    {
        static string SessionCcode = "";
        static string SessionLcode = "";
        static string SessionUserName;
        static string SessionUserID;
        System.Web.UI.WebControls.DataGrid grid =
                          new System.Web.UI.WebControls.DataGrid();

        DataTable DataCell = new DataTable();
        string Date_Value_Str1;

        string SSQL = "";
        DataTable AutoDTable = new DataTable();
        DataTable mEmployeeDT = new DataTable();
        DataTable MEmployeeDS = new DataTable();
        DataTable DataCells = new DataTable(); // mLocalDS
        DataTable PayRollDT = new DataTable();
        DataTable mLocalDS = new DataTable();
        DataTable mLocalDS1 = new DataTable();
        DateTime date1;
        DateTime date2;
        DateTime Date2 = new DateTime();
        int intK;
        static string SessionUserType;
        string Date_Value_Str;
        string Date_value_str1;
        string[] Time_Minus_Value_Check;
        string WagesType;
        int dayCount;

        string[] Att_Date_Check;
        string Att_Already_Date_Check;
        string Att_Year_Check;
        string Month_Name_Change;
        string halfPresent;

        string Datestr = "";
        string Datestr1 = "";
        DateTime ShiftdateStartIN;
        DateTime ShiftdateEndIN;
        DateTime InTime_Check;
        DateTime InToTime_Check;
        DateTime EmpdateIN;
        TimeSpan InTime_TimeSpan;
        string From_Time_Str = "";
        string To_Time_Str = "";
        string Time_IN_Str;
        string Time_OUT_Str;
        string Shift_Start_Time;
        string Shift_End_Time;
        string Final_Shift;
        string Total_Time_get;
        int time_Check_dbl;
        Boolean Shift_Check_blb = false;

        string[] OThour_Str;
        int OT_Hour;
        double OT_Min;
        double OT_Time;
        string OT_Hour1 = "";
        string Final_InTime;

        DateTime dtime;
        DateTime dtime1;

        TimeSpan ts4 = new TimeSpan();

        DataTable DS_Time = new DataTable();
        DataTable DS_InTime = new DataTable();
        string Final_OutTime = "";
        DataTable Shift_DS_Change = new DataTable();

        Int32 K = 0;

        string Shift_Start_Time_Change = null;
        string Shift_End_Time_Change = null;
        string Employee_Time_Change = "";
        DateTime ShiftdateStartIN_Change = default(DateTime);
        DateTime ShiftdateEndIN_Change = default(DateTime);
        DateTime EmpdateIN_Change = default(DateTime);

        double Final_Count;

        double Present_Count;
        double Absent_Count;
        decimal Fixed_Work_Days;
        decimal Month_WH_Count;
        double Present_WH_Count;
        double NFH_Days_Count;
        double NFH_Days_Present_Count;


        bool isPresent = false;



        DataTable AutoDataTable = new DataTable();
        DataTable mDataSet = new DataTable();
        DataTable dsEmployee = new DataTable();
        DataTable mEmployeeDS = new DataTable();
        int ik = 0;
     
        DateTime fromdate;
       
        string SSQL_OUT;
       

       
        string Employee_Week_Name;
        string Assign_Week_Name;
       
        
        string mm;
      
        string CatName = "";
        int shiftCount = 0;

        // GET: Report
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ReportMaster()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();
                SessionUserType = Session["Isadmin"].ToString();
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }


        public JsonResult GettShifType()
        {
            DataTable SHift = new DataTable();
            string SSQL = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SHift = ManualAttend.GetDetails(SSQL);

            List<ManualAttend> empList = new List<ManualAttend>();

            for (int i = 0; i < SHift.Rows.Count; i++)
            {
                ManualAttend student = new ManualAttend();

                student.ShiftType = SHift.Rows[i]["ShiftDesc"].ToString();

                empList.Add(student);
            }

            //student.ShiftType = "No Shift";
            //empList.Add(student);

            return Json(empList, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetDepartment()
        {
            //var Dept = db_Entity.Department_Mst.ToList();


            DataTable dt = new DataTable();

            string SQL = "Select *from Department_Mst  ";
            //SQL = SQL + "where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";

            dt = MasterDate.GetDetails(SQL);

            List<MasterDate> Dept = new List<MasterDate>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate List = new MasterDate();
                List.ID = dt.Rows[i]["DeptCode"].ToString();
                List.Name = dt.Rows[i]["DeptName"].ToString();

                Dept.Add(List);
            }

            return Json(Dept, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmployee()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();


            string SQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            dt = MasterDate.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.ID = dt.Rows[i]["MachineID"].ToString();
                list.Name = dt.Rows[i]["MachineID"].ToString()+"-->"+dt.Rows[i]["FirstName"].ToString();
                

                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GettWagesType()
        {
            DataTable SHift = new DataTable();
            string SSQL = "select Distinct EmpTypeCd,EmpType from MstEmployeeType order by EmpType Asc";
            SHift = ManualAttend.GetDetails(SSQL);

            List<ManualAttend> empList = new List<ManualAttend>();

            for (int i = 0; i < SHift.Rows.Count; i++)
            {
                ManualAttend student = new ManualAttend();
                student.ID = SHift.Rows[i]["EmpTypeCd"].ToString();
                student.WagesType = SHift.Rows[i]["EmpType"].ToString();

                empList.Add(student);
            }

            return Json(empList, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetEmpID()
        {

            DataTable dt = new DataTable();
            if (SessionUserName == "IF")
            {
                string SQL = "select EmpNo,ExistingCode,(FirstName + '.' + MiddleInitial) as EmpName,DM.DeptName from Employee_Mst EM inner join Department_Mst DM on EM.DeptName=DM.DeptCode where EM.IsActive='Yes' And (Wages='1' or Wages='2')";
                SQL = SQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
                SQL = SQL + " And DM.CompCode='" + SessionCcode + "' And DM.LocCode='" + SessionLcode + "'";
                dt = ManualAttend.GetDetails(SQL);
            }
            else
            {
                string SQL = "select EmpNo,ExistingCode,(FirstName + '.' + MiddleInitial) as EmpName,DM.DeptName from Employee_Mst EM inner join Department_Mst DM on EM.DeptName=DM.DeptCode where EM.IsActive='Yes'";
                SQL = SQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
                SQL = SQL + " And DM.CompCode='" + SessionCcode + "' And DM.LocCode='" + SessionLcode + "'";
                dt = ManualAttend.GetDetails(SQL);
            }
            List<ManualAttend> PartyList = new List<ManualAttend>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend Qtylist = new ManualAttend();
                Qtylist.EmpNo = dt.Rows[i]["EmpNo"].ToString();
                Qtylist.ExistingCode = dt.Rows[i]["ExistingCode"].ToString();
                Qtylist.EmpName = dt.Rows[i]["EmpName"].ToString();
                Qtylist.DeptName = dt.Rows[i]["DeptName"].ToString();
                PartyList.Add(Qtylist);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetEditEmpID(string EmpNo)
        {
            DataTable employees = new DataTable();

            string query = "select EmpNo,ExistingCode,(FirstName + '.' + MiddleInitial) as EmpName,DM.DeptName from Employee_Mst EM inner join Department_Mst DM on EM.DeptName=DM.DeptName where EM.IsActive='Yes' and EmpNo='" + EmpNo + "'";

            employees = ManualAttend.GetDetails(query);

            List<ManualAttend> empList = new List<ManualAttend>();
            for (int i = 0; i < employees.Rows.Count; i++)
            {
                ManualAttend student = new ManualAttend();
                student.EmpNo = employees.Rows[i]["EmpNo"].ToString();
                student.ExistingCode = employees.Rows[i]["ExistingCode"].ToString();
                student.EmpName = employees.Rows[i]["EmpName"].ToString();
                student.DeptName = employees.Rows[i]["DeptName"].ToString();


                empList.Add(student);
            }


            return Json(empList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetReportMenu()
        {
            DataTable SHift = new DataTable();
            string SSQL = "select Distinct Report_Head from ReportMenu_Main";
            SHift = ManualAttend.GetDetails(SSQL);

            List<ManualAttend> empList = new List<ManualAttend>();

            for (int i = 0; i < SHift.Rows.Count; i++)
            {
                ManualAttend student = new ManualAttend();
                student.UserName = SHift.Rows[i]["Report_Head"].ToString();

                empList.Add(student);
            }

            return Json(empList, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetReportSubMenu(string Report_Head)
        {
            DataTable SHift = new DataTable();
            if (SessionUserName == "IF")
            {
                string SSQL = "select Distinct Report_SUb from ReportMenu_Sub  where Report_Head = '" + Report_Head + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
                SHift = ManualAttend.GetDetails(SSQL);
            }
            else
            {
                string SSQL = "select Distinct Report_SUb from ReportMenu_Sub  where Report_Head = '" + Report_Head + "'";
                SHift = ManualAttend.GetDetails(SSQL);
            }


            List<ManualAttend> empList = new List<ManualAttend>();

            for (int i = 0; i < SHift.Rows.Count; i++)
            {
                ManualAttend student = new ManualAttend();
                student.WagesType = SHift.Rows[i]["Report_SUb"].ToString();

                empList.Add(student);
            }

            return Json(empList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult EmployeeList()
        {

            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                EmployeefullList();

                return View();

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }


        }

        public ActionResult MailReport()
        {

            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                EmployeefullList();

                return View();

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult DayAttendanceDayWise(string FromDate, string ShiftType)
        {


            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                GetDayAttendanceDayWise(FromDate, ShiftType);
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            

            

        }

        public ActionResult MusterReportBWDates(string WagesType, string FromDate, string ToDate)
        {

            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                GetDayAttendanceDayWiseBTDates(WagesType, FromDate, ToDate);

                return View();

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

         
        }

        public void GetDayAttendanceDayWiseBTDates(string WagesType, string FromDate, string ToDate)
        {
            if (SessionUserType == "2")
            {
                NonAdminGetDayAttendanceBTDates_Changes(WagesType, FromDate, ToDate);
            }
            else
            {
                double Total_Time_get;
                DataTable dt = new DataTable();
                DataTable dt1 = new DataTable();

                AutoDTable.Columns.Add("SNo");
                AutoDTable.Columns.Add("EmployeeNo");
                AutoDTable.Columns.Add("ExistingCode");
                AutoDTable.Columns.Add("Name");
                AutoDTable.Columns.Add("DeptName");

                date1 = Convert.ToDateTime(FromDate);
                string dat = ToDate;
                Date2 = Convert.ToDateTime(dat);

                int daycount = (int)((Date2 - date1).TotalDays);
                int daysAdded = 0;

                while (daycount >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                    //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                    AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                    daycount -= 1;
                    daysAdded += 1;
                }

                AutoDTable.Columns.Add("Total Days");


                SSQL = "select EM.MachineID,isnull(EM.FirstName,'') as FirstName,EM.ExistingCode,DM.DeptName,EM.OTEligible from Employee_Mst EM";
                SSQL = SSQL + " inner join Department_Mst DM on EM.DeptName = DM.DeptCode";
                SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' and EM.IsActive='Yes' ";

                if (WagesType != "0")
                {
                    SSQL = SSQL + " And Wages='" + WagesType + "'";
                }

                SSQL = SSQL + " Group by EM.MachineID,EM.FirstName,EM.ExistingCode,DM.DeptName,EM.OTEligible order by MachineID";
                dt = ManualAttend.GetDetails(SSQL);

                intK = 0;

                Present_WH_Count = 0;

                int SNo = 1;

                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();

                    string MachineID = dt.Rows[i]["MachineID"].ToString();

                    if (MachineID == "2")
                    {
                        string Ss = "dhghdg";
                    }
                    AutoDTable.Rows[intK]["SNo"] = SNo;
                    AutoDTable.Rows[intK]["EmployeeNo"] = dt.Rows[i]["MachineID"].ToString();
                    AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
                    AutoDTable.Rows[intK]["Name"] = dt.Rows[i]["FirstName"].ToString();
                    AutoDTable.Rows[intK]["DeptName"] = dt.Rows[i]["DeptName"].ToString();

                    string OTEllible = "";
                    OTEllible = dt.Rows[i]["OTEligible"].ToString();

                    int count = 5;
                    decimal count1 = 0;



                    for (int j = 0; j < daysAdded; j++)
                    {
                        DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                        string Date1 = dayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);

                        if (SessionUserName == "IF")
                        {
                            SSQL = "select Wh_Present_Count,Present,Total_Hrs,OTHours,CatName from IFLogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Date1 + "',120)";

                            dt1 = ManualAttend.GetDetails(SSQL);

                        }
                        else
                        {
                            SSQL = "select Wh_Present_Count,Present,Total_Hrs,OTHours,CatName from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Date1 + "',120)";

                            dt1 = ManualAttend.GetDetails(SSQL);
                        }
                        if (dt1.Rows.Count > 0)
                        {
                            decimal count2 = 0;
                            string Category = "";
                            Category = dt1.Rows[0]["CatName"].ToString();
                            string dept = "";
                            dept = AutoDTable.Rows[intK]["DeptName"].ToString();
                            Present_WH_Count = Convert.ToDouble(dt1.Rows[0]["Wh_Present_Count"].ToString());
                            Present_Count = Convert.ToDouble(dt1.Rows[0]["Present"].ToString());
                            Total_Time_get = Convert.ToDouble(dt1.Rows[0]["Total_Hrs"].ToString());


                            if (Category.ToUpper() == "STAFF" || dept.ToUpper() == "TEXTILES")
                            {
                                if (Total_Time_get >= 9)
                                {
                                    OT_Time = OT_Time + (Total_Time_get - 8);

                                    AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "X" + "/ " + OT_Time + "</b></span>";
                                    count2 = count2 + 1;
                                }
                                else if (Total_Time_get >= 4 & Total_Time_get < 8)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:black><b>" + "H" + "</b></span>";
                                    count2 = count2 + Convert.ToDecimal(0.5);
                                }
                                else if (Total_Time_get == 8)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "X" + "</b></span>";
                                    count2 = count2 + 1;
                                }
                                else
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "A" + "</b></span>";

                                }

                            }

                            else if (Category.ToUpper() == "LABOUR" || dept.ToUpper() != "TEXTILES")
                            {
                                if (Total_Time_get >= 13)
                                {
                                    OT_Time = OT_Time + (Total_Time_get - 12);

                                    AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "X" + "/ " + OT_Time + "</b></span>";
                                    count2 = count2 + 1;
                                }
                                else if (Total_Time_get >= 6 & Total_Time_get < 12)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:black><b>" + "H" + "</b></span>";
                                    count2 = count2 + Convert.ToDecimal(0.5);
                                }
                                else if (Total_Time_get == 12)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "X" + "</b></span>";
                                    count2 = count2 + 1;
                                }
                                else
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "A" + "</b></span>";
                                }
                            }




                            count1 = count1 + count2;









                            OT_Time = 0;

                            count = count + 1;
                        }



                    }

                    AutoDTable.Rows[intK]["Total Days"] = count1;

                    intK = intK + 1;

                    SNo = SNo + 1;

                    count1 = 0;


                }

                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=DAY ATTENDANCE BETWEEN DATES.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">DAY ATTENANCE BETWEEN DATES REPORT &nbsp;&nbsp;&nbsp;</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
                Response.Write("&nbsp;&nbsp;&nbsp;");
                Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();


            }


        }

        public void NonAdminGetDayAttendanceBTDates_Changes(string WagesType, string FromDate, string ToDate)
        {

            double Total_Time_get;
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();

            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("EmployeeNo");
            AutoDTable.Columns.Add("ExistingCode");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("DeptName");

            date1 = Convert.ToDateTime(FromDate);
            string dat = ToDate;
            Date2 = Convert.ToDateTime(dat);

            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                daycount -= 1;
                daysAdded += 1;
            }

            AutoDTable.Columns.Add("Total Days");


            SSQL = "select EM.MachineID,isnull(EM.FirstName,'') as FirstName,EM.ExistingCode,DM.DeptName,EM.OTEligible from Employee_Mst EM";
            SSQL = SSQL + " inner join Department_Mst DM on EM.DeptName = DM.DeptCode";
            SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' and EM.IsActive='Yes' ";

            if (WagesType != "0")
            {
                SSQL = SSQL + " And Wages='" + WagesType + "'";
            }

            SSQL = SSQL + " Group by EM.MachineID,EM.FirstName,EM.ExistingCode,DM.DeptName,EM.OTEligible order by MachineID";
            dt = ManualAttend.GetDetails(SSQL);

            intK = 0;

            Present_WH_Count = 0;

            int SNo = 1;

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                string MachineID = dt.Rows[i]["MachineID"].ToString();

                if (MachineID == "2")
                {
                    string Ss = "dhghdg";
                }
                AutoDTable.Rows[intK]["SNo"] = SNo;
                AutoDTable.Rows[intK]["EmployeeNo"] = dt.Rows[i]["MachineID"].ToString();
                AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
                AutoDTable.Rows[intK]["Name"] = dt.Rows[i]["FirstName"].ToString();
                AutoDTable.Rows[intK]["DeptName"] = dt.Rows[i]["DeptName"].ToString();

                string OTEllible = "";
                OTEllible = dt.Rows[i]["OTEligible"].ToString();

                int count = 5;
                decimal count1 = 0;



                for (int j = 0; j < daysAdded; j++)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                    string Date1 = dayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);

                    if (SessionUserName == "IF")
                    {
                        SSQL = "select Wh_Present_Count,Present,Total_Hrs,OTHours,CatName from IFLogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Date1 + "',120)";

                        dt1 = ManualAttend.GetDetails(SSQL);

                    }
                    else
                    {
                        SSQL = "select Wh_Present_Count,Present,Total_Hrs,OTHours,CatName from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Date1 + "',120)";

                        dt1 = ManualAttend.GetDetails(SSQL);
                    }
                    if (dt1.Rows.Count > 0)
                    {
                        decimal count2 = 0;
                        string Category = "";
                        Category = dt1.Rows[0]["CatName"].ToString();
                        string dept = "";
                        dept = AutoDTable.Rows[intK]["DeptName"].ToString();
                        Present_WH_Count = Convert.ToDouble(dt1.Rows[0]["Wh_Present_Count"].ToString());
                        Present_Count = Convert.ToDouble(dt1.Rows[0]["Present"].ToString());
                        Total_Time_get = Convert.ToDouble(dt1.Rows[0]["Total_Hrs"].ToString());


                        if (Category.ToUpper() == "STAFF" || dept.ToUpper() == "TEXTILES")
                        {
                            if (Total_Time_get >= 9)
                            {
                                OT_Time = OT_Time + (Total_Time_get - 8);

                                AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "X" + "/ " + OT_Time + "</b></span>";
                                count2 = count2 + 1;
                            }
                            else if (Total_Time_get >= 4 & Total_Time_get < 8)
                            {
                                AutoDTable.Rows[intK][count] = "<span style=color:black><b>" + "H" + "</b></span>";
                                count2 = count2 + Convert.ToDecimal(0.5);
                            }
                            else if (Total_Time_get == 8)
                            {
                                AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "X" + "</b></span>";
                                count2 = count2 + 1;
                            }
                            else
                            {
                                AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "A" + "</b></span>";

                            }

                        }

                        else if (Category.ToUpper() == "LABOUR" || dept.ToUpper() != "TEXTILES")
                        {
                            if (Total_Time_get >= 13)
                            {
                                OT_Time = OT_Time + (Total_Time_get - 12);

                                AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "X" + "/ " + OT_Time + "</b></span>";
                                count2 = count2 + 1;
                            }
                            else if (Total_Time_get >= 6 & Total_Time_get < 12)
                            {
                                AutoDTable.Rows[intK][count] = "<span style=color:black><b>" + "H" + "</b></span>";
                                count2 = count2 + Convert.ToDecimal(0.5);
                            }
                            else if (Total_Time_get == 12)
                            {
                                AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "X" + "</b></span>";
                                count2 = count2 + 1;
                            }
                            else
                            {
                                AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "A" + "</b></span>";
                            }
                        }




                        count1 = count1 + count2;









                        OT_Time = 0;

                        count = count + 1;
                    }



                }

                AutoDTable.Rows[intK]["Total Days"] = count1;

                intK = intK + 1;

                SNo = SNo + 1;

                count1 = 0;


            }

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=DAY ATTENDANCE BETWEEN DATES.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">DAY ATTENANCE BETWEEN DATES REPORT &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            Response.Write("&nbsp;&nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();


        }


        public void GetDayAttendanceDayWise(string FromDate, string ShiftType1)
        {

            DataSet ds = new DataSet();
            DataTable AutoDTable = new DataTable();
            string SSQL = "";

            if (SessionUserType == "2")
            {
                NonAdminGetAttdDayWise_Change(FromDate, ShiftType1);
            }
            else
            {


                DataTable Shift_DS = new DataTable();
                
                Document document = new Document(PageSize.A4, 25, 25, 45, 40);
                Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

                writer.PageEvent = new HeaderFooter();
                document.Open();


                PdfPTable table = new PdfPTable(5);
                float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
                table.SetWidths(widths);
                table.WidthPercentage = 100;

                PdfPTable table1 = new PdfPTable(7);
                float[] widths1 = new float[] { 10f, 15f, 15f, 35f, 15f, 15f, 15f };
                table1.SetWidths(widths1);
                table1.WidthPercentage = 100;

                PdfPTable table2 = new PdfPTable(4);
                float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
                table2.SetWidths(widths2);
                table2.WidthPercentage = 100;

                PdfPCell cell;

                DataTable Depart = new DataTable();
                if (SessionUserName == "IF")
                {
                    SSQL = "select MachineID,isnull(DeptName,'') As DeptName,Shift,isnull(FirstName,'') as FirstName,TimeIN,TimeOUT,";
                    SSQL = SSQL + "SubCatName,Total_Hrs1 from IFLogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "' ";

                    if (ShiftType1 != "0")
                    {
                        SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
                    }
                    if (FromDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                    }

                    SSQL = SSQL + "  And Shift !='No Shift'  And TimeIN!=''";

                    AutoDTable = ManualAttend.GetDetails(SSQL);
                }


                else
                {
                    SSQL = "select MachineID,isnull(DeptName,'') As DeptName,Shift,isnull(FirstName,'') as FirstName,TimeIN,TimeOUT,";
                    SSQL = SSQL + "SubCatName,Total_Hrs1 from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

                    if (ShiftType1 != "0")
                    {
                        SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
                    }
                    if (FromDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                    }

                    SSQL = SSQL + "  And Shift !='No Shift'  And TimeIN!=''";

                    AutoDTable = ManualAttend.GetDetails(SSQL);
                }

                SSQL = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                if (ShiftType1 != "0")
                {
                    SSQL = SSQL + " And ShiftDesc='" + ShiftType1 + "'";
                }
                //SSQL = " Group by ShiftDesc Asc ";

                Shift_DS = ManualAttend.GetDetails(SSQL);

                SSQL = "select distinct DeptCode, DeptName from Department_Mst order by DeptName Asc";
                Depart = ManualAttend.GetDetails(SSQL);

                DataTable SubCat = new DataTable();

                SSQL = "select SubCatName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' group by  SubCatName ";
                SubCat = ManualAttend.GetDetails(SSQL);

                //table1.TableEvent = new BorderEvent();

                if (AutoDTable.Rows.Count > 0)
                {
                    cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);


                    cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("DAY ATTENDANCE DAY WISE", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Total No.of Employee: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.BackgroundColor = BaseColor.YELLOW;
                    cell.Colspan = 2;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    //cell.Border = 0;
                    //cell.Colspan = 7;
                    ////cell.PaddingTop = 5f;
                    ////cell.PaddingBottom = 5f;
                    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    //table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.Colspan = 7;
                    //cell.PaddingTop = 5f;
                    //cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("EmpNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("GrandTOT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.BOTTOM_BORDER;
                    cell.Colspan = 7;
                    //cell.PaddingTop = 5f;
                    //cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    int Count = 0;
                    int Count1 = 0;
                    int Count2 = 0;
                    int Count3 = 0;
                    int SubInsider = 0;
                    int SubOutSider = 0;

                    int SNo = 1;

                    int IntK = 0;


                    for (int j = 0; j < Shift_DS.Rows.Count; j++)
                    {
                        string Shift = Shift_DS.Rows[j]["ShiftDesc"].ToString();


                        cell = PhraseCell(new Phrase(" " + Shift_DS.Rows[j]["ShiftDesc"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.Colspan = 7;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        for (int In = 0; In < AutoDTable.Rows.Count; In++)
                        {
                            string EmpNames = AutoDTable.Rows[In]["FirstName"].ToString();
                            string Department = AutoDTable.Rows[In]["DeptName"].ToString();
                            string ShiftType = AutoDTable.Rows[In]["Shift"].ToString();

                            if (EmpNames == "" && Department == "" && Shift == ShiftType)
                            {
                                cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["SubCatName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["TimeOUT"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["Total_Hrs1"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                SNo = SNo + 1;
                            }

                        }

                        Count = 0;



                        for (int k = 0; k < Depart.Rows.Count; k++)
                        {
                            string Deprt = Depart.Rows[k]["DeptCode"].ToString();

                            cell = PhraseCell(new Phrase(" " + Depart.Rows[k]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.Colspan = 7;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            table1.AddCell(cell);
                            Count1 = 0;

                            Count2 = 0;

                            SubInsider = 0;
                            SubOutSider = 0;

                            //for (int m = 0; m < SubCat.Rows.Count; m++)
                            //{
                            //    string Sub = SubCat.Rows[m]["SubCatName"].ToString();

                            //    Count3 = 0;

                            for (int i = 0; i < AutoDTable.Rows.Count; i++)
                            {
                                string Deprt1 = AutoDTable.Rows[i]["DeptName"].ToString();
                                string Shift1 = AutoDTable.Rows[i]["Shift"].ToString();
                                string Sub1 = AutoDTable.Rows[i]["SubCatName"].ToString();

                                if (Deprt == Deprt1 && Shift == Shift1)
                                {
                                    cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["SubCatName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOUT"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs1"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    Count = Count + 1;

                                    Count2 = Count2 + 1;

                                    //Count3 = Count3 + 1;

                                    SNo = SNo + 1;

                                }
                            }

                            if (Count2 > 0)
                            {

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" Total: " + Count2, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                            }


                            if (Count2 == 0)
                            {
                                table1.DeleteLastRow();
                            }
                        }

                        if (Count == 0)
                        {
                            table1.DeleteLastRow();
                        }
                    }

                    //if (Count == 0)
                    //{
                    //    table1.DeleteLastRow();
                    //}

                }
                else
                {

                    cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("DAY ATTENDANCE DAY WISE", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" No Data Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 7;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                }

                table.SpacingBefore = 10f;
                table.SpacingAfter = 10f;

                document.Add(table);

                table1.SpacingBefore = 5f;
                table1.SpacingAfter = 5f;

                document.Add(table1);


                document.Close();

                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=DAY_ATTENDANCE_DAY_WISE.pdf");
                Response.ContentType = "application/pdf";
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(bytes);
                Response.End();
                Response.Close();
            }

        }




        public void NonAdminGetAttdDayWise_Change(string FromDate, string ShiftType1)
        {
            DataTable Shift_DS = new DataTable();


            Document document = new Document(PageSize.A4, 25, 25, 45, 40);
            Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            writer.PageEvent = new HeaderFooter();
            document.Open();


            PdfPTable table = new PdfPTable(5);
            float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
            table.SetWidths(widths);
            table.WidthPercentage = 100;

            PdfPTable table1 = new PdfPTable(7);
            float[] widths1 = new float[] { 10f, 15f, 15f, 35f, 15f, 15f, 15f };
            table1.SetWidths(widths1);
            table1.WidthPercentage = 100;

            PdfPTable table2 = new PdfPTable(4);
            float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            table2.SetWidths(widths2);
            table2.WidthPercentage = 100;

            PdfPCell cell;

            DataTable Depart = new DataTable();

            if (SessionUserName == "IF")
            {
                SSQL = "select MachineID,isnull(DeptName,'') As DeptName,Shift,isnull(FirstName,'') as FirstName,TimeIN,TimeOUT,";
                SSQL = SSQL + "SubCatName,Total_Hrs1 from IFLogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "' ";

                if (ShiftType1 != "0")
                {
                    SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
                }
                if (FromDate != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                }

                SSQL = SSQL + "  And Shift !='No Shift'  And TimeIN!=''";

                AutoDTable = ManualAttend.GetDetails(SSQL);
            }


            else
            {
                SSQL = "select MachineID,isnull(DeptName,'') As DeptName,Shift,isnull(FirstName,'') as FirstName,TimeIN,TimeOUT,";
                SSQL = SSQL + "SubCatName,Total_Hrs1 from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

                if (ShiftType1 != "0")
                {
                    SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
                }
                if (FromDate != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                }

                SSQL = SSQL + "  And Shift !='No Shift'  And TimeIN!=''";

                AutoDTable = ManualAttend.GetDetails(SSQL);
            }

            SSQL = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            if (ShiftType1 != "0")
            {
                SSQL = SSQL + " And ShiftDesc='" + ShiftType1 + "'";
            }
            //SSQL = " Group by ShiftDesc Asc ";

            Shift_DS = ManualAttend.GetDetails(SSQL);

            SSQL = "select distinct DeptCode, DeptName from Department_Mst order by DeptName Asc";
            Depart = ManualAttend.GetDetails(SSQL);

            DataTable SubCat = new DataTable();

            SSQL = "select SubCatName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' group by  SubCatName ";
            SubCat = ManualAttend.GetDetails(SSQL);

            //table1.TableEvent = new BorderEvent();

            if (AutoDTable.Rows.Count > 0)
            {
                cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("DAY ATTENDANCE DAY WISE", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Total No.of Employee: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.BackgroundColor = BaseColor.YELLOW;
                cell.Colspan = 2;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                //cell.Border = 0;
                //cell.Colspan = 7;
                ////cell.PaddingTop = 5f;
                ////cell.PaddingBottom = 5f;
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER;
                cell.Colspan = 7;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("EmpNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("GrandTOT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.BOTTOM_BORDER;
                cell.Colspan = 7;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                int Count = 0;
                int Count1 = 0;
                int Count2 = 0;
                int Count3 = 0;
                int SubInsider = 0;
                int SubOutSider = 0;

                int SNo = 1;

                int IntK = 0;


                for (int j = 0; j < Shift_DS.Rows.Count; j++)
                {
                    string Shift = Shift_DS.Rows[j]["ShiftDesc"].ToString();


                    cell = PhraseCell(new Phrase(" " + Shift_DS.Rows[j]["ShiftDesc"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 7;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    for (int In = 0; In < AutoDTable.Rows.Count; In++)
                    {
                        string EmpNames = AutoDTable.Rows[In]["FirstName"].ToString();
                        string Department = AutoDTable.Rows[In]["DeptName"].ToString();
                        string ShiftType = AutoDTable.Rows[In]["Shift"].ToString();

                        if (EmpNames == "" && Department == "" && Shift == ShiftType)
                        {
                            cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["SubCatName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["TimeOUT"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[In]["Total_Hrs"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            SNo = SNo + 1;
                        }

                    }

                    Count = 0;



                    for (int k = 0; k < Depart.Rows.Count; k++)
                    {
                        string Deprt = Depart.Rows[k]["DeptCode"].ToString();

                        cell = PhraseCell(new Phrase(" " + Depart.Rows[k]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.Colspan = 7;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        table1.AddCell(cell);
                        Count1 = 0;

                        Count2 = 0;

                        SubInsider = 0;
                        SubOutSider = 0;

                        for (int m = 0; m < SubCat.Rows.Count; m++)
                        {
                            string Sub = SubCat.Rows[m]["SubCatName"].ToString();

                            Count3 = 0;

                            for (int i = 0; i < AutoDTable.Rows.Count; i++)
                            {
                                string Deprt1 = AutoDTable.Rows[i]["DeptName"].ToString();
                                string Shift1 = AutoDTable.Rows[i]["Shift"].ToString();
                                string Sub1 = AutoDTable.Rows[i]["SubCatName"].ToString();

                                if (Deprt == Deprt1 && Shift == Shift1 && Sub == Sub1)
                                {
                                    cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["SubCatName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOUT"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    Count = Count + 1;

                                    Count2 = Count2 + 1;

                                    Count3 = Count3 + 1;

                                    SNo = SNo + 1;

                                }
                            }

                            if (Count3 > 0)
                            {

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" Total: " + Count3, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                            }
                        }

                        if (Count2 == 0)
                        {
                            table1.DeleteLastRow();
                        }
                    }

                    if (Count == 0)
                    {
                        table1.DeleteLastRow();
                    }
                }

                //if (Count == 0)
                //{
                //    table1.DeleteLastRow();
                //}

            }
            else
            {

                cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("DAY ATTENDANCE DAY WISE", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" No Data Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 7;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

            }

            table.SpacingBefore = 10f;
            table.SpacingAfter = 10f;

            document.Add(table);

            table1.SpacingBefore = 5f;
            table1.SpacingAfter = 5f;

            document.Add(table1);


            document.Close();

            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=DAY_ATTENDANCE_DAY_WISE.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();

        }



        public void NonAdminEmployeefullList()
        {
            double Total_Time_get;
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();

            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("CatName");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("ExistingCode");
            AutoDTable.Columns.Add("EmpNo");
            AutoDTable.Columns.Add("DeptName");

            AutoDTable.Columns.Add("Designation");
            AutoDTable.Columns.Add("WagesType");
            AutoDTable.Columns.Add("Salary");
            AutoDTable.Columns.Add("OTEligible");
            AutoDTable.Columns.Add("PFEligible");
            AutoDTable.Columns.Add("ESIEligible");

            AutoDTable.Columns.Add("ISActive");




            SSQL = " Select EM.CatName,EM.MachineID,EM.FirstName,EM.ExistingCode,EM.EmpNo,DEP.DeptName,DM.DesignName,WAG.EmpType,EM.BasicSalary,";
            SSQL = SSQL + " EM.OTEligible,Eligible_PF,Eligible_ESI,EM.ISActive ";
            SSQL = SSQL + " from Employee_Mst EM inner join Designation_Mst DM on EM.Designation = DM.DesignNo ";
            SSQL = SSQL + " inner join Department_Mst DEP on  EM.DeptName= DEP.DeptCode";
            SSQL = SSQL + "   inner join MstEmployeeType WAG on  EM.Wages = WAG.EmpCategory";
            SSQL = SSQL + " WHERE  EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' ";

            dt = ManualAttend.GetDetails(SSQL);

            if (dt.Rows.Count != 0)
            {
                int sno = 1;
                for (int k = 0; k < dt.Rows.Count; k++)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();


                    AutoDTable.Rows[k]["SNo"] = sno;
                    AutoDTable.Rows[k]["CatName"] = dt.Rows[k]["CatName"].ToString();
                    AutoDTable.Rows[k]["MachineID"] = dt.Rows[k]["MachineID"].ToString();
                    AutoDTable.Rows[k]["Name"] = dt.Rows[k]["FirstName"].ToString();
                    AutoDTable.Rows[k]["ExistingCode"] = dt.Rows[k]["ExistingCode"].ToString();
                    AutoDTable.Rows[k]["EmpNo"] = dt.Rows[k]["EmpNo"].ToString();
                    AutoDTable.Rows[k]["DeptName"] = dt.Rows[k]["DeptName"].ToString();


                    AutoDTable.Rows[k]["Designation"] = dt.Rows[k]["DesignName"].ToString();
                    AutoDTable.Rows[k]["WagesType"] = dt.Rows[k]["EmpType"].ToString();
                    AutoDTable.Rows[k]["Salary"] = dt.Rows[k]["BasicSalary"].ToString();
                    AutoDTable.Rows[k]["OTEligible"] = dt.Rows[k]["OTEligible"].ToString();

                    AutoDTable.Rows[k]["PFEligible"] = dt.Rows[k]["Eligible_PF"].ToString();
                    AutoDTable.Rows[k]["ESIEligible"] = dt.Rows[k]["Eligible_ESI"].ToString();
                    AutoDTable.Rows[k]["ISActive"] = dt.Rows[k]["ISActive"].ToString();


                    sno = sno + 1;
                }
            }




            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=EMPLOYEE DETAILS.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='14'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='14'>");
            Response.Write("<a style=\"font-weight:bold\">EMPLOYEE DETAILS &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            //Response.Write("<tr Font-Bold='true' align='center'>");
            //Response.Write("<td colspan='10'>");
            //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            //Response.Write("&nbsp;&nbsp;&nbsp;");
            //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }

        public void EmployeefullList()
        {
            if (SessionUserType == "2")
            {
                NonAdminEmployeefullList();
            }
            else
            {
                double Total_Time_get;
                DataTable dt = new DataTable();
                DataTable dt1 = new DataTable();

                AutoDTable.Columns.Add("SNo");
                AutoDTable.Columns.Add("CatName");
                AutoDTable.Columns.Add("MachineID");
                AutoDTable.Columns.Add("Name");
                AutoDTable.Columns.Add("ExistingCode");
                AutoDTable.Columns.Add("EmpNo");
                AutoDTable.Columns.Add("DeptName");

                AutoDTable.Columns.Add("Designation");
                AutoDTable.Columns.Add("WagesType");
                AutoDTable.Columns.Add("Salary");
                AutoDTable.Columns.Add("OTEligible");
                AutoDTable.Columns.Add("PFEligible");
                AutoDTable.Columns.Add("ESIEligible");

                AutoDTable.Columns.Add("ISActive");




                SSQL = " Select EM.CatName,EM.MachineID,EM.FirstName,EM.ExistingCode,EM.EmpNo,DEP.DeptName,DM.DesignName,WAG.EmpType,EM.BasicSalary,";
                SSQL = SSQL + " EM.OTEligible,Eligible_PF,Eligible_ESI,EM.ISActive ";
                SSQL = SSQL + " from Employee_Mst EM inner join Designation_Mst DM on EM.Designation = DM.DesignNo ";
                SSQL = SSQL + " inner join Department_Mst DEP on  EM.DeptName= DEP.DeptCode";
                SSQL = SSQL + "   inner join MstEmployeeType WAG on  EM.Wages = WAG.EmpCategory";
                SSQL = SSQL + " WHERE  EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' ";

                dt = ManualAttend.GetDetails(SSQL);

                if (dt.Rows.Count != 0)
                {
                    int sno = 1;
                    for (int k = 0; k < dt.Rows.Count; k++)
                    {
                        AutoDTable.NewRow();
                        AutoDTable.Rows.Add();


                        AutoDTable.Rows[k]["SNo"] = sno;
                        AutoDTable.Rows[k]["CatName"] = dt.Rows[k]["CatName"].ToString();
                        AutoDTable.Rows[k]["MachineID"] = dt.Rows[k]["MachineID"].ToString();
                        AutoDTable.Rows[k]["Name"] = dt.Rows[k]["FirstName"].ToString();
                        AutoDTable.Rows[k]["ExistingCode"] = dt.Rows[k]["ExistingCode"].ToString();
                        AutoDTable.Rows[k]["EmpNo"] = dt.Rows[k]["EmpNo"].ToString();
                        AutoDTable.Rows[k]["DeptName"] = dt.Rows[k]["DeptName"].ToString();


                        AutoDTable.Rows[k]["Designation"] = dt.Rows[k]["DesignName"].ToString();
                        AutoDTable.Rows[k]["WagesType"] = dt.Rows[k]["EmpType"].ToString();
                        AutoDTable.Rows[k]["Salary"] = dt.Rows[k]["BasicSalary"].ToString();
                        AutoDTable.Rows[k]["OTEligible"] = dt.Rows[k]["OTEligible"].ToString();

                        AutoDTable.Rows[k]["PFEligible"] = dt.Rows[k]["Eligible_PF"].ToString();
                        AutoDTable.Rows[k]["ESIEligible"] = dt.Rows[k]["Eligible_ESI"].ToString();
                        AutoDTable.Rows[k]["ISActive"] = dt.Rows[k]["ISActive"].ToString();


                        sno = sno + 1;
                    }
                }




                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=EMPLOYEE DETAILS.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='14'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='14'>");
                Response.Write("<a style=\"font-weight:bold\">EMPLOYEE DETAILS &nbsp;&nbsp;&nbsp;</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
                //Response.Write("<tr Font-Bold='true' align='center'>");
                //Response.Write("<td colspan='10'>");
                //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
                //Response.Write("&nbsp;&nbsp;&nbsp;");
                //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();


            }


        }


    


        public ActionResult OTTimeBWTDates(string ShiftType, string FromDate, string ToDate)
        {


            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                GetOTTimeBWTDates(ShiftType, FromDate, ToDate);
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

           
        }


        public void GetOTTimeBWTDates(string ShiftType, string FromDate, string ToDate)
        {
            if (SessionUserType == "2")
            {
                NonAdminGetOTTimeBWTDates_Changes(ShiftType, FromDate, ToDate);
            }
            else
            {
                double Total_Time_get;
                DataTable dt = new DataTable();
                DataTable dt1 = new DataTable();

                AutoDTable.Columns.Add("SNo");
                AutoDTable.Columns.Add("EmployeeNo");
                AutoDTable.Columns.Add("ExistingCode");
                AutoDTable.Columns.Add("Name");
                AutoDTable.Columns.Add("DeptName");

                date1 = Convert.ToDateTime(FromDate);
                string dat = ToDate;
                Date2 = Convert.ToDateTime(dat);

                int daycount = (int)((Date2 - date1).TotalDays);
                int daysAdded = 0;

                while (daycount >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                    //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                    AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                    daycount -= 1;
                    daysAdded += 1;
                }

                //AutoDTable.Columns.Add("OT Hours");
                //AutoDTable.Columns.Add("OT Salary");

                SSQL = "select MachineID,isnull(FirstName,'') as [FirstName],ExistingCode,DeptName from LogTime_Days";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                if (ShiftType != "0")
                {
                    SSQL = SSQL + " And Shift='" + ShiftType + "'";
                }
                SSQL = SSQL + " And Present > 0";
                SSQL = SSQL + " Group by MachineID,[FirstName],ExistingCode,DeptName order by MachineID";
                dt = MasterDate.GetDetails(SSQL);

                intK = 0;

                int SNo = 1;

                for (int i = 0; i < dt.Rows.Count; i++)
                {



                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();

                    string MachineID = dt.Rows[i]["MachineID"].ToString();

                    AutoDTable.Rows[intK]["SNo"] = SNo;
                    AutoDTable.Rows[intK]["EmployeeNo"] = dt.Rows[i]["MachineID"].ToString();
                    AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
                    AutoDTable.Rows[intK]["Name"] = dt.Rows[i]["FirstName"].ToString();
                    AutoDTable.Rows[intK]["DeptName"] = dt.Rows[i]["DeptName"].ToString();

                    int count = 5;

                    for (int j = 0; j < daysAdded; j++)
                    {
                        DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                        string Date1 = dayy.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                        SSQL = "select OTHours from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(Date1).ToString("yyyy/MM/dd") + "',120)";

                        dt1 = MasterDate.GetDetails(SSQL);

                        if (dt1.Rows.Count > 0)
                        {
                            AutoDTable.Rows[intK][count] = dt1.Rows[0]["OTHours"].ToString();

                        }
                        else
                        {

                            AutoDTable.Rows[intK][count] = 0;

                        }

                        count = count + 1;

                    }



                    intK = intK + 1;

                    SNo = SNo + 1;

                }

                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=OTTime_Between_Dates Report.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">OT REPORT - BETWEEN DATES &nbsp;&nbsp;&nbsp;</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
                Response.Write("&nbsp;&nbsp;&nbsp;");
                Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();

            }

        }

        public void NonAdminGetOTTimeBWTDates_Changes(string ShiftType, string FromDate, string ToDate)
        {
            double Total_Time_get;
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();

            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("EmployeeNo");
            AutoDTable.Columns.Add("ExistingCode");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("DeptName");

            date1 = Convert.ToDateTime(FromDate);
            string dat = ToDate;
            Date2 = Convert.ToDateTime(dat);

            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                daycount -= 1;
                daysAdded += 1;
            }

            //AutoDTable.Columns.Add("OT Hours");
            //AutoDTable.Columns.Add("OT Salary");

            SSQL = "select MachineID,isnull(FirstName,'') as [FirstName],ExistingCode,DeptName from LogTime_Days";
            SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            if (ShiftType != "0")
            {
                SSQL = SSQL + " And Shift='" + ShiftType + "'";
            }
            SSQL = SSQL + " And Present > 0";
            SSQL = SSQL + " Group by MachineID,[FirstName],ExistingCode,DeptName order by MachineID";
            dt = MasterDate.GetDetails(SSQL);

            intK = 0;

            int SNo = 1;

            for (int i = 0; i < dt.Rows.Count; i++)
            {



                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                string MachineID = dt.Rows[i]["MachineID"].ToString();

                AutoDTable.Rows[intK]["SNo"] = SNo;
                AutoDTable.Rows[intK]["EmployeeNo"] = dt.Rows[i]["MachineID"].ToString();
                AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
                AutoDTable.Rows[intK]["Name"] = dt.Rows[i]["FirstName"].ToString();
                AutoDTable.Rows[intK]["DeptName"] = dt.Rows[i]["DeptName"].ToString();

                int count = 5;

                for (int j = 0; j < daysAdded; j++)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                    string Date1 = dayy.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                    SSQL = "select OTHours from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(Date1).ToString("yyyy/MM/dd") + "',120)";

                    dt1 = MasterDate.GetDetails(SSQL);

                    if (dt1.Rows.Count > 0)
                    {
                        AutoDTable.Rows[intK][count] = dt1.Rows[0]["OTHours"].ToString();

                    }
                    else
                    {

                        AutoDTable.Rows[intK][count] = 0;

                    }

                    count = count + 1;

                }



                intK = intK + 1;

                SNo = SNo + 1;

            }

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=OTTime_Between_Dates Report.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">OT REPORT - BETWEEN DATES &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            Response.Write("&nbsp;&nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }



        // payrollotreport
        public ActionResult payrollotreport(string WagesType, string FromDate, string ToDate)
        {


            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                Fill_Day_Attd_Between_Dates_OT(WagesType, FromDate, ToDate);
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

          
        }





        // AbsentReportBetweenDates
        public ActionResult AbsentReportBetweenDates(string WagesType, string FromDate, string ToDate)
        {


            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                GetAbsentReportBetweenDates(WagesType, FromDate, ToDate);

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

           
        }





        //AbsentReportDayWise
        public ActionResult AbsentReportDayWise(string FromDate)
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                AbsentReportDays(FromDate);

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

          
        }


        //Weekly Payroll Attendance

        public ActionResult WeeklyPayroll(string WagesType, string FromDate, string ToDate)
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }



        //Hours Attendance between Dates
        public ActionResult HoursBtwnDates(string WagesType, string FromDate, string ToDate)
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                HoursAttendancebetweenDates(WagesType, FromDate, ToDate);

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

           
        }







        /// DayAttendanceSummary
        public ActionResult DayAttendanceSummary(string FromDate,string Dept)
        {

            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                DaySummaryExcel(FromDate,Dept);

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult BreakDayAttendanceSummary(string FromDate,string Dept)
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                BreaksummaryExcel(FromDate,Dept);

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }





        //  belowhour
        public ActionResult belowhour(string BelowHour, string FromDate, string ShiftType)
        {

            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                belowAttendance(BelowHour, FromDate, ShiftType);

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }





        // Abovehour
        public ActionResult Abovehour(string AboveHour, string FromDate, string ShiftType)
        {

            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                AboveAttendance(AboveHour, FromDate, ShiftType);

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }




        //Betweenhour
        public ActionResult Betweenhour(string AboveHour, string BelowHour, string FromDate, string ShiftType)
        {

            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                BetweenAttendance(AboveHour, BelowHour, FromDate, ShiftType);

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }





        //  SalaryConsolidate
        public ActionResult SalaryConsolidate(string FromDate, string ShiftType)
        {

            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                GetDaySalaryDetails(FromDate, ShiftType);

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public void GetAbsentReportBetweenDates(string WagesType, string FromDate, string ToDate)
        {

            if (SessionUserType == "2")
            {
                NonAdminGetAbsentReportBetweenDates(WagesType, FromDate, ToDate);
            }
            else
            {
                double Total_Time_get;
                DataTable dt = new DataTable();
                DataTable dt1 = new DataTable();

                AutoDTable.Columns.Add("SNo");
                AutoDTable.Columns.Add("EmployeeNo");
                AutoDTable.Columns.Add("ExistingCode");
                AutoDTable.Columns.Add("Name");
                AutoDTable.Columns.Add("DeptName");
                int abbsentcount = 0;
                date1 = Convert.ToDateTime(FromDate);
                string dat = ToDate;
                Date2 = Convert.ToDateTime(dat);

                int daycount = (int)((Date2 - date1).TotalDays);
                int daysAdded = 0;

                while (daycount >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                    //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                    AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                    daycount -= 1;
                    daysAdded += 1;
                }

                AutoDTable.Columns.Add("Total Days");

                SSQL = "select MachineID,isnull(FirstName,'') as [FirstName],ExistingCode,DeptName from Employee_Mst";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and IsActive='Yes'";

                if (WagesType != "0")
                {
                    SSQL = SSQL + " And Wages='" + WagesType + "'";
                }

                SSQL = SSQL + " Group by MachineID,[FirstName],ExistingCode,DeptName order by MachineID";
                dt = MasterDate.GetDetails(SSQL);

                intK = 0;

                int SNo = 1;

                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    int count = 0;
                    DataTable da = new DataTable();
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    int daycount1 = (int)((Date2 - date1).TotalDays);
                    int daysAdded1 = 0;
                    string MachineID = dt.Rows[i]["MachineID"].ToString();
                    count = 5;
                    abbsentcount = 0;
                    AutoDTable.Rows[intK]["SNo"] = SNo;
                    AutoDTable.Rows[intK]["EmployeeNo"] = dt.Rows[i]["MachineID"].ToString();
                    AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
                    AutoDTable.Rows[intK]["Name"] = dt.Rows[i]["FirstName"].ToString();
                    //Department code checking 
                    SSQL = "select * from Department_Mst where DeptCode='" + dt.Rows[i]["DeptName"].ToString() + "'";
                    da = MasterDate.GetDetails(SSQL);
                    if (da.Rows.Count != 0)
                    {
                        AutoDTable.Rows[intK]["DeptName"] = da.Rows[0]["DeptName"].ToString();
                    }
                    else
                    {
                        AutoDTable.Rows[intK]["DeptName"] = "0";
                    }

                   

                    while (daycount1 >= 0)
                    {


                        DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded1).ToShortDateString());

                        SSQL = "select Attn_Date_Str,Present,Shift,Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And MachineID='" + MachineID + "' And  CONVERT(datetime, Attn_Date_Str,120) = CONVERT(datetime, '" + Convert.ToDateTime(dayy).ToString("yyyy/MM/dd") + "', 120)";
                        // SSQL = SSQL + " And CONVERT(datetime,Attn_Date_Str,103) <= CONVERT(datetime,'" + ToDate + "',103)";
                        dt1 = MasterDate.GetDetails(SSQL);

                        if (dt1.Rows.Count <= 0)
                        {



                            AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "A" + "</b></span>";
                            abbsentcount += 1;
                            count = count + 1;


                        }
                        else
                        {
                            Present_Count = 0;

                            Total_Time_get = 0;

                            OT_Time = 0;




                            Present_Count = Convert.ToDouble(dt1.Rows[0]["Present"].ToString());
                            // Total_Time_get = Convert.ToDouble(dt1.Rows[j]["Total_Hrs"].ToString());

                            if (Present_Count == 1 || Present_Count == 0.5)
                            {



                            }

                            else
                            {
                                AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "A" + "</b></span>";
                                abbsentcount += 1;
                            }

                            count = count + 1;

                            OT_Time = 0;


                        }
                        daycount1 -= 1;
                        daysAdded1 += 1;
                    }

                    AutoDTable.Rows[intK]["Total Days"] = abbsentcount;

                    intK = intK + 1;

                    SNo = SNo + 1;

                }

                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=ABSENT REPORT BETWEEN DATES.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">ABSENT REPORT BETWEEN DATES &nbsp;&nbsp;&nbsp;</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
                Response.Write("&nbsp;&nbsp;&nbsp;");
                Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
        }
        public void NonAdminGetAbsentReportBetweenDates(string WagesType, string FromDate, string ToDate)
        {
            double Total_Time_get;
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();

            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("EmployeeNo");
            AutoDTable.Columns.Add("ExistingCode");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("DeptName");
            int abbsentcount = 0;
            date1 = Convert.ToDateTime(FromDate);
            string dat = ToDate;
            Date2 = Convert.ToDateTime(dat);

            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                daycount -= 1;
                daysAdded += 1;
            }

            AutoDTable.Columns.Add("Total Days");

            SSQL = "select MachineID,isnull(FirstName,'') as [FirstName],ExistingCode,DeptName from Employee_Mst";
            SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and IsActive='Yes'";

            if (WagesType != "0")
            {
                SSQL = SSQL + " And Wages='" + WagesType + "'";
            }

            SSQL = SSQL + " Group by MachineID,[FirstName],ExistingCode,DeptName order by MachineID";
            dt = MasterDate.GetDetails(SSQL);

            intK = 0;

            int SNo = 1;

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                int count = 0;

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                int daycount1 = (int)((Date2 - date1).TotalDays);
                int daysAdded1 = 0;
                string MachineID = dt.Rows[i]["MachineID"].ToString();
                count = 5;
                abbsentcount = 0;
                AutoDTable.Rows[intK]["SNo"] = SNo;
                AutoDTable.Rows[intK]["EmployeeNo"] = dt.Rows[i]["MachineID"].ToString();
                AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
                AutoDTable.Rows[intK]["Name"] = dt.Rows[i]["FirstName"].ToString();
                AutoDTable.Rows[intK]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
                while (daycount1 >= 0)
                {


                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded1).ToShortDateString());

                    SSQL = "select Attn_Date_Str,Present,Shift,Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And MachineID='" + MachineID + "' And  CONVERT(datetime, Attn_Date_Str,120) = CONVERT(datetime, '" + Convert.ToDateTime(dayy).ToString("yyyy/MM/dd") + "', 120)";
                    // SSQL = SSQL + " And CONVERT(datetime,Attn_Date_Str,103) <= CONVERT(datetime,'" + ToDate + "',103)";
                    dt1 = MasterDate.GetDetails(SSQL);

                    if (dt1.Rows.Count <= 0)
                    {



                        AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "A" + "</b></span>";
                        abbsentcount += 1;
                        count = count + 1;


                    }
                    else
                    {
                        Present_Count = 0;

                        Total_Time_get = 0;

                        OT_Time = 0;




                        Present_Count = Convert.ToDouble(dt1.Rows[0]["Present"].ToString());
                        // Total_Time_get = Convert.ToDouble(dt1.Rows[j]["Total_Hrs"].ToString());

                        if (Present_Count == 1 || Present_Count == 0.5)
                        {



                        }

                        else
                        {
                            AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "A" + "</b></span>";
                            abbsentcount += 1;
                        }

                        count = count + 1;

                        OT_Time = 0;


                    }
                    daycount1 -= 1;
                    daysAdded1 += 1;
                }

                AutoDTable.Rows[intK]["Total Days"] = abbsentcount;

                intK = intK + 1;

                SNo = SNo + 1;

            }

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=ABSENT REPORT BETWEEN DATES.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">ABSENT REPORT BETWEEN DATES &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            Response.Write("&nbsp;&nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }


        public void Fill_Day_Attd_Between_Dates_OT(string WagesType, string FromDate, string ToDate)
        {
            if (SessionUserID == "2")
            {
                NonAdminFill_Day_Attd_Between_Dates_OT(WagesType, FromDate, ToDate);
            }
            else
            {
                DataTable dt1 = new DataTable();
                DataTable dsEmployee = new DataTable();
                AutoDTable.Columns.Add("MachineID");
                AutoDTable.Columns.Add("EmpNo");
                AutoDTable.Columns.Add("ExistingCode");
                AutoDTable.Columns.Add("DeptName");
                AutoDTable.Columns.Add("FirstName");
                AutoDTable.Columns.Add("MachineID_Encrypt");

                DataCell.Columns.Add("Department");
                DataCell.Columns.Add("EmpNo");
                DataCell.Columns.Add("ExistingCode");

                DataCell.Columns.Add("FirstName");
                DataCell.Columns.Add("HrSalary");
                DataCell.Columns.Add("NoHrs");

                Double TotalDays;
                date1 = Convert.ToDateTime(FromDate);
                string dat = ToDate;
                Date2 = Convert.ToDateTime(dat);

                int daycount = (int)((Date2 - date1).TotalDays);
                int daysAdded = 0;
                bool isPresent = false;
                int intI = 0;
                int intK = 0;

                string halfPresent = "0";


                while (daycount >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                    //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                    mEmployeeDT.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                    daycount -= 1;
                    daysAdded += 1;
                }

                try
                {
                    SSQL = "";
                    SSQL = "select isnull(DeptName,'') as [DeptName], Cast(MachineID As int) As MachineID";
                    SSQL = SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
                    SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
                    SSQL = SSQL + ",isnull(MachineID_Encrypt,'') as [MachineID_Encrypt]";
                    SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes' And OTEligible='Yes' and Wages='" + WagesType + "'";
                    SSQL = SSQL + " Order By DeptName, MachineID";


                    //SSQL = "select MachineID,isnull(FirstName,'') as [FirstName],ExistingCode,DeptName,MachineID_Encrypt from LogTime_Days";
                    //SSQL = SSQL + " Where CompCode='" + Ccode + "' And LocCode='" + Lcode + "'";


                    //SSQL = SSQL + " Group by MachineID,[FirstName],ExistingCode,DeptName,MachineID_Encrypt order by MachineID";


                    dsEmployee = MasterDate.GetDetails(SSQL);
                    if (dsEmployee.Rows.Count <= 0)
                    {

                    }

                    else
                    {
                        int i1 = 0;
                        for (int j1 = 0; j1 < dsEmployee.Rows.Count; j1++)
                        {

                            AutoDTable.NewRow();
                            AutoDTable.Rows.Add();
                            string Machineid = dsEmployee.Rows[j1]["MachineID"].ToString();
                            AutoDTable.Rows[i1][0] = dsEmployee.Rows[j1]["MachineID"].ToString();
                            AutoDTable.Rows[i1][1] = dsEmployee.Rows[j1]["MachineID"].ToString();
                            AutoDTable.Rows[i1][2] = dsEmployee.Rows[j1]["ExistingCode"].ToString();
                            AutoDTable.Rows[i1][3] = dsEmployee.Rows[j1]["DeptName"].ToString();
                            AutoDTable.Rows[i1][4] = dsEmployee.Rows[j1]["FirstName"].ToString();
                            AutoDTable.Rows[i1][5] = dsEmployee.Rows[j1]["MachineID_Encrypt"].ToString();
                            i1++;

                            SSQL = "select Sum(OTHours) as OTHours  from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And MachineID='" + Machineid + "' And CONVERT(datetime,Attn_Date_Str,103) >= CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',103)";
                            SSQL = SSQL + " And CONVERT(datetime,Attn_Date_Str,120) <= CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                            dt1 = MasterDate.GetDetails(SSQL);
                            string othours = "";
                            if (dt1.Rows.Count > 0)
                            {

                                othours = dt1.Rows[0]["OTHours"].ToString();
                            }



                            DataCell.NewRow();
                            DataCell.Rows.Add();
                            DataCell.Rows[j1]["Department"] = AutoDTable.Rows[j1]["DeptName"].ToString();
                            //  DataCell.Rows[j1]["MachineID"] = AutoDTable.Rows[j1]["MachineID"].ToString();
                            DataCell.Rows[j1]["EmpNo"] = AutoDTable.Rows[j1]["EmpNo"].ToString();
                            DataCell.Rows[j1]["ExistingCode"] = AutoDTable.Rows[j1]["ExistingCode"].ToString();
                            DataCell.Rows[j1]["FirstName"] = AutoDTable.Rows[j1]["FirstName"].ToString();
                            DataCell.Rows[j1]["HrSalary"] = 0;
                            if (othours == "")
                            {
                                DataCell.Rows[j1]["NoHrs"] = 0;
                            }
                            else
                            {
                                DataCell.Rows[j1]["NoHrs"] = dt1.Rows[0]["OTHours"].ToString();
                            }

                        }
                    }

                }
                catch (Exception e)
                {

                }

                //Fill_AbsentDays(FromDate);

                //WriteAbsent(FromDate);


                grid.DataSource = DataCell;
                grid.DataBind();
                string attachment = "attachment;filename=PayrollOT.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);

                Response.Write("<table>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">Payroll OT Report &nbsp;&nbsp;&nbsp;</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\"> Date -" + FromDate + " - " + ToDate + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
        }

        public void NonAdminFill_Day_Attd_Between_Dates_OT(string WagesType, string FromDate, string ToDate)
        {
            DataTable dt1 = new DataTable();
            DataTable dsEmployee = new DataTable();
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("EmpNo");
            AutoDTable.Columns.Add("ExistingCode");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("FirstName");
            AutoDTable.Columns.Add("MachineID_Encrypt");

            DataCell.Columns.Add("Department");
            DataCell.Columns.Add("EmpNo");
            DataCell.Columns.Add("ExistingCode");

            DataCell.Columns.Add("FirstName");
            DataCell.Columns.Add("HrSalary");
            DataCell.Columns.Add("NoHrs");

            Double TotalDays;
            date1 = Convert.ToDateTime(FromDate);
            string dat = ToDate;
            Date2 = Convert.ToDateTime(dat);

            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;
            bool isPresent = false;
            int intI = 0;
            int intK = 0;

            string halfPresent = "0";


            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                mEmployeeDT.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                daycount -= 1;
                daysAdded += 1;
            }

            try
            {
                SSQL = "";
                SSQL = "select isnull(DeptName,'') as [DeptName], Cast(MachineID As int) As MachineID";
                SSQL = SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
                SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
                SSQL = SSQL + ",isnull(MachineID_Encrypt,'') as [MachineID_Encrypt]";
                SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes' And OTEligible='Yes' and Wages='" + WagesType + "'";
                SSQL = SSQL + " Order By DeptName, MachineID";



                dsEmployee = MasterDate.GetDetails(SSQL);
                if (dsEmployee.Rows.Count <= 0)
                {

                }

                else
                {
                    int i1 = 0;
                    for (int j1 = 0; j1 < dsEmployee.Rows.Count; j1++)
                    {

                        AutoDTable.NewRow();
                        AutoDTable.Rows.Add();
                        string Machineid = dsEmployee.Rows[j1]["MachineID"].ToString();
                        AutoDTable.Rows[i1][0] = dsEmployee.Rows[j1]["MachineID"].ToString();
                        AutoDTable.Rows[i1][1] = dsEmployee.Rows[j1]["MachineID"].ToString();
                        AutoDTable.Rows[i1][2] = dsEmployee.Rows[j1]["ExistingCode"].ToString();
                        AutoDTable.Rows[i1][3] = dsEmployee.Rows[j1]["DeptName"].ToString();
                        AutoDTable.Rows[i1][4] = dsEmployee.Rows[j1]["FirstName"].ToString();
                        AutoDTable.Rows[i1][5] = dsEmployee.Rows[j1]["MachineID_Encrypt"].ToString();
                        i1++;

                        SSQL = "select Sum(OTHours) as OTHours  from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And MachineID='" + Machineid + "' And CONVERT(datetime,Attn_Date_Str,103) >= CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',103)";
                        SSQL = SSQL + " And CONVERT(datetime,Attn_Date_Str,120) <= CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                        dt1 = MasterDate.GetDetails(SSQL);
                        string othours = "";
                        if (dt1.Rows.Count > 0)
                        {

                            othours = dt1.Rows[0]["OTHours"].ToString();
                        }



                        DataCell.NewRow();
                        DataCell.Rows.Add();
                        DataCell.Rows[j1]["Department"] = AutoDTable.Rows[j1]["DeptName"].ToString();
                        //  DataCell.Rows[j1]["MachineID"] = AutoDTable.Rows[j1]["MachineID"].ToString();
                        DataCell.Rows[j1]["EmpNo"] = AutoDTable.Rows[j1]["EmpNo"].ToString();
                        DataCell.Rows[j1]["ExistingCode"] = AutoDTable.Rows[j1]["ExistingCode"].ToString();
                        DataCell.Rows[j1]["FirstName"] = AutoDTable.Rows[j1]["FirstName"].ToString();
                        DataCell.Rows[j1]["HrSalary"] = 0;
                        if (othours == "")
                        {
                            DataCell.Rows[j1]["NoHrs"] = 0;
                        }
                        else
                        {
                            DataCell.Rows[j1]["NoHrs"] = dt1.Rows[0]["OTHours"].ToString();
                        }

                    }
                }

            }
            catch (Exception e)
            {

            }




            grid.DataSource = DataCell;
            grid.DataBind();
            string attachment = "attachment;filename=PayrollOT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write("<table>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">Payroll OT Report &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\"> Date -" + FromDate + " - " + ToDate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }

        public void GetDaySalaryDetails(string FromDate, string ShiftType)
        {
            DataSet ds = new DataSet();
            DataTable AutoDTable = new DataTable();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            decimal rounded = 0;
            decimal roundedsalary = 0;
            decimal staffonedaysalary;
            AutoDTable.Columns.Add("EmpCode");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("TimeIN");
            AutoDTable.Columns.Add("TimeOUT");
            AutoDTable.Columns.Add("Fixed_Salary");
            AutoDTable.Columns.Add("Total_Hrs");
            AutoDTable.Columns.Add("Wages");
            AutoDTable.Columns.Add("Shift");
            AutoDTable.Columns.Add("SubCatName");

            if (SessionUserType == "2")
            {
                NOnAdminGetDaySalaryDetails(FromDate, ShiftType);
            }
            else
            {


                DataTable Shift_DS = new DataTable();


                Document document = new Document(PageSize.A4, 25, 25, 45, 40);
                Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

                writer.PageEvent = new HeaderFooter();
                document.Open();


                PdfPTable table = new PdfPTable(5);
                float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
                table.SetWidths(widths);
                table.WidthPercentage = 100;

                PdfPTable table1 = new PdfPTable(9);
                float[] widths1 = new float[] { 10f, 15f, 15f, 20f, 15f, 15f, 15f, 15f, 15f };
                table1.SetWidths(widths1);
                table1.WidthPercentage = 100;

                PdfPTable table2 = new PdfPTable(4);
                float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
                table2.SetWidths(widths2);
                table2.WidthPercentage = 100;

                PdfPCell cell;

                DataTable Depart = new DataTable();


                //SSQL = "select MachineID,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT,";
                //SSQL = SSQL + "SubCatName,Total_Hrs,CatName,BasicSalary from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

                //if (ShiftType != "0")
                //{
                //    SSQL = SSQL + " And Shift='" + ShiftType + "'";
                //}
                //if (FromDate != "")
                //{
                //    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                //}
                //SSQL = SSQL + " And Present !='0.0'";

                //dt1 = MasterDate.GetDetails(SSQL);

                SSQL = "select MachineID,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT,";
                SSQL = SSQL + "SubCatName,Total_Hrs,CatName,BasicSalary from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

                if (ShiftType != "0")
                {
                    SSQL = SSQL + " And Shift='" + ShiftType + "'";
                }
                if (FromDate != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                }
                SSQL = SSQL + " And Present !='0.0'";

                dt = MasterDate.GetDetails(SSQL);

                string staffsalary = "";
                string staffwages = "";

                string Machineid = "";
                string workinghours = "";
                string stdwrkhrs = "";
                decimal staffonehoursalary;
                decimal Totalwages;
                staffonedaysalary = 0;
                if (dt.Rows.Count > 0)
                {
                    for (int k = 0; k < dt.Rows.Count; k++)
                    {
                        Machineid = dt.Rows[k]["MachineID"].ToString();
                        staffwages = dt.Rows[k]["CatName"].ToString();
                        if (staffwages == "STAFF")
                        {
                            staffsalary = dt.Rows[k]["BasicSalary"].ToString();
                            staffonedaysalary = Convert.ToDecimal(staffsalary.ToString()) / 26;
                            roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                        }
                        else
                        {
                            staffsalary = dt.Rows[k]["BasicSalary"].ToString();
                            staffonedaysalary = Convert.ToDecimal(staffsalary.ToString());
                            roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));
                        }
                        workinghours = dt.Rows[k]["Total_Hrs"].ToString();
                        SSQL = "Select * from Employee_Mst where MachineID='" + Machineid + "'";
                        dt1 = MasterDate.GetDetails(SSQL);
                        if (dt1.Rows.Count > 0)
                        {
                            stdwrkhrs = dt1.Rows[0]["StdWrkHrs"].ToString();
                            if (stdwrkhrs == "" || stdwrkhrs == "0")
                            {
                                stdwrkhrs = "8";
                            }
                        }
                        staffonehoursalary = staffonedaysalary / Convert.ToDecimal(stdwrkhrs.ToString());
                        Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());
                        rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));

                        AutoDTable.NewRow();
                        AutoDTable.Rows.Add();
                        AutoDTable.Rows[k]["EmpCode"] = dt.Rows[k]["MachineID"].ToString();
                        AutoDTable.Rows[k]["DeptName"] = dt.Rows[k]["DeptName"].ToString();
                        AutoDTable.Rows[k]["Name"] = dt.Rows[k]["FirstName"].ToString();
                        AutoDTable.Rows[k]["TimeIN"] = dt.Rows[k]["TimeIN"].ToString();
                        AutoDTable.Rows[k]["TimeOUT"] = dt.Rows[k]["TimeOUT"].ToString();
                        AutoDTable.Rows[k]["Fixed_Salary"] = roundedsalary.ToString();

                        AutoDTable.Rows[k]["Total_Hrs"] = dt.Rows[k]["Total_Hrs"].ToString();
                        AutoDTable.Rows[k]["Wages"] = rounded;
                        AutoDTable.Rows[k]["Shift"] = dt.Rows[k]["Shift"].ToString();
                        AutoDTable.Rows[k]["SubCatName"] = dt.Rows[k]["SubCatName"].ToString();

                    }

                }



                SSQL = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                if (ShiftType != "0")
                {
                    SSQL = SSQL + " And ShiftDesc='" + ShiftType + "'";
                }
                //SSQL = " Group by ShiftDesc Asc ";

                Shift_DS = MasterDate.GetDetails(SSQL);

                SSQL = "select distinct DeptCode as DeptName from Department_Mst order by DeptName Asc";
                Depart = MasterDate.GetDetails(SSQL);

                DataTable SubCat = new DataTable();

                SSQL = "select SubCatName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' group by  SubCatName ";
                SubCat = MasterDate.GetDetails(SSQL);

                //table1.TableEvent = new BorderEvent();

                if (AutoDTable.Rows.Count > 0)
                {
                    int general = 0;
                    int shift1 = 0;
                    int shift2 = 0;
                    int generalcount = 0;
                    int shift1count = 0;
                    int shift2count = 0;
                    int srno = 1;

                    cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Salary Consolidate", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Total No.of Employee: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.BackgroundColor = BaseColor.YELLOW;
                    cell.Colspan = 2;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    //cell.Border = 0;
                    //cell.Colspan = 7;
                    ////cell.PaddingTop = 5f;
                    ////cell.PaddingBottom = 5f;
                    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    //table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.Colspan = 9;
                    //cell.PaddingTop = 5f;
                    //cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("EmpNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("Dept ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("Fixed Salary", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("Total Hrs", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("Wages", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.BOTTOM_BORDER;
                    cell.Colspan = 9;
                    //cell.PaddingTop = 5f;
                    //cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    int Count = 0;
                    int Count1 = 0;
                    int Count2 = 0;
                    int Count3 = 0;
                    int deptsalary = 0;
                    int SubInsider = 0;
                    int SubOutSider = 0;
                    int depttotalsalary = 0;
                    int deptwages = 0;
                    int depttotalwages = 0;

                    int SNo = 1;


                    for (int j = 0; j < Shift_DS.Rows.Count; j++)
                    {
                        string Shift = Shift_DS.Rows[j]["ShiftDesc"].ToString();

                        cell = PhraseCell(new Phrase(" " + Shift_DS.Rows[j]["ShiftDesc"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.Colspan = 9;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        Count = 0;



                        for (int k = 0; k < Depart.Rows.Count; k++)
                        {
                            string Deprt = Depart.Rows[k]["DeptName"].ToString();

                            cell = PhraseCell(new Phrase(" " + Depart.Rows[k]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.Colspan = 9;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            table1.AddCell(cell);
                            Count1 = 0;

                            Count2 = 0;


                            SubInsider = 0;
                            SubOutSider = 0;

                            for (int m = 0; m < SubCat.Rows.Count; m++)
                            {
                                string Sub = SubCat.Rows[m]["SubCatName"].ToString();

                                Count3 = 0;
                                depttotalsalary = 0;

                                depttotalwages = 0;
                                for (int i = 0; i < AutoDTable.Rows.Count; i++)
                                {
                                    deptsalary = 0;
                                    string Deprt1 = AutoDTable.Rows[i]["DeptName"].ToString();
                                    string Shift1 = AutoDTable.Rows[i]["Shift"].ToString();
                                    string Sub1 = AutoDTable.Rows[i]["SubCatName"].ToString();
                                    deptsalary = Convert.ToInt32(AutoDTable.Rows[i]["Fixed_Salary"].ToString());
                                    deptwages = Convert.ToInt32(AutoDTable.Rows[i]["Wages"].ToString());
                                    if (Deprt == Deprt1 && Shift == Shift1 && Sub == Sub1)
                                    {
                                        cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["EmpCode"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["SubCatName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Name"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOUT"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Fixed_Salary"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);


                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Wages"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        Count = Count + 1;

                                        Count2 = Count2 + 1;

                                        Count3 = Count3 + 1;
                                        depttotalsalary += deptsalary;
                                        depttotalwages += deptwages;

                                        if (Shift1 == "GENERAL")
                                        {
                                            general += deptwages;
                                            generalcount = generalcount + 1;
                                        }
                                        else if (Shift1 == "SHIFT1")
                                        {
                                            shift1 += deptwages;
                                            shift1count = shift1count + 1;
                                        }
                                        else if (Shift1 == "SHIFT2")
                                        {
                                            shift2 += deptwages;
                                            shift2count = shift2count + 1;
                                        }
                                        SNo = SNo + 1;

                                    }
                                }

                                if (Count3 > 0)
                                {

                                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);


                                    cell = PhraseCell(new Phrase(" Total: " + depttotalsalary, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + depttotalwages, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);


                                }
                            }

                            if (Count2 == 0)
                            {
                                table1.DeleteLastRow();
                            }

                        }

                        if (Count == 0)
                        {
                            table1.DeleteLastRow();
                        }
                    }

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    //  cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.Colspan = 4;
                    //cell.PaddingTop = 5f;
                    //cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table2.AddCell(cell);

                    cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table2.AddCell(cell);

                    cell = PhraseCell(new Phrase("ShiftName", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table2.AddCell(cell);

                    cell = PhraseCell(new Phrase("No.Of Employees ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 10f;
                    cell.PaddingBottom = 10f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table2.AddCell(cell);

                    cell = PhraseCell(new Phrase("Total Wages ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER; // | Rectangle.LEFT_BORDER;   
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table2.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.BOTTOM_BORDER;
                    cell.Colspan = 5;
                    //cell.PaddingTop = 5f;
                    //cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table2.AddCell(cell);

                    int totalemployee = 0;
                    int totalwages = 0;
                    for (int m = 0; m < Shift_DS.Rows.Count; m++)
                    {
                        string sgiftcom = Shift_DS.Rows[m]["ShiftDesc"].ToString();

                        cell = PhraseCell(new Phrase(" " + srno, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                        cell.PaddingTop = 10f;
                        cell.PaddingBottom = 10f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table2.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + sgiftcom, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                        cell.PaddingTop = 10f;
                        cell.PaddingBottom = 10f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table2.AddCell(cell);
                        if (sgiftcom == "GENERAL")
                        {
                            totalemployee += generalcount;
                            totalwages += general;
                            cell = PhraseCell(new Phrase(" " + generalcount, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                            cell.PaddingTop = 10f;
                            cell.PaddingBottom = 10f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table2.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + general, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                            cell.PaddingTop = 10f;
                            cell.PaddingBottom = 10f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table2.AddCell(cell);
                        }
                        else if (sgiftcom == "SHIFT1")

                        {
                            totalemployee += shift1count;
                            totalwages += shift1;

                            cell = PhraseCell(new Phrase(" " + shift1count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                            cell.PaddingTop = 10f;
                            cell.PaddingBottom = 10f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table2.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + shift1, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                            cell.PaddingTop = 10f;
                            cell.PaddingBottom = 10f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table2.AddCell(cell);
                        }

                        else if (sgiftcom == "SHIFT2")

                        {
                            totalemployee += shift2count;
                            totalwages += shift2;

                            cell = PhraseCell(new Phrase(" " + shift2count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                            cell.PaddingTop = 10f;
                            cell.PaddingBottom = 10f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table2.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + shift2, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                            cell.PaddingTop = 10f;
                            cell.PaddingBottom = 10f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table2.AddCell(cell);
                        }
                        srno = srno + 1;



                    }

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table2.AddCell(cell);

                    cell = PhraseCell(new Phrase(" Total: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table2.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + totalemployee, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table2.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + totalwages, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table2.AddCell(cell);


                    table.SpacingBefore = 10f;
                    table.SpacingAfter = 10f;

                    document.Add(table);

                    table1.SpacingBefore = 5f;
                    table1.SpacingAfter = 5f;

                    document.Add(table1);

                    table2.SpacingBefore = 5f;
                    table2.SpacingAfter = 5f;

                    document.Add(table2);

                    document.Close();

                    byte[] bytes = memoryStream.ToArray();
                    memoryStream.Close();
                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment; filename=Salary Consolidate .pdf");
                    Response.ContentType = "application/pdf";
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.End();
                    Response.Close();
                }
            }
        }


        public void NOnAdminGetDaySalaryDetails(string FromDate, string ShiftType)
        {
            DataSet ds = new DataSet();
            DataTable AutoDTable = new DataTable();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            decimal rounded = 0;
            decimal roundedsalary = 0;
            decimal staffonedaysalary;
            AutoDTable.Columns.Add("EmpCode");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("TimeIN");
            AutoDTable.Columns.Add("TimeOUT");
            AutoDTable.Columns.Add("Fixed_Salary");
            AutoDTable.Columns.Add("Total_Hrs");
            AutoDTable.Columns.Add("Wages");
            AutoDTable.Columns.Add("Shift");
            AutoDTable.Columns.Add("SubCatName");


            DataTable Shift_DS = new DataTable();


            Document document = new Document(PageSize.A4, 25, 25, 45, 40);
            Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            writer.PageEvent = new HeaderFooter();
            document.Open();


            PdfPTable table = new PdfPTable(5);
            float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
            table.SetWidths(widths);
            table.WidthPercentage = 100;

            PdfPTable table1 = new PdfPTable(9);
            float[] widths1 = new float[] { 10f, 15f, 15f, 20f, 15f, 15f, 15f, 15f, 15f };
            table1.SetWidths(widths1);
            table1.WidthPercentage = 100;

            PdfPTable table2 = new PdfPTable(4);
            float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            table2.SetWidths(widths2);
            table2.WidthPercentage = 100;

            PdfPCell cell;

            DataTable Depart = new DataTable();


            //SSQL = "select MachineID,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT,";
            //SSQL = SSQL + "SubCatName,Total_Hrs,CatName,BasicSalary from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

            //if (ShiftType != "0")
            //{
            //    SSQL = SSQL + " And Shift='" + ShiftType + "'";
            //}
            //if (FromDate != "")
            //{
            //    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            //}
            //SSQL = SSQL + " And Present !='0.0'";

            //dt1 = MasterDate.GetDetails(SSQL);

            SSQL = "select MachineID,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT,";
            SSQL = SSQL + "SubCatName,Total_Hrs,CatName,BasicSalary from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

            if (ShiftType != "0")
            {
                SSQL = SSQL + " And Shift='" + ShiftType + "'";
            }
            if (FromDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            }
            SSQL = SSQL + " And Present !='0.0'";

            dt = MasterDate.GetDetails(SSQL);

            string staffsalary = "";
            string staffwages = "";

            string Machineid = "";
            string workinghours = "";
            string stdwrkhrs = "";
            decimal staffonehoursalary;
            decimal Totalwages;
            staffonedaysalary = 0;
            if (dt.Rows.Count > 0)
            {
                for (int k = 0; k < dt.Rows.Count; k++)
                {
                    Machineid = dt.Rows[k]["MachineID"].ToString();
                    staffwages = dt.Rows[k]["CatName"].ToString();
                    if (staffwages == "STAFF")
                    {
                        staffsalary = dt.Rows[k]["BasicSalary"].ToString();
                        staffonedaysalary = Convert.ToDecimal(staffsalary.ToString()) / 26;
                        roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                    }
                    else
                    {
                        staffsalary = dt.Rows[k]["BasicSalary"].ToString();
                        staffonedaysalary = Convert.ToDecimal(staffsalary.ToString());
                        roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));
                    }
                    workinghours = dt.Rows[k]["Total_Hrs"].ToString();
                    SSQL = "Select * from Employee_Mst where MachineID='" + Machineid + "'";
                    dt1 = MasterDate.GetDetails(SSQL);
                    if (dt1.Rows.Count > 0)
                    {
                        stdwrkhrs = dt1.Rows[0]["StdWrkHrs"].ToString();
                        if (stdwrkhrs == "" || stdwrkhrs == "0")
                        {
                            stdwrkhrs = "8";
                        }
                    }
                    staffonehoursalary = staffonedaysalary / Convert.ToDecimal(stdwrkhrs.ToString());
                    Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());
                    rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));

                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[k]["EmpCode"] = dt.Rows[k]["MachineID"].ToString();
                    AutoDTable.Rows[k]["DeptName"] = dt.Rows[k]["DeptName"].ToString();
                    AutoDTable.Rows[k]["Name"] = dt.Rows[k]["FirstName"].ToString();
                    AutoDTable.Rows[k]["TimeIN"] = dt.Rows[k]["TimeIN"].ToString();
                    AutoDTable.Rows[k]["TimeOUT"] = dt.Rows[k]["TimeOUT"].ToString();
                    AutoDTable.Rows[k]["Fixed_Salary"] = roundedsalary.ToString();

                    AutoDTable.Rows[k]["Total_Hrs"] = dt.Rows[k]["Total_Hrs"].ToString();
                    AutoDTable.Rows[k]["Wages"] = rounded;
                    AutoDTable.Rows[k]["Shift"] = dt.Rows[k]["Shift"].ToString();
                    AutoDTable.Rows[k]["SubCatName"] = dt.Rows[k]["SubCatName"].ToString();

                }

            }



            SSQL = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            if (ShiftType != "0")
            {
                SSQL = SSQL + " And ShiftDesc='" + ShiftType + "'";
            }
            //SSQL = " Group by ShiftDesc Asc ";

            Shift_DS = MasterDate.GetDetails(SSQL);

            SSQL = "select distinct DeptCode as DeptName from Department_Mst order by DeptName Asc";
            Depart = MasterDate.GetDetails(SSQL);

            DataTable SubCat = new DataTable();

            SSQL = "select SubCatName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' group by  SubCatName ";
            SubCat = MasterDate.GetDetails(SSQL);

            //table1.TableEvent = new BorderEvent();

            if (AutoDTable.Rows.Count > 0)
            {
                int general = 0;
                int shift1 = 0;
                int shift2 = 0;
                int generalcount = 0;
                int shift1count = 0;
                int shift2count = 0;
                int srno = 1;

                cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Salary Consolidate", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Total No.of Employee: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.BackgroundColor = BaseColor.YELLOW;
                cell.Colspan = 2;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                //cell.Border = 0;
                //cell.Colspan = 7;
                ////cell.PaddingTop = 5f;
                ////cell.PaddingBottom = 5f;
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER;
                cell.Colspan = 9;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("EmpNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Dept ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Fixed Salary", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Total Hrs", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Wages", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.BOTTOM_BORDER;
                cell.Colspan = 9;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                int Count = 0;
                int Count1 = 0;
                int Count2 = 0;
                int Count3 = 0;
                int deptsalary = 0;
                int SubInsider = 0;
                int SubOutSider = 0;
                int depttotalsalary = 0;
                int deptwages = 0;
                int depttotalwages = 0;

                int SNo = 1;


                for (int j = 0; j < Shift_DS.Rows.Count; j++)
                {
                    string Shift = Shift_DS.Rows[j]["ShiftDesc"].ToString();

                    cell = PhraseCell(new Phrase(" " + Shift_DS.Rows[j]["ShiftDesc"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 9;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    Count = 0;



                    for (int k = 0; k < Depart.Rows.Count; k++)
                    {
                        string Deprt = Depart.Rows[k]["DeptName"].ToString();

                        cell = PhraseCell(new Phrase(" " + Depart.Rows[k]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.Colspan = 9;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        table1.AddCell(cell);
                        Count1 = 0;

                        Count2 = 0;


                        SubInsider = 0;
                        SubOutSider = 0;

                        for (int m = 0; m < SubCat.Rows.Count; m++)
                        {
                            string Sub = SubCat.Rows[m]["SubCatName"].ToString();

                            Count3 = 0;
                            depttotalsalary = 0;

                            depttotalwages = 0;
                            for (int i = 0; i < AutoDTable.Rows.Count; i++)
                            {
                                deptsalary = 0;
                                string Deprt1 = AutoDTable.Rows[i]["DeptName"].ToString();
                                string Shift1 = AutoDTable.Rows[i]["Shift"].ToString();
                                string Sub1 = AutoDTable.Rows[i]["SubCatName"].ToString();
                                deptsalary = Convert.ToInt32(AutoDTable.Rows[i]["Fixed_Salary"].ToString());
                                deptwages = Convert.ToInt32(AutoDTable.Rows[i]["Wages"].ToString());
                                if (Deprt == Deprt1 && Shift == Shift1 && Sub == Sub1)
                                {
                                    cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["EmpCode"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["SubCatName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Name"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOUT"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Fixed_Salary"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);


                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Wages"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    Count = Count + 1;

                                    Count2 = Count2 + 1;

                                    Count3 = Count3 + 1;
                                    depttotalsalary += deptsalary;
                                    depttotalwages += deptwages;

                                    if (Shift1 == "GENERAL")
                                    {
                                        general += deptwages;
                                        generalcount = generalcount + 1;
                                    }
                                    else if (Shift1 == "SHIFT1")
                                    {
                                        shift1 += deptwages;
                                        shift1count = shift1count + 1;
                                    }
                                    else if (Shift1 == "SHIFT2")
                                    {
                                        shift2 += deptwages;
                                        shift2count = shift2count + 1;
                                    }
                                    SNo = SNo + 1;

                                }
                            }

                            if (Count3 > 0)
                            {

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);


                                cell = PhraseCell(new Phrase(" Total: " + depttotalsalary, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" " + depttotalwages, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);


                            }
                        }

                        if (Count2 == 0)
                        {
                            table1.DeleteLastRow();
                        }

                    }

                    if (Count == 0)
                    {
                        table1.DeleteLastRow();
                    }
                }

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                //  cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                cell.Colspan = 4;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table2.AddCell(cell);

                cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table2.AddCell(cell);

                cell = PhraseCell(new Phrase("ShiftName", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table2.AddCell(cell);

                cell = PhraseCell(new Phrase("No.Of Employees ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                cell.PaddingTop = 10f;
                cell.PaddingBottom = 10f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table2.AddCell(cell);

                cell = PhraseCell(new Phrase("Total Wages ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER; // | Rectangle.LEFT_BORDER;   
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table2.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.BOTTOM_BORDER;
                cell.Colspan = 5;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table2.AddCell(cell);

                int totalemployee = 0;
                int totalwages = 0;
                for (int m = 0; m < Shift_DS.Rows.Count; m++)
                {
                    string sgiftcom = Shift_DS.Rows[m]["ShiftDesc"].ToString();

                    cell = PhraseCell(new Phrase(" " + srno, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                    cell.PaddingTop = 10f;
                    cell.PaddingBottom = 10f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table2.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + sgiftcom, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                    cell.PaddingTop = 10f;
                    cell.PaddingBottom = 10f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table2.AddCell(cell);
                    if (sgiftcom == "GENERAL")
                    {
                        totalemployee += generalcount;
                        totalwages += general;
                        cell = PhraseCell(new Phrase(" " + generalcount, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                        cell.PaddingTop = 10f;
                        cell.PaddingBottom = 10f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table2.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + general, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                        cell.PaddingTop = 10f;
                        cell.PaddingBottom = 10f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table2.AddCell(cell);
                    }
                    else if (sgiftcom == "SHIFT1")

                    {
                        totalemployee += shift1count;
                        totalwages += shift1;

                        cell = PhraseCell(new Phrase(" " + shift1count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                        cell.PaddingTop = 10f;
                        cell.PaddingBottom = 10f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table2.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + shift1, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                        cell.PaddingTop = 10f;
                        cell.PaddingBottom = 10f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table2.AddCell(cell);
                    }

                    else if (sgiftcom == "SHIFT2")

                    {
                        totalemployee += shift2count;
                        totalwages += shift2;

                        cell = PhraseCell(new Phrase(" " + shift2count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                        cell.PaddingTop = 10f;
                        cell.PaddingBottom = 10f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table2.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + shift2, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;

                        cell.PaddingTop = 10f;
                        cell.PaddingBottom = 10f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table2.AddCell(cell);
                    }
                    srno = srno + 1;



                }

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table2.AddCell(cell);

                cell = PhraseCell(new Phrase(" Total: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table2.AddCell(cell);

                cell = PhraseCell(new Phrase(" " + totalemployee, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table2.AddCell(cell);

                cell = PhraseCell(new Phrase(" " + totalwages, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table2.AddCell(cell);


                table.SpacingBefore = 10f;
                table.SpacingAfter = 10f;

                document.Add(table);

                table1.SpacingBefore = 5f;
                table1.SpacingAfter = 5f;

                document.Add(table1);

                table2.SpacingBefore = 5f;
                table2.SpacingAfter = 5f;

                document.Add(table2);

                document.Close();

                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=Salary Consolidate .pdf");
                Response.ContentType = "application/pdf";
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(bytes);
                Response.End();
                Response.Close();
            }

        }


        public void BetweenAttendance(string AboveHour, string BelowHour, string FromDate, string ShiftType)
        {
            if (SessionUserType == "2")
            {
                NonAdminBetweenAttendance(AboveHour, BelowHour, FromDate, ShiftType);
            }
            else
            {
                Decimal Ded1;
                Decimal Ded2;
                Ded1 = (Convert.ToDecimal(AboveHour.ToString()));
                Decimal DAboveHours = (Math.Round(Ded1, 1, MidpointRounding.AwayFromZero));

                Ded2 = (Convert.ToDecimal(BelowHour.ToString()));
                Decimal DBelowHours = (Math.Round(Ded2, 1, MidpointRounding.AwayFromZero));

                Document document = new Document(PageSize.A4, 25, 25, 45, 40);
                Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

                writer.PageEvent = new HeaderFooter();
                document.Open();


                PdfPTable table = new PdfPTable(5);
                float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
                table.SetWidths(widths);
                table.WidthPercentage = 100;

                PdfPTable table1 = new PdfPTable(8);
                float[] widths1 = new float[] { 10f, 15f, 15f, 15f, 25f, 15f, 15f, 15f };
                table1.SetWidths(widths1);
                table1.WidthPercentage = 100;

                PdfPTable table2 = new PdfPTable(4);
                float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
                table2.SetWidths(widths2);
                table2.WidthPercentage = 100;

                PdfPCell cell;

                SSQL = "select MachineID,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,ExistingCode, TimeIN,TimeOut,Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                if (FromDate != "")
                {
                    SSQL = SSQL + " And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                }

                if (AboveHour != "")
                {

                    SSQL = SSQL + " AND Total_Hrs BETWEEN'" + DBelowHours + "' And '" + DAboveHours + "' And Present > 0 ";
                }


                AutoDTable = MasterDate.GetDetails(SSQL);

                if (AutoDTable.Rows.Count > 0)
                {
                    cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Between Hours Report", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Total No.of Employee Between '" + BelowHour + "' and '" + AboveHour + "' Hours: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.BackgroundColor = BaseColor.YELLOW;
                    cell.Colspan = 2;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    //cell.Border = 0;
                    //cell.Colspan = 7;
                    ////cell.PaddingTop = 5f;
                    ////cell.PaddingBottom = 5f;
                    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    //table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.Colspan = 8;
                    //cell.PaddingTop = 5f;
                    //cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("MachineID", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("Existing Code ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("Dept", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("total Hours", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.BOTTOM_BORDER;
                    cell.Colspan = 8;
                    //cell.PaddingTop = 5f;
                    //cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);



                    int SNo = 1;
                    // MachineID,DeptName,Shift,isnull(FirstName, '') + '.' + isnull(LastName, '') as FirstName,ExistingCode, TimeIN,TimeOut,Total_Hrs
                    for (int i = 0; i < AutoDTable.Rows.Count; i++)
                    {
                        cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["ExistingCode"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOut"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        SNo = SNo + 1;
                    }


                }
                else
                {
                    cell = PhraseCell(new Phrase("No Data Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 20f;
                    //cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                }
                table.SpacingBefore = 10f;
                table.SpacingAfter = 10f;

                document.Add(table);

                table1.SpacingBefore = 10f;
                table1.SpacingAfter = 10f;

                document.Add(table1);


                document.Close();

                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=Between_HOURS.pdf");
                Response.ContentType = "application/pdf";
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(bytes);
                Response.End();
                Response.Close();
            }
        }

        public void NonAdminBetweenAttendance(string AboveHour, string BelowHour, string FromDate, string ShiftType)
        {

            Decimal Ded1;
            Decimal Ded2;
            Ded1 = (Convert.ToDecimal(AboveHour.ToString()));
            Decimal DAboveHours = (Math.Round(Ded1, 1, MidpointRounding.AwayFromZero));

            Ded2 = (Convert.ToDecimal(BelowHour.ToString()));
            Decimal DBelowHours = (Math.Round(Ded2, 1, MidpointRounding.AwayFromZero));

            Document document = new Document(PageSize.A4, 25, 25, 45, 40);
            Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            writer.PageEvent = new HeaderFooter();
            document.Open();


            PdfPTable table = new PdfPTable(5);
            float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
            table.SetWidths(widths);
            table.WidthPercentage = 100;

            PdfPTable table1 = new PdfPTable(8);
            float[] widths1 = new float[] { 10f, 15f, 15f, 15f, 25f, 15f, 15f, 15f };
            table1.SetWidths(widths1);
            table1.WidthPercentage = 100;

            PdfPTable table2 = new PdfPTable(4);
            float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            table2.SetWidths(widths2);
            table2.WidthPercentage = 100;

            PdfPCell cell;

            SSQL = "select MachineID,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,ExistingCode, TimeIN,TimeOut,Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            if (FromDate != "")
            {
                SSQL = SSQL + " And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            }

            if (AboveHour != "")
            {

                SSQL = SSQL + " AND Total_Hrs BETWEEN'" + DBelowHours + "' And '" + DAboveHours + "' And Present > 0 ";
            }


            AutoDTable = MasterDate.GetDetails(SSQL);

            if (AutoDTable.Rows.Count > 0)
            {
                cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Between Hours Report", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Total No.of Employee Between '" + BelowHour + "' and '" + AboveHour + "' Hours: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.BackgroundColor = BaseColor.YELLOW;
                cell.Colspan = 2;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                //cell.Border = 0;
                //cell.Colspan = 7;
                ////cell.PaddingTop = 5f;
                ////cell.PaddingBottom = 5f;
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER;
                cell.Colspan = 8;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("MachineID", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Existing Code ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Dept", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("total Hours", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.BOTTOM_BORDER;
                cell.Colspan = 8;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);



                int SNo = 1;
                // MachineID,DeptName,Shift,isnull(FirstName, '') + '.' + isnull(LastName, '') as FirstName,ExistingCode, TimeIN,TimeOut,Total_Hrs
                for (int i = 0; i < AutoDTable.Rows.Count; i++)
                {
                    cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["ExistingCode"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOut"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    SNo = SNo + 1;
                }


            }
            else
            {
                cell = PhraseCell(new Phrase("No Data Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 20f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

            }
            table.SpacingBefore = 10f;
            table.SpacingAfter = 10f;

            document.Add(table);

            table1.SpacingBefore = 10f;
            table1.SpacingAfter = 10f;

            document.Add(table1);


            document.Close();

            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=Between_HOURS.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();

        }



        public void AboveAttendance(string AboveHour, string FromDate, string ShiftType)
        {

            if (SessionUserType == "2")
            {
                NonAdminAboveAttendance(AboveHour, FromDate, ShiftType);
            }
            else
            {
                Decimal Ded1;
                Ded1 = (Convert.ToDecimal(AboveHour.ToString()));
                Decimal DAboveHours = (Math.Round(Ded1, 1, MidpointRounding.AwayFromZero));
                Document document = new Document(PageSize.A4, 25, 25, 45, 40);
                Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

                writer.PageEvent = new HeaderFooter();
                document.Open();


                PdfPTable table = new PdfPTable(5);
                float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
                table.SetWidths(widths);
                table.WidthPercentage = 100;

                PdfPTable table1 = new PdfPTable(8);
                float[] widths1 = new float[] { 10f, 15f, 15f, 15f, 25f, 15f, 15f, 15f };
                table1.SetWidths(widths1);
                table1.WidthPercentage = 100;

                PdfPTable table2 = new PdfPTable(4);
                float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
                table2.SetWidths(widths2);
                table2.WidthPercentage = 100;

                PdfPCell cell;

                SSQL = "select MachineID,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,ExistingCode, TimeIN,TimeOut,Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                if (FromDate != "")
                {
                    SSQL = SSQL + " And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                }

                if (AboveHour != "")
                {
                    SSQL = SSQL + " AND Total_Hrs >='" + DAboveHours + "' And Present > 0";
                }


                AutoDTable = MasterDate.GetDetails(SSQL);

                if (AutoDTable.Rows.Count > 0)
                {
                    cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Above Hours Report", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Total No.of Employee Above '" + AboveHour + "' Hours: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.BackgroundColor = BaseColor.YELLOW;
                    cell.Colspan = 2;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    //cell.Border = 0;
                    //cell.Colspan = 7;
                    ////cell.PaddingTop = 5f;
                    ////cell.PaddingBottom = 5f;
                    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    //table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.Colspan = 8;
                    //cell.PaddingTop = 5f;
                    //cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("MachineID", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("Existing Code ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("Dept", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("total Hours", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.BOTTOM_BORDER;
                    cell.Colspan = 8;
                    //cell.PaddingTop = 5f;
                    //cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);



                    int SNo = 1;
                    // MachineID,DeptName,Shift,isnull(FirstName, '') + '.' + isnull(LastName, '') as FirstName,ExistingCode, TimeIN,TimeOut,Total_Hrs
                    for (int i = 0; i < AutoDTable.Rows.Count; i++)
                    {
                        cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["ExistingCode"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOut"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        SNo = SNo + 1;
                    }



                }
                else
                {
                    cell = PhraseCell(new Phrase("No Data Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 20f;
                    //cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                }


                table.SpacingBefore = 10f;
                table.SpacingAfter = 10f;

                document.Add(table);

                table1.SpacingBefore = 10f;
                table1.SpacingAfter = 10f;

                document.Add(table1);


                document.Close();

                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=ABOVE_HOURS.pdf");
                Response.ContentType = "application/pdf";
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(bytes);
                Response.End();
                Response.Close();





            }
        }
        public void NonAdminAboveAttendance(string AboveHour, string FromDate, string ShiftType)
        {
            Decimal Ded1;
            Ded1 = (Convert.ToDecimal(AboveHour.ToString()));
            Decimal DAboveHours = (Math.Round(Ded1, 1, MidpointRounding.AwayFromZero));
            Document document = new Document(PageSize.A4, 25, 25, 45, 40);
            Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            writer.PageEvent = new HeaderFooter();
            document.Open();


            PdfPTable table = new PdfPTable(5);
            float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
            table.SetWidths(widths);
            table.WidthPercentage = 100;

            PdfPTable table1 = new PdfPTable(8);
            float[] widths1 = new float[] { 10f, 15f, 15f, 15f, 25f, 15f, 15f, 15f };
            table1.SetWidths(widths1);
            table1.WidthPercentage = 100;

            PdfPTable table2 = new PdfPTable(4);
            float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            table2.SetWidths(widths2);
            table2.WidthPercentage = 100;

            PdfPCell cell;

            SSQL = "select MachineID,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,ExistingCode, TimeIN,TimeOut,Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            if (FromDate != "")
            {
                SSQL = SSQL + " And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            }

            if (AboveHour != "")
            {
                SSQL = SSQL + " AND Total_Hrs >='" + DAboveHours + "' And Present > 0";
            }


            AutoDTable = MasterDate.GetDetails(SSQL);

            if (AutoDTable.Rows.Count > 0)
            {
                cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Above Hours Report", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Total No.of Employee Above '" + AboveHour + "' Hours: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.BackgroundColor = BaseColor.YELLOW;
                cell.Colspan = 2;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                //cell.Border = 0;
                //cell.Colspan = 7;
                ////cell.PaddingTop = 5f;
                ////cell.PaddingBottom = 5f;
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER;
                cell.Colspan = 8;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("MachineID", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Existing Code ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Dept", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("total Hours", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.BOTTOM_BORDER;
                cell.Colspan = 8;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);



                int SNo = 1;
                // MachineID,DeptName,Shift,isnull(FirstName, '') + '.' + isnull(LastName, '') as FirstName,ExistingCode, TimeIN,TimeOut,Total_Hrs
                for (int i = 0; i < AutoDTable.Rows.Count; i++)
                {
                    cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["ExistingCode"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOut"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    SNo = SNo + 1;
                }



            }
            else
            {
                cell = PhraseCell(new Phrase("No Data Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 20f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

            }


            table.SpacingBefore = 10f;
            table.SpacingAfter = 10f;

            document.Add(table);

            table1.SpacingBefore = 10f;
            table1.SpacingAfter = 10f;

            document.Add(table1);


            document.Close();

            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=ABOVE_HOURS.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();



        }

        public void belowAttendance(string BelowHour, string FromDate, string ShiftType)
        {
            if (SessionUserType == "2")
            {
                NonAdminBeloweAttendance(BelowHour, FromDate, ShiftType);
            }
            else
            {

            }

            Decimal Ded1;
            Ded1 = (Convert.ToDecimal(BelowHour.ToString()));
            Decimal DBelowHours = (Math.Round(Ded1, 1, MidpointRounding.AwayFromZero));

            Document document = new Document(PageSize.A4, 25, 25, 45, 40);
            Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            writer.PageEvent = new HeaderFooter();
            document.Open();


            PdfPTable table = new PdfPTable(5);
            float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
            table.SetWidths(widths);
            table.WidthPercentage = 100;

            PdfPTable table1 = new PdfPTable(8);
            float[] widths1 = new float[] { 10f, 15f, 15f, 15f, 25f, 15f, 15f, 15f };
            table1.SetWidths(widths1);
            table1.WidthPercentage = 100;

            PdfPTable table2 = new PdfPTable(4);
            float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            table2.SetWidths(widths2);
            table2.WidthPercentage = 100;

            PdfPCell cell;

            SSQL = "select MachineID,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,ExistingCode, TimeIN,TimeOut,Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            if (FromDate != "")
            {
                SSQL = SSQL + " And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            }

            if (BelowHour != "")
            {
                SSQL = SSQL + " AND Total_Hrs <='" + DBelowHours + "' And Present > 0";
            }


            AutoDTable = MasterDate.GetDetails(SSQL);

            if (AutoDTable.Rows.Count > 0)
            {
                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Below Hours Report", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Total No.of Employee Below '" + BelowHour + "' Hours: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.BackgroundColor = BaseColor.YELLOW;
                cell.Colspan = 2;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                //cell.Border = 0;
                //cell.Colspan = 7;
                ////cell.PaddingTop = 5f;
                ////cell.PaddingBottom = 5f;
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER;
                cell.Colspan = 8;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("MachineID", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Existing Code ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Dept", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("total Hours", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.BOTTOM_BORDER;
                cell.Colspan = 8;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                int Count = 0;
                int Count1 = 0;
                int Count2 = 0;
                int Count3 = 0;
                int SubInsider = 0;
                int SubOutSider = 0;

                int SNo = 1;
                // MachineID,DeptName,Shift,isnull(FirstName, '') + '.' + isnull(LastName, '') as FirstName,ExistingCode, TimeIN,TimeOut,Total_Hrs
                for (int i = 0; i < AutoDTable.Rows.Count; i++)
                {
                    cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["ExistingCode"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOut"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    SNo = SNo + 1;
                }




            }
            else
            {
                cell = PhraseCell(new Phrase("No Data Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 20f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

            }

            table.SpacingBefore = 10f;
            table.SpacingAfter = 10f;

            document.Add(table);

            table1.SpacingBefore = 10f;
            table1.SpacingAfter = 10f;

            document.Add(table1);


            document.Close();

            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=BELOW_HOURS.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();




        }

        public void NonAdminBeloweAttendance(string BelowHour, string FromDate, string ShiftType)
        {


            Decimal Ded1;
            Ded1 = (Convert.ToDecimal(BelowHour.ToString()));
            Decimal DBelowHours = (Math.Round(Ded1, 1, MidpointRounding.AwayFromZero));

            Document document = new Document(PageSize.A4, 25, 25, 45, 40);
            Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            writer.PageEvent = new HeaderFooter();
            document.Open();


            PdfPTable table = new PdfPTable(5);
            float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
            table.SetWidths(widths);
            table.WidthPercentage = 100;

            PdfPTable table1 = new PdfPTable(8);
            float[] widths1 = new float[] { 10f, 15f, 15f, 15f, 25f, 15f, 15f, 15f };
            table1.SetWidths(widths1);
            table1.WidthPercentage = 100;

            PdfPTable table2 = new PdfPTable(4);
            float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            table2.SetWidths(widths2);
            table2.WidthPercentage = 100;

            PdfPCell cell;

            SSQL = "select MachineID,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,ExistingCode, TimeIN,TimeOut,Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            if (FromDate != "")
            {
                SSQL = SSQL + " And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            }

            if (BelowHour != "")
            {
                SSQL = SSQL + " AND Total_Hrs <='" + DBelowHours + "' And Present > 0";
            }


            AutoDTable = MasterDate.GetDetails(SSQL);

            if (AutoDTable.Rows.Count > 0)
            {
                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Below Hours Report", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Total No.of Employee Below '" + BelowHour + "' Hours: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.BackgroundColor = BaseColor.YELLOW;
                cell.Colspan = 2;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                //cell.Border = 0;
                //cell.Colspan = 7;
                ////cell.PaddingTop = 5f;
                ////cell.PaddingBottom = 5f;
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER;
                cell.Colspan = 8;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("MachineID", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Existing Code ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Dept", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("total Hours", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.BOTTOM_BORDER;
                cell.Colspan = 8;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                int Count = 0;
                int Count1 = 0;
                int Count2 = 0;
                int Count3 = 0;
                int SubInsider = 0;
                int SubOutSider = 0;

                int SNo = 1;
                // MachineID,DeptName,Shift,isnull(FirstName, '') + '.' + isnull(LastName, '') as FirstName,ExistingCode, TimeIN,TimeOut,Total_Hrs
                for (int i = 0; i < AutoDTable.Rows.Count; i++)
                {
                    cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["ExistingCode"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOut"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    SNo = SNo + 1;
                }




            }
            else
            {
                cell = PhraseCell(new Phrase("No Data Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 20f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

            }

            table.SpacingBefore = 10f;
            table.SpacingAfter = 10f;

            document.Add(table);

            table1.SpacingBefore = 10f;
            table1.SpacingAfter = 10f;

            document.Add(table1);


            document.Close();

            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=BELOW_HOURS.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();
        }


        public void BreaksummaryExcel(string FromDate,string Dept)
        {

            DataTable dtIPaddress = new DataTable();

            string SSQL = "select * from IPAddress_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            dtIPaddress = MasterDate.GetDetails(SSQL);

            string mIpAddress_IN = "";
            string mIpAddress_OUT = "";

            if (dtIPaddress.Rows.Count > 0)
            {
                for (int i = 0; i < dtIPaddress.Rows.Count; i++)
                {
                    if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                    {
                        mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                    else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                    {
                        mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                }
            }

            if (SessionUserID == "2")
            {
                Fill_BreakTime_Multi_TimeIN(FromDate,Dept);
                Break_Write_Multin(FromDate,Dept);


                grid.HeaderStyle.Font.Bold = true;
                grid.DataSource = DataCells;
                grid.DataBind();
                string attachment = "attachment;filename=BREAK TIME SUMMARY.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td font-Bold='true' colspan='12'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td font-Bold='true' colspan='12'>");
                Response.Write("<a style=\"font-weight:bold\"> BREAK TIME SUMMARY REPORT &nbsp;&nbsp;&nbsp;" + FromDate + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                //Response.Write("<tr Font-Bold='true' align='center'>");
                //Response.Write("<td font-Bold='true' colspan='8'>");
                //Response.Write("<a style=\"font-weight:bold\">" + Date + "</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                Fill_BreakTime_Multi_TimeIN(FromDate,Dept);
                Break_Write_Multin(FromDate,Dept);



                grid.HeaderStyle.Font.Bold = true;
                grid.DataSource = DataCells;
                grid.DataBind();
                string attachment = "attachment;filename=BREAK TIME SUMMARY.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td font-Bold='true' colspan='12'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td font-Bold='true' colspan='12'>");
                Response.Write("<a style=\"font-weight:bold\"> BREAK TIME SUMMARY REPORT &nbsp;&nbsp;&nbsp;" + FromDate + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                //Response.Write("<tr Font-Bold='true' align='center'>");
                //Response.Write("<td font-Bold='true' colspan='8'>");
                //Response.Write("<a style=\"font-weight:bold\">" + Date + "</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }



        }




        public void DaySummaryExcel(string FromDate,string Dept)
        {
            DataTable dtIPaddress = new DataTable();

            string SSQL = "select * from IPAddress_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            dtIPaddress = MasterDate.GetDetails(SSQL);

            string mIpAddress_IN = "";
            string mIpAddress_OUT = "";

            if (dtIPaddress.Rows.Count > 0)
            {
                for (int i = 0; i < dtIPaddress.Rows.Count; i++)
                {
                    if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                    {
                        mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                    else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                    {
                        mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                }
            }



            if (SessionUserID == "2")
            {
                NonAdminGetFill_Multi_timeIN(FromDate);
                NonAdminGetWrite_MultiIN(FromDate);


                grid.HeaderStyle.Font.Bold = true;
                grid.DataSource = DataCells;
                grid.DataBind();
                string attachment = "attachment;filename=DAY ATTENDANCE SUMMARY.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td font-Bold='true' colspan='12'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td font-Bold='true' colspan='12'>");
                Response.Write("<a style=\"font-weight:bold\"> DAY ATTENDANCE SUMMARY REPORT &nbsp;&nbsp;&nbsp;" + FromDate + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                //Response.Write("<tr Font-Bold='true' align='center'>");
                //Response.Write("<td font-Bold='true' colspan='8'>");
                //Response.Write("<a style=\"font-weight:bold\">" + Date + "</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                Fill_Multi_timeIN(FromDate,Dept);
                Write_MultiIN(FromDate);



                grid.HeaderStyle.Font.Bold = true;
                grid.DataSource = DataCells;
                grid.DataBind();
                string attachment = "attachment;filename=DAY ATTENDANCE SUMMARY.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td font-Bold='true' colspan='12'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td font-Bold='true' colspan='12'>");
                Response.Write("<a style=\"font-weight:bold\"> DAY ATTENDANCE SUMMARY REPORT &nbsp;&nbsp;&nbsp;" + FromDate + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                //Response.Write("<tr Font-Bold='true' align='center'>");
                //Response.Write("<td font-Bold='true' colspan='8'>");
                //Response.Write("<a style=\"font-weight:bold\">" + Date + "</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }

        }

        public void Fill_BreakTime_Multi_TimeIN(string FromDate,string Dept)
        {
            try
            {                                            
                AutoDataTable.Columns.Add("DeptName");
                AutoDataTable.Columns.Add("Designation");
                AutoDataTable.Columns.Add("MachineID");
                AutoDataTable.Columns.Add("MachineID_Encrypt");
                AutoDataTable.Columns.Add("EmpNo");
                AutoDataTable.Columns.Add("ExistingCode");
                AutoDataTable.Columns.Add("FirstName");
                AutoDataTable.Columns.Add("TimeIN1");
                AutoDataTable.Columns.Add("TimeIN2");
                AutoDataTable.Columns.Add("TimeIN3");
                AutoDataTable.Columns.Add("TimeIN4");
                AutoDataTable.Columns.Add("TimeOUT1");
                AutoDataTable.Columns.Add("TimeOUT2");
                AutoDataTable.Columns.Add("TimeOUT3");
                AutoDataTable.Columns.Add("TimeOUT4");





                SSQL = "";
                SSQL = "select isnull(DEP.DeptName,'') as [DeptName],Cast(EM.MachineID As int) As MachineID,EM.MachineID_Encrypt";
                SSQL = SSQL + ",EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
                SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName],isnull(DM.DesignName,'')as[Designation]";
                SSQL = SSQL + " from Employee_Mst EM inner join Designation_Mst DM on EM.Designation = DM.DesignNo ";
                SSQL = SSQL + " inner join Department_Mst DEP on  EM.DeptName= DEP.DeptCode  Where EM.Compcode='" + SessionCcode + "'";

                SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";

                if (Dept!= "0")
                {
                    SSQL = SSQL + " And EM.DeptName='" + Dept +"'";
                }

                SSQL = SSQL + " Order By DeptName, MachineID";

                dsEmployee = MasterDate.GetDetails(SSQL);

                if (dsEmployee.Rows.Count <= 0)
                {
                    return;
                }
                else
                {
                    Int32 DSVAL = 0;
                    for (int i = 0; i < dsEmployee.Rows.Count; i++)
                    {
                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();

                        AutoDataTable.Rows[DSVAL][0] = dsEmployee.Rows[i]["DeptName"].ToString();
                        AutoDataTable.Rows[DSVAL][1] = dsEmployee.Rows[i]["Designation"].ToString();
                        AutoDataTable.Rows[DSVAL][2] = dsEmployee.Rows[i]["MachineID"].ToString();
                        AutoDataTable.Rows[DSVAL][3] = dsEmployee.Rows[i]["MachineID_Encrypt"].ToString();
                        AutoDataTable.Rows[DSVAL][4] = dsEmployee.Rows[i]["EmpNo"].ToString();
                        AutoDataTable.Rows[DSVAL][5] = dsEmployee.Rows[i]["ExistingCode"].ToString();
                        AutoDataTable.Rows[DSVAL][6] = dsEmployee.Rows[i]["FirstName"].ToString();

                        DSVAL += 1;
                    }
                }
                //}
            }
            catch (Exception e)
            {
            }

        }


       



public void Break_Write_Multin(string FromDate,string Dept)
        {

            try
            {
                int intI = 1;
                int intK = 1;
                int intCol = 0;


                DataCells.Columns.Add("SNo");
                DataCells.Columns.Add("DeptName");
                DataCells.Columns.Add("Designation");
                DataCells.Columns.Add("EmpNo");
                DataCells.Columns.Add("ExistingCode");
                DataCells.Columns.Add("FirstName");
                DataCells.Columns.Add("TimeIN1");
                DataCells.Columns.Add("TimeOUT1");
                DataCells.Columns.Add("Total1");
                DataCells.Columns.Add("TimeIN2");
                DataCells.Columns.Add("TimeOUT2");
                DataCells.Columns.Add("Total2");
                DataCells.Columns.Add("TimeIN3");
                DataCells.Columns.Add("TimeOUT3");
                DataCells.Columns.Add("Total3");
                DataCells.Columns.Add("TimeIN4");
                DataCells.Columns.Add("TimeOUT4");
                DataCells.Columns.Add("Total4");
                DataCells.Columns.Add("Final Total");

                for (intCol = 0; intCol < AutoDataTable.Rows.Count; intCol++)
                {
                    DataCells.NewRow();
                    DataCells.Rows.Add();

                    DataCells.Rows[intCol]["SNo"] = intCol + 1;
                    DataCells.Rows[intCol]["DeptName"] = AutoDataTable.Rows[intCol]["DeptName"];
                    DataCells.Rows[intCol]["Designation"] = AutoDataTable.Rows[intCol]["Designation"];
                    DataCells.Rows[intCol]["EmpNo"] = AutoDataTable.Rows[intCol]["EmpNo"];
                    DataCells.Rows[intCol]["ExistingCode"] = AutoDataTable.Rows[intCol]["ExistingCode"];
                    DataCells.Rows[intCol]["FirstName"] = AutoDataTable.Rows[intCol]["FirstName"];
                }

                intCol = 5;


                fromdate = Convert.ToDateTime(FromDate);


                intK = 1;
                intI = 1;

                for (int intRow = 0; intRow < AutoDataTable.Rows.Count; intRow++)
                {
                    intK = 1;
                    int colIndex = intK;
                    bool isPresent = false;
                    isPresent = false;
                    string Machine_ID_Str = "";
                    string OT_Week_OFF_Machine_No = "";
                    string Date_Value_Str = "";
                    string Date_Value_Str1 = "";

                    string Time_IN_Str = "";
                    string Time_Out_Str = "";
                    string Total_Time_get = "";
                    Int32 j = 0;
                    double time_Check_dbl = 0;
                    isPresent = false;

                    string MID = AutoDataTable.Rows[intRow][2].ToString();
                    Machine_ID_Str = AutoDataTable.Rows[intRow][3].ToString();
                    Date_Value_Str = string.Format(FromDate, "dd-MM-yyyy");

                    DateTime Dateval = Convert.ToDateTime(Date_Value_Str).AddDays(1);
                    if (MID == "509")
                    {

                        MID = "509";
                    }


                    if (Machine_ID_Str == "MjAwNQ==")
                    {
                        Machine_ID_Str = "MjAwNQ==";
                    }

                    Date_Value_Str1 = string.Format(Convert.ToString(Dateval), "dd-MM-yyyy");

                    //Shift Check shift1
                    string Final_Shift = "";
                    DataTable Shift_DS = new DataTable();
                    Int32 k = 0;
                    bool Shift_Check_blb = false;
                    string Shift_Start_Time = null;
                    string Shift_End_Time = null;
                    string Employee_Time = "";
                    DateTime ShiftdateStartIN = default(DateTime);
                    DateTime ShiftdateEndIN = default(DateTime);
                    string Final_InTime = "";
                    string Final_OutTime = "";
                    string From_Time_Str = "";
                    string To_Time_Str = "";

                    DateTime EmpdateIN = default(DateTime);
                    DayOfWeek day = fromdate.DayOfWeek;
                    string WeekofDay = day.ToString();
                    SSQL = "Select TimeIN from LogTime_Lunch where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "07:00' And TimeIN <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "07:00' Order by TimeIN ASC";
                    mLocalDS = ManualAttend.GetDetails(SSQL);
                
                    if (mLocalDS.Rows.Count != 0)
                    {
                                                   
                            if (mLocalDS.Rows.Count == 1)
                            {

                                DataCells.Rows[intRow][6] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0]["TimeIN"]);


                            }
                            if (mLocalDS.Rows.Count == 2)
                            {
                                DataCells.Rows[intRow][6] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0]["TimeIN"]);
                                DataCells.Rows[intRow][7] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[1]["TimeIN"]);

                            }
                            else if (mLocalDS.Rows.Count == 3)
                            {
                                DataCells.Rows[intRow][6] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0]["TimeIN"]);
                                DataCells.Rows[intRow][7] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[1]["TimeIN"]);
                                DataCells.Rows[intRow][9] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[2]["TimeIN"]);


                            }
                            else if (mLocalDS.Rows.Count == 4)
                            {
                                DataCells.Rows[intRow][6] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0]["TimeIN"]);
                                DataCells.Rows[intRow][7] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[1]["TimeIN"]);
                                DataCells.Rows[intRow][9] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[2]["TimeIN"]);
                                DataCells.Rows[intRow][10] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[3]["TimeIN"]);

                            }

                        else if (mLocalDS.Rows.Count == 5)
                        {
                            DataCells.Rows[intRow][6] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0]["TimeIN"]);
                            DataCells.Rows[intRow][7] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[1]["TimeIN"]);
                            DataCells.Rows[intRow][9] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[2]["TimeIN"]);
                            DataCells.Rows[intRow][10] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[3]["TimeIN"]);
                            DataCells.Rows[intRow][12] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[4]["TimeIN"]);

                        }
                        else if (mLocalDS.Rows.Count == 6)
                        {
                            DataCells.Rows[intRow][6] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0]["TimeIN"]);
                            DataCells.Rows[intRow][7] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[1]["TimeIN"]);
                            DataCells.Rows[intRow][9] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[2]["TimeIN"]);
                            DataCells.Rows[intRow][10] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[3]["TimeIN"]);
                            DataCells.Rows[intRow][12] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[4]["TimeIN"]);
                            DataCells.Rows[intRow][13] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[5]["TimeIN"]);

                        }

                        else if (mLocalDS.Rows.Count == 7)
                        {
                            DataCells.Rows[intRow][6] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0]["TimeIN"]);
                            DataCells.Rows[intRow][7] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[1]["TimeIN"]);
                            DataCells.Rows[intRow][9] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[2]["TimeIN"]);
                            DataCells.Rows[intRow][10] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[3]["TimeIN"]);
                            DataCells.Rows[intRow][12] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[4]["TimeIN"]);
                            DataCells.Rows[intRow][13] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[5]["TimeIN"]);
                            DataCells.Rows[intRow][15] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[6]["TimeIN"]);
                        }
                        else if (mLocalDS.Rows.Count == 8)
                        {
                            DataCells.Rows[intRow][6] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0]["TimeIN"]);
                            DataCells.Rows[intRow][7] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[1]["TimeIN"]);
                            DataCells.Rows[intRow][9] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[2]["TimeIN"]);
                            DataCells.Rows[intRow][10] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[3]["TimeIN"]);
                            DataCells.Rows[intRow][12] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[4]["TimeIN"]);
                            DataCells.Rows[intRow][13] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[5]["TimeIN"]);
                            DataCells.Rows[intRow][15] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[6]["TimeIN"]);
                            DataCells.Rows[intRow][16] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[7]["TimeIN"]);

                        }

                    }



                   


                    string in1 = "";
                    string in2 = "";
                    string in3 = "";
                    string in4 = "";
                    string out1 = "";
                    string out2 = "";
                    string out3 = "";
                    string out4 = "";

                    if (DataCells.Rows.Count > 0)
                    {

                        in1 = DataCells.Rows[intRow]["TimeIN1"].ToString();
                        in2 = DataCells.Rows[intRow]["TimeIN2"].ToString();
                        in3 = DataCells.Rows[intRow]["TimeIN3"].ToString();
                        in4 = DataCells.Rows[intRow]["TimeIN4"].ToString();


                        out1 = DataCells.Rows[intRow]["TimeOUT1"].ToString();
                        out2 = DataCells.Rows[intRow]["TimeOUT2"].ToString();
                        out3 = DataCells.Rows[intRow]["TimeOUT3"].ToString();
                        out4 = DataCells.Rows[intRow]["TimeOUT4"].ToString();

                        if (in1 != "" && out1 != "")
                        {

                            String Emp_Total_Work_Time_1 = "";
                            DateTime date3 = Convert.ToDateTime(in1);
                            DateTime date4 = Convert.ToDateTime(out1);
                            TimeSpan ts1;
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();
                            Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;

                            string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "";
                            }
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "";
                            }
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                TimeSpan ts4 = new TimeSpan();
                                date4 = System.Convert.ToDateTime(out1).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {

                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {

                                    Emp_Total_Work_Time_1 = "";
                                }

                            }
                            else
                            {
                                //ts4 = ts4.Add(ts1);
                                Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                    //  time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                }
                            }
                            DataCells.Rows[intRow]["Total1"] = Emp_Total_Work_Time_1;
                        }
                        else if (in1 == "" || out1 == "")
                        {
                            DataCells.Rows[intRow]["Total1"] = "";
                        }

                        //Second IN OUT

                        if (in2 != "" && out2 != "")
                        {

                            String Emp_Total_Work_Time_1 = "";
                            DateTime date3 = Convert.ToDateTime(in2);
                            DateTime date4 = Convert.ToDateTime(out2);
                            TimeSpan ts1;
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();
                            Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;

                            string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "";
                            }
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "";
                            }
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                TimeSpan ts4 = new TimeSpan();
                                date4 = System.Convert.ToDateTime(out2).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {

                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {

                                    Emp_Total_Work_Time_1 = "";
                                }

                            }
                            else
                            {
                                //ts4 = ts4.Add(ts1);
                                Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                    //  time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                }
                            }
                            DataCells.Rows[intRow]["Total2"] = Emp_Total_Work_Time_1;
                        }
                        else if (in2 == "" || out2 == "")
                        {
                            DataCells.Rows[intRow]["Total2"] = "";
                        }

                        //Third IN OUT

                        if (in3 != "" && out3 != "")
                        {

                            String Emp_Total_Work_Time_1 = "";
                            DateTime date3 = Convert.ToDateTime(in3);
                            DateTime date4 = Convert.ToDateTime(out3);
                            TimeSpan ts1;
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();
                            Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;

                            string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "";
                            }
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "";
                            }
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                TimeSpan ts4 = new TimeSpan();
                                date4 = System.Convert.ToDateTime(out3).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {

                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {

                                    Emp_Total_Work_Time_1 = "";
                                }

                            }
                            else
                            {
                                // ts4 = ts4.Add(ts1);
                                Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                    //  time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                }
                            }
                            DataCells.Rows[intRow]["Total3"] = Emp_Total_Work_Time_1;
                        }
                        else if (in3 == "" || out3 == "")
                        {
                            DataCells.Rows[intRow]["Total3"] = "";
                        }




                        //Fourth IN OUT
                        if (in4 != "" && out4 != "")
                        {

                            String Emp_Total_Work_Time_1 = "";
                            DateTime date3 = Convert.ToDateTime(in4);
                            DateTime date4 = Convert.ToDateTime(out4);
                            TimeSpan ts1;
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();
                            Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;

                            string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "";
                            }
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "";
                            }
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                TimeSpan ts4 = new TimeSpan();
                                date4 = System.Convert.ToDateTime(out4).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {

                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {

                                    Emp_Total_Work_Time_1 = "";
                                }

                            }
                            else
                            {
                                //ts4 = ts4.Add(ts1);
                                Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                    //  time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                }
                            }
                            DataCells.Rows[intRow]["Total4"] = Emp_Total_Work_Time_1;
                        }
                        else if (in4 == "" || out4 == "")
                        {
                            DataCells.Rows[intRow]["Total4"] = "";
                        }

                    }
                    //Grand Total CalCulation

                    //Emp_Total_Work_Time_1 = "00:00";

                    TimeSpan t6;
                    TimeSpan t7;
                    TimeSpan t8;
                    TimeSpan t9;
                    TimeSpan t5;
                    string Time_IN_Str1 = "";
                    string Time_IN_Str2 = "";
                    string Time_IN_Str3 = "";
                    string Time_IN_Str4 = "";
                    string displayTime = "";
                    string displayTime1 = "0";
                    DateTime date6;

                    Time_IN_Str1 = DataCells.Rows[intRow]["Total1"].ToString();

                    Time_IN_Str2 = DataCells.Rows[intRow]["Total2"].ToString();

                    Time_IN_Str3 = DataCells.Rows[intRow]["Total3"].ToString();

                    Time_IN_Str4 = DataCells.Rows[intRow]["Total4"].ToString();


                    if (Time_IN_Str1 == "-" | Time_IN_Str2 == "-")
                    {
                        time_Check_dbl = 0;
                    }
                    else if (Time_IN_Str1 == "" | Time_IN_Str2 == "" | Time_IN_Str3 == "" | Time_IN_Str4 == "")
                    {
                        time_Check_dbl = 0;

                        if (Time_IN_Str1 == "")
                        {
                            DataCells.Rows[intRow][8] = "";
                        }
                        else
                        {
                            t5 = TimeSpan.Parse(Time_IN_Str1);
                            //t6 = TimeSpan.Parse(Time_IN_Str2);
                            // t7 = t5.Add(t6);

                            displayTime = t5.ToString();
                            date6 = Convert.ToDateTime(displayTime);
                            displayTime1 = date6.ToString("HH:mm ");
                        }
                        if (Time_IN_Str2 == "")
                        {
                            // DataCells.Rows[intRow][10] = "0";
                        }
                        else
                        {
                            int Times_Days = 1;
                            t5 = TimeSpan.Parse(Time_IN_Str1);
                            t6 = TimeSpan.Parse(Time_IN_Str2);
                            t7 = t5.Add(t6);
                            if (Times_Days <= t7.Days)
                            {
                                displayTime1 = "00:00";
                            }
                            else
                            {

                                displayTime = t7.ToString();
                                date6 = Convert.ToDateTime(displayTime);
                                displayTime1 = date6.ToString("HH:mm ");
                            }


                        }

                        if (Time_IN_Str3 == "")
                        {
                            // DataCells.Rows[intRow][13] = "0";
                        }
                        else
                        {
                            int Times_Days = 1;

                            t5 = TimeSpan.Parse(Time_IN_Str1);
                            t6 = TimeSpan.Parse(Time_IN_Str2);
                            t8 = TimeSpan.Parse(Time_IN_Str3);
                            t7 = t5.Add(t6);
                            t7 = t7.Add(t8);
                            if (Times_Days <= t7.Days)
                            {
                                displayTime1 = "00:00";
                            }
                            else
                            {
                                displayTime = t7.ToString();
                                date6 = Convert.ToDateTime(displayTime);
                                displayTime1 = date6.ToString("HH:mm ");
                            }
                        }

                    }
                    else
                    {
                        int Times_Days = 1;
                        t5 = TimeSpan.Parse(Time_IN_Str1);
                        t6 = TimeSpan.Parse(Time_IN_Str2);
                        t8 = TimeSpan.Parse(Time_IN_Str3);
                        t9 = TimeSpan.Parse(Time_IN_Str4);
                        t7 = t5.Add(t6);
                        t7 = t7.Add(t8);
                        t7 = t7.Add(t9);
                        if (Times_Days <= t7.Days)
                        {
                            displayTime1 = "00:00";
                        }
                        else
                        {

                            mm = t7.Hours.ToString();
                            displayTime = t7.ToString();
                            date6 = Convert.ToDateTime(displayTime);
                            displayTime1 = date6.ToString("HH:mm ");
                        }

                    }
                    if (displayTime1 == "" || displayTime1 == "")
                    {

                    }
                    else
                    {
                        DataCells.Rows[intRow]["Final Total"] = displayTime1;
                    }


                    colIndex += shiftCount;
                    intK += 1;
                    intI += 1;
                }
            }


            catch (Exception ex)
            {
            }


        }






        public void Fill_Multi_timeIN(string FromDate, string Dept)
        {
            try
            {


                //DateTime fromdate = Convert.ToDateTime(Date1);
                //DateTime todate = Convert.ToDateTime(Date2);
                //int dayCount = (int)((todate - fromdate).TotalDays);
                //if (dayCount > 0)
                //{
                //AutoDataTable.Columns.Add("DAY ATTENDANCE SUMMARY");
                //AutoDataTable.NewRow();
                //AutoDataTable.Rows.Add();


                //AutoDataTable.NewRow();
                //AutoDataTable.Rows.Add("DAY ATTENDANCE SUMMARY");
                //AutoDataTable.NewRow();
                //AutoDataTable.Rows.Add();
                //AutoDataTable.Columns.Add("SNo");
                AutoDataTable.Columns.Add("DeptName");
                AutoDataTable.Columns.Add("Designation");
                AutoDataTable.Columns.Add("MachineID");
                AutoDataTable.Columns.Add("MachineID_Encrypt");
                AutoDataTable.Columns.Add("EmpNo");
                AutoDataTable.Columns.Add("ExistingCode");
                AutoDataTable.Columns.Add("FirstName");
                AutoDataTable.Columns.Add("TimeIN1");
                AutoDataTable.Columns.Add("TimeIN2");
                AutoDataTable.Columns.Add("TimeIN3");
                AutoDataTable.Columns.Add("TimeIN4");
                AutoDataTable.Columns.Add("TimeOUT1");
                AutoDataTable.Columns.Add("TimeOUT2");
                AutoDataTable.Columns.Add("TimeOUT3");
                AutoDataTable.Columns.Add("TimeOUT4");


                if (SessionUserName == "IF")
                {

                    SSQL = "";
                    SSQL = "select isnull(DEP.DeptName,'') as [DeptName],Cast(EM.MachineID As int) As MachineID,EM.MachineID_Encrypt";
                    SSQL = SSQL + ",EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
                    SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName],isnull(DM.DesignName,'')as[Designation]";
                    SSQL = SSQL + " from Employee_Mst EM inner join Designation_Mst DM on EM.Designation = DM.DesignNo ";
                    SSQL = SSQL + " inner join Department_Mst DEP on  EM.DeptName= DEP.DeptCode  Where EM.Compcode='" + SessionCcode + "'";

                    SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes' and (EM.Wages='1' or EM.Wages='2')";
                    if (Dept != "0")
                    {
                        SSQL = SSQL + " And EM.DeptName='" + Dept + "'";
                    }
                    SSQL = SSQL + " Order By DeptName, MachineID";
                }
                else
                {
                    SSQL = "";
                    SSQL = "select isnull(DEP.DeptName,'') as [DeptName],Cast(EM.MachineID As int) As MachineID,EM.MachineID_Encrypt";
                    SSQL = SSQL + ",EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
                    SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName],isnull(DM.DesignName,'')as[Designation]";
                    SSQL = SSQL + " from Employee_Mst EM inner join Designation_Mst DM on EM.Designation = DM.DesignNo ";
                    SSQL = SSQL + " inner join Department_Mst DEP on  EM.DeptName= DEP.DeptCode  Where EM.Compcode='" + SessionCcode + "'";

                    SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";
                    if (Dept != "0")
                    {
                        SSQL = SSQL + " And EM.DeptName='" + Dept + "'";
                    }
                    SSQL = SSQL + " Order By DeptName, MachineID";
                }
                dsEmployee = MasterDate.GetDetails(SSQL);

                if (dsEmployee.Rows.Count <= 0)
                {
                    return;
                }
                else
                {
                    Int32 DSVAL = 0;
                    for (int i = 0; i < dsEmployee.Rows.Count; i++)
                    {
                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();

                        AutoDataTable.Rows[DSVAL][0] = dsEmployee.Rows[i]["DeptName"].ToString();
                        AutoDataTable.Rows[DSVAL][1] = dsEmployee.Rows[i]["Designation"].ToString();
                        AutoDataTable.Rows[DSVAL][2] = dsEmployee.Rows[i]["MachineID"].ToString();
                        AutoDataTable.Rows[DSVAL][3] = dsEmployee.Rows[i]["MachineID_Encrypt"].ToString();
                        AutoDataTable.Rows[DSVAL][4] = dsEmployee.Rows[i]["EmpNo"].ToString();
                        AutoDataTable.Rows[DSVAL][5] = dsEmployee.Rows[i]["ExistingCode"].ToString();
                        AutoDataTable.Rows[DSVAL][6] = dsEmployee.Rows[i]["FirstName"].ToString();

                        DSVAL += 1;
                    }
                }
                //}
            }
            catch (Exception e)
            {
            }
        }



        public void Write_MultiIN(string FromDate)
        {

            try
            {
                int intI = 1;
                int intK = 1;
                int intCol = 0;


                DataCells.Columns.Add("SNo");
                DataCells.Columns.Add("DeptName");
                DataCells.Columns.Add("Designation");
                DataCells.Columns.Add("EmpNo");
                DataCells.Columns.Add("ExistingCode");
                DataCells.Columns.Add("FirstName");
                DataCells.Columns.Add("TimeIN1");
                DataCells.Columns.Add("TimeOUT1");
                DataCells.Columns.Add("Total1");
                DataCells.Columns.Add("TimeIN2");
                DataCells.Columns.Add("TimeOUT2");
                DataCells.Columns.Add("Total2");
                DataCells.Columns.Add("TimeIN3");
                DataCells.Columns.Add("TimeOUT3");
                DataCells.Columns.Add("Total3");
                DataCells.Columns.Add("TimeIN4");
                DataCells.Columns.Add("TimeOUT4");
                DataCells.Columns.Add("Total4");
                DataCells.Columns.Add("Final Total");

                for (intCol = 0; intCol < AutoDataTable.Rows.Count; intCol++)
                {
                    DataCells.NewRow();
                    DataCells.Rows.Add();

                    DataCells.Rows[intCol]["SNo"] = intCol + 1;
                    DataCells.Rows[intCol]["DeptName"] = AutoDataTable.Rows[intCol]["DeptName"];
                    DataCells.Rows[intCol]["Designation"] = AutoDataTable.Rows[intCol]["Designation"];
                    DataCells.Rows[intCol]["EmpNo"] = AutoDataTable.Rows[intCol]["EmpNo"];
                    DataCells.Rows[intCol]["ExistingCode"] = AutoDataTable.Rows[intCol]["ExistingCode"];
                    DataCells.Rows[intCol]["FirstName"] = AutoDataTable.Rows[intCol]["FirstName"];
                }

                intCol = 5;


                fromdate = Convert.ToDateTime(FromDate);


                intK = 1;
                intI = 1;
                if (SessionUserName == "IF")
                {
                    DataTable dt1 = new DataTable();
                    for (int intRow = 0; intRow < AutoDataTable.Rows.Count; intRow++)
                    {
                        string Machine_ID_Str = "";
                        string MID = AutoDataTable.Rows[intRow][2].ToString();
                        Machine_ID_Str = AutoDataTable.Rows[intRow][3].ToString();
                        Date_Value_Str = string.Format(FromDate, "dd-MM-yyyy");


                        SSQL = "Select TimeIN,TimeOUT,Total_Hrs1 from IFLogTime_Days  where MachineID='" + MID + "' And Compcode='" + SessionCcode.ToString() + "' ";
                        SSQL = SSQL + "  And LocCode='" + SessionLcode.ToString() + "' and Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "'";
                        dt1 = ManualAttend.GetDetails(SSQL);

                        if (dt1.Rows.Count != 0)
                        {
                            string intime = "";
                            string OutTime = "";
                            intime = dt1.Rows[0]["TimeIN"].ToString();
                            OutTime = dt1.Rows[0]["TimeOUT"].ToString();

                            DataCells.Rows[intRow]["TimeIN1"] = dt1.Rows[0]["TimeIN"].ToString();
                            DataCells.Rows[intRow]["TimeOUT1"] = dt1.Rows[0]["TimeOUT"].ToString();
                            if (intime == "" || OutTime == "")
                            {
                                DataCells.Rows[intRow]["Total1"] = "";


                                DataCells.Rows[intRow]["Final Total"] = "";
                            }
                            else
                            {
                                DataCells.Rows[intRow]["Total1"] = dt1.Rows[0]["Total_Hrs1"].ToString();


                                DataCells.Rows[intRow]["Final Total"] = dt1.Rows[0]["Total_Hrs1"].ToString();
                            }
                        }
                    }
                }
                else
                {
                    for (int intRow = 0; intRow < AutoDataTable.Rows.Count; intRow++)
                    {
                        intK = 1;
                        int colIndex = intK;
                        bool isPresent = false;
                        isPresent = false;
                        string Machine_ID_Str = "";
                        string OT_Week_OFF_Machine_No = "";
                        string Date_Value_Str = "";
                        string Date_Value_Str1 = "";

                        string Time_IN_Str = "";
                        string Time_Out_Str = "";
                        string Total_Time_get = "";
                        Int32 j = 0;
                        double time_Check_dbl = 0;
                        isPresent = false;

                        string MID = AutoDataTable.Rows[intRow][2].ToString();
                        Machine_ID_Str = AutoDataTable.Rows[intRow][3].ToString();
                        Date_Value_Str = string.Format(FromDate, "dd-MM-yyyy");

                        DateTime Dateval = Convert.ToDateTime(Date_Value_Str).AddDays(1);
                        if (MID == "509")
                        {

                            MID = "509";
                        }


                        if (Machine_ID_Str == "MjAwNQ==")
                        {
                            Machine_ID_Str = "MjAwNQ==";
                        }

                        Date_Value_Str1 = string.Format(Convert.ToString(Dateval), "dd-MM-yyyy");

                        //Shift Check shift1
                        string Final_Shift = "";
                        DataTable Shift_DS = new DataTable();
                        Int32 k = 0;
                        bool Shift_Check_blb = false;
                        string Shift_Start_Time = null;
                        string Shift_End_Time = null;
                        string Employee_Time = "";
                        DateTime ShiftdateStartIN = default(DateTime);
                        DateTime ShiftdateEndIN = default(DateTime);
                        string Final_InTime = "";
                        string Final_OutTime = "";
                        string From_Time_Str = "";
                        string To_Time_Str = "";

                        DateTime EmpdateIN = default(DateTime);
                        DayOfWeek day = fromdate.DayOfWeek;
                        string WeekofDay = day.ToString();
                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeIN >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

                        mLocalDS = ManualAttend.GetDetails(SSQL);
                        //if (mLocalDS.Rows.Count >= 1)
                        //{
                        //    for (int ival = 0; ival < mLocalDS.Rows.Count; ival++)
                        //    {
                        //        if (ival <= 3)
                        //        {
                        //            int ival1 = 5 + ival;

                        //            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);
                        //        }

                        //    }
                        //}

                        //string From_Time_Str = "";
                        //string To_Time_Str = "";
                        //string Final_InTime = "";
                        //string Final_OutTime = "";
                        //string Final_Shift = "";
                        //DataTable Shift_DS = new DataTable();
                        //Int32 k = 0;
                        //bool Shift_Check_blb = false;
                        //string Shift_Start_Time = null;
                        //string Shift_End_Time = null;
                        //string Employee_Time = "";
                        //DateTime ShiftdateStartIN = default(DateTime);
                        //DateTime ShiftdateEndIN = default(DateTime);
                        //DateTime EmpdateIN = default(DateTime);

                        if (mLocalDS.Rows.Count != 0)
                        {
                            SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";

                            Shift_DS = ManualAttend.GetDetails(SSQL);
                            Shift_Check_blb = false;
                            Final_InTime = mLocalDS.Rows[0]["TimeIN"].ToString();
                            for (k = 0; k < Shift_DS.Rows.Count; k++)
                            {

                                string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                int b = Convert.ToInt16(a.ToString());
                                Shift_Start_Time = fromdate.AddDays(b).ToString("dd/MM/yyyy") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                int b1 = Convert.ToInt16(a1.ToString());
                                Shift_End_Time = fromdate.AddDays(b1).ToString("dd/MM/yyyy") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                                ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                                EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());

                                if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                                {
                                    Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                                    Shift_Check_blb = true;
                                    break;
                                }
                            }
                            if (Shift_Check_blb == false)
                            {
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + fromdate.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                            }
                            else
                            {
                                if (Final_Shift == "SHIFT1")
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:30' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
                                }
                                else
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                                }
                            }
                        }
                        else
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                        }

                        mLocalDS1 = ManualAttend.GetDetails(SSQL);
                        if (mLocalDS.Rows.Count >= 1)
                        {

                            for (int ival = 0; ival < mLocalDS.Rows.Count; ival++)
                            {
                                int ival1 = 6 + ival;
                                if (ival == 0)
                                {

                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                                }
                                else if (ival == 1)
                                {
                                    ival1 = 6 + ival + 2;
                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                                }
                                else if (ival == 2)
                                {
                                    ival1 = 6 + ival + 4;
                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                                }
                                else if (ival == 3)
                                {
                                    ival1 = 6 + ival + 6;
                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                                }
                            }
                        }



                        if (mLocalDS1.Rows.Count >= 1)
                        {
                            for (int ival = 0; ival < mLocalDS1.Rows.Count; ival++)
                            {
                                int ival1 = 6 + ival;

                                if (ival == 0)
                                {
                                    ival1 = ival1 + 1;
                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);


                                }
                                else if (ival == 1)
                                {
                                    ival1 = 6 + ival + 3;
                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);


                                }
                                else if (ival == 2)
                                {
                                    ival1 = 6 + ival + 5;
                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);


                                }
                                else if (ival == 3)
                                {
                                    ival1 = 6 + ival + 7;
                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);


                                }
                            }
                        }


                        string in1 = "";
                        string in2 = "";
                        string in3 = "";
                        string in4 = "";
                        string out1 = "";
                        string out2 = "";
                        string out3 = "";
                        string out4 = "";

                        if (DataCells.Rows.Count > 0)
                        {

                            in1 = DataCells.Rows[intRow]["TimeIN1"].ToString();
                            in2 = DataCells.Rows[intRow]["TimeIN2"].ToString();
                            in3 = DataCells.Rows[intRow]["TimeIN3"].ToString();
                            in4 = DataCells.Rows[intRow]["TimeIN4"].ToString();


                            out1 = DataCells.Rows[intRow]["TimeOUT1"].ToString();
                            out2 = DataCells.Rows[intRow]["TimeOUT2"].ToString();
                            out3 = DataCells.Rows[intRow]["TimeOUT3"].ToString();
                            out4 = DataCells.Rows[intRow]["TimeOUT4"].ToString();

                            if (in1 != "" && out1 != "")
                            {

                                String Emp_Total_Work_Time_1 = "";
                                DateTime date3 = Convert.ToDateTime(in1);
                                DateTime date4 = Convert.ToDateTime(out1);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    TimeSpan ts4 = new TimeSpan();
                                    date4 = System.Convert.ToDateTime(out1).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }

                                }
                                else
                                {
                                    //ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                        //  time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                                DataCells.Rows[intRow]["Total1"] = Emp_Total_Work_Time_1;
                            }
                            else if (in1 == "" || out1 == "")
                            {
                                DataCells.Rows[intRow]["Total1"] = "";
                            }

                            //Second IN OUT

                            if (in2 != "" && out2 != "")
                            {

                                String Emp_Total_Work_Time_1 = "";
                                DateTime date3 = Convert.ToDateTime(in2);
                                DateTime date4 = Convert.ToDateTime(out2);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    TimeSpan ts4 = new TimeSpan();
                                    date4 = System.Convert.ToDateTime(out2).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }

                                }
                                else
                                {
                                    //ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                        //  time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                                DataCells.Rows[intRow]["Total2"] = Emp_Total_Work_Time_1;
                            }
                            else if (in2 == "" || out2 == "")
                            {
                                DataCells.Rows[intRow]["Total2"] = "";
                            }

                            //Third IN OUT

                            if (in3 != "" && out3 != "")
                            {

                                String Emp_Total_Work_Time_1 = "";
                                DateTime date3 = Convert.ToDateTime(in3);
                                DateTime date4 = Convert.ToDateTime(out3);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    TimeSpan ts4 = new TimeSpan();
                                    date4 = System.Convert.ToDateTime(out3).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }

                                }
                                else
                                {
                                    // ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                        //  time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                                DataCells.Rows[intRow]["Total3"] = Emp_Total_Work_Time_1;
                            }
                            else if (in3 == "" || out3 == "")
                            {
                                DataCells.Rows[intRow]["Total3"] = "";
                            }




                            //Fourth IN OUT
                            if (in4 != "" && out4 != "")
                            {

                                String Emp_Total_Work_Time_1 = "";
                                DateTime date3 = Convert.ToDateTime(in4);
                                DateTime date4 = Convert.ToDateTime(out4);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    TimeSpan ts4 = new TimeSpan();
                                    date4 = System.Convert.ToDateTime(out4).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }

                                }
                                else
                                {
                                    //ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                        //  time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                                DataCells.Rows[intRow]["Total4"] = Emp_Total_Work_Time_1;
                            }
                            else if (in4 == "" || out4 == "")
                            {
                                DataCells.Rows[intRow]["Total4"] = "";
                            }

                        }
                        //Grand Total CalCulation

                        //Emp_Total_Work_Time_1 = "00:00";

                        TimeSpan t6;
                        TimeSpan t7;
                        TimeSpan t8;
                        TimeSpan t9;
                        TimeSpan t5;
                        string Time_IN_Str1 = "";
                        string Time_IN_Str2 = "";
                        string Time_IN_Str3 = "";
                        string Time_IN_Str4 = "";
                        string displayTime = "";
                        string displayTime1 = "0";
                        DateTime date6;

                        Time_IN_Str1 = DataCells.Rows[intRow]["Total1"].ToString();

                        Time_IN_Str2 = DataCells.Rows[intRow]["Total2"].ToString();

                        Time_IN_Str3 = DataCells.Rows[intRow]["Total3"].ToString();

                        Time_IN_Str4 = DataCells.Rows[intRow]["Total4"].ToString();


                        if (Time_IN_Str1 == "-" | Time_IN_Str2 == "-")
                        {
                            time_Check_dbl = 0;
                        }
                        else if (Time_IN_Str1 == "" | Time_IN_Str2 == "" | Time_IN_Str3 == "" | Time_IN_Str4 == "")
                        {
                            time_Check_dbl = 0;

                            if (Time_IN_Str1 == "")
                            {
                                DataCells.Rows[intRow][8] = "";
                            }
                            else
                            {
                                t5 = TimeSpan.Parse(Time_IN_Str1);
                                //t6 = TimeSpan.Parse(Time_IN_Str2);
                                // t7 = t5.Add(t6);

                                displayTime = t5.ToString();
                                date6 = Convert.ToDateTime(displayTime);
                                displayTime1 = date6.ToString("HH:mm ");
                            }
                            if (Time_IN_Str2 == "")
                            {
                                // DataCells.Rows[intRow][10] = "0";
                            }
                            else
                            {
                                int Times_Days = 1;
                                t5 = TimeSpan.Parse(Time_IN_Str1);
                                t6 = TimeSpan.Parse(Time_IN_Str2);
                                t7 = t5.Add(t6);
                                if (Times_Days <= t7.Days)
                                {
                                    displayTime1 = "00:00";
                                }
                                else
                                {

                                    displayTime = t7.ToString();
                                    date6 = Convert.ToDateTime(displayTime);
                                    displayTime1 = date6.ToString("HH:mm ");
                                }


                            }

                            if (Time_IN_Str3 == "")
                            {
                                // DataCells.Rows[intRow][13] = "0";
                            }
                            else
                            {
                                int Times_Days = 1;

                                t5 = TimeSpan.Parse(Time_IN_Str1);
                                t6 = TimeSpan.Parse(Time_IN_Str2);
                                t8 = TimeSpan.Parse(Time_IN_Str3);
                                t7 = t5.Add(t6);
                                t7 = t7.Add(t8);
                                if (Times_Days <= t7.Days)
                                {
                                    displayTime1 = "00:00";
                                }
                                else
                                {
                                    displayTime = t7.ToString();
                                    date6 = Convert.ToDateTime(displayTime);
                                    displayTime1 = date6.ToString("HH:mm ");
                                }
                            }

                        }
                        else
                        {
                            int Times_Days = 1;
                            t5 = TimeSpan.Parse(Time_IN_Str1);
                            t6 = TimeSpan.Parse(Time_IN_Str2);
                            t8 = TimeSpan.Parse(Time_IN_Str3);
                            t9 = TimeSpan.Parse(Time_IN_Str4);
                            t7 = t5.Add(t6);
                            t7 = t7.Add(t8);
                            t7 = t7.Add(t9);
                            if (Times_Days <= t7.Days)
                            {
                                displayTime1 = "00:00";
                            }
                            else
                            {

                                mm = t7.Hours.ToString();
                                displayTime = t7.ToString();
                                date6 = Convert.ToDateTime(displayTime);
                                displayTime1 = date6.ToString("HH:mm ");
                            }

                        }
                        if (displayTime1 == "" || displayTime1 == "")
                        {

                        }
                        else
                        {
                            DataCells.Rows[intRow]["Final Total"] = displayTime1;
                        }


                        colIndex += shiftCount;
                        intK += 1;
                        intI += 1;
                    }
                }
            }


            catch (Exception ex)
            {
            }

        }




        public void NonAdminGetFill_Multi_timeIN(string FromDate)
        {
            try
            {


                //DateTime fromdate = Convert.ToDateTime(Date1);
                //DateTime todate = Convert.ToDateTime(Date2);
                //int dayCount = (int)((todate - fromdate).TotalDays);
                //if (dayCount > 0)
                //{
                //AutoDataTable.Columns.Add("DAY ATTENDANCE SUMMARY");
                //AutoDataTable.NewRow();
                //AutoDataTable.Rows.Add();


                //AutoDataTable.NewRow();
                //AutoDataTable.Rows.Add("DAY ATTENDANCE SUMMARY");
                //AutoDataTable.NewRow();
                //AutoDataTable.Rows.Add();
                //AutoDataTable.Columns.Add("SNo");
                AutoDataTable.Columns.Add("DeptName");
                AutoDataTable.Columns.Add("Designation");
                AutoDataTable.Columns.Add("MachineID");
                AutoDataTable.Columns.Add("MachineID_Encrypt");
                AutoDataTable.Columns.Add("EmpNo");
                AutoDataTable.Columns.Add("ExistingCode");
                AutoDataTable.Columns.Add("FirstName");
                AutoDataTable.Columns.Add("TimeIN1");
                AutoDataTable.Columns.Add("TimeIN2");
                AutoDataTable.Columns.Add("TimeIN3");
                AutoDataTable.Columns.Add("TimeIN4");
                AutoDataTable.Columns.Add("TimeOUT1");
                AutoDataTable.Columns.Add("TimeOUT2");
                AutoDataTable.Columns.Add("TimeOUT3");
                AutoDataTable.Columns.Add("TimeOUT4");



                if (SessionUserName == "IF")
                {
                    SSQL = "";
                    SSQL = "select isnull(DEP.DeptName,'') as [DeptName],Cast(EM.MachineID As int) As MachineID,EM.MachineID_Encrypt";
                    SSQL = SSQL + ",EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
                    SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName],isnull(DM.DesignName,'')as[Designation]";
                    SSQL = SSQL + " from Employee_Mst EM inner join Designation_Mst DM on EM.Designation = DM.DesignNo ";
                    SSQL = SSQL + " inner join Department_Mst DEP on  EM.DeptName= DEP.DeptCode  Where EM.Compcode='" + SessionCcode + "' ";

                    SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes' and (EM.Wages='1' or EM.Wages='2')";
                    //if (SessionUserType == "2")
                    //{
                    //    SSQL = SSQL + " And IsNonAdmin='1'";
                    //}
                    SSQL = SSQL + " Order By DeptName, MachineID";
                }

                else
                {
                    SSQL = "";
                    SSQL = "select isnull(DEP.DeptName,'') as [DeptName],Cast(EM.MachineID As int) As MachineID,EM.MachineID_Encrypt";
                    SSQL = SSQL + ",EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
                    SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName],isnull(DM.DesignName,'')as[Designation]";
                    SSQL = SSQL + " from Employee_Mst EM inner join Designation_Mst DM on EM.Designation = DM.DesignNo ";
                    SSQL = SSQL + " inner join Department_Mst DEP on  EM.DeptName= DEP.DeptCode  Where EM.Compcode='" + SessionCcode + "'";

                    SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";
                    //if (SessionUserType == "2")
                    //{
                    //    SSQL = SSQL + " And IsNonAdmin='1'";
                    //}
                    SSQL = SSQL + " Order By DeptName, MachineID";
                }
                dsEmployee = MasterDate.GetDetails(SSQL);

                if (dsEmployee.Rows.Count <= 0)
                {
                    return;
                }
                else
                {
                    Int32 DSVAL = 0;
                    for (int i = 0; i < dsEmployee.Rows.Count; i++)
                    {
                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();

                        AutoDataTable.Rows[DSVAL][0] = dsEmployee.Rows[i]["DeptName"].ToString();
                        AutoDataTable.Rows[DSVAL][1] = dsEmployee.Rows[i]["Designation"].ToString();
                        AutoDataTable.Rows[DSVAL][2] = dsEmployee.Rows[i]["MachineID"].ToString();
                        AutoDataTable.Rows[DSVAL][3] = dsEmployee.Rows[i]["MachineID_Encrypt"].ToString();
                        AutoDataTable.Rows[DSVAL][4] = dsEmployee.Rows[i]["EmpNo"].ToString();
                        AutoDataTable.Rows[DSVAL][5] = dsEmployee.Rows[i]["ExistingCode"].ToString();
                        AutoDataTable.Rows[DSVAL][6] = dsEmployee.Rows[i]["FirstName"].ToString();

                        DSVAL += 1;
                    }
                }
                //}
            }
            catch (Exception e)
            {
            }
        }


        public void NonAdminGetWrite_MultiIN(string FromDate)
        {


            try
            {
                int intI = 1;
                int intK = 1;
                int intCol = 0;


                DataCells.Columns.Add("SNo");
                DataCells.Columns.Add("DeptName");
                DataCells.Columns.Add("Designation");
                DataCells.Columns.Add("EmpNo");
                DataCells.Columns.Add("ExistingCode");
                DataCells.Columns.Add("FirstName");
                DataCells.Columns.Add("TimeIN1");
                DataCells.Columns.Add("TimeOUT1");
                DataCells.Columns.Add("Total1");
                DataCells.Columns.Add("TimeIN2");
                DataCells.Columns.Add("TimeOUT2");
                DataCells.Columns.Add("Total2");
                DataCells.Columns.Add("TimeIN3");
                DataCells.Columns.Add("TimeOUT3");
                DataCells.Columns.Add("Total3");
                DataCells.Columns.Add("TimeIN4");
                DataCells.Columns.Add("TimeOUT4");
                DataCells.Columns.Add("Total4");
                DataCells.Columns.Add("Final Total");

                for (intCol = 0; intCol < AutoDataTable.Rows.Count; intCol++)
                {
                    DataCells.NewRow();
                    DataCells.Rows.Add();

                    DataCells.Rows[intCol]["SNo"] = intCol + 1;
                    DataCells.Rows[intCol]["DeptName"] = AutoDataTable.Rows[intCol]["DeptName"];
                    DataCells.Rows[intCol]["Designation"] = AutoDataTable.Rows[intCol]["Designation"];
                    DataCells.Rows[intCol]["EmpNo"] = AutoDataTable.Rows[intCol]["EmpNo"];
                    DataCells.Rows[intCol]["ExistingCode"] = AutoDataTable.Rows[intCol]["ExistingCode"];
                    DataCells.Rows[intCol]["FirstName"] = AutoDataTable.Rows[intCol]["FirstName"];
                }

                intCol = 5;


                fromdate = Convert.ToDateTime(FromDate);


                intK = 1;
                intI = 1;
                if (SessionUserName == "IF")
                {
                    DataTable dt1 = new DataTable();
                    for (int intRow = 0; intRow < AutoDataTable.Rows.Count; intRow++)
                    {
                        string Machine_ID_Str = "";
                        string MID = AutoDataTable.Rows[intRow][2].ToString();
                        Machine_ID_Str = AutoDataTable.Rows[intRow][3].ToString();
                        Date_Value_Str = string.Format(FromDate, "dd-MM-yyyy");


                        SSQL = "Select TimeIN,TimeOUT,Total_Hrs1 from IFLogTime_Days  where MachineID='" + MID + "' And Compcode='" + SessionCcode.ToString() + "' ";
                        SSQL = SSQL + "  And LocCode='" + SessionLcode.ToString() + "' and Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + "'";
                        dt1 = ManualAttend.GetDetails(SSQL);

                        if (dt1.Rows.Count != 0)
                        {
                            string intime = "";
                            string OutTime = "";
                            intime = dt1.Rows[0]["TimeIN"].ToString();
                            OutTime = dt1.Rows[0]["TimeOUT"].ToString();

                            DataCells.Rows[intRow]["TimeIN1"] = dt1.Rows[0]["TimeIN"].ToString();
                            DataCells.Rows[intRow]["TimeOUT1"] = dt1.Rows[0]["TimeOUT"].ToString();
                            if (intime == "" || OutTime == "")
                            {
                                DataCells.Rows[intRow]["Total1"] = "";


                                DataCells.Rows[intRow]["Final Total"] = "";
                            }
                            else
                            {
                                DataCells.Rows[intRow]["Total1"] = dt1.Rows[0]["Total_Hrs1"].ToString();


                                DataCells.Rows[intRow]["Final Total"] = dt1.Rows[0]["Total_Hrs1"].ToString();
                            }
                        }
                    }
                }
                else
                {
                    for (int intRow = 0; intRow < AutoDataTable.Rows.Count; intRow++)
                    {
                        intK = 1;
                        int colIndex = intK;
                        bool isPresent = false;
                        isPresent = false;
                        string Machine_ID_Str = "";
                        string OT_Week_OFF_Machine_No = "";
                        string Date_Value_Str = "";
                        string Date_Value_Str1 = "";

                        string Time_IN_Str = "";
                        string Time_Out_Str = "";
                        string Total_Time_get = "";
                        Int32 j = 0;
                        double time_Check_dbl = 0;
                        isPresent = false;

                        string MID = AutoDataTable.Rows[intRow][2].ToString();
                        Machine_ID_Str = AutoDataTable.Rows[intRow][3].ToString();
                        Date_Value_Str = string.Format(FromDate, "dd-MM-yyyy");

                        DateTime Dateval = Convert.ToDateTime(Date_Value_Str).AddDays(1);
                        if (MID == "509")
                        {

                            MID = "509";
                        }


                        if (Machine_ID_Str == "MjAwNQ==")
                        {
                            Machine_ID_Str = "MjAwNQ==";
                        }

                        Date_Value_Str1 = string.Format(Convert.ToString(Dateval), "dd-MM-yyyy");

                        //Shift Check shift1
                        string Final_Shift = "";
                        DataTable Shift_DS = new DataTable();
                        Int32 k = 0;
                        bool Shift_Check_blb = false;
                        string Shift_Start_Time = null;
                        string Shift_End_Time = null;
                        string Employee_Time = "";
                        DateTime ShiftdateStartIN = default(DateTime);
                        DateTime ShiftdateEndIN = default(DateTime);
                        string Final_InTime = "";
                        string Final_OutTime = "";
                        string From_Time_Str = "";
                        string To_Time_Str = "";

                        DateTime EmpdateIN = default(DateTime);
                        DayOfWeek day = fromdate.DayOfWeek;
                        string WeekofDay = day.ToString();
                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeIN >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

                        mLocalDS = ManualAttend.GetDetails(SSQL);
                        //if (mLocalDS.Rows.Count >= 1)
                        //{
                        //    for (int ival = 0; ival < mLocalDS.Rows.Count; ival++)
                        //    {
                        //        if (ival <= 3)
                        //        {
                        //            int ival1 = 5 + ival;

                        //            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);
                        //        }

                        //    }
                        //}

                        //string From_Time_Str = "";
                        //string To_Time_Str = "";
                        //string Final_InTime = "";
                        //string Final_OutTime = "";
                        //string Final_Shift = "";
                        //DataTable Shift_DS = new DataTable();
                        //Int32 k = 0;
                        //bool Shift_Check_blb = false;
                        //string Shift_Start_Time = null;
                        //string Shift_End_Time = null;
                        //string Employee_Time = "";
                        //DateTime ShiftdateStartIN = default(DateTime);
                        //DateTime ShiftdateEndIN = default(DateTime);
                        //DateTime EmpdateIN = default(DateTime);

                        if (mLocalDS.Rows.Count != 0)
                        {
                            SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";

                            Shift_DS = ManualAttend.GetDetails(SSQL);
                            Shift_Check_blb = false;
                            Final_InTime = mLocalDS.Rows[0]["TimeIN"].ToString();
                            for (k = 0; k < Shift_DS.Rows.Count; k++)
                            {

                                string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                int b = Convert.ToInt16(a.ToString());
                                Shift_Start_Time = fromdate.AddDays(b).ToString("dd/MM/yyyy") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                int b1 = Convert.ToInt16(a1.ToString());
                                Shift_End_Time = fromdate.AddDays(b1).ToString("dd/MM/yyyy") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                                ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                                EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());

                                if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                                {
                                    Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                                    Shift_Check_blb = true;
                                    break;
                                }
                            }
                            if (Shift_Check_blb == false)
                            {
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + fromdate.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                            }
                            else
                            {
                                if (Final_Shift == "SHIFT1")
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:30' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
                                }
                                else
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                                }
                            }
                        }
                        else
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                        }

                        mLocalDS1 = ManualAttend.GetDetails(SSQL);
                        if (mLocalDS.Rows.Count >= 1)
                        {

                            for (int ival = 0; ival < mLocalDS.Rows.Count; ival++)
                            {
                                int ival1 = 6 + ival;
                                if (ival == 0)
                                {

                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                                }
                                else if (ival == 1)
                                {
                                    ival1 = 6 + ival + 2;
                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                                }
                                else if (ival == 2)
                                {
                                    ival1 = 6 + ival + 4;
                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                                }
                                else if (ival == 3)
                                {
                                    ival1 = 6 + ival + 6;
                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                                }
                            }
                        }



                        if (mLocalDS1.Rows.Count >= 1)
                        {
                            for (int ival = 0; ival < mLocalDS1.Rows.Count; ival++)
                            {
                                int ival1 = 6 + ival;

                                if (ival == 0)
                                {
                                    ival1 = ival1 + 1;
                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);


                                }
                                else if (ival == 1)
                                {
                                    ival1 = 6 + ival + 3;
                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);


                                }
                                else if (ival == 2)
                                {
                                    ival1 = 6 + ival + 5;
                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);


                                }
                                else if (ival == 3)
                                {
                                    ival1 = 6 + ival + 7;
                                    DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);


                                }
                            }
                        }


                        string in1 = "";
                        string in2 = "";
                        string in3 = "";
                        string in4 = "";
                        string out1 = "";
                        string out2 = "";
                        string out3 = "";
                        string out4 = "";

                        if (DataCells.Rows.Count > 0)
                        {

                            in1 = DataCells.Rows[intRow]["TimeIN1"].ToString();
                            in2 = DataCells.Rows[intRow]["TimeIN2"].ToString();
                            in3 = DataCells.Rows[intRow]["TimeIN3"].ToString();
                            in4 = DataCells.Rows[intRow]["TimeIN4"].ToString();


                            out1 = DataCells.Rows[intRow]["TimeOUT1"].ToString();
                            out2 = DataCells.Rows[intRow]["TimeOUT2"].ToString();
                            out3 = DataCells.Rows[intRow]["TimeOUT3"].ToString();
                            out4 = DataCells.Rows[intRow]["TimeOUT4"].ToString();

                            if (in1 != "" && out1 != "")
                            {

                                String Emp_Total_Work_Time_1 = "";
                                DateTime date3 = Convert.ToDateTime(in1);
                                DateTime date4 = Convert.ToDateTime(out1);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    TimeSpan ts4 = new TimeSpan();
                                    date4 = System.Convert.ToDateTime(out1).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }

                                }
                                else
                                {
                                    //ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                        //  time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                                DataCells.Rows[intRow]["Total1"] = Emp_Total_Work_Time_1;
                            }
                            else if (in1 == "" || out1 == "")
                            {
                                DataCells.Rows[intRow]["Total1"] = "";
                            }

                            //Second IN OUT

                            if (in2 != "" && out2 != "")
                            {

                                String Emp_Total_Work_Time_1 = "";
                                DateTime date3 = Convert.ToDateTime(in2);
                                DateTime date4 = Convert.ToDateTime(out2);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    TimeSpan ts4 = new TimeSpan();
                                    date4 = System.Convert.ToDateTime(out2).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }

                                }
                                else
                                {
                                    //ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                        //  time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                                DataCells.Rows[intRow]["Total2"] = Emp_Total_Work_Time_1;
                            }
                            else if (in2 == "" || out2 == "")
                            {
                                DataCells.Rows[intRow]["Total2"] = "";
                            }

                            //Third IN OUT

                            if (in3 != "" && out3 != "")
                            {

                                String Emp_Total_Work_Time_1 = "";
                                DateTime date3 = Convert.ToDateTime(in3);
                                DateTime date4 = Convert.ToDateTime(out3);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    TimeSpan ts4 = new TimeSpan();
                                    date4 = System.Convert.ToDateTime(out3).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }

                                }
                                else
                                {
                                    // ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                        //  time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                                DataCells.Rows[intRow]["Total3"] = Emp_Total_Work_Time_1;
                            }
                            else if (in3 == "" || out3 == "")
                            {
                                DataCells.Rows[intRow]["Total3"] = "";
                            }




                            //Fourth IN OUT
                            if (in4 != "" && out4 != "")
                            {

                                String Emp_Total_Work_Time_1 = "";
                                DateTime date3 = Convert.ToDateTime(in4);
                                DateTime date4 = Convert.ToDateTime(out4);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    TimeSpan ts4 = new TimeSpan();
                                    date4 = System.Convert.ToDateTime(out4).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "";
                                    }

                                }
                                else
                                {
                                    //ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "";
                                        //  time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                                DataCells.Rows[intRow]["Total4"] = Emp_Total_Work_Time_1;
                            }
                            else if (in4 == "" || out4 == "")
                            {
                                DataCells.Rows[intRow]["Total4"] = "";
                            }

                        }
                        //Grand Total CalCulation

                        //Emp_Total_Work_Time_1 = "00:00";

                        TimeSpan t6;
                        TimeSpan t7;
                        TimeSpan t8;
                        TimeSpan t9;
                        TimeSpan t5;
                        string Time_IN_Str1 = "";
                        string Time_IN_Str2 = "";
                        string Time_IN_Str3 = "";
                        string Time_IN_Str4 = "";
                        string displayTime = "";
                        string displayTime1 = "0";
                        DateTime date6;

                        Time_IN_Str1 = DataCells.Rows[intRow]["Total1"].ToString();

                        Time_IN_Str2 = DataCells.Rows[intRow]["Total2"].ToString();

                        Time_IN_Str3 = DataCells.Rows[intRow]["Total3"].ToString();

                        Time_IN_Str4 = DataCells.Rows[intRow]["Total4"].ToString();


                        if (Time_IN_Str1 == "-" | Time_IN_Str2 == "-")
                        {
                            time_Check_dbl = 0;
                        }
                        else if (Time_IN_Str1 == "" | Time_IN_Str2 == "" | Time_IN_Str3 == "" | Time_IN_Str4 == "")
                        {
                            time_Check_dbl = 0;

                            if (Time_IN_Str1 == "")
                            {
                                DataCells.Rows[intRow][8] = "";
                            }
                            else
                            {
                                t5 = TimeSpan.Parse(Time_IN_Str1);
                                //t6 = TimeSpan.Parse(Time_IN_Str2);
                                // t7 = t5.Add(t6);

                                displayTime = t5.ToString();
                                date6 = Convert.ToDateTime(displayTime);
                                displayTime1 = date6.ToString("HH:mm ");
                            }
                            if (Time_IN_Str2 == "")
                            {
                                // DataCells.Rows[intRow][10] = "0";
                            }
                            else
                            {
                                int Times_Days = 1;
                                t5 = TimeSpan.Parse(Time_IN_Str1);
                                t6 = TimeSpan.Parse(Time_IN_Str2);
                                t7 = t5.Add(t6);
                                if (Times_Days <= t7.Days)
                                {
                                    displayTime1 = "00:00";
                                }
                                else
                                {

                                    displayTime = t7.ToString();
                                    date6 = Convert.ToDateTime(displayTime);
                                    displayTime1 = date6.ToString("HH:mm ");
                                }


                            }

                            if (Time_IN_Str3 == "")
                            {
                                // DataCells.Rows[intRow][13] = "0";
                            }
                            else
                            {
                                int Times_Days = 1;

                                t5 = TimeSpan.Parse(Time_IN_Str1);
                                t6 = TimeSpan.Parse(Time_IN_Str2);
                                t8 = TimeSpan.Parse(Time_IN_Str3);
                                t7 = t5.Add(t6);
                                t7 = t7.Add(t8);
                                if (Times_Days <= t7.Days)
                                {
                                    displayTime1 = "00:00";
                                }
                                else
                                {
                                    displayTime = t7.ToString();
                                    date6 = Convert.ToDateTime(displayTime);
                                    displayTime1 = date6.ToString("HH:mm ");
                                }
                            }

                        }
                        else
                        {
                            int Times_Days = 1;
                            t5 = TimeSpan.Parse(Time_IN_Str1);
                            t6 = TimeSpan.Parse(Time_IN_Str2);
                            t8 = TimeSpan.Parse(Time_IN_Str3);
                            t9 = TimeSpan.Parse(Time_IN_Str4);
                            t7 = t5.Add(t6);
                            t7 = t7.Add(t8);
                            t7 = t7.Add(t9);
                            if (Times_Days <= t7.Days)
                            {
                                displayTime1 = "00:00";
                            }
                            else
                            {

                                mm = t7.Hours.ToString();
                                displayTime = t7.ToString();
                                date6 = Convert.ToDateTime(displayTime);
                                displayTime1 = date6.ToString("HH:mm ");
                            }

                        }
                        if (displayTime1 == "" || displayTime1 == "")
                        {

                        }
                        else
                        {
                            DataCells.Rows[intRow]["Final Total"] = displayTime1;
                        }


                        colIndex += shiftCount;
                        intK += 1;
                        intI += 1;
                    }
                }
            }


            catch (Exception ex)
            {
            }


        }















        public void AbsentReportDays(string FromDate)
        {

            if (SessionUserID == "2")
            {
                NonAdminFill_AbsentDays(FromDate);
            }
            else
            {
                DataTable dt1 = new DataTable();
                DataTable dsEmployee = new DataTable();
                AutoDTable.Columns.Add("MachineID");
                AutoDTable.Columns.Add("EmpNo");
                AutoDTable.Columns.Add("ExistingCode");
                AutoDTable.Columns.Add("DeptName");
                AutoDTable.Columns.Add("FirstName");
                AutoDTable.Columns.Add("MachineID_Encrypt");

                DataCell.Columns.Add("MachineID");
                DataCell.Columns.Add("EmpNo");
                DataCell.Columns.Add("ExistingCode");
                DataCell.Columns.Add("DeptName");
                DataCell.Columns.Add("FirstName");
                DataCell.Columns.Add(FromDate);
                try
                {
                    SSQL = "";
                    SSQL = "select isnull(DeptName,'') as [DeptName], Cast(MachineID As int) As MachineID";
                    SSQL = SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
                    SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
                    SSQL = SSQL + ",isnull(MachineID_Encrypt,'') as [MachineID_Encrypt]";
                    SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'";
                    SSQL = SSQL + " Order By DeptName, MachineID";


                    //SSQL = "select MachineID,isnull(FirstName,'') as [FirstName],ExistingCode,DeptName,MachineID_Encrypt from LogTime_Days";
                    //SSQL = SSQL + " Where CompCode='" + Ccode + "' And LocCode='" + Lcode + "'";


                    //SSQL = SSQL + " Group by MachineID,[FirstName],ExistingCode,DeptName,MachineID_Encrypt order by MachineID";


                    dsEmployee = MasterDate.GetDetails(SSQL);
                    if (dsEmployee.Rows.Count <= 0)
                    {

                    }

                    else
                    {
                        int i1 = 0;
                        for (int j1 = 0; j1 < dsEmployee.Rows.Count; j1++)
                        {
                            AutoDTable.NewRow();
                            AutoDTable.Rows.Add();
                            string Machineid = dsEmployee.Rows[j1]["MachineID"].ToString();
                            AutoDTable.Rows[i1][0] = dsEmployee.Rows[j1]["MachineID"].ToString();
                            AutoDTable.Rows[i1][1] = dsEmployee.Rows[j1]["MachineID"].ToString();
                            AutoDTable.Rows[i1][2] = dsEmployee.Rows[j1]["ExistingCode"].ToString();
                            AutoDTable.Rows[i1][3] = dsEmployee.Rows[j1]["DeptName"].ToString();
                            AutoDTable.Rows[i1][4] = dsEmployee.Rows[j1]["FirstName"].ToString();
                            AutoDTable.Rows[i1][5] = dsEmployee.Rows[j1]["MachineID_Encrypt"].ToString();
                            i1++;

                            SSQL = "select Attn_Date_Str,Present,Shift,Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And MachineID='" + Machineid + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                            dt1 = MasterDate.GetDetails(SSQL);
                            string Absent_Value = "";
                            if (dt1.Rows.Count > 0)
                            {
                                Absent_Value = dt1.Rows[0]["Present"].ToString();
                            }

                            else
                            {
                                Absent_Value = "0.0";
                            }



                            DataCell.NewRow();
                            DataCell.Rows.Add();
                            DataCell.Rows[j1]["DeptName"] = AutoDTable.Rows[j1]["DeptName"].ToString();
                            DataCell.Rows[j1]["MachineID"] = AutoDTable.Rows[j1]["MachineID"].ToString();
                            DataCell.Rows[j1]["EmpNo"] = AutoDTable.Rows[j1]["EmpNo"].ToString();
                            DataCell.Rows[j1]["ExistingCode"] = AutoDTable.Rows[j1]["ExistingCode"].ToString();
                            DataCell.Rows[j1]["FirstName"] = AutoDTable.Rows[j1]["FirstName"].ToString();


                            if (Absent_Value == "0.0")
                            {
                                DataCell.Rows[j1][FromDate] = "<span style=color:Red><b>" + "A" + "</b></span>";
                            }
                            else
                            {

                            }
                        }
                    }

                }
                catch (Exception e)
                {

                }

                //Fill_AbsentDays(FromDate);

                //WriteAbsent(FromDate);


                grid.DataSource = DataCell;
                grid.DataBind();
                string attachment = "attachment;filename=ABSENT REPORT DAYWISE.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);

                Response.Write("<table>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">ABSENT REPORT DAYWISE &nbsp;&nbsp;&nbsp;</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\"> Date -" + FromDate + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }


        }


        public void NonAdminFill_AbsentDays(string FromDate)
        {
            DataTable dt1 = new DataTable();
            DataTable dsEmployee = new DataTable();
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("EmpNo");
            AutoDTable.Columns.Add("ExistingCode");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("FirstName");
            AutoDTable.Columns.Add("MachineID_Encrypt");

            DataCell.Columns.Add("MachineID");
            DataCell.Columns.Add("EmpNo");
            DataCell.Columns.Add("ExistingCode");
            DataCell.Columns.Add("DeptName");
            DataCell.Columns.Add("FirstName");
            DataCell.Columns.Add(FromDate);
            try
            {
                SSQL = "";
                SSQL = "select isnull(DeptName,'') as [DeptName], Cast(MachineID As int) As MachineID";
                SSQL = SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
                SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
                SSQL = SSQL + ",isnull(MachineID_Encrypt,'') as [MachineID_Encrypt]";
                SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'";
                SSQL = SSQL + " Order By DeptName, MachineID";


                //SSQL = "select MachineID,isnull(FirstName,'') as [FirstName],ExistingCode,DeptName,MachineID_Encrypt from LogTime_Days";
                //SSQL = SSQL + " Where CompCode='" + Ccode + "' And LocCode='" + Lcode + "'";


                //SSQL = SSQL + " Group by MachineID,[FirstName],ExistingCode,DeptName,MachineID_Encrypt order by MachineID";


                dsEmployee = MasterDate.GetDetails(SSQL);
                if (dsEmployee.Rows.Count <= 0)
                {

                }

                else
                {
                    int i1 = 0;
                    for (int j1 = 0; j1 < dsEmployee.Rows.Count; j1++)
                    {
                        AutoDTable.NewRow();
                        AutoDTable.Rows.Add();
                        string Machineid = dsEmployee.Rows[j1]["MachineID"].ToString();
                        AutoDTable.Rows[i1][0] = dsEmployee.Rows[j1]["MachineID"].ToString();
                        AutoDTable.Rows[i1][1] = dsEmployee.Rows[j1]["MachineID"].ToString();
                        AutoDTable.Rows[i1][2] = dsEmployee.Rows[j1]["ExistingCode"].ToString();
                        AutoDTable.Rows[i1][3] = dsEmployee.Rows[j1]["DeptName"].ToString();
                        AutoDTable.Rows[i1][4] = dsEmployee.Rows[j1]["FirstName"].ToString();
                        AutoDTable.Rows[i1][5] = dsEmployee.Rows[j1]["MachineID_Encrypt"].ToString();
                        i1++;

                        SSQL = "select Attn_Date_Str,Present,Shift,Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And MachineID='" + Machineid + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                        dt1 = MasterDate.GetDetails(SSQL);
                        string Absent_Value = "";
                        if (dt1.Rows.Count > 0)
                        {
                            Absent_Value = dt1.Rows[0]["Present"].ToString();
                        }

                        else
                        {
                            Absent_Value = "0.0";
                        }



                        DataCell.NewRow();
                        DataCell.Rows.Add();
                        DataCell.Rows[j1]["DeptName"] = AutoDTable.Rows[j1]["DeptName"].ToString();
                        DataCell.Rows[j1]["MachineID"] = AutoDTable.Rows[j1]["MachineID"].ToString();
                        DataCell.Rows[j1]["EmpNo"] = AutoDTable.Rows[j1]["EmpNo"].ToString();
                        DataCell.Rows[j1]["ExistingCode"] = AutoDTable.Rows[j1]["ExistingCode"].ToString();
                        DataCell.Rows[j1]["FirstName"] = AutoDTable.Rows[j1]["FirstName"].ToString();


                        if (Absent_Value == "0.0")
                        {
                            DataCell.Rows[j1][FromDate] = "<span style=color:Red><b>" + "A" + "</b></span>";
                        }
                        else
                        {

                        }
                    }
                }

            }
            catch (Exception e)
            {

            }

            //Fill_AbsentDays(FromDate);

            //WriteAbsent(FromDate);


            grid.DataSource = DataCell;
            grid.DataBind();
            string attachment = "attachment;filename=ABSENT REPORT DAYWISE.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write("<table>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">ABSENT REPORT DAYWISE &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\"> Date -" + FromDate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }



        public void HoursAttendancebetweenDates(string WagesType, string FromDate, string ToDate)
        {

            if (SessionUserType == "2")
            {
                NonAdminGetHoursAttendancebetweenDates(WagesType, FromDate, ToDate);
            }
            else
            {

                DataTable dt1 = new DataTable();
                AutoDTable.Columns.Add("EmployeeNo");
                AutoDTable.Columns.Add("MachineID");
                AutoDTable.Columns.Add("ExistingCode");
                AutoDTable.Columns.Add("DeptName");
                AutoDTable.Columns.Add("FirstName");
                AutoDTable.Columns.Add("Designation");

                mEmployeeDT.Columns.Add("SNo");
                mEmployeeDT.Columns.Add("EmployeeNo");
                mEmployeeDT.Columns.Add("ExistingCode");
                mEmployeeDT.Columns.Add("Name");
                mEmployeeDT.Columns.Add("DeptName");

                Double TotalDays;
                date1 = Convert.ToDateTime(FromDate);
                string dat = ToDate;
                Date2 = Convert.ToDateTime(dat);

                int daycount = (int)((Date2 - date1).TotalDays);
                int daysAdded = 0;
                bool isPresent = false;
                int intI = 0;
                int intK = 0;

                string halfPresent = "0";
                string Wages = " ";


                while (daycount >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                    //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                    mEmployeeDT.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                    daycount -= 1;
                    daysAdded += 1;
                }

                mEmployeeDT.Columns.Add("TotalHrs");
                mEmployeeDT.Columns.Add("TotalDays");
                mEmployeeDT.Columns.Add("Othr");

                SSQL = "";
                SSQL = "select EM.MachineID,EM.MachineID_Encrypt,EM.ExistingCode,DEP.DeptName,isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
                SSQL = SSQL + ",DM.DesignName,EM.OTEligible from Employee_Mst EM inner join Designation_Mst DM on EM.Designation = DM.DesignNo ";
                SSQL = SSQL + " inner join Department_Mst DEP on EM.DeptName = DEP.DeptCode Where EM.Compcode='" + SessionCcode.ToString() + "'";
                SSQL = SSQL + " And EM.LocCode='" + SessionLcode.ToString() + "' ";

                if (SessionUserType == "2")
                {
                    SSQL = SSQL + " And IsNonAdmin='1'";
                }


                if (WagesType != "0")
                {
                    SSQL = SSQL + " and   EM.Wages='" + WagesType + "' And EM.IsActive='Yes' order by MachineID Asc";
                }
                else
                {
                    SSQL = SSQL + " And EM.IsActive='Yes' order by MachineID Asc";
                }

                SSQL = SSQL + " ";
                MEmployeeDS = MasterDate.GetDetails(SSQL);


                double Total_Time_get;

                if (MEmployeeDS.Rows.Count > 0)
                {
                    intK = 0;

                    int SNo = 1;

                    for (int i = 0; i < MEmployeeDS.Rows.Count; i++)
                    {

                        mEmployeeDT.NewRow();
                        mEmployeeDT.Rows.Add();

                        string MachineID = MEmployeeDS.Rows[i]["MachineID"].ToString();

                        mEmployeeDT.Rows[intK]["SNo"] = SNo;
                        mEmployeeDT.Rows[intK]["EmployeeNo"] = MEmployeeDS.Rows[i]["MachineID"].ToString();
                        mEmployeeDT.Rows[intK]["ExistingCode"] = MEmployeeDS.Rows[i]["ExistingCode"].ToString();
                        mEmployeeDT.Rows[intK]["Name"] = MEmployeeDS.Rows[i]["FirstName"].ToString();
                        mEmployeeDT.Rows[intK]["DeptName"] = MEmployeeDS.Rows[i]["DeptName"].ToString();

                        double Total_Hr = 0;

                        int count = 5;
                        Total_Time_get = 0;

                        for (int j = 0; j < daysAdded; j++)
                        {
                            DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                            string Date1 = dayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);

                            if (SessionUserName == "IF")
                            {
                                SSQL = "select Present,Total_Hrs from IFLogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and Present !='0'";
                                SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Date1 + "',120)";

                            }
                            else
                            {
                                SSQL = "select Present,Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and Present !='0'";
                                SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Date1 + "',120)";
                            }
                            dt1 = ManualAttend.GetDetails(SSQL);

                            if (dt1.Rows.Count > 0)
                            {

                                Present_Count = Convert.ToDouble(dt1.Rows[0]["Present"].ToString());
                                Total_Time_get = Convert.ToDouble(dt1.Rows[0]["Total_Hrs"].ToString());
                                if(WagesType=="2")
                                {
                                    Total_Time_get = Total_Time_get - 1;
                                }
                               
                                mEmployeeDT.Rows[intK][count] = Total_Time_get;

                            }
                            if(dt1.Rows.Count==0)
                            {
                                mEmployeeDT.Rows[intK][count] = 0;
                            }


                            //if (Total_Time_get > 0)
                            //{
                            //    mEmployeeDT.Rows[intK][count] = Total_Time_get-1;
                            //}
                            //else
                            //{
                            //    mEmployeeDT.Rows[intK][count] = Total_Time_get-1;
                            //}
                            Total_Hr = Total_Hr + Total_Time_get;
                            count = count + 1;
                            Total_Time_get = 0;
                        }

                        mEmployeeDT.Rows[intK]["TotalHrs"] = Total_Hr.ToString();
                        mEmployeeDT.Rows[intK]["TotalDays"] = daysAdded.ToString();
                        mEmployeeDT.Rows[intK]["Othr"] = "0";
                        intK = intK + 1;

                        SNo = SNo + 1;

                    }

                }

                HourseUploadDataTableToExcel(mEmployeeDT);
                //  DayAttndBWDates(WagesType, FromDate, ToDate);

                //grid.DataSource = mEmployeeDT;
                //grid.DataBind();
                //string attachment = "attachment;filename=HOUR ATTENDANCE BETWEEN DATES.xls";
                //Response.ClearContent();
                //Response.AddHeader("content-disposition", attachment);
                //Response.ContentType = "application/ms-excel";
                //grid.HeaderStyle.Font.Bold = true;
                //System.IO.StringWriter stw = new System.IO.StringWriter();
                //HtmlTextWriter htextw = new HtmlTextWriter(stw);
                //grid.RenderControl(htextw);
                //Response.Write("<table>");
                //Response.Write("<tr Font-Bold='true' align='center'>");
                //Response.Write("<td colspan='10'>");
                //Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
                //Response.Write("--");
                //Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                //Response.Write("<tr Font-Bold='true' align='center'>");
                //Response.Write("<td colspan='10'>");
                //Response.Write("<a style=\"font-weight:bold\">HOUR ATTENANCE BETWEEN DATES REPORT &nbsp;&nbsp;&nbsp;</a>");

                //Response.Write("</td>");
                //Response.Write("</tr>");
                //Response.Write("<tr Font-Bold='true' align='center'>");
                //Response.Write("<td colspan='10'>");
                //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
                //Response.Write("&nbsp;&nbsp;&nbsp;");
                //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                //Response.Write("</table>");
                //Response.Write(stw.ToString());
                //Response.End();
                //Response.Clear();



            }

        }
        public void NonAdminGetHoursAttendancebetweenDates(string WagesType, string FromDate, string ToDate)
        {
            DataTable dt1 = new DataTable();
            AutoDTable.Columns.Add("EmployeeNo");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("ExistingCode");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("FirstName");
            AutoDTable.Columns.Add("Designation");

            mEmployeeDT.Columns.Add("SNo");
            mEmployeeDT.Columns.Add("EmployeeNo");
            mEmployeeDT.Columns.Add("ExistingCode");
            mEmployeeDT.Columns.Add("Name");
            mEmployeeDT.Columns.Add("DeptName");

            Double TotalDays;
            date1 = Convert.ToDateTime(FromDate);
            string dat = ToDate;
            Date2 = Convert.ToDateTime(dat);

            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;
            bool isPresent = false;
            int intI = 0;
            int intK = 0;

            string halfPresent = "0";


            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                mEmployeeDT.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                daycount -= 1;
                daysAdded += 1;
            }

            mEmployeeDT.Columns.Add("TotalDays");

            SSQL = "";
            SSQL = "select MachineID,MachineID_Encrypt,ExistingCode,DeptName,isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + ",Designation,OTEligible from Employee_Mst Where Compcode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "' ";

            if (SessionUserType == "2")
            {
                SSQL = SSQL + " And IsNonAdmin='1'";
            }


            if (WagesType != "- select -")
            {
                SSQL = SSQL + " and  Wages='" + WagesType + "' And IsActive='Yes' order by MachineID Asc";
            }
            else
            {
                SSQL = SSQL + " And IsActive='Yes' order by MachineID Asc";
            }

            SSQL = SSQL + " ";
            MEmployeeDS = MasterDate.GetDetails(SSQL);


            double Total_Time_get;

            if (MEmployeeDS.Rows.Count > 0)
            {
                intK = 0;

                int SNo = 1;

                for (int i = 0; i < MEmployeeDS.Rows.Count; i++)
                {

                    mEmployeeDT.NewRow();
                    mEmployeeDT.Rows.Add();

                    string MachineID = MEmployeeDS.Rows[i]["MachineID"].ToString();

                    mEmployeeDT.Rows[intK]["SNo"] = SNo;
                    mEmployeeDT.Rows[intK]["EmployeeNo"] = MEmployeeDS.Rows[i]["MachineID"].ToString();
                    mEmployeeDT.Rows[intK]["ExistingCode"] = MEmployeeDS.Rows[i]["ExistingCode"].ToString();
                    mEmployeeDT.Rows[intK]["Name"] = MEmployeeDS.Rows[i]["FirstName"].ToString();
                    mEmployeeDT.Rows[intK]["DeptName"] = MEmployeeDS.Rows[i]["DeptName"].ToString();


                    int count = 5;
                    Total_Time_get = 0;

                    for (int j = 0; j < daysAdded; j++)
                    {
                        DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                        string Date1 = dayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);

                        if (SessionUserName == "IF")
                        {
                            SSQL = "select Present,Total_Hrs from IFLogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and Present !='0'";
                            SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Date1 + "',120)";

                        }
                        else
                        {
                            SSQL = "select Present,Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and Present !='0'";
                            SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Date1 + "',120)";
                        }
                        dt1 = ManualAttend.GetDetails(SSQL);

                        if (dt1.Rows.Count > 0)
                        {

                            Present_Count = Convert.ToDouble(dt1.Rows[0]["Present"].ToString());
                            Total_Time_get = Convert.ToDouble(dt1.Rows[0]["Total_Hrs"].ToString());

                        }


                        if (Total_Time_get > 0)
                        {
                            mEmployeeDT.Rows[intK][count] = Total_Time_get;
                        }
                        else
                        {
                            mEmployeeDT.Rows[intK][count] = Total_Time_get;
                        }

                        count = count + 1;
                        Total_Time_get = 0;
                    }

                    mEmployeeDT.Rows[intK]["TotalDays"] = daysAdded.ToString();

                    intK = intK + 1;

                    SNo = SNo + 1;

                }

            }


            //  DayAttndBWDates(WagesType, FromDate, ToDate);

            grid.DataSource = mEmployeeDT;
            grid.DataBind();
            string attachment = "attachment;filename=HOUR ATTENDANCE BETWEEN DATES.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">HOUR ATTENANCE BETWEEN DATES REPORT &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            Response.Write("&nbsp;&nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }




        protected void HourseUploadDataTableToExcel(DataTable dtRecords)
        {
            string XlsPath = Server.MapPath(@"~/Add_data/HOUR ATTENDANCE BETWEEN DATES.xls");
            string attachment = string.Empty;
            if (XlsPath.IndexOf("\\") != -1)
            {
                string[] strFileName = XlsPath.Split(new char[] { '\\' });
                attachment = "attachment; filename=" + strFileName[strFileName.Length - 1];
            }
            else
                attachment = "attachment; filename=" + XlsPath;
            try
            {
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/vnd.ms-excel";
                string tab = string.Empty;

                foreach (DataColumn datacol in dtRecords.Columns)
                {
                    Response.Write(tab + datacol.ColumnName);
                    tab = "\t";
                }
                Response.Write("\n");

                foreach (DataRow dr in dtRecords.Rows)
                {
                    tab = "";
                    for (int j = 0; j < dtRecords.Columns.Count; j++)
                    {
                        Response.Write(tab + Convert.ToString(dr[j]));
                        tab = "\t";
                    }

                    Response.Write("\n");
                }
                Response.End();
            }
            catch (Exception ex)
            {
                //Response.Write(ex.Message);
            }
        }
       




        public ActionResult DayAttendanceDayWiseWithOT(string FromDate, string ShiftType)
        {

            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                GetDayAttendanceDayWiseWithOT(FromDate, ShiftType);

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public void GetDayAttendanceDayWiseWithOT(string FromDate, string ShiftType1)
        {

            DataSet ds = new DataSet();
            DataTable AutoDTable = new DataTable();


            if (SessionUserType == "2")
            {
                NonAdminGetAttdDayWiseWithOT_Change(FromDate, ShiftType1);
            }
            else
            {



                DataTable Shift_DS = new DataTable();


                Document document = new Document(PageSize.A4, 25, 25, 45, 40);
                Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

                writer.PageEvent = new HeaderFooter();
                document.Open();


                PdfPTable table = new PdfPTable(5);
                float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
                table.SetWidths(widths);
                table.WidthPercentage = 100;

                PdfPTable table1 = new PdfPTable(8);
                float[] widths1 = new float[] { 10f, 15f, 15f, 35f, 15f, 15f, 15f, 15f };
                table1.SetWidths(widths1);
                table1.WidthPercentage = 100;

                PdfPTable table2 = new PdfPTable(4);
                float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
                table2.SetWidths(widths2);
                table2.WidthPercentage = 100;

                PdfPCell cell;

                DataTable Depart = new DataTable();


                SSQL = "select MachineID,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT,";
                SSQL = SSQL + "SubCatName,Total_Hrs,OTHours from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

                if (ShiftType1 != "0")
                {
                    SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
                }
                if (FromDate != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                }

                SSQL = SSQL + " And Present > 0 ";

                AutoDTable = ManualAttend.GetDetails(SSQL);


                SSQL = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                if (ShiftType1 != "0")
                {
                    SSQL = SSQL + " And ShiftDesc='" + ShiftType1 + "'";
                }
                //SSQL = " Group by ShiftDesc Asc ";

                Shift_DS = ManualAttend.GetDetails(SSQL);

                SSQL = "select distinct DeptCode as DeptName from Department_Mst order by DeptName Asc";
                Depart = ManualAttend.GetDetails(SSQL);

                DataTable SubCat = new DataTable();

                SSQL = "select SubCatName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' group by  SubCatName ";
                SubCat = ManualAttend.GetDetails(SSQL);

                //table1.TableEvent = new BorderEvent();

                if (AutoDTable.Rows.Count > 0)
                {
                    cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("DAY ATTENDANCE DAY WISE WITH OT HOURS", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Total No.of Employee: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.BackgroundColor = BaseColor.YELLOW;
                    cell.Colspan = 2;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    //cell.Border = 0;
                    //cell.Colspan = 7;
                    ////cell.PaddingTop = 5f;
                    ////cell.PaddingBottom = 5f;
                    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    //table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.Colspan = 8;
                    //cell.PaddingTop = 5f;
                    //cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("EmpNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("GrandTOT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("OTHours", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.BOTTOM_BORDER;
                    cell.Colspan = 8;
                    //cell.PaddingTop = 5f;
                    //cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    int Count = 0;
                    int Count1 = 0;
                    int Count2 = 0;
                    int Count3 = 0;
                    int SubInsider = 0;
                    int SubOutSider = 0;

                    int SNo = 1;

                    for (int j = 0; j < Shift_DS.Rows.Count; j++)
                    {
                        string Shift = Shift_DS.Rows[j]["ShiftDesc"].ToString();

                        cell = PhraseCell(new Phrase(" " + Shift_DS.Rows[j]["ShiftDesc"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.Colspan = 8;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        Count = 0;



                        for (int k = 0; k < Depart.Rows.Count; k++)
                        {
                            string Deprt = Depart.Rows[k]["DeptName"].ToString();

                            cell = PhraseCell(new Phrase(" " + Depart.Rows[k]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.Colspan = 8;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            table1.AddCell(cell);
                            Count1 = 0;

                            Count2 = 0;

                            SubInsider = 0;
                            SubOutSider = 0;

                            for (int m = 0; m < SubCat.Rows.Count; m++)
                            {
                                string Sub = SubCat.Rows[m]["SubCatName"].ToString();

                                Count3 = 0;

                                for (int i = 0; i < AutoDTable.Rows.Count; i++)
                                {
                                    string Deprt1 = AutoDTable.Rows[i]["DeptName"].ToString();
                                    string Shift1 = AutoDTable.Rows[i]["Shift"].ToString();
                                    string Sub1 = AutoDTable.Rows[i]["SubCatName"].ToString();

                                    if (Deprt == Deprt1 && Shift == Shift1 && Sub == Sub1)
                                    {
                                        cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["SubCatName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOUT"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["OTHours"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                        cell.Border = 0;
                                        cell.PaddingTop = 5f;
                                        cell.PaddingBottom = 5f;
                                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        table1.AddCell(cell);

                                        Count = Count + 1;

                                        Count2 = Count2 + 1;

                                        Count3 = Count3 + 1;

                                        SNo = SNo + 1;

                                    }
                                }

                                if (Count3 > 0)
                                {

                                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" Total: " + Count3, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                }
                            }

                            if (Count2 == 0)
                            {
                                table1.DeleteLastRow();
                            }
                        }

                        if (Count == 0)
                        {
                            table1.DeleteLastRow();
                        }
                    }

                    //if (Count == 0)
                    //{
                    //    table1.DeleteLastRow();
                    //}

                }
                else
                {

                    cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("DAY ATTENDANCE DAY WISE WITH OT HOURS", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" No Data Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 8;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                }

                table.SpacingBefore = 10f;
                table.SpacingAfter = 10f;

                document.Add(table);

                table1.SpacingBefore = 5f;
                table1.SpacingAfter = 5f;

                document.Add(table1);


                document.Close();

                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=DAY_ATTENDANCE_DAY_WISE_OTHOURS.pdf");
                Response.ContentType = "application/pdf";
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(bytes);
                Response.End();
                Response.Close();
            }

        }

        public void NonAdminGetAttdDayWiseWithOT_Change(string FromDate, string ShiftType1)
        {


            DataTable Shift_DS = new DataTable();


            Document document = new Document(PageSize.A4, 25, 25, 45, 40);
            Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            writer.PageEvent = new HeaderFooter();
            document.Open();


            PdfPTable table = new PdfPTable(5);
            float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
            table.SetWidths(widths);
            table.WidthPercentage = 100;

            PdfPTable table1 = new PdfPTable(8);
            float[] widths1 = new float[] { 10f, 15f, 15f, 35f, 15f, 15f, 15f, 15f };
            table1.SetWidths(widths1);
            table1.WidthPercentage = 100;

            PdfPTable table2 = new PdfPTable(4);
            float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            table2.SetWidths(widths2);
            table2.WidthPercentage = 100;

            PdfPCell cell;

            DataTable Depart = new DataTable();


            SSQL = "select MachineID,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT,";
            SSQL = SSQL + "SubCatName,Total_Hrs,OTHours from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

            if (ShiftType1 != "0")
            {
                SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
            }
            if (FromDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            }

            SSQL = SSQL + " And Present > 0 ";

            AutoDTable = ManualAttend.GetDetails(SSQL);


            SSQL = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            if (ShiftType1 != "0")
            {
                SSQL = SSQL + " And ShiftDesc='" + ShiftType1 + "'";
            }
            //SSQL = " Group by ShiftDesc Asc ";

            Shift_DS = ManualAttend.GetDetails(SSQL);

            SSQL = "select distinct DeptCode as DeptName from Department_Mst order by DeptName Asc";
            Depart = ManualAttend.GetDetails(SSQL);

            DataTable SubCat = new DataTable();

            SSQL = "select SubCatName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' group by  SubCatName ";
            SubCat = ManualAttend.GetDetails(SSQL);

            //table1.TableEvent = new BorderEvent();

            if (AutoDTable.Rows.Count > 0)
            {
                cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("DAY ATTENDANCE DAY WISE WITH OT HOURS", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Total No.of Employee: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.BackgroundColor = BaseColor.YELLOW;
                cell.Colspan = 2;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                //cell.Border = 0;
                //cell.Colspan = 7;
                ////cell.PaddingTop = 5f;
                ////cell.PaddingBottom = 5f;
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER;
                cell.Colspan = 8;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("EmpNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("GrandTOT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("OTHours", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.BOTTOM_BORDER;
                cell.Colspan = 8;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                int Count = 0;
                int Count1 = 0;
                int Count2 = 0;
                int Count3 = 0;
                int SubInsider = 0;
                int SubOutSider = 0;

                int SNo = 1;

                for (int j = 0; j < Shift_DS.Rows.Count; j++)
                {
                    string Shift = Shift_DS.Rows[j]["ShiftDesc"].ToString();

                    cell = PhraseCell(new Phrase(" " + Shift_DS.Rows[j]["ShiftDesc"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 8;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    Count = 0;



                    for (int k = 0; k < Depart.Rows.Count; k++)
                    {
                        string Deprt = Depart.Rows[k]["DeptName"].ToString();

                        cell = PhraseCell(new Phrase(" " + Depart.Rows[k]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.Colspan = 8;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        table1.AddCell(cell);
                        Count1 = 0;

                        Count2 = 0;

                        SubInsider = 0;
                        SubOutSider = 0;

                        for (int m = 0; m < SubCat.Rows.Count; m++)
                        {
                            string Sub = SubCat.Rows[m]["SubCatName"].ToString();

                            Count3 = 0;

                            for (int i = 0; i < AutoDTable.Rows.Count; i++)
                            {
                                string Deprt1 = AutoDTable.Rows[i]["DeptName"].ToString();
                                string Shift1 = AutoDTable.Rows[i]["Shift"].ToString();
                                string Sub1 = AutoDTable.Rows[i]["SubCatName"].ToString();

                                if (Deprt == Deprt1 && Shift == Shift1 && Sub == Sub1)
                                {
                                    cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["SubCatName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOUT"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["OTHours"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                    cell.Border = 0;
                                    cell.PaddingTop = 5f;
                                    cell.PaddingBottom = 5f;
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    table1.AddCell(cell);

                                    Count = Count + 1;

                                    Count2 = Count2 + 1;

                                    Count3 = Count3 + 1;

                                    SNo = SNo + 1;

                                }
                            }

                            if (Count3 > 0)
                            {

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                                cell = PhraseCell(new Phrase(" Total: " + Count3, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                                cell.Border = 0;
                                cell.PaddingTop = 5f;
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table1.AddCell(cell);

                            }
                        }

                        if (Count2 == 0)
                        {
                            table1.DeleteLastRow();
                        }
                    }

                    if (Count == 0)
                    {
                        table1.DeleteLastRow();
                    }
                }

                //if (Count == 0)
                //{
                //    table1.DeleteLastRow();
                //}

            }
            else
            {

                cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("DAY ATTENDANCE DAY WISE WITH OT HOURS", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" No Data Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 8;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

            }

            table.SpacingBefore = 10f;
            table.SpacingAfter = 10f;

            document.Add(table);

            table1.SpacingBefore = 5f;
            table1.SpacingAfter = 5f;

            document.Add(table1);


            document.Close();

            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=DAY_ATTENDANCE_DAY_WISE_OTHOURS.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();
        }



        public ActionResult PayRollAttendance(string WagesType, string FromDate, string ToDate)
        {

            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                GetPayRollAttendance(WagesType, FromDate, ToDate);

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }//GetPatRollAttendance Start

        public void GetPayRollAttendance(string WagesType, string FromDate, string ToDate)
        {

            if (SessionUserID == "2")
            {
                NonAdminGetPayRollAttendance_Changes(WagesType, FromDate, ToDate);
            }
            else
            {
                DateTime date1;
                date1 = Convert.ToDateTime(FromDate);
                string dat = ToDate;
                DateTime Date2 = Convert.ToDateTime(dat);
                int daycount = (int)((Date2 - date1).TotalDays);
                int daysAdded = 0;

                while (daycount >= 0)
                {
                    daycount = daycount - 1;
                    daysAdded = daysAdded + 1;
                }

                DataTable dsEmployee = new DataTable();
                DataTable ds_check = new DataTable();
                DataTable labourCheck = new DataTable();
                DataTable stafCheck = new DataTable();
                DataTable Pivot = new DataTable();

                AutoDTable.Columns.Add("SNo");
                AutoDTable.Columns.Add("MachineID");
                AutoDTable.Columns.Add("Token No");
                AutoDTable.Columns.Add("EmpName");
                AutoDTable.Columns.Add("DeptName");
                AutoDTable.Columns.Add("Days");
                AutoDTable.Columns.Add("Workinghours");
                AutoDTable.Columns.Add("OT Hours");
                AutoDTable.Columns.Add("OT Days");

                AutoDTable.Columns.Add("W.H.P");
                AutoDTable.Columns.Add("N/FH");
                AutoDTable.Columns.Add("NFH W.Days");
                AutoDTable.Columns.Add("SPG Allow");
                AutoDTable.Columns.Add("Canteen");
                AutoDTable.Columns.Add("Fixed W.Days");
                AutoDTable.Columns.Add("DAY SHIFT");
                AutoDTable.Columns.Add("NIGHT SHIFT");
                AutoDTable.Columns.Add("Total Month");

                // SSQL = "";
                // SSQL = "select EmpTypeCd from MstEmployeeType where EmpType='" + WagesType + "'";
                // ds_check= MasterDate.GetDetails(SSQL);

                //string Wage_val= ds_check.Rows[0]["EmpTypeCd"].ToString();

                SSQL = "";
                SSQL = "select Cast(EM.MachineID As int) As MachineID,EM.ExistingCode,isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as FirstName,isnull(DM.DeptName,'') as DeptName ";
                SSQL = SSQL + " ,EM.OTEligible,EM.CatName from Employee_Mst EM Inner join Department_Mst DM on EM.DeptName = DM.DeptCode ";
                SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";
                if (WagesType != "0")
                {
                    SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
                }
                SSQL = SSQL + " And DM.Compcode='" + SessionCcode + "' And DM.LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " Order By EM.MachineID";

                dsEmployee = MasterDate.GetDetails(SSQL);

                int IntK = 0;

                int SNo = 0;

                for (int i = 0; i < dsEmployee.Rows.Count; i++)
                {
                    SNo = SNo + 1;
                    string category = "";
                    string dept = "";
                    string totaltime = "";
                    int min = 0;
                    int hour = 0;
                    category = dsEmployee.Rows[i]["CatName"].ToString();
                    dept = dsEmployee.Rows[i]["DeptName"].ToString();
                    string MachId = dsEmployee.Rows[i]["MachineID"].ToString();

                    if (MachId == "205")
                    {
                        MachId = "205";
                    }
                    if (SessionUserName == "IF")
                    {
                        SSQL = "";

                        SSQL = SSQL + " select SUM(EM.Present) As Present,SUM(EM.OTHours) As OTHours  ,SUM(isnull(EM.Wh_Count,'0')) As Wh_Count,SUM(isnull(EM.Wh_Present_Count,'0')) As Wh_Present_Count";
                        // SSQL = SSQL + "";
                        SSQL = SSQL + "  from LogTime_Days EM Inner join Department_Mst DM on EM.DeptName = DM.DeptCode where EM.Total_Hrs >= '8.0'  and(EM.CatName = 'STAFF' OR DM.DeptName = 'TEXTILES') and ";

                        SSQL = SSQL + "  EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.MachineID ='" + dsEmployee.Rows[i]["MachineID"].ToString() + "'";

                        if (FromDate != "")
                        {
                            SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,120) >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                        }

                        if (ToDate != "")
                        {
                            SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,120) <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                        }

                        // SSQL = SSQL + " Group by MachineID,ExistingCode,FirstName,LastName";

                        DataCells = MasterDate.GetDetails(SSQL);




                        SSQL = "";
                        SSQL = SSQL + "select sum(datediff(SECOND, 0, Total_Hrs1)) / 3600 as Totalhours ,(sum(datepart(MINUTE,Cast([Total_Hrs1] as Datetime)))%60+(sum(datepart(SECOND,Cast([Total_Hrs1] as Datetime)))/60))%60 as Totalminutes ";
                        //SSQL = SSQL + " select convert(char(5),dateadd(second,SUM ( DATEPART(hh,(convert(datetime,EM.Total_Hrs1,1))) * 3600 + ";
                        //SSQL = SSQL + "  DATEPART(mi, (convert(datetime, EM.Total_Hrs1, 1))) * 60 + DATEPART(ss, (convert(datetime, Total_Hrs1, 1)))),0),108) as Totalhours ";
                        SSQL = SSQL + "  FROM LogTime_Days EM Inner join Department_Mst DM on EM.DeptName = DM.DeptCode ";
                        SSQL = SSQL + "  where CONVERT(DATETIME,EM.Total_Hrs1,120) <'8:00'  and(CatName = 'STAFF' OR DM.DeptName = 'TEXTILES') and ";

                        SSQL = SSQL + "   EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.MachineID ='" + dsEmployee.Rows[i]["MachineID"].ToString() + "'";

                        if (FromDate != "")
                        {
                            SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,120) >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                        }

                        if (ToDate != "")
                        {
                            SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,120) <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                        }

                        // SSQL = SSQL + " Group by Totalhours";

                        stafCheck = MasterDate.GetDetails(SSQL);
                        if (stafCheck.Rows.Count != 0)
                        {
                            if (stafCheck.Rows[0]["Totalhours"].ToString() == "")
                            {
                                hour = 0;
                            }
                            else
                            {
                                min = Convert.ToInt32(stafCheck.Rows[0]["Totalminutes"].ToString());
                                hour = Convert.ToInt32(stafCheck.Rows[0]["Totalhours"].ToString());

                                if (min >= 30)
                                {

                                    hour = hour + 1;
                                }
                                else if (min < 30)
                                {

                                }
                                else
                                {
                                    hour = 0;
                                }

                            }

                        }





                        SSQL = "SELECT * FROM ( SELECT Shift FROM LogTime_Days where MachineID='" + dsEmployee.Rows[i]["MachineID"].ToString() + "' And CONVERT(DATETIME,Attn_Date_Str,120) >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120) <=  CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120) ) as s";
                        SSQL = SSQL + " PIVOT (count(Shift) FOR [Shift] IN (GENERAL,SHIFT1,SHIFT2,SHIFT3,SHIFT4) )AS pvt";
                        Pivot = MasterDate.GetDetails(SSQL);
                    }
                    else
                    {
                        if (category == "STAFF" || dept == "TEXTILES")
                        {


                            SSQL = "";

                            SSQL = SSQL + " select SUM(EM.Present) As Present,SUM(EM.OTHours) As OTHours  ,SUM(isnull(EM.Wh_Count,'0')) As Wh_Count,SUM(isnull(EM.Wh_Present_Count,'0')) As Wh_Present_Count";
                            // SSQL = SSQL + "";
                            SSQL = SSQL + "  from LogTime_Days EM Inner join Department_Mst DM on EM.DeptName = DM.DeptCode where EM.Total_Hrs >= '4.0'  and(EM.CatName = 'STAFF' OR DM.DeptName = 'TEXTILES') and ";

                            SSQL = SSQL + "  EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.MachineID ='" + dsEmployee.Rows[i]["MachineID"].ToString() + "'";

                            if (FromDate != "")
                            {
                                SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,120) >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                            }

                            if (ToDate != "")
                            {
                                SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,120) <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                            }

                            // SSQL = SSQL + " Group by MachineID,ExistingCode,FirstName,LastName";

                            DataCells = MasterDate.GetDetails(SSQL);




                            SSQL = "";
                            SSQL = SSQL + "select sum(datediff(SECOND, 0, Total_Hrs1)) / 3600 as Totalhours ,(sum(datepart(MINUTE,Cast([Total_Hrs1] as Datetime)))%60+(sum(datepart(SECOND,Cast([Total_Hrs1] as Datetime)))/60))%60 as Totalminutes ";
                            //SSQL = SSQL + " select convert(char(5),dateadd(second,SUM ( DATEPART(hh,(convert(datetime,EM.Total_Hrs1,1))) * 3600 + ";
                            //SSQL = SSQL + "  DATEPART(mi, (convert(datetime, EM.Total_Hrs1, 1))) * 60 + DATEPART(ss, (convert(datetime, Total_Hrs1, 1)))),0),108) as Totalhours ";
                            SSQL = SSQL + "  FROM LogTime_Days EM Inner join Department_Mst DM on EM.DeptName = DM.DeptCode ";
                            SSQL = SSQL + "  where CONVERT(DATETIME,EM.Total_Hrs1,120) <'8:00'  and(CatName = 'STAFF' OR DM.DeptName = 'TEXTILES') and ";

                            SSQL = SSQL + "   EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.MachineID ='" + dsEmployee.Rows[i]["MachineID"].ToString() + "'";

                            if (FromDate != "")
                            {
                                SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,120) >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                            }

                            if (ToDate != "")
                            {
                                SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,120) <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                            }

                            // SSQL = SSQL + " Group by Totalhours";

                            stafCheck = MasterDate.GetDetails(SSQL);
                            if (stafCheck.Rows.Count != 0)
                            {
                                if (stafCheck.Rows[0]["Totalhours"].ToString() == "")
                                {
                                    hour = 0;
                                }
                                else
                                {
                                    min = Convert.ToInt32(stafCheck.Rows[0]["Totalminutes"].ToString());
                                    hour = Convert.ToInt32(stafCheck.Rows[0]["Totalhours"].ToString());

                                    if (min >= 30)
                                    {

                                        hour = hour + 1;
                                    }
                                    else if (min < 30)
                                    {

                                    }
                                    else
                                    {
                                        hour = 0;
                                    }

                                }

                            }

                        }

                        if (category == "LABOUR" && dept != "TEXTILES")
                        {


                            SSQL = "";

                            SSQL = SSQL + " select SUM(EM.Present) As Present,SUM(EM.OTHours) As OTHours  ,SUM(isnull(EM.Wh_Count,'0')) As Wh_Count,SUM(isnull(EM.Wh_Present_Count,'0')) As Wh_Present_Count";

                            SSQL = SSQL + "  from LogTime_Days EM Inner join Department_Mst DM on EM.DeptName = DM.DeptCode where EM.Total_Hrs >= '12.0'  and EM.CatName = 'LABOUR' And DM.DeptName != 'TEXTILES' and ";

                            SSQL = SSQL + "  EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.MachineID ='" + dsEmployee.Rows[i]["MachineID"].ToString() + "'";

                            if (FromDate != "")
                            {
                                SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,120) >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                            }

                            if (ToDate != "")
                            {
                                SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,120) <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                            }

                            // SSQL = SSQL + " Group by MachineID,ExistingCode,FirstName,LastName";

                            DataCells = MasterDate.GetDetails(SSQL);






                            SSQL = "";
                            SSQL = SSQL + "select sum(datediff(SECOND, 0, Total_Hrs1)) / 3600 as Totalhours,(sum(datepart(MINUTE,Cast([Total_Hrs1] as Datetime)))%60+(sum(datepart(SECOND,Cast([Total_Hrs1] as Datetime)))/60))%60 as Totalminutes ";
                            //SSQL = SSQL + " select convert(char(5),dateadd(second,SUM ( DATEPART(hh,(convert(datetime,EM.Total_Hrs1,1))) * 3600 + ";
                            //SSQL = SSQL + "  DATEPART(mi, (convert(datetime, EM.Total_Hrs1, 1))) * 60 + DATEPART(ss, (convert(datetime, Total_Hrs1, 1)))),0),108)as Totalhours ";
                            SSQL = SSQL + "  FROM LogTime_Days EM Inner join Department_Mst DM on EM.DeptName = DM.DeptCode ";
                            SSQL = SSQL + "  where CONVERT(DATETIME,EM.Total_Hrs1,120) < '12:00'  and CatName = 'LABOUR' And DM.DeptName != 'TEXTILES' and ";

                            SSQL = SSQL + "   EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.MachineID ='" + dsEmployee.Rows[i]["MachineID"].ToString() + "'";

                            if (FromDate != "")
                            {
                                SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,120) >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                            }

                            if (ToDate != "")
                            {
                                SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,120) <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                            }

                            // SSQL = SSQL + " Group by Totalhours";

                            labourCheck = MasterDate.GetDetails(SSQL);
                            if (labourCheck.Rows.Count != 0)
                            {
                                if (labourCheck.Rows[0]["Totalhours"].ToString() == "")
                                {
                                    hour = 0;
                                }
                                else
                                {
                                    min = Convert.ToInt32(labourCheck.Rows[0]["Totalminutes"].ToString());
                                    hour = Convert.ToInt32(labourCheck.Rows[0]["Totalhours"].ToString());
                                    if (min >= 30)
                                    {

                                        hour = hour + 1;
                                    }
                                    else if (min < 30)
                                    {

                                    }
                                    else
                                    {
                                        hour = 0;
                                    }



                                }


                            }
                        }

                        SSQL = "SELECT * FROM ( SELECT Shift FROM LogTime_Days where MachineID='" + dsEmployee.Rows[i]["MachineID"].ToString() + "' And CONVERT(DATETIME,Attn_Date_Str,120) >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120) <=  CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120) ) as s";
                        SSQL = SSQL + " PIVOT (count(Shift) FOR [Shift] IN (GENERAL,SHIFT1,SHIFT2,SHIFT3,SHIFT4) )AS pvt";
                        Pivot = MasterDate.GetDetails(SSQL);
                    }
                    double GENERAL = 0;
                    double SHIFT1 = 0;
                    double SHIFT2 = 0;
                    double SHIFT3 = 0;
                    double SHIFT4 = 0;
                    double WhCount = 0;
                    double WhPresent = 0;
                    //double FixedWDays = 0;

                    if (DataCells.Rows.Count <= 0)
                    {

                    }
                    else
                    {
                        if (Pivot.Rows.Count <= 0)
                        {
                            GENERAL = 0;
                            SHIFT1 = 0;
                            SHIFT2 = 0;
                            SHIFT3 = 0;
                            SHIFT4 = 0;
                        }
                        else
                        {
                            GENERAL = Convert.ToDouble(Pivot.Rows[0]["GENERAL"].ToString());
                            SHIFT1 = Convert.ToDouble(Pivot.Rows[0]["SHIFT1"].ToString());
                            SHIFT2 = Convert.ToDouble(Pivot.Rows[0]["SHIFT2"].ToString());
                            SHIFT3 = Convert.ToDouble(Pivot.Rows[0]["SHIFT3"].ToString());
                            SHIFT4 = Convert.ToDouble(Pivot.Rows[0]["SHIFT4"].ToString());

                        }
                    }


                    double DayShift = GENERAL + SHIFT1;


                    if (DataCells.Rows.Count <= 0)
                    {

                    }
                    else
                    {
                        if (DataCells.Rows[0]["Wh_Count"].ToString() == "")
                        {
                            WhCount = 0;

                        }
                        if (DataCells.Rows[0]["Wh_Present_Count"].ToString() == "")
                        {
                            WhPresent = 0;
                        }
                        else
                        {
                            WhCount = Convert.ToDouble(DataCells.Rows[0]["Wh_Count"].ToString());
                            WhPresent = Convert.ToDouble(DataCells.Rows[0]["Wh_Present_Count"].ToString());
                        }

                    }


                    double FixedWDays = daysAdded - WhCount;

                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    if (DataCells.Rows.Count <= 0)
                    {



                        AutoDTable.Rows[IntK]["SNo"] = SNo;
                        AutoDTable.Rows[IntK]["MachineID"] = dsEmployee.Rows[i]["MachineID"].ToString();
                        AutoDTable.Rows[IntK]["Token No"] = dsEmployee.Rows[i]["MachineID"].ToString();
                        AutoDTable.Rows[IntK]["EmpName"] = dsEmployee.Rows[i]["FirstName"].ToString();
                        AutoDTable.Rows[IntK]["DeptName"] = dsEmployee.Rows[i]["DeptName"].ToString();
                        AutoDTable.Rows[IntK]["Days"] = "0";
                        AutoDTable.Rows[IntK]["OT Days"] = "0";
                        AutoDTable.Rows[IntK]["OT Hours"] = "0";
                        AutoDTable.Rows[IntK]["Workinghours"] = "0";
                        AutoDTable.Rows[IntK]["W.H.P"] = "0";
                        AutoDTable.Rows[IntK]["N/FH"] = "0";
                        AutoDTable.Rows[IntK]["NFH W.Days"] = "0";
                        AutoDTable.Rows[IntK]["SPG Allow"] = "0";
                        AutoDTable.Rows[IntK]["Canteen"] = "0";
                        AutoDTable.Rows[IntK]["Fixed W.Days"] = FixedWDays;
                        AutoDTable.Rows[IntK]["DAY SHIFT"] = DayShift;
                        AutoDTable.Rows[IntK]["NIGHT SHIFT"] = SHIFT2;
                        AutoDTable.Rows[IntK]["Total Month"] = daysAdded;






                    }
                    else
                    {
                        for (int j = 0; j < DataCells.Rows.Count; j++)
                        {

                            AutoDTable.Rows[IntK]["SNo"] = SNo;
                            AutoDTable.Rows[IntK]["MachineID"] = dsEmployee.Rows[i]["MachineID"].ToString();
                            AutoDTable.Rows[IntK]["Token No"] = dsEmployee.Rows[i]["MachineID"].ToString();
                            AutoDTable.Rows[IntK]["EmpName"] = dsEmployee.Rows[i]["FirstName"].ToString();
                            AutoDTable.Rows[IntK]["DeptName"] = dsEmployee.Rows[i]["DeptName"].ToString();
                            if (DataCells.Rows[j]["Present"].ToString() == "")
                            {
                                AutoDTable.Rows[IntK]["Days"] = 0;
                            }
                            else
                            {
                                AutoDTable.Rows[IntK]["Days"] = (Convert.ToDecimal(DataCells.Rows[j]["Present"].ToString()) - Convert.ToDecimal(DataCells.Rows[j]["Wh_Present_Count"].ToString())).ToString();
                            }
                            AutoDTable.Rows[IntK]["OT Days"] = "0";

                            if ((dsEmployee.Rows[i]["OTEligible"].ToString()).ToUpper() == "YES")
                            {
                                AutoDTable.Rows[IntK]["OT Hours"] = DataCells.Rows[j]["OTHours"].ToString();
                            }
                            else
                            {
                                AutoDTable.Rows[IntK]["OT Hours"] = 0;
                            }

                            AutoDTable.Rows[IntK]["Workinghours"] = hour;
                            string wpresent = DataCells.Rows[j]["Wh_Present_Count"].ToString();
                            if (wpresent == "")

                            {
                                AutoDTable.Rows[IntK]["W.H.P"] = "0";
                            }
                            else
                            {
                                AutoDTable.Rows[IntK]["W.H.P"] = DataCells.Rows[j]["Wh_Present_Count"].ToString();
                            }
                            AutoDTable.Rows[IntK]["N/FH"] = "0";
                            AutoDTable.Rows[IntK]["NFH W.Days"] = "0";
                            AutoDTable.Rows[IntK]["SPG Allow"] = "0";
                            AutoDTable.Rows[IntK]["Canteen"] = "0";
                            AutoDTable.Rows[IntK]["Fixed W.Days"] = FixedWDays;
                            AutoDTable.Rows[IntK]["DAY SHIFT"] = DayShift;
                            AutoDTable.Rows[IntK]["NIGHT SHIFT"] = SHIFT2;
                            AutoDTable.Rows[IntK]["Total Month"] = daysAdded;



                        }
                    }

                    IntK = IntK + 1;
                }

                UploadDataTableToExcel(AutoDTable);

            }



        }

        public void NonAdminGetPayRollAttendance_Changes(string WagesType, string FromDate, string ToDate)
        {
            DateTime date1;
            date1 = Convert.ToDateTime(FromDate);
            string dat = ToDate;
            DateTime Date2 = Convert.ToDateTime(dat);
            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;

            while (daycount >= 0)
            {
                daycount = daycount - 1;
                daysAdded = daysAdded + 1;
            }

            DataTable dsEmployee = new DataTable();
            DataTable Pivot = new DataTable();

            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Token No");
            AutoDTable.Columns.Add("EmpName");
            AutoDTable.Columns.Add("Days");
            AutoDTable.Columns.Add("OT Hours");
            AutoDTable.Columns.Add("OT Days");
            AutoDTable.Columns.Add("W.H");
            AutoDTable.Columns.Add("W.H.P");
            AutoDTable.Columns.Add("N/FH");
            AutoDTable.Columns.Add("NFH W.Days");
            AutoDTable.Columns.Add("SPG Allow");
            AutoDTable.Columns.Add("Canteen");
            AutoDTable.Columns.Add("Fixed W.Days");
            AutoDTable.Columns.Add("DAY SHIFT");
            AutoDTable.Columns.Add("NIGHT SHIFT");
            AutoDTable.Columns.Add("Total Month");


            SSQL = "";
            SSQL = "select isnull(DeptName,'') as [DeptName], Cast(MachineID As int) As MachineID,MachineID_Encrypt";
            SSQL = SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
            SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName],ShiftType";
            SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'";
            if (SessionUserID == "2")
            {
                SSQL = SSQL + " And IsNonAdmin='1'";
            }

            if (WagesType != "0")
            {
                SSQL = SSQL + " And Wages='" + WagesType + "'";
            }
            SSQL = SSQL + " Order By MachineID";

            dsEmployee = MasterDate.GetDetails(SSQL);

            int IntK = 0;

            int SNo = 0;

            for (int i = 0; i < dsEmployee.Rows.Count; i++)
            {
                SNo = SNo + 1;

                SSQL = "";
                SSQL = "select MachineID,ExistingCode,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,";
                SSQL = SSQL + "SUM(Present) As Present,SUM(OTHours) As OTHours,SUM(isnull(Wh_Count,'0')) As Wh_Count,SUM(isnull(Wh_Present_Count,'0')) As Wh_Present_Count";
                //SSQL = SSQL + "Shift";
                SSQL = SSQL + " from LogTime_Days  Where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID ='" + dsEmployee.Rows[i]["EmpNo"].ToString() + "'";

                if (FromDate != "0")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120) >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                }

                if (ToDate != "0")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120) <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                }

                SSQL = SSQL + " Group by MachineID,ExistingCode,FirstName,LastName";

                DataCells = MasterDate.GetDetails(SSQL);

                SSQL = "SELECT * FROM ( SELECT Shift FROM LogTime_Days where MachineID='" + dsEmployee.Rows[i]["EmpNo"].ToString() + "' And CONVERT(DATETIME,Attn_Date_Str,103) >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120) <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120) ) as s";
                SSQL = SSQL + " PIVOT (count(Shift) FOR [Shift] IN (GENERAL,SHIFT1,SHIFT2) )AS pvt";
                Pivot = MasterDate.GetDetails(SSQL);

                double GENERAL = 0;
                double SHIFT1 = 0;
                double SHIFT2 = 0;
                double WhCount = 0;
                double WhPresent = 0;
                //double FixedWDays = 0;

                if (DataCells.Rows.Count <= 0)
                {

                }
                else
                {
                    GENERAL = Convert.ToDouble(Pivot.Rows[0]["GENERAL"].ToString());
                    SHIFT1 = Convert.ToDouble(Pivot.Rows[0]["SHIFT1"].ToString());
                    SHIFT2 = Convert.ToDouble(Pivot.Rows[0]["SHIFT2"].ToString());
                }


                double DayShift = GENERAL + SHIFT1;


                if (DataCells.Rows.Count <= 0)
                {

                }
                else
                {
                    WhCount = Convert.ToDouble(DataCells.Rows[0]["Wh_Count"].ToString());
                    WhPresent = Convert.ToDouble(DataCells.Rows[0]["Wh_Present_Count"].ToString());
                }


                double FixedWDays = daysAdded - WhCount;

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                if (DataCells.Rows.Count <= 0)
                {



                    AutoDTable.Rows[IntK]["SNo"] = SNo;
                    AutoDTable.Rows[IntK]["MachineID"] = dsEmployee.Rows[i]["MachineID"].ToString();
                    AutoDTable.Rows[IntK]["Token No"] = dsEmployee.Rows[i]["MachineID"].ToString();
                    AutoDTable.Rows[IntK]["EmpName"] = dsEmployee.Rows[i]["FirstName"].ToString();
                    AutoDTable.Rows[IntK]["Days"] = "0";
                    AutoDTable.Rows[IntK]["OT Days"] = "0";
                    AutoDTable.Rows[IntK]["OT Hours"] = "0";
                    AutoDTable.Rows[IntK]["W.H"] = "0";
                    AutoDTable.Rows[IntK]["W.H.P"] = "0";
                    AutoDTable.Rows[IntK]["N/FH"] = "0";
                    AutoDTable.Rows[IntK]["NFH W.Days"] = "0";
                    AutoDTable.Rows[IntK]["SPG Allow"] = "0";
                    AutoDTable.Rows[IntK]["Canteen"] = "0";
                    AutoDTable.Rows[IntK]["Fixed W.Days"] = FixedWDays;
                    AutoDTable.Rows[IntK]["DAY SHIFT"] = DayShift;
                    AutoDTable.Rows[IntK]["NIGHT SHIFT"] = SHIFT2;
                    AutoDTable.Rows[IntK]["Total Month"] = daysAdded;






                }
                else
                {
                    for (int j = 0; j < DataCells.Rows.Count; j++)
                    {

                        AutoDTable.Rows[IntK]["SNo"] = SNo;
                        AutoDTable.Rows[IntK]["MachineID"] = DataCells.Rows[j]["MachineID"].ToString();
                        AutoDTable.Rows[IntK]["Token No"] = DataCells.Rows[j]["MachineID"].ToString();
                        AutoDTable.Rows[IntK]["EmpName"] = DataCells.Rows[j]["FirstName"].ToString();
                        AutoDTable.Rows[IntK]["Days"] = DataCells.Rows[j]["Present"].ToString();
                        AutoDTable.Rows[IntK]["OT Days"] = "0";
                        AutoDTable.Rows[IntK]["OT Hours"] = DataCells.Rows[j]["OTHours"].ToString();
                        AutoDTable.Rows[IntK]["W.H"] = DataCells.Rows[j]["Wh_Count"].ToString();
                        AutoDTable.Rows[IntK]["W.H.P"] = DataCells.Rows[j]["Wh_Present_Count"].ToString();
                        AutoDTable.Rows[IntK]["N/FH"] = "0";
                        AutoDTable.Rows[IntK]["NFH W.Days"] = "0";
                        AutoDTable.Rows[IntK]["SPG Allow"] = "0";
                        AutoDTable.Rows[IntK]["Canteen"] = "0";
                        AutoDTable.Rows[IntK]["Fixed W.Days"] = FixedWDays;
                        AutoDTable.Rows[IntK]["DAY SHIFT"] = DayShift;
                        AutoDTable.Rows[IntK]["NIGHT SHIFT"] = SHIFT2;
                        AutoDTable.Rows[IntK]["Total Month"] = daysAdded;



                    }
                }

                IntK = IntK + 1;
            }

            UploadDataTableToExcel(AutoDTable);

        }

        //GetPatRollAttendance End


        public ActionResult MisMatchShifDayWise(string FromDate)
        {

            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                GetMisMatchShifDayWise(FromDate);

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public void GetMisMatchShifDayWise(string FromDate)
        {
            if (SessionUserType == "2")
            {
                NonAdminGetMisMatchShifDayWise_Changes(FromDate);
            }
            else
            {

                Document document = new Document(PageSize.A4, 25, 25, 45, 40);
                Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

                writer.PageEvent = new HeaderFooter();
                document.Open();


                PdfPTable table = new PdfPTable(5);
                float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
                table.SetWidths(widths);
                table.WidthPercentage = 100;

                PdfPTable table1 = new PdfPTable(7);
                float[] widths1 = new float[] { 10f, 15f, 15f, 35f, 15f, 15f, 15f };
                table1.SetWidths(widths1);
                table1.WidthPercentage = 100;

                PdfPTable table2 = new PdfPTable(4);
                float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
                table2.SetWidths(widths2);
                table2.WidthPercentage = 100;

                PdfPCell cell;

                DataTable Depart = new DataTable();

                if (SessionUserName == "IF")
                {
                    SSQL = "select MachineID,ExistingCode,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT,";
                    SSQL = SSQL + "Total_Hrs,Shift from IFLogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

                    if (FromDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                    }
                    SSQL = SSQL + " and  (TypeName='Mismatch' or TypeName='GENERAL Mismatch') ";
                }
                else
                {
                    SSQL = "select MachineID,ExistingCode,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT,";
                    SSQL = SSQL + "Total_Hrs,Shift from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

                    if (FromDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                    }
                    SSQL = SSQL + " and  (TypeName='Mismatch' or TypeName='GENERAL Mismatch') ";
                }



                //SSQL = SSQL + " And Present > 0";

                AutoDTable = MasterDate.GetDetails(SSQL);

                //table1.TableEvent = new BorderEvent();

                if (AutoDTable.Rows.Count > 0)
                {
                    cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("MISMATCH REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Total No.of Employee: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.BackgroundColor = BaseColor.YELLOW;
                    cell.Colspan = 2;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.Colspan = 7;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("EmpNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("ExistingCode", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("GrandTOT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.BOTTOM_BORDER;
                    cell.Colspan = 7;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    int SNo = 1;

                    for (int i = 0; i < AutoDTable.Rows.Count; i++)
                    {

                        string NoSHift = AutoDTable.Rows[i]["Shift"].ToString();

                        if (NoSHift == "No Shift" || NoSHift == "GENERAL")
                        {

                            cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["ExistingCode"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOUT"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                            cell.Border = 0;
                            cell.PaddingTop = 5f;
                            cell.PaddingBottom = 5f;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table1.AddCell(cell);

                            SNo = SNo + 1;


                        }


                    }



                    table.SpacingBefore = 10f;
                    table.SpacingAfter = 10f;

                    document.Add(table);

                    table1.SpacingBefore = 5f;
                    table1.SpacingAfter = 5f;

                    document.Add(table1);


                    document.Close();

                    byte[] bytes = memoryStream.ToArray();
                    memoryStream.Close();
                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment; filename=MisMatchReport.pdf");
                    Response.ContentType = "application/pdf";
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.End();
                    Response.Close();
                }
                else
                {

                    cell = PhraseCell(new Phrase(" Data Not Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 5;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    table.SpacingBefore = 10f;
                    table.SpacingAfter = 10f;

                    document.Add(table);


                    document.Close();

                    byte[] bytes = memoryStream.ToArray();
                    memoryStream.Close();
                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment; filename=MisMatchReport.pdf");
                    Response.ContentType = "application/pdf";
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.End();
                    Response.Close();
                }
            }
        }
        public void NonAdminGetMisMatchShifDayWise_Changes(string FromDate)
        {

            Document document = new Document(PageSize.A4, 25, 25, 45, 40);
            Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            writer.PageEvent = new HeaderFooter();
            document.Open();


            PdfPTable table = new PdfPTable(5);
            float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
            table.SetWidths(widths);
            table.WidthPercentage = 100;

            PdfPTable table1 = new PdfPTable(7);
            float[] widths1 = new float[] { 10f, 15f, 15f, 35f, 15f, 15f, 15f };
            table1.SetWidths(widths1);
            table1.WidthPercentage = 100;

            PdfPTable table2 = new PdfPTable(4);
            float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            table2.SetWidths(widths2);
            table2.WidthPercentage = 100;

            PdfPCell cell;

            DataTable Depart = new DataTable();




            if (SessionUserName == "IF")
            {
                SSQL = "select MachineID,ExistingCode,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT,";
                SSQL = SSQL + "Total_Hrs,Shift from IFLogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

                if (FromDate != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                }
                SSQL = SSQL + " and  (TypeName='Mismatch' or TypeName='GENERAL Mismatch') ";
            }
            else
            {
                SSQL = "select MachineID,ExistingCode,DeptName,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT,";
                SSQL = SSQL + "Total_Hrs,Shift from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

                if (FromDate != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                }
                SSQL = SSQL + " and  (TypeName='Mismatch' or TypeName='GENERAL Mismatch') ";
            }


            AutoDTable = MasterDate.GetDetails(SSQL);

            //table1.TableEvent = new BorderEvent();

            if (AutoDTable.Rows.Count > 0)
            {
                cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("MISMATCH REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Total No.of Employee: " + AutoDTable.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.BackgroundColor = BaseColor.YELLOW;
                cell.Colspan = 2;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Shift Date: ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER;
                cell.Colspan = 7;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("EmpNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("ExistingCode", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeIN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeOUT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("GrandTOT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.BOTTOM_BORDER;
                cell.Colspan = 7;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                int SNo = 1;

                for (int i = 0; i < AutoDTable.Rows.Count; i++)
                {

                    string NoSHift = AutoDTable.Rows[i]["Shift"].ToString();

                    if (NoSHift == "No Shift" || NoSHift == "GENERAL")
                    {

                        cell = PhraseCell(new Phrase(" " + SNo, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["ExistingCode"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["TimeOUT"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total_Hrs"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        SNo = SNo + 1;


                    }


                }



                table.SpacingBefore = 10f;
                table.SpacingAfter = 10f;

                document.Add(table);

                table1.SpacingBefore = 5f;
                table1.SpacingAfter = 5f;

                document.Add(table1);


                document.Close();

                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=MisMatchReport.pdf");
                Response.ContentType = "application/pdf";
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(bytes);
                Response.End();
                Response.Close();
            }
            else
            {

                cell = PhraseCell(new Phrase(" Data Not Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                table.SpacingBefore = 10f;
                table.SpacingAfter = 10f;

                document.Add(table);


                document.Close();

                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=MisMatchReport.pdf");
                Response.ContentType = "application/pdf";
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(bytes);
                Response.End();
                Response.Close();
            }

        }









        public ActionResult EmployeeWiseAttendBTDates(string EmpNo, string FromDate, string ToDate)
        {

            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                GetEmployeeWiseDayAyyendanceBTDates(EmpNo, FromDate, ToDate);

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }




        // GetEmployeeWiseDayAyyendanceBTDates Start






        public void GetEmployeeWiseDayAyyendanceBTDates(string EmpNo, string FromDate, string ToDate)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string EmpName;

            if (SessionUserID == "2")
            {
                NonAdminGetEmployeeWiseDayAyyendanceBTDates_Change(EmpNo, FromDate, ToDate);
            }
            else
            {

                double Total_Time_get;
                // DataTable dt = new DataTable();
                DataTable dt1 = new DataTable();



                date1 = Convert.ToDateTime(FromDate);
                string dat = ToDate;
                Date2 = Convert.ToDateTime(dat);

                int daycount = (int)((Date2 - date1).TotalDays);
                int daysAdded = 0;

                while (daycount >= 0)
                {
                    //DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                    ////string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                    //AutoDTable.Row.Add(Convert.ToString(dayy.ToShortDateString()));

                    daycount -= 1;
                    daysAdded += 1;
                }

                AutoDTable.Columns.Add("Date");
                AutoDTable.Columns.Add("1st IN");
                AutoDTable.Columns.Add("1st OUT");
                //AutoDTable.Columns.Add("1st Total Hours");
                //AutoDTable.Columns.Add("2nd IN");
                //AutoDTable.Columns.Add("2nd OUT");
                //AutoDTable.Columns.Add("2nd Total Hours");
                AutoDTable.Columns.Add("Final Total Hours");
                //AutoDTable.Columns.Add("Hours");
                //AutoDTable.Columns.Add("OT_Hours");

                EmpName = "";

                SSQL = "select MachineID,isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as FirstName,ExistingCode,DeptName from Employee_Mst";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";

                if (EmpNo != "")
                {
                    SSQL = SSQL + " And MachineID='" + EmpNo + "'";
                }

                // SSQL = SSQL + " Group by MachineID,FirstName,ExistingCode,DeptName order by MachineID";
                dt = MasterDate.GetDetails(SSQL);

                if (dt.Rows.Count > 0)
                {

                    EmpName = dt.Rows[0]["FirstName"].ToString();

                }

                intK = 0;

                int SNo = 1;

                for (int i = 0; i < dt.Rows.Count; i++)
                {



                    string MachineID = dt.Rows[i]["MachineID"].ToString();

                    //AutoDTable.Rows[intK]["SNo"] = SNo;
                    //AutoDTable.Rows[intK]["EmployeeNo"] = dt.Rows[i]["MachineID"].ToString();
                    //AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
                    //AutoDTable.Rows[intK]["Name"] = dt.Rows[i]["FirstName"].ToString();
                    //AutoDTable.Rows[intK]["DeptName"] = dt.Rows[i]["DeptName"].ToString();


                    int count = 5;

                    for (int j = 0; j < daysAdded; j++)
                    {
                        AutoDTable.NewRow();
                        AutoDTable.Rows.Add();

                        DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                        string Date1 = dayy.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                        if (SessionUserName == "IF")
                        {
                            SSQL = "select TimeIN,TimeOUT,Total_Hrs1,OTHours from IFLogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(Date1).ToString("yyyy/MM/dd") + "',120)";

                        }
                        else
                        {
                            SSQL = "select TimeIN,TimeOUT,Total_Hrs1,OTHours from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(Date1).ToString("yyyy/MM/dd") + "',120)";
                        }

                        dt1 = MasterDate.GetDetails(SSQL);

                        if (dt1.Rows.Count > 0)
                        {

                            AutoDTable.Rows[intK]["Date"] = Date1;
                            AutoDTable.Rows[intK]["1st IN"] = dt1.Rows[0]["TimeIN"].ToString();
                            AutoDTable.Rows[intK]["1st OUT"] = dt1.Rows[0]["TimeOUT"].ToString();
                            //AutoDTable.Rows[intK]["1st Total Hours"] = dt1.Rows[0]["Attn_Date_Str"].ToString();
                            AutoDTable.Rows[intK]["Final Total Hours"] = dt1.Rows[0]["Total_Hrs1"].ToString();
                            //AutoDTable.Rows[intK]["OT_Hours"] = dt1.Rows[0]["OTHours"].ToString();
                            intK = intK + 1;
                        }
                        else
                        {


                            AutoDTable.Rows[intK]["Date"] = Date1;
                            AutoDTable.Rows[intK]["1st IN"] = 0;
                            AutoDTable.Rows[intK]["1st OUT"] = 0;
                            //AutoDTable.Rows[intK]["1st Total Hours"] = 0;
                            AutoDTable.Rows[intK]["Final Total Hours"] = 0;
                            //AutoDTable.Rows[intK]["OT_Hours"] = 0;
                            intK = intK + 1;
                        }

                    }

                    //AutoDTable.Rows[intK]["Total Days"] = daysAdded.ToString();



                    SNo = SNo + 1;

                }

                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=EmployeeWise.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
                Response.Write(" &nbsp;&nbsp;");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">EMPLOYEE WISE DAY ATTENDANCE BETWEEN DATES</a>");
                Response.Write(" &nbsp;&nbsp;&nbsp; ");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">From:" + FromDate + "</a>");
                Response.Write(" &nbsp;-- &nbsp;");
                Response.Write("<a style=\"font-weight:bold\">To:" + ToDate + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write(" <a style=\"font-weight:bold\">EMPCODE: ");
                Response.Write("<a style=\"font-weight:bold\">" + EmpNo + "</a>");
                Response.Write(" &nbsp; &nbsp; &nbsp; ");
                Response.Write("<a style=\"font-weight:bold\">EMPNAME: ");
                Response.Write("<a style=\"font-weight:bold\">" + EmpName + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
        }

        public void NonAdminGetEmployeeWiseDayAyyendanceBTDates_Change(string EmpNo, string FromDate, string ToDate)
        {
            double Total_Time_get;

            DataTable dt1 = new DataTable();

            DataTable dt = new DataTable();

            string EmpName;

            date1 = Convert.ToDateTime(FromDate);
            string dat = ToDate;
            Date2 = Convert.ToDateTime(dat);

            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;

            while (daycount >= 0)
            {
                //DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                ////string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                //AutoDTable.Row.Add(Convert.ToString(dayy.ToShortDateString()));

                daycount -= 1;
                daysAdded += 1;
            }

            AutoDTable.Columns.Add("Date");
            AutoDTable.Columns.Add("1st IN");
            AutoDTable.Columns.Add("1st OUT");
            //AutoDTable.Columns.Add("1st Total Hours");
            //AutoDTable.Columns.Add("2nd IN");
            //AutoDTable.Columns.Add("2nd OUT");
            //AutoDTable.Columns.Add("2nd Total Hours");
            AutoDTable.Columns.Add("Final Total Hours");
            //AutoDTable.Columns.Add("Hours");
            //AutoDTable.Columns.Add("OT_Hours");

            EmpName = "";

            SSQL = "select MachineID,isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as FirstName,ExistingCode,DeptName from Employee_Mst";
            SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";

            if (EmpNo != "")
            {
                SSQL = SSQL + " And MachineID='" + EmpNo + "'";
            }

            // SSQL = SSQL + " Group by MachineID,FirstName,ExistingCode,DeptName order by MachineID";
            dt = MasterDate.GetDetails(SSQL);

            if (dt.Rows.Count > 0)
            {

                EmpName = dt.Rows[0]["FirstName"].ToString();

            }

            intK = 0;

            int SNo = 1;

            for (int i = 0; i < dt.Rows.Count; i++)
            {



                string MachineID = dt.Rows[i]["MachineID"].ToString();

                //AutoDTable.Rows[intK]["SNo"] = SNo;
                //AutoDTable.Rows[intK]["EmployeeNo"] = dt.Rows[i]["MachineID"].ToString();
                //AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
                //AutoDTable.Rows[intK]["Name"] = dt.Rows[i]["FirstName"].ToString();
                //AutoDTable.Rows[intK]["DeptName"] = dt.Rows[i]["DeptName"].ToString();


                int count = 5;

                for (int j = 0; j < daysAdded; j++)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();

                    DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                    string Date1 = dayy.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                    if (SessionUserName == "IF")
                    {
                        SSQL = "select TimeIN,TimeOUT,Total_Hrs1,OTHours from IFLogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(Date1).ToString("yyyy/MM/dd") + "',120)";

                    }
                    else
                    {
                        SSQL = "select TimeIN,TimeOUT,Total_Hrs1,OTHours from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(Date1).ToString("yyyy/MM/dd") + "',120)";
                    }
                    dt1 = MasterDate.GetDetails(SSQL);

                    if (dt1.Rows.Count > 0)
                    {

                        AutoDTable.Rows[intK]["Date"] = Date1;
                        AutoDTable.Rows[intK]["1st IN"] = dt1.Rows[0]["TimeIN"].ToString();
                        AutoDTable.Rows[intK]["1st OUT"] = dt1.Rows[0]["TimeOUT"].ToString();
                        //AutoDTable.Rows[intK]["1st Total Hours"] = dt1.Rows[0]["Attn_Date_Str"].ToString();
                        AutoDTable.Rows[intK]["Final Total Hours"] = dt1.Rows[0]["Total_Hrs1"].ToString();
                        //AutoDTable.Rows[intK]["OT_Hours"] = dt1.Rows[0]["OTHours"].ToString();
                        intK = intK + 1;
                    }
                    else
                    {


                        AutoDTable.Rows[intK]["Date"] = Date1;
                        AutoDTable.Rows[intK]["1st IN"] = 0;
                        AutoDTable.Rows[intK]["1st OUT"] = 0;
                        //AutoDTable.Rows[intK]["1st Total Hours"] = 0;
                        AutoDTable.Rows[intK]["Final Total Hours"] = 0;
                        //AutoDTable.Rows[intK]["OT_Hours"] = 0;
                        intK = intK + 1;
                    }

                }

                //AutoDTable.Rows[intK]["Total Days"] = daysAdded.ToString();



                SNo = SNo + 1;

            }

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=EmployeeWise.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
            Response.Write(" &nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">EMPLOYEE WISE DAY ATTENDANCE BETWEEN DATES</a>");
            Response.Write(" &nbsp;&nbsp;&nbsp; ");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">From:" + FromDate + "</a>");
            Response.Write(" &nbsp;-- &nbsp;");
            Response.Write("<a style=\"font-weight:bold\">To:" + ToDate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write(" <a style=\"font-weight:bold\">EMPCODE: ");
            Response.Write("<a style=\"font-weight:bold\">" + EmpNo + "</a>");
            Response.Write(" &nbsp; &nbsp; &nbsp; ");
            Response.Write("<a style=\"font-weight:bold\">EMPNAME: ");
            Response.Write("<a style=\"font-weight:bold\">" + EmpName + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }


        // GetEmployeeWiseDayAyyendanceBTDates End







        public static string UTF8Decryption(string encryptpwd)
        {
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }

        public static string UTF8Encryption(string password)
        {
            string strmsg = string.Empty;
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            strmsg = Convert.ToBase64String(encode);
            return strmsg;
        }

        public string Left_Val(string Value, int Length)
        {
            if (Value.Length >= Length)
            {
                return Value.Substring(0, Length);
            }
            else
            {
                return Value;
            }
        }

        private static void DrawLine(PdfWriter writer, float x1, float y1, float x2, float y2)
        {
            PdfContentByte contentByte = writer.DirectContent;
            //contentByte.SetColorStroke(color);
            contentByte.MoveTo(x1, y1);
            contentByte.LineTo(x2, y2);
            contentByte.Stroke();
        }

        private static PdfPCell PhraseCell(Phrase phrase, int align)
        {
            PdfPCell cell = new PdfPCell(phrase);
            //cell.BorderColor = Color.WHITE;
            //cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingBottom = 2f;
            cell.PaddingTop = 0f;
            return cell;
        }

        protected void UploadDataTableToExcel(DataTable dtRecords)
        {
            string XlsPath = Server.MapPath(@"~/Add_data/EmployeePayroll.xls");
            string attachment = string.Empty;
            if (XlsPath.IndexOf("\\") != -1)
            {
                string[] strFileName = XlsPath.Split(new char[] { '\\' });
                attachment = "attachment; filename=" + strFileName[strFileName.Length - 1];
            }
            else
                attachment = "attachment; filename=" + XlsPath;
            try
            {
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/vnd.ms-excel";
                string tab = string.Empty;

                foreach (DataColumn datacol in dtRecords.Columns)
                {
                    Response.Write(tab + datacol.ColumnName);
                    tab = "\t";
                }
                Response.Write("\n");

                foreach (DataRow dr in dtRecords.Rows)
                {
                    tab = "";
                    for (int j = 0; j < dtRecords.Columns.Count; j++)
                    {
                        Response.Write(tab + Convert.ToString(dr[j]));
                        tab = "\t";
                    }

                    Response.Write("\n");
                }
                Response.End();
            }
            catch (Exception ex)
            {
                //Response.Write(ex.Message);
            }
        }
        public void GetImproperPunch(string FromDate)
        {

            if (SessionUserID == "2")
            {
                NonAdminGetImproperPunch_Changes(FromDate);
            }
            else
            {

                DataTable dt = new DataTable();
                DataTable dt1 = new DataTable();
                DataTable dt2 = new DataTable();

                double Count = 0;
                double Count1;

                DateTime dayy = Convert.ToDateTime(FromDate);

                //string Date1 = dayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);


                string SSQL = "";

                if (SessionUserName == "IF")
                {

                    SSQL = " select MachineID,DeptName,ExistingCode,isnull(FirstName,'') from Employee_Mst where IsActive='Yes' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    dt2 = ManualAttend.GetDetails(SSQL);


                    SSQL = "";


                    SSQL = "select MachineID,DeptName,ExistingCode,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT";
                    SSQL = SSQL + " from IFLogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

                    //if (ShiftType != "0")
                    //{
                    //    SSQL = SSQL + " And Shift='" + ShiftType + "'";
                    //}

                    if (FromDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(dayy).ToString("yyyy/MM/dd") + "',120)";
                    }

                    SSQL = SSQL + " And TYPENAME='Proper'";

                    dt = ManualAttend.GetDetails(SSQL);

                    SSQL = "select LD.MachineID,DM.DeptName,LD.ExistingCode,LD.Shift,isnull(LD.FirstName,'') as FirstName,LD.TimeIN,LD.TimeOUT";
                    SSQL = SSQL + " from IFLogTime_Days LD  inner join Department_Mst DM on LD.DeptName = DM.DeptCode";
                    SSQL = SSQL + " where LD.CompCode='" + SessionCcode + "' ANd LD.LocCode='" + SessionLcode + "'";
                    //if (ShiftType != "0")
                    //{
                    //    SSQL = SSQL + " And Shift='" + ShiftType + "'";
                    //}

                    if (FromDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(dayy).ToString("yyyy/MM/dd") + "',120)";
                    }

                    SSQL = SSQL + " And LD.TYPENAME='Improper'";


                    dt1 = ManualAttend.GetDetails(SSQL);
                }



                else
                {
                    SSQL = " select MachineID,DeptName,ExistingCode,isnull(FirstName,'') from Employee_Mst where IsActive='Yes' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    dt2 = ManualAttend.GetDetails(SSQL);


                    SSQL = "";


                    SSQL = "select MachineID,DeptName,ExistingCode,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT";
                    SSQL = SSQL + " from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

                    //if (ShiftType != "0")
                    //{
                    //    SSQL = SSQL + " And Shift='" + ShiftType + "'";
                    //}

                    if (FromDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(dayy).ToString("yyyy/MM/dd") + "',120)";
                    }

                    SSQL = SSQL + " And TYPENAME='Proper'";

                    dt = ManualAttend.GetDetails(SSQL);

                    SSQL = "select LD.MachineID,DM.DeptName,LD.ExistingCode,LD.Shift,isnull(LD.FirstName,'') as FirstName,LD.TimeIN,LD.TimeOUT";
                    SSQL = SSQL + " from LogTime_Days LD  inner join Department_Mst DM on LD.DeptName = DM.DeptCode";
                    SSQL = SSQL + " where LD.CompCode='" + SessionCcode + "' ANd LD.LocCode='" + SessionLcode + "'";
                    //if (ShiftType != "0")
                    //{
                    //    SSQL = SSQL + " And Shift='" + ShiftType + "'";
                    //}

                    if (FromDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(dayy).ToString("yyyy/MM/dd") + "',120)";
                    }

                    SSQL = SSQL + " And LD.TYPENAME='Improper'";


                    dt1 = ManualAttend.GetDetails(SSQL);
                }

                //for (int j = 0; j < dt.Rows.Count; j++)
                //{
                //    string IN = dt.Rows[j]["TimeIN"].ToString();
                //    string OUT = dt.Rows[j]["TimeOUT"].ToString();


                //    if (IN == "" && OUT == "" || IN == "NULL" && OUT == "NULL")
                //    {

                //    }
                //    else if (IN == "" || OUT == "")
                //    {
                //        Count = Count + 1;
                //    }

                //}

                Document document = new Document(PageSize.A4, 40, 30, 40, 40);
                Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

                //writer.PageEvent = new HeaderFooter();
                document.Open();


                PdfPTable table = new PdfPTable(5);
                float[] widths = new float[] { 38f, 5f, 40f, 10f, 12f };
                table.SetWidths(widths);
                table.WidthPercentage = 100;

                PdfPTable table1 = new PdfPTable(6);
                float[] widths1 = new float[] { 15f, 15f, 15f, 35f, 15f, 15f };
                table1.SetWidths(widths1);
                table1.WidthPercentage = 100;

                PdfPTable table2 = new PdfPTable(4);
                float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
                table2.SetWidths(widths2);
                table2.WidthPercentage = 100;

                PdfPCell cell;

                cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("IMPROPER PUNCHES REPORT‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ‎‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 3f;
                cell.PaddingBottom = 3f;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                //cell = PhraseCell(new Phrase("Number of Emplyee Worked: ‎" + dt.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                //cell.Border = 0;
                ////cell.Colspan = 2;
                //cell.BackgroundColor = BaseColor.YELLOW;
                //cell.PaddingTop = 5f;
                //cell.PaddingBottom = 5f;
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //table.AddCell(cell);

                cell = PhraseCell(new Phrase("‎ ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                //cell.Colspan = 3;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("‎ ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                //cell.Colspan = 3;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("‎ ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                //cell.Colspan = 3;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("ShiftDate: ‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                //cell.Colspan = 2;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ‎" + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                //cell.Colspan = 2;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                // Total Number of Employees‎
                cell = PhraseCell(new Phrase("Total Number of Employees                       : ‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                //cell.Colspan = 3;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ‎" + dt2.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                //cell.Colspan = 2;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("‎ ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                //cell.Colspan = 3;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 2;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                // Total Number of Proper Punches‎

                cell = PhraseCell(new Phrase("Total Number of Proper Punches              : ‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                //cell.Colspan = 3;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);


                cell = PhraseCell(new Phrase(" ‎" + (Convert.ToDouble(dt.Rows.Count)).ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.BackgroundColor = BaseColor.GREEN;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("‎ ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                //cell.Colspan = 3;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 2;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                // Total Number of Improper Punches

                cell = PhraseCell(new Phrase("Total Number of Improper Punches         : ‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                //cell.Colspan = 3;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);


                cell = PhraseCell(new Phrase(" ‎" + Convert.ToDouble(dt1.Rows.Count).ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.BackgroundColor = BaseColor.RED;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("‎ ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                //cell.Colspan = 3;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ‎‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 2;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                //cell = PhraseCell(new Phrase(" ‎‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                //cell.Border = 0;
                //cell.Colspan = 5;
                //cell.PaddingTop = 3f;
                //cell.PaddingBottom = 3f;
                //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                //table.AddCell(cell);

                cell = PhraseCell(new Phrase("EmpCode", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 0;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("‎ExCode‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 0;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("Dept‎. ‎Name‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 0;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("‎Name‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 0;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("‎TimeIN‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 0;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase("TimeOUT‎‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 0;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                Count1 = 0;

                for (int i = 0; i < dt1.Rows.Count; i++)
                {

                    string IN = dt1.Rows[i]["TimeIN"].ToString();
                    string OUT = dt1.Rows[i]["TimeOUT"].ToString();


                    //if (IN == "" && OUT == "" || IN == "NULL" && OUT == "NULL")
                    //{

                    //}
                    //else if (IN == "" || OUT == "")
                    if (IN == "" || OUT == "")
                    {
                        cell = PhraseCell(new Phrase(" ‎" + dt1.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.Colspan = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase("‎ " + dt1.Rows[i]["ExistingCode"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.Colspan = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase("‎ " + dt1.Rows[i]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.Colspan = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase("‎ " + dt1.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.Colspan = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.PaddingLeft = 3f;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase("‎ " + dt1.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.Colspan = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);

                        cell = PhraseCell(new Phrase("‎ " + dt1.Rows[i]["TimeOUT"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = 0;
                        cell.Colspan = 0;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table1.AddCell(cell);
                    }

                    Count1 = Count1 + 1;

                }

                cell = PhraseCell(new Phrase("Grand Total  ‎:‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table1.AddCell(cell);

                cell = PhraseCell(new Phrase(" ‎" + Count1, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                cell.Colspan = 0;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table1.AddCell(cell);

                table.SpacingBefore = 10f;
                table.SpacingAfter = 10f;

                document.Add(table);

                table1.SpacingBefore = 10f;
                table1.SpacingAfter = 10f;

                document.Add(table1);


                document.Close();

                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=IMPROPER_PUNCH.pdf");
                Response.ContentType = "application/pdf";
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(bytes);
                Response.End();
                Response.Close();

            }


        }


        public void NonAdminGetImproperPunch_Changes(string FromDate)
        {

            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            double Count = 0;
            double Count1;

            DateTime dayy = Convert.ToDateTime(FromDate);

            //string Date1 = dayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);


            string SSQL = "";
            if (SessionUserName == "IF")
            {

                SSQL = " select MachineID,DeptName,ExistingCode,isnull(FirstName,'') from Employee_Mst where IsActive='Yes' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                dt2 = ManualAttend.GetDetails(SSQL);


                SSQL = "";


                SSQL = "select MachineID,DeptName,ExistingCode,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT";
                SSQL = SSQL + " from IFLogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

                //if (ShiftType != "0")
                //{
                //    SSQL = SSQL + " And Shift='" + ShiftType + "'";
                //}

                if (FromDate != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(dayy).ToString("yyyy/MM/dd") + "',120)";
                }

                SSQL = SSQL + " And TYPENAME='Proper'";

                dt = ManualAttend.GetDetails(SSQL);

                SSQL = "select LD.MachineID,DM.DeptName,LD.ExistingCode,LD.Shift,isnull(LD.FirstName,'') as FirstName,LD.TimeIN,LD.TimeOUT";
                SSQL = SSQL + " from IFLogTime_Days LD  inner join Department_Mst DM on LD.DeptName = DM.DeptCode";
                SSQL = SSQL + " where LD.CompCode='" + SessionCcode + "' ANd LD.LocCode='" + SessionLcode + "'";
                //if (ShiftType != "0")
                //{
                //    SSQL = SSQL + " And Shift='" + ShiftType + "'";
                //}

                if (FromDate != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(dayy).ToString("yyyy/MM/dd") + "',120)";
                }

                SSQL = SSQL + " And LD.TYPENAME='Improper'";


                dt1 = ManualAttend.GetDetails(SSQL);
            }



            else
            {
                SSQL = " select MachineID,DeptName,ExistingCode,isnull(FirstName,'') from Employee_Mst where IsActive='Yes' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                dt2 = ManualAttend.GetDetails(SSQL);


                SSQL = "";


                SSQL = "select MachineID,DeptName,ExistingCode,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT";
                SSQL = SSQL + " from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "'";

                //if (ShiftType != "0")
                //{
                //    SSQL = SSQL + " And Shift='" + ShiftType + "'";
                //}

                if (FromDate != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(dayy).ToString("yyyy/MM/dd") + "',120)";
                }

                SSQL = SSQL + " And TYPENAME='Proper'";

                dt = ManualAttend.GetDetails(SSQL);

                SSQL = "select LD.MachineID,DM.DeptName,LD.ExistingCode,LD.Shift,isnull(LD.FirstName,'') as FirstName,LD.TimeIN,LD.TimeOUT";
                SSQL = SSQL + " from LogTime_Days LD  inner join Department_Mst DM on LD.DeptName = DM.DeptCode";
                SSQL = SSQL + " where LD.CompCode='" + SessionCcode + "' ANd LD.LocCode='" + SessionLcode + "'";
                //if (ShiftType != "0")
                //{
                //    SSQL = SSQL + " And Shift='" + ShiftType + "'";
                //}

                if (FromDate != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,120)= CONVERT(DATETIME,'" + Convert.ToDateTime(dayy).ToString("yyyy/MM/dd") + "',120)";
                }

                SSQL = SSQL + " And LD.TYPENAME='Improper'";


                dt1 = ManualAttend.GetDetails(SSQL);
            }


            //for (int j = 0; j < dt.Rows.Count; j++)
            //{
            //    string IN = dt.Rows[j]["TimeIN"].ToString();
            //    string OUT = dt.Rows[j]["TimeOUT"].ToString();


            //    if (IN == "" && OUT == "" || IN == "NULL" && OUT == "NULL")
            //    {

            //    }
            //    else if (IN == "" || OUT == "")
            //    {
            //        Count = Count + 1;
            //    }

            //}

            Document document = new Document(PageSize.A4, 40, 30, 40, 40);
            Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            //writer.PageEvent = new HeaderFooter();
            document.Open();


            PdfPTable table = new PdfPTable(5);
            float[] widths = new float[] { 38f, 5f, 40f, 10f, 12f };
            table.SetWidths(widths);
            table.WidthPercentage = 100;

            PdfPTable table1 = new PdfPTable(6);
            float[] widths1 = new float[] { 15f, 15f, 15f, 35f, 15f, 15f };
            table1.SetWidths(widths1);
            table1.WidthPercentage = 100;

            PdfPTable table2 = new PdfPTable(4);
            float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            table2.SetWidths(widths2);
            table2.WidthPercentage = 100;

            PdfPCell cell;

            cell = PhraseCell(new Phrase("" + SessionCcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.Colspan = 5;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("" + SessionLcode, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.Colspan = 5;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("IMPROPER PUNCHES REPORT‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.Colspan = 5;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase(" ‎‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.Colspan = 5;
            cell.PaddingTop = 3f;
            cell.PaddingBottom = 3f;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Number of Emplyee Worked: ‎" + dt.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            //cell.Colspan = 2;
            cell.BackgroundColor = BaseColor.YELLOW;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("‎ ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            //cell.Colspan = 3;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("‎ ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            //cell.Colspan = 3;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("ShiftDate: ‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            //cell.Colspan = 2;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase(" ‎" + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            //cell.Colspan = 2;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.AddCell(cell);

            // Total Number of Employees‎
            cell = PhraseCell(new Phrase("Total Number of Employees                       : ‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            //cell.Colspan = 3;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase(" ‎" + dt.Rows.Count, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            //cell.Colspan = 2;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("‎ ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            //cell.Colspan = 3;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase(" ‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            // Total Number of Proper Punches‎

            cell = PhraseCell(new Phrase("Total Number of Proper Punches              : ‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            //cell.Colspan = 3;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.AddCell(cell);


            cell = PhraseCell(new Phrase(" ‎" + (Convert.ToDouble(dt.Rows.Count) - Convert.ToDouble(dt1.Rows.Count)).ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.BackgroundColor = BaseColor.GREEN;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("‎ ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            //cell.Colspan = 3;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase(" ‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            // Total Number of Improper Punches

            cell = PhraseCell(new Phrase("Total Number of Improper Punches         : ‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            //cell.Colspan = 3;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.AddCell(cell);


            cell = PhraseCell(new Phrase(" ‎" + Convert.ToDouble(dt1.Rows.Count).ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.BackgroundColor = BaseColor.RED;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("‎ ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            //cell.Colspan = 3;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase(" ‎‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            //cell = PhraseCell(new Phrase(" ‎‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //cell.Border = 0;
            //cell.Colspan = 5;
            //cell.PaddingTop = 3f;
            //cell.PaddingBottom = 3f;
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //table.AddCell(cell);

            cell = PhraseCell(new Phrase("EmpCode", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.Colspan = 0;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table1.AddCell(cell);

            cell = PhraseCell(new Phrase("‎ExCode‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.Colspan = 0;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table1.AddCell(cell);

            cell = PhraseCell(new Phrase("Dept‎. ‎Name‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.Colspan = 0;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table1.AddCell(cell);

            cell = PhraseCell(new Phrase("‎Name‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.Colspan = 0;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table1.AddCell(cell);

            cell = PhraseCell(new Phrase("‎TimeIN‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.Colspan = 0;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table1.AddCell(cell);

            cell = PhraseCell(new Phrase("TimeOUT‎‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.Colspan = 0;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table1.AddCell(cell);

            Count1 = 0;

            for (int i = 0; i < dt1.Rows.Count; i++)
            {

                string IN = dt1.Rows[i]["TimeIN"].ToString();
                string OUT = dt1.Rows[i]["TimeOUT"].ToString();


                //if (IN == "" && OUT == "" || IN == "NULL" && OUT == "NULL")
                //{

                //}
                //else if (IN == "" || OUT == "")
                if (IN == "" || OUT == "")
                {
                    cell = PhraseCell(new Phrase(" ‎" + dt1.Rows[i]["MachineID"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("‎ " + dt1.Rows[i]["ExistingCode"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("‎ " + dt1.Rows[i]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("‎ " + dt1.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("‎ " + dt1.Rows[i]["TimeIN"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);

                    cell = PhraseCell(new Phrase("‎ " + dt1.Rows[i]["TimeOUT"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 0;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell);
                }

                Count1 = Count1 + 1;

            }

            cell = PhraseCell(new Phrase("Grand Total  ‎:‎", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = 0;
            cell.Colspan = 5;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table1.AddCell(cell);

            cell = PhraseCell(new Phrase(" ‎" + Count1, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
            cell.Colspan = 0;
            cell.PaddingTop = 5f;
            cell.PaddingBottom = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table1.AddCell(cell);

            table.SpacingBefore = 10f;
            table.SpacingAfter = 10f;

            document.Add(table);

            table1.SpacingBefore = 10f;
            table1.SpacingAfter = 10f;

            document.Add(table1);


            document.Close();

            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=IMPROPER_PUNCH.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();


        }





        public ActionResult ImproperPunch(string FromDate)
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                GetImproperPunch(FromDate);

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public JsonResult AttendanceConversion(string FromDate,string ToDate,string WagesType)
        {
            DataTable SHift = new DataTable();
            string Year = "";
            string Month = "";
            string SSQL = "";
            DataTable DT = new DataTable();

            List<ManualAttend> empList = new List<ManualAttend>();
            date1 = Convert.ToDateTime(FromDate);
            Date2 = Convert.ToDateTime(ToDate);
            int daycount = (int)((Date2 - date1).TotalDays);
            string Mn = Convert.ToDateTime(FromDate).Month.ToString();

            string yy = Convert.ToDateTime(FromDate).Year.ToString();


            SSQL = "select EM.EmpNo,LD.ExistingCode,LD.DeptName,sum(LD.Present) as Present,sum(LD.Wh_Count) as Wh_Count,sum(LD.NFH_Count) as NFH_Count,";
            SSQL = SSQL + "sum(LD.Wh_Present_Count) as Wh_Present_Count,sum(LD.NFH_Present_Count) as NFH_Present_Count";
            SSQL = SSQL + " from Employee_Mst EM inner join LogTime_Days LD on EM.EmpNo=LD.MachineID where LD.CompCode='" + SessionCcode + "' And LD.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And CONVERT(datetime,LD.Attn_Date_Str,120) >= CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            SSQL = SSQL + " And CONVERT(datetime,LD.Attn_Date_Str,120) <= CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
            DT = ManualAttend.GetDetails(SSQL);


            return Json(empList, JsonRequestBehavior.AllowGet);

        }


    }
}