﻿using AttendanceMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using iTextSharp.text;
using iTextSharp.text.pdf;
namespace AttendanceMVC.Controllers
{

    public class SalaryProcessController : Controller
    {
        static string Ccode = "";
        static string Lcode = "";
        static string SessionCcode = "";
        static string SessionLcode = "";
        static string SessionUserName;
        static string SessionUserID;
        System.Web.UI.WebControls.DataGrid grid =
                          new System.Web.UI.WebControls.DataGrid();

        string SSQL = "";

        // GET: SalaryProcess
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ManualDeduction()
        {
            if (Session["SessionCcode"] != null)
            {
                SessionCcode = Session["SessionCcode"].ToString();
                SessionLcode = Session["SessionLcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public JsonResult GetEmpID()
        {
            DataTable dt = new DataTable();
            string SQL = "Select EM.EmpNo,EM.ExistingCode,EM.FirstName,DM.DeptName from Employee_Mst EM inner join Department_Mst DM on EM.DeptName=DM.DeptCode where EM.IsActive='Yes' and EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "'";
            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> PartyList = new List<ManualAttend>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend Qtylist = new ManualAttend();
                Qtylist.EmpNo = dt.Rows[i]["EmpNo"].ToString();
                Qtylist.ExistingCode = dt.Rows[i]["ExistingCode"].ToString();
                Qtylist.EmpName = dt.Rows[i]["FirstName"].ToString();
                Qtylist.DeptName = dt.Rows[i]["DeptName"].ToString();
                PartyList.Add(Qtylist);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetEditEmpID(string EmpNo)
        {
            DataTable employees = new DataTable();

            string query = "select EM.EmpNo,EM.ExistingCode,(EM.FirstName + '.' + EM.MiddleInitial) as EmpName,DM.DeptName,";
            query = query + "DE.DesignName,EM.BasicSalary,";
            query = query + "MT.EmpType  from Employee_Mst EM inner join Department_Mst DM on EM.DeptName=DM.DeptCode";
            query = query + " inner join Designation_Mst DE on DE.DesignNo=EM.Designation";
            query = query + " inner join MstEmployeeType MT on MT.EmpTypeCd=EM.Wages where EM.IsActive='Yes' and EM.EmpNo='" + EmpNo + "'";
            query = query + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            query = query + " And DM.CompCode='" + SessionCcode + "' And DM.LocCode='" + SessionLcode + "'";
            query = query + " And DE.CompCode='" + SessionCcode + "' And DE.LocCode='" + SessionLcode + "'";

            employees = ManualAttend.GetDetails(query);

            List<ManualAttend> empList = new List<ManualAttend>();
            for (int i = 0; i < employees.Rows.Count; i++)
            {
                ManualAttend student = new ManualAttend();
                student.EmpNo = employees.Rows[i]["EmpNo"].ToString();
                student.ExistingCode = employees.Rows[i]["ExistingCode"].ToString();
                student.EmpName = employees.Rows[i]["EmpName"].ToString();
                student.DeptName = employees.Rows[i]["DeptName"].ToString();
                student.WagesType = employees.Rows[i]["EmpType"].ToString();
                student.LeaveDesc = employees.Rows[i]["DesignName"].ToString();
                student.BrokerID = employees.Rows[i]["BasicSalary"].ToString();


                empList.Add(student);
            }


            return Json(empList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveDeduction(ManualAttend EmpDetails)
        {
            string Query = "";
            string status = "Insert";
            DataTable dt_Save = new DataTable();


            SSQL = "";
            SSQL = SSQL + "select * from ManDeduction_Details where EmpNo='" + EmpDetails.EmpNo + "' and convert(datetime,FromDate,103)=convert(datetime,'" + EmpDetails.AttnDate + "',103) and ";
            SSQL = SSQL + "convert(datetime,ToDate,103)=convert(datetime,'" + EmpDetails.AttnDateStr + "',103) and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dt_Save = ManualAttend.GetDetails(SSQL);

            if (dt_Save.Rows.Count != 0)
            {
                SSQL = "";
                SSQL = SSQL + "delete from ManDeduction_Details where EmpNo='" + EmpDetails.EmpNo + "' and convert(datetime,FromDate,103)=convert(datetime,'" + EmpDetails.AttnDate + "',103) and ";
                SSQL = SSQL + "convert(datetime,ToDate,103)=convert(datetime,'" + EmpDetails.AttnDateStr + "',103) and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                ManualAttend.GetDetails(SSQL);

                status = "Update";
            }

            SSQL = "";
            SSQL = SSQL + "Insert into ManDeduction_Details(EmpNo,ExistingCode,EmpName,EmpType,Allowance1,Allowance2,";
            SSQL = SSQL + "Allowance3,Deduction1,Deduction2,Deduction3,OthersDeduction1,OthersDeduction2,FromDate,ToDate,CompCode,LocCode) values(";
            SSQL = SSQL + "'" + EmpDetails.EmpNo + "','" + EmpDetails.ExistingCode + "','" + EmpDetails.EmpName + "','" + EmpDetails.WagesType + "',";
            SSQL = SSQL + "'" + EmpDetails.Allowance3 + "','" + EmpDetails.Allowance4 + "','" + EmpDetails.Allowance5 + "','" + EmpDetails.Deduction3 + "',";
            SSQL = SSQL + "'" + EmpDetails.Deduction4 + "',0,0,'" + EmpDetails.Deduction5 + "',convert(datetime,'" + EmpDetails.AttnDate + "',103),";
            SSQL = SSQL + "convert(datetime,'" + EmpDetails.AttnDateStr + "',103),'" + SessionCcode + "','" + SessionLcode + "')";


            ManualAttend.GetDetails(SSQL);

            return Json(status);
        }

    }
}