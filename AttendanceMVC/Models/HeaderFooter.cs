﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttendanceMVC.Models
{
    public class HeaderFooter : IPdfPageEvent
    {
        public void OnChapter(PdfWriter writer, Document document, float paragraphPosition, Paragraph title)
        {
            //throw new NotImplementedException();
        }

        public void OnChapterEnd(PdfWriter writer, Document document, float paragraphPosition)
        {
            //throw new NotImplementedException();
        }

        public void OnCloseDocument(PdfWriter writer, Document document)
        {
            //throw new NotImplementedException();
        }

        public void OnEndPage(PdfWriter writer, Document document)
        {
            //throw new NotImplementedException();

            // Make your taPdfPTable footerTbl = new PdfPTable(1);
            Font font = FontFactory.GetFont("Arial", 9, Font.NORMAL);

            //tbl header
            PdfPTable headerTbl = new PdfPTable(1);

            //tbl footer
            PdfPTable footerTbl = new PdfPTable(3);

            float[] widths = new float[] { 50, 30, 20 };
            footerTbl.WidthPercentage = 100;

            //Header code
            float[] widths1 = new float[] { 50 };
            headerTbl.WidthPercentage = 100;

            headerTbl.TotalWidth = document.PageSize.Width - document.LeftMargin;
            iTextSharp.text.Rectangle page1 = document.PageSize;

            iTextSharp.text.Image foot1 = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~") + "/assets/img/logo.png");
            foot1.ScalePercent(40);

            headerTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell79 = new PdfPCell(foot1);
            cell79.HorizontalAlignment = Element.ALIGN_LEFT;
            cell79.VerticalAlignment = Element.ALIGN_BOTTOM;
            cell79.Border = 0;
            cell79.Colspan = 0;
            headerTbl.AddCell(cell79);
            //Header code

            //footer code
            // footerTbl.TotalWidth = document.PageSize.Width - document.LeftMargin ;
            footerTbl.TotalWidth = document.PageSize.Width - document.RightMargin;
            iTextSharp.text.Rectangle page = document.PageSize;



            //img footer Left Side
            iTextSharp.text.Image foot = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~") + "/assets/img/logo.png");
            foot.ScalePercent(40);

            footerTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell69 = new PdfPCell(foot);
            cell69.HorizontalAlignment = Element.ALIGN_LEFT;
            cell69.VerticalAlignment = Element.ALIGN_BOTTOM;
            cell69.Border = 0;
            cell69.Colspan = 0;
            footerTbl.AddCell(cell69);




            //page number in Center
            Chunk myFooter = new Chunk("Page" + (document.PageNumber), FontFactory.GetFont(FontFactory.HELVETICA_OBLIQUE, 12));
            PdfPCell footer = new PdfPCell(new Phrase(myFooter));
            footer.Border = 0;
            footer.HorizontalAlignment = Element.ALIGN_CENTER;
            footerTbl.AddCell(footer);


            //Datetime In Right Side
            DateTime dat = DateTime.Now;
            PdfPCell cell99 = new PdfPCell(new Phrase("Date :" + dat));
            cell99.Border = 0;
            cell99.Colspan = 0;
            cell99.Border = 0;
            cell99.HorizontalAlignment = Element.ALIGN_RIGHT;
            // cell99.VerticalAlignment = Element.ALIGN_BOTTOM;

            footerTbl.AddCell(cell99);
            //footer code


            //Adding Header
            headerTbl.WriteSelectedRows(0, -1, document.PageSize.GetLeft(20), document.PageSize.GetTop(15), writer.DirectContent);

            //Adding footer
            footerTbl.WriteSelectedRows(0, -1, 10, (document.BottomMargin + -15), writer.DirectContent);
        }

        public void OnGenericTag(PdfWriter writer, Document document, Rectangle rect, string text)
        {
            //throw new NotImplementedException();
        }

        public void OnOpenDocument(PdfWriter writer, Document document)
        {
            //throw new NotImplementedException();
        }

        public void OnParagraph(PdfWriter writer, Document document, float paragraphPosition)
        {
            //throw new NotImplementedException();
        }

        public void OnParagraphEnd(PdfWriter writer, Document document, float paragraphPosition)
        {
            //throw new NotImplementedException();
        }

        public void OnSection(PdfWriter writer, Document document, float paragraphPosition, int depth, Paragraph title)
        {
            //throw new NotImplementedException();
        }

        public void OnSectionEnd(PdfWriter writer, Document document, float paragraphPosition)
        {
            //throw new NotImplementedException();
        }

        public void OnStartPage(PdfWriter writer, Document document)
        {
            //throw new NotImplementedException();
        }
    }
}