﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttendanceMVC.Models
{
    public class PersonalDetails
    {
        public string ID { get; set; }
        public string MartialStatus { get; set; }
        public string Nationality { get; set; }
        public string Religion { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string PhyChallenged { get; set; }
        public string PhyReason { get; set; }
        public string StdWorkingHrs { get; set; }
        public string IncentAmt { get; set; }
        public string AgentName { get; set; }
        public string AgentType { get; set; }
        public string RecruitThrg { get; set; }
        public string RecruitMobile { get; set; }
        public string Nominee { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string GuardianName { get; set; }
        public string PermAddr { get; set; }
        public string PermTaluk { get; set; }
        public string PermDist { get; set; }
        public string TempAddr { get; set; }
        public string TempTaluk { get; set; }
        public string TempDist { get; set; }
        public string State { get; set; }
        public string IdenMark1 { get; set; }
        public string IdenMark2 { get; set; }
        public string ParentMob1 { get; set; }
        public string ParentMob2 { get; set; }
        public string DocType { get; set; }
        public string DocNo { get; set; }
        public string DocDesc { get; set; }
        public string BloodGrp { get; set; }
        public string WeekOff { get; set; }
        public string Community { get; set; }
        public string Caste { get; set; }
    }
}