﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AttendanceMVC.Models
{
    public class ManualAttend
    {
        static string connString = ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString; // Read the connection string from the web.config file
        public bool sessionValue { set; get; }
        public string ID { set; get; }
        public string EmpNo { set; get; }
        public string ExistingCode { set; get; }
        public string Machine_No { set; get; }

        public string DeptName { get; set; }
        public string EmpName { set; get; }
        public string AttnStatus { set; get; }
        public string AttnDate { set; get; }
        public string AttnDateStr { set; get; }
        public string TimeIn { set; get; }
        public string TimeOut { set; get; }
        public string LogTimeIn { set; get; }
        public string LogTimeOut { set; get; }
        public string Enter_Date { set; get; }
        public string UserName { set; get; }
        public string Password { set; get; }
        public string UserType { set; get; }

        public string WagesType { set; get; }
        public string ShiftType { set; get; }
        public string CodeVal { get; set; }
        public string TypeVal { get; set; }

        public string TotalDays { get; set; }

        public string LeaveType { get; set; }

        public string LeaveDesc { get; set; }

        // User Rights Variables 
        public string ModuleName { get; set; }
        public string MenuName { get; set; }
        public string FormName { get; set; }
        public string View { get; set; }


        public string Days { get; set; }
        public string Convey { get; set; }
        public string BasicSalary { get; set; }
        public string Amount { get; set; }
        public string HRA { get; set; }

        public string BrokerID { get;  set; }
        public string Allowance3 { set; get; }
        public string Allowance4 { set; get; }
        public string Allowance5 { set; get; }
        public string Deduction3 { set; get; }
        public string Deduction4 { set; get; }
        public string Deduction5 { set; get; }

        public List<ManualAttend> GetItemDet { get; set; }

        public static DataTable GetDetails(string query)
        {
            using (SqlConnection con = new SqlConnection(connString))
            {
                con.Open();

                //var query = "Select *from LogTime_Days where Present='1.0' and Attn_Date_Str='2017/02/28'";
                SqlCommand com = new SqlCommand(query, con);
                DataTable ds = new DataTable();

                SqlDataAdapter sda = new SqlDataAdapter(query, con);
                sda.Fill(ds);
                con.Close();
                return ds;
            }
        }
    }
}