﻿$(function () {
    $('#example1').DataTable();
    $('#example11').DataTable();
});

function LoadEmployeeDetails() {

   
     $.ajax({
        type: "GET",
        url: '/Employee/GetEmployeeApproval',
        success: function (data) {
            var data1 = $('#example1').DataTable();
            data1.clear();
            $.each(data, function (key, item) {
                data1.row.add([
                item.MachineID,
                item.ExistingCode,
                item.Name,
                item.DeptName,
                item.EmpType,
               "<button class='btn btn-success' type='button' onClick='ApproveEmployee(\"" + item.MachineID + "\")'><i class='fa fa-thumbs-up'></i></button>",
               "<button class='btn btn-warning' type='button' onClick='PendingEmployee(\"" + item.MachineID + "\")'><i class='fa fa-clock-o'></i></button>",
               "<button class='btn btn-danger' type='button' onClick='CancelEmployee(\"" + item.MachineID + "\")'><i class='fa fa-times-circle'></i></button>"
                ]).draw();
            });
        }
    });
}

function LoadEmployeeStatusDetails() {

   
     $.ajax({
        type: "GET",
        url: '/Employee/GetEmployeeStatus',
        success: function (data) {
            var data1 = $('#example11').DataTable();
            data1.clear();
            $.each(data, function (key, item) {
                data1.row.add([
                item.MachineID,
                item.ExistingCode,
                item.Name,
                item.DeptName,
                item.EmpType,
                item.AgentName
                ]).draw();
            });
        }
    });
}

$(document).ready(function () {
    LoadEmployeeDetails();
    LoadEmployeeStatusDetails();
});

function ApproveEmployee(index) {
    $.ajax({
        type: "GET",
        url: '/Employee/GetApproveEmployee/?ID=' + index + '',
        success: function (data) {
            if (data == "Approved") {
                LoadEmployeeDetails();
                alert('Approved Successfully..');              
            }           
        }
    });
}

function PendingEmployee(index) {
    $.ajax({
        type: "GET",
        url: '/Employee/GetPendingEmployee/?ID=' + index + '',
        success: function (data) {
            if (data == "Pending") {
                LoadEmployeeDetails();
                alert('Put it in Pending Successfully..');               
            }           
        }
    });
}

function CancelEmployee(index) {
    $.ajax({
        type: "GET",
        url: '/Employee/GetCancelEmployee/?ID=' + index + '',
        success: function (data) {
            if (data == "Cancel") {
                LoadEmployeeDetails();
                alert('Cancelled Successfully..');               
            }            
        }
    });
}