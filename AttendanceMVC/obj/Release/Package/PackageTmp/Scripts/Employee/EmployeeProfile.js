﻿
$(function () {

   
    //$('.select2').select2();
    //$('#GPINexample').DataTable();
    //$('#GoodsDelvexample').DataTable();
    $('#example1').DataTable();
    $('#example2').DataTable();
    $('#example3').DataTable();
    $('#example4').DataTable();
    $('#example5').DataTable();
    $('#example6').DataTable();
    $('#example7').DataTable();
    $('#example8').DataTable();
    $('#example9').DataTable();
    $('#example10').DataTable();
    $('#example11').DataTable();
    $('#example12').DataTable();
});


function ProgressBarShow() {
    //  alert("test");
    $('#Download_loader').show();
}

function ProgressBarHide() {
    $('#Download_loader').hide();
}

function ShowEligiblePF() {

    var PF = $('#ddlPFEligible').val();

    if (PF == "Yes") {

        $("#txtPFNo").removeAttr("disabled");
        $("#txtPFDate").removeAttr("disabled");
    }
    else {
        $("#txtPFNo").attr("disabled", "disabled");
        $("#txtPFDate").attr("disabled", "disabled");
    }
   
}
function ShowESIEligible() {

    var PF = $('#ddlESIEligible').val();

    if (PF == "Yes") {

        $("#txtESINo").removeAttr("disabled");
        $("#txtESIDate").removeAttr("disabled");
    }
    else {
        $("#txtESINo").attr("disabled", "disabled");
        $("#txtESIDate").attr("disabled", "disabled");
    }

}

function ShowCanteenEligible() {

    var PF = $('#ddlCanteenEligible').val();

    if (PF == "Yes") {

        $("#ddlCanteenName").removeAttr("disabled");
        $("#ddlCanteenOperator").removeAttr("disabled");
        $("#txtEmpAmt").removeAttr("disabled");
        $("#txtMgmAmt").removeAttr("disabled");
        LoadCanteen();
        $("#txtEmpAmt").val('0.0');
        $("#txtMgmAmt").val('0.0');
    }
    else {
        LoadCanteen();
        $("#ddlCanteenName").attr("disabled", "disabled");
        $("#ddlCanteenOperator").attr("disabled", "disabled");
        $("#txtEmpAmt").attr("disabled", "disabled");
        $("#txtMgmAmt").attr("disabled", "disabled");
        $("#txtEmpAmt").val('0.0');
        $("#txtMgmAmt").val('0.0');
    }

}
function ShowTeaIEligible() {

    var PF = $('#ddlTeaEligible').val();

    if (PF == "Yes") {

        $("#ddlTeaCanteenName").removeAttr("disabled");
        $("#txtTeaAmt").removeAttr("disabled");
        $("#txtTeaAmt").val('0.0');
        LoadTeaCanteen();
      
    }
    else {
        $("#ddlTeaCanteenName").attr("disabled", "disabled");
        $("#txtTeaAmt").attr("disabled", "disabled");
        $("#txtTeaAmt").val('0.0');
        LoadTeaCanteen();
    }

}

function LoadDepartment() {
    //alert('Dept');
   
    $.ajax({
        type: "GET",
        url: '/Employee/GetDepartment',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlDeptName').empty();
            $("#ddlDeptName").append($('<option></option>').val('').text('--Select Department--'));
            $('#ddlMstDept').empty();
            $("#ddlMstDept").append($('<option></option>').val('').text('-Select-'));
            $.each(data, function (i, val) {

                $("#ddlDeptName").append($('<option></option>').val(val.ID).text(val.Name));
                $("#ddlMstDept").append($('<option></option>').val(val.ID).text(val.Name));
            })
        }
    })
}

function LoadGrade() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Employee/GetGrade',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlGrade').empty();
            $("#ddlGrade").append($('<option></option>').val('0').text('--Select Grade--'));
            $.each(data, function (i, val) {

                $("#ddlGrade").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}
function LoadCate() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Employee/GetCateName',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlCategoryName').empty();
            $("#ddlCategoryName").append($('<option></option>').val('0').text('--Select Category--'));
            $.each(data, function (i, val) {

                $("#ddlCategoryName").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}
function LoadCanteen() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Employee/GetCanteenName',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlCanteenName').empty();
            $("#ddlCanteenName").append($('<option></option>').val('0').text('--Select Canteen--'));
            $.each(data, function (i, val) {

                $("#ddlCanteenName").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function LoadCanteenOpt() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Employee/GetCanteenNameOpt',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlCanteenOperator').empty();
            $("#ddlCanteenOperator").append($('<option></option>').val('0').text('--Select Operator--'));
            $.each(data, function (i, val) {

                $("#ddlCanteenOperator").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function LoadTeaCanteen() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Employee/GetCanteenName',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlTeaCanteenName').empty();
            $("#ddlTeaCanteenName").append($('<option></option>').val('0').text('--Select Canteen--'));
            $.each(data, function (i, val) {

                $("#ddlTeaCanteenName").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}
function LoadDesignation() {
    //alert('Dept');

    var DeptID = $('#ddlDeptName').val();
    //if (DeptID != '0') {
        //alert(DeptID);
        $.ajax({
            type: "GET",
            url: '/Employee/GetDeptDesignation',
            success: function (data) {
                //console.log(data);
                //Employee = data;
                $('#ddlDesignation').empty();
                $("#ddlDesignation").append($('<option></option>').val('0').text('--Select Designation--'));
                $.each(data, function (i, val) {

                    $("#ddlDesignation").append($('<option></option>').val(val.ID).text(val.Name));

                })
            }
        })
    //}
    //else {
    //    $('#ddlDesignation').empty();
    //    $("#ddlDesignation").append($('<option></option>').val('0').text('--Select Designation--'));
    //}
}


function LoadEmployeeType() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Employee/GetEmployeeType',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlEmpType').empty();
            $("#ddlEmpType").append($('<option></option>').val('').text('--Select Employee Type--'));
            $.each(data, function (i, val) {

                $("#ddlEmpType").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function LoadQualification() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Employee/GetQulification',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlQulify').empty();
            $("#ddlQulify").append($('<option></option>').val('0').text('--Select Qualification--'));
            $.each(data, function (i, val) {

                $("#ddlQulify").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function LoadBank() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Employee/GetBank',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlBankName').empty();
            $("#ddlBankName").append($('<option></option>').val('0').text('--Select Bank--'));
            $.each(data, function (i, val) {

                $("#ddlBankName").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function LoadCaste() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Employee/GetCaste',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlCaste').empty();
            $("#ddlCaste").append($('<option></option>').val('0').text('--Select Caste--'));
            $.each(data, function (i, val) {

                $("#ddlCaste").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function LoadCommunity() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Employee/GetCommunity',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlCommunity').empty();
            $("#ddlCommunity").append($('<option></option>').val('0').text('--Select Community--'));
            $.each(data, function (i, val) {

                $("#ddlCommunity").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function LoadAgent() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Employee/GetAgent',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlAgentName').empty();
            $("#ddlAgentName").append($('<option></option>').val('0').text('--Select Source--'));
            $.each(data, function (i, val) {

                $("#ddlAgentName").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function LoadDepartmentDataTable() {

    $.ajax({
        type: "GET",
        url: '/Employee/GetDepartment',
        success: function (data) {
            var data1 = $('#example1').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.ID,
                item.Name,
               "<button class='btn btn-success' type='button' onClick='EditDept(\"" + item.ID + "\")'><i class='fa fa-pencil'></i></button> <button class='btn btn-danger' type='button' onClick='DeleteDept(\"" + item.ID + "\")'><i class='fa fa-times-circle'></i></button>"
                ]).draw();
            });
        }
    });
}
function LoadCateTable() {

    $.ajax({
        type: "GET",
        url: '/Employee/GetCateName',
        success: function (data) {
            var data1 = $('#example9').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.ID,
                item.Name,
               "<button class='btn btn-success' type='button' onClick='EditCate(\"" + item.ID + "\")'><i class='fa fa-pencil'></i></button> <button class='btn btn-danger' type='button' onClick='DeleteCate(\"" + item.ID + "\")'><i class='fa fa-times-circle'></i></button>"
                ]).draw();
            });
        }
    });
}
function LoadCanteenTable() {

    $.ajax({
        type: "GET",
        url: '/Employee/GetCanteenName',
        success: function (data) {
            var data1 = $('#example10').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.ID,
                item.Name,
               "<button class='btn btn-success' type='button' onClick='EditCanteen(\"" + item.ID + "\")'><i class='fa fa-pencil'></i></button> <button class='btn btn-danger' type='button' onClick='DeleteCanteen(\"" + item.ID + "\")'><i class='fa fa-times-circle'></i></button>"
                ]).draw();
            });
        }
    });
}
function LoadCanteenOptTable() {

    $.ajax({
        type: "GET",
        url: '/Employee/GetCanteenNameOpt',
        success: function (data) {
            var data1 = $('#example12').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.ID,
                item.Name,
               "<button class='btn btn-success' type='button' onClick='EditCanteenOperator(\"" + item.ID + "\")'><i class='fa fa-pencil'></i></button> <button class='btn btn-danger' type='button' onClick='DeleteCanteenOperator(\"" + item.ID + "\")'><i class='fa fa-times-circle'></i></button>"
                ]).draw();
            });
        }
    });
}
function EditCanteenOperator(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetEditCanteenOperator/?ID=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;
                $("#txtCanteenOptID").val(item.ID);
                $("#txtCanteenNameOpt").val(item.Name);
             

            });

        }
    });

}
function DeleteCanteenOperator(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetDeleteCanteenOperator/?ID=' + index + '',
        success: function (data) {
            if (data == "Delete") {
                LoadCanteenOpt();
                LoadCanteenOptTable();
                alert('Deleted Successfully..');
                $('#modal-dialog-CanteenOpt').modal('hide');
            }
            else if (data == "Already") {

                alert('Operator Exists in Employee Details...');
                $('#modal-dialog-CanteenOpt').modal('hide');

            }
        }
    });
}
function LoadAgentTable() {

    $.ajax({
        type: "GET",
        url: '/Employee/GetAgentName',
        success: function (data) {
            var data1 = $('#example11').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.ID,
                item.Name,
                item.Deduction1,
                item.Deduction2,
               "<button class='btn btn-success' type='button' onClick='EditAgent(\"" + item.ID + "\")'><i class='fa fa-pencil'></i></button> <button class='btn btn-danger' type='button' onClick='DeleteAgent(\"" + item.ID + "\")'><i class='fa fa-times-circle'></i></button>"
                ]).draw();
            });
        }
    });
}
function EditAgent(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetEditAgent/?ID=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;
                $("#txtSourceID").val(item.ID);
                $("#txtSourceName").val(item.Name);
                $("#txt8HoursAmt").val(item.Deduction1);
                $("#txt12HoursAmt").val(item.Deduction2);

            });

        }
    });

}
function DeleteAgent(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetDeleteAgent/?ID=' + index + '',
        success: function (data) {
            if (data == "Delete") {
                LoadAgent();
                LoadAgentTable();
                alert('Deleted Successfully..');
                $('#modal-dialog-Source').modal('hide');
            }
            else if (data == "Already") {

                alert('Source Exists in Employee Details...');
                $('#modal-dialog-Source').modal('hide');

            }
        }
    });
}
function LoadGradeataTable() {

    $.ajax({
        type: "GET",
        url: '/Employee/GetGradeName',
        success: function (data) {
            var data1 = $('#example8').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.ID,
                item.Name,
               "<button class='btn btn-success' type='button' onClick='EditGrade(\"" + item.ID + "\")'><i class='fa fa-pencil'></i></button> <button class='btn btn-danger' type='button' onClick='DeleteGrade(\"" + item.ID + "\")'><i class='fa fa-times-circle'></i></button>"
                ]).draw();
            });
        }
    });
}
function EditGrade(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetEditGrade/?ID=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;
                $("#txtGradeID").val(item.ID);
                $("#txtGradeName").val(item.Name);

            });

        }
    });

}
function DeleteGrade(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetDeleteGrade/?ID=' + index + '',
        success: function (data) {
            if (data == "Delete") {
                LoadGrade();
                LoadGradeataTable();
                alert('Deleted Successfully..');
                $('#modal-dialog-Grade').modal('hide');
            }
            else if (data == "Already") {

                alert('Grade Exists in Employee Details...');
                $('#modal-dialog-Grade').modal('hide');

            }
        }
    });
}

function EditDept(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetEditDepartment/?ID=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;
                $("#txtDeptID").val(item.ID);
                $("#txtDept").val(item.Name);
                
            });

        }
    });

}


function DeleteDept(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetDeleteDepartment/?ID=' + index + '',
        success: function (data) {
            if (data == "Delete") {
                LoadDepartment();
                LoadDepartmentDataTable();
                alert('Deleted Successfully..');
                $('#modal-dialog').modal('hide');
            }
            else if (data == "Already") {
               
                alert('Department Exists in Employee Details...');
                $('#modal-dialog').modal('hide');

            }
        }
    });
}
function LoadDesignationDataTable() {

    $.ajax({
        type: "GET",
        url: '/Employee/GetDesignation',
        success: function (data) {
            var data1 = $('#example2').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.DeptName,
                item.ID,
                item.Name,
                item.AgentName,
               "<button class='btn btn-success' type='button' onClick='EditDesign(\"" + item.ID + "\")'><i class='fa fa-pencil'></i></button> <button class='btn btn-danger' type='button' onClick='DeleteDesign(\"" + item.ID + "\")'><i class='fa fa-times-circle'></i></button>"
                ]).draw();
            });
        }
    });
}


function LoadEmployeeTypeDataTable() {

    $.ajax({
        type: "GET",
        url: '/Employee/GetEmployeeType',
        success: function (data) {
            var data1 = $('#example3').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.ID,
                item.Name,
                item.Category,
               "<button class='btn btn-success' type='button' onClick='EditEmpType(\"" + item.ID + "\")'><i class='fa fa-pencil'></i></button> <button class='btn btn-danger' type='button' onClick='DeleteEmpType(\"" + item.ID + "\")'><i class='fa fa-times-circle'></i></button>"
                ]).draw();
            });
        }
    });
}

function EditEmpType(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetEditEmpType/?ID=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;
                $('#ddlMstCategory').val(item.Category).change();
                $("#txtEmpTypeID").val(item.ID);
                $("#txtEmpType").val(item.Name);
           
                  
            });

        }
    });

}

function DeleteEmpType(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetDeleteEmpType/?ID=' + index + '',
        success: function (data) {
            if (data == "Delete") {
                LoadEmployeeType();
                LoadEmployeeTypeDataTable();
                alert('Deleted Successfully..');
                $('#modal-dialog2').modal('hide');
            }
            else if (data == "Already") {
               
                alert('Employee Type Exists in Employee Details...');
                //$('#modal-dialog2').modal('hide');

            }
        }
    });
}
function EditDesign(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetEditDesignation/?ID=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;
                $("#txtMstDesignationID").val(item.ID);
                $("#txtMstDesignation").val(item.Name);
                $("#ddlMstDept").val(item.DeptName).change();
                $("#txtMstWorkerCode").val(item.AgentName);

            });
        }
    });

}


function DeleteDesign(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetDeleteDesignation/?ID=' + index + '',
        success: function (data) {
            if (data == "Delete") {
                LoadDesignation();
                LoadDesignationDataTable();
                alert('Deleted Successfully..');
                $('#modal-dialog2').modal('hide');
            }
            else if (data == "Already") {
               
                alert('Designation Exists in Employee Details...');
                //$('#modal-dialog2').modal('hide');

            }
        }
    });
}

function LoadQualificationDataTable() {

    $.ajax({
        type: "GET",
        url: '/Employee/GetQulification',
        success: function (data) {
            var data1 = $('#example4').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.ID,
                item.Name,
                "<button class='btn btn-success' type='button' onClick='EditQualification(\"" + item.ID + "\")'><i class='fa fa-pencil'></i></button> <button class='btn btn-danger' type='button' onClick='DeleteQualification(\"" + item.ID + "\")'><i class='fa fa-times-circle'></i></button>"
                ]).draw();
            });
        }
    });
}

function EditQualification(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetEditQualification/?ID=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;
                $("#txtMstQulificationID").val(item.ID);
                $("#txtMstQulification").val(item.Name);

            });

        }
    });

}

function DeleteQualification(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetDeleteQualification/?ID=' + index + '',
        success: function (data) {
            if (data == "Delete") {
                LoadQualification();
                LoadQualificationDataTable();
                alert('Deleted Successfully..');
                $('#modal-dialog4').modal('hide');
            }
            else if (data == "Already") {

                alert('Qualification Exists in Employee Details...');
                

            }
        }
    });
}

function LoadBankDataTable() {

    $.ajax({
        type: "GET",
        url: '/Employee/GetBank',
        success: function (data) {
            var data1 = $('#example5').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.ID,
                item.Name,
                item.Branch,
               "<button class='btn btn-success' type='button' onClick='EditBank(\"" + item.ID + "\")'><i class='fa fa-pencil'></i></button> <button class='btn btn-danger' type='button' onClick='DeleteBank(\"" + item.ID + "\")'><i class='fa fa-times-circle'></i></button>"
                ]).draw();
            });
        }
    });
}

function EditBank(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetEditBank/?ID=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;
                $("#txtMstBankNameID").val(item.ID);
                $("#txtMstBankName").val(item.Name);
                $("#txtMstBranchCode").val(item.Branch);

            });

        }
    });

}

function DeleteBank(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetDeleteBank/?ID=' + index + '',
        success: function (data) {
            if (data == "Delete") {
                LoadBank();
                LoadBankDataTable();
                alert('Deleted Successfully..');
                $('#modal-dialog12').modal('hide');
            }
            else if (data == "Already") {

                alert('Bank Exists in Employee Details...');


            }
        }
    });
}

function LoadCasteDataTable() {

    $.ajax({
        type: "GET",
        url: '/Employee/GetCaste',
        success: function (data) {
            var data1 = $('#example6').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.ID,
                item.Name,
               
               "<button class='btn btn-success' type='button' onClick='EditCaste(\"" + item.ID + "\")'><i class='fa fa-pencil'></i></button> <button class='btn btn-danger' type='button' onClick='DeleteCaste(\"" + item.ID + "\")'><i class='fa fa-times-circle'></i></button>"
                ]).draw();
            });
        }
    });
}

function EditCaste(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetEditCaste/?ID=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;
                $("#txtMstCasteID").val(item.ID);
                $("#txtMstCaste").val(item.Name);
             
            });

        }
    });

}

function DeleteCaste(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetDeleteCaste/?ID=' + index + '',
        success: function (data) {
            if (data == "Delete") {
                LoadCaste();
                LoadCasteDataTable();
                alert('Deleted Successfully..');
                $('#modal-dialog13').modal('hide');
            }
            else if (data == "Already") {

                alert('Caste Exists in Employee Details...');


            }
        }
    });
}

function LoadCommunityDataTable() {
    //alert('hi');
    $.ajax({
        type: "GET",
        url: '/Employee/GetCommunity',
        success: function (data) {
            var data1 = $('#example7').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.ID,
                item.Name,

               "<button class='btn btn-success' type='button' onClick='EditCommunity(\"" + item.ID + "\")'><i class='fa fa-pencil'></i></button> <button class='btn btn-danger' type='button' onClick='DeleteCommunity(\"" + item.ID + "\")'><i class='fa fa-times-circle'></i></button>"
                ]).draw();
            });
        }
    });

    //loadData();
}

function EditCommunity(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetEditCommunity/?ID=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;
                $("#txtMstCommunityID").val(item.ID);
                $("#txtMstCommunity").val(item.Name);

            });

        }
    });

}

function DeleteCommunity(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetDeleteCommunity/?ID=' + index + '',
        success: function (data) {
            if (data == "Delete") {
                LoadCommunity();
                LoadCommunityDataTable();
                alert('Deleted Successfully..');
                $('#modal-dialog23').modal('hide');
            }
            else if (data == "Already") {

                alert('Commmunity Exists in Employee Details...');
           }
        }
    });
}

function loadData() {

    $.ajax({
        type: "GET",
        url: '/Employee/EmployeeEditList',
        success: function (data) {
            
            //alert(data.length);
            if(data.length!=0)
            {
            $.each(data, function (key, item) {
                document.getElementById('txtMachineID').disabled = true;


                //alert(item.Designation);
                $("#txtTransID").val(item.TransID);
                $("#txtTransName").val(item.TransName);
                $("#txtAddress1").val(item.Address1);
                $("#txtAddress2").val(item.Address2);
                $("#City").val(item.City);
                $("#Pincode").val(item.Pincode);
                $("#txtVehicleNo").val(item.VehicleNo);
                $("#Mobile").val(item.MobileNo);
                $("#txtStdCode").val(item.Tel_No_Code);
                $("#Tel_no").val(item.PhoneNo);
                $("#txtTransType").val(item.TransType).change();


                $('#ddlCategory').val(item.Category).change();
                $('#ddlSubCatName').val(item.SubCatName).change();
                $('#ddlShift').val(item.Shift).change();

                $('#txtMachineID').val(item.MachineID);
                $('#txtExistingCode').val(item.ExistingCode);
                $('#txtTicketNo').val(item.MachineID);
                $('#txtFirstName').val(item.Name);
                $('#txtLastName').val(item.LastName);
                $('#txtDOB').val(item.DOB);
                $('#txtAge').val(item.Age);
                $('#ddlGender').val(item.Gender).change();
                $('#txtDOJ').val(item.DOJ);
                $('#txtRejoinDate').val(item.RejoinDate);
                $('#ddlDeptName').val(item.DeptName).change();
                $('#ddlDesignation').val(item.Designation).change();
                $('#ddlEmpType').val(item.EmpType).change();
                $('#ddlQulify').val(item.Qualification).change();
                $('#txtEmpMobileNo').val(item.EmpMobileNo);
                $('#ddlOTEligible').val(item.OTEligible).change();
                $('#ddlPFEligible').val(item.PFEligible).change();
                $('#txtPFNo').val(item.PFNo);
                $('#txtPFDate').val(item.PFDate);
                $('#ddlESIEligible').val(item.ESIEligible).change();
                $('#txtESINo').val(item.ESINo);
                $('#txtESIDate').val(item.ESIDate);
                $('#txtHostelRoom').val(item.HostelRoom);
                $('#txtBusRoute').val(item.BusRoute);
                $('#txtBusNo').val(item.BusNo);
                $("input[name=ActYes][value=" + item.ActiveMode + "]").attr('checked', true);
                $('#txtReleaveDate').val(item.ReleaveDate);
                if (item.IsNonAdmin == "1")
                {
                    $('#chkIsNonAdmin').attr('checked', true);
                }
                else
                {
                    $('#chkIsNonAdmin').attr('checked', false);
                }
                
                $("input[name=SalaryThrough][value=" + item.SalaryThrough + "]").attr('checked', true);
                $('#ddlBankName').val(item.BankName).change();
                $('#txtIFSCCode').val(item.IFSCCode);
                $('#txtBranch').val(item.Branch);
                $('#txtBasicSal').val(item.BasicSal);
                $('#txtOTSal').val(item.OTSal);
                $('#txtAllowance1').val(item.Allowance1);
                $('#txtAllowance2').val(item.Allowance2);
                $('#txtDeduction1').val(item.Deduction1);
                $('#txtDeduction2').val(item.Deduction2);
                $('#txtPFSal').val(item.PFSal);
                $('#txtVPFSal').val(item.VPFSal);
                $('#txtOthers1').val(item.Others1);
                $('#txtOthers2').val(item.Others2);
              
                $('#ddlDesignation').val(item.Designation).change();


                $('#ddlMartialStatus').val(item.MartialStatus).change();
                $('#ddlNationality').val(item.Nationality).change();
                $('#ddlReligion').val(item.Religion).change();
                $('#txtHeight').val(item.Height);
                $('#txtWeight').val(item.Weight);
                $("input[name=PhyChallenged][value=" + item.PhyChallenged + "]").attr('checked', true);
                $('#txtPhyReason').val(item.PhyReason);
                $('#txtStdWorkingHrs').val(item.StdWorkingHrs);
                $('#txtIncentAmt').val(item.IncentAmt);
                $('#ddlBloodGrp').val(item.BloodGrp).change();
                $('#ddlCaste').val(item.Caste).change();
                $('#ddlCommunity').val(item.Community).change();
                $('#ddlAgentName').val(item.AgentName).change();
                $('#txtAgentType').val(item.AgentType);
                $('#txtRecruitThrg').val(item.RecruitThrg);
                $('#txtRecruitMobile').val(item.RecruitMobile);
                $('#ddlWeekOff').val(item.WeekOff).change();
                $('#txtNominee').val(item.Nominee);
                $('#txtFatherName').val(item.FatherName);
                $('#txtMotherName').val(item.MotherName);
                $('#txtGuardianName').val(item.GuardianName);
                $('#txtPermAddr').val(item.PermAddr);
                $('#txtPermTaluk').val(item.PermTaluk);
                $('#txtPermDist').val(item.PermDist);
                $('#txtTempAddr').val(item.TempAddr);
                $('#txtTempTaluk').val(item.TempTaluk);
                $('#txtTempDist').val(item.TempDist);
                $('#ddlState').val(item.State).change();
                $('#txtIdenMark1').val(item.IdenMark1);
                $('#txtIdenMark2').val(item.IdenMark2);
                $('#txtParentMob1').val(item.ParentMob1);
                $('#txtParentMob2').val(item.ParentMob2);
                $('#ddlDocType').val(item.DocType).change();
                $('#txtDocNo').val(item.DocNo);
                $('#txtDocDesc').val(item.DocDesc);
               
                
            });
            }         
        }
    });
}

$(document).ready(function () {
LoadDesignation();
    LoadDepartment();
    
    LoadEmployeeType();
    LoadQualification();
    LoadBank();
    LoadCaste();
    LoadCommunity();
    LoadAgent();
    LoadDepartmentDataTable();
    LoadDesignationDataTable();
    LoadEmployeeTypeDataTable();
    LoadQualificationDataTable();
    LoadBankDataTable();
    LoadCasteDataTable();
    LoadCommunityDataTable();
    LoadGradeataTable();
    LoadGrade();
    LoadCate();
    LoadCanteen();
    LoadCateTable();
    LoadCanteenTable();
    LoadCanteenOptTable();
    LoadAgentTable();
    LoadTeaCanteen();
    LoadCanteenOpt();
    ProgressBarShow();
    setTimeout(function () {
        loadData();
        ProgressBarHide();
     }, 9000);


});
$('#btnDeptsave').click(function () {

    //alert('hi');
    //validation 
    var isAllValid = true;

    if ($('#txtDept').val().trim() == '') {
        document.getElementById("Depterror").innerHTML = "Enter the Department";
        isAllValid = false;
    }
    else {

        document.getElementById("Depterror").innerHTML = " "; // remove it
    }


    //Save if valid
    if (isAllValid) {
        var RoomDet = {
            Name: $('#txtDept').val().trim(),
            ID: $('#txtDeptID').val().trim(),
        }
        console.log(RoomDet);
        $.ajax({
            url: "/Employee/InsertDepartment",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    LoadDepartment();
                    LoadDepartmentDataTable();
                    $('#txtDept').val('');
                    $('#txtDeptID').val('');
                    $('#modal-dialog').modal('hide');
                    alert('Inserted Successfully..');
                }
                else if (d == "Update") {
                    LoadDepartment();
                    LoadDepartmentDataTable();
                    $('#txtDept').val('');
                    $('#txtDeptID').val('');
                    $('#modal-dialog').modal('hide');
                    alert('Update Successfully...');

                }
                else if (d == "Already") {
                    LoadDepartment();
                    LoadDepartmentDataTable();

                    alert('Already Exists...');

                }
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});


//Add Grade
$('#btnGradesave').click(function () {

    //alert('hi');
    //validation 
    var isAllValid = true;

    if ($('#txtGradeName').val().trim() == '') {
        document.getElementById("gradeterror").innerHTML = "Enter the Grade";
        isAllValid = false;
    }
    else {

        document.getElementById("gradeterror").innerHTML = " "; // remove it
    }


    //Save if valid
    if (isAllValid) {
        var RoomDet = {
           // alert('Name');
            Name: $('#txtGradeName').val().trim(),
            ID: $('#txtGradeID').val().trim(),
            //alert(Name)
        }
        console.log(RoomDet);
        $.ajax({
            
            url: "/Employee/InsertGrade",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    LoadGrade();
                    LoadGradeataTable();
                    $('#txtGradeName').val('');
                    $('#txtGradeID').val('');
                    $('#modal-dialog-Grade').modal('hide');
                    alert('Inserted Successfully..');
                }
                else if (d == "Update") {
                    LoadGrade();
                    LoadGradeataTable();
                    $('#txtGradeName').val('');
                    $('#txtGradeID').val('');
                    $('#modal-dialog-Grade').modal('hide');
                    alert('Update Successfully...');

                }
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});

//Add Category
$('#btnCategorysave').click(function () {

   //alert('hi');
    //validation 
    var isAllValid = true;

    if ($('#txtCategoryName').val().trim() == '') {
        document.getElementById("CateNameerror").innerHTML = "Enter the Category";
        isAllValid = false;
    }
    else {

        document.getElementById("CateNameerror").innerHTML = " "; // remove it
    }


    //Save if valid
    if (isAllValid) {
        var RoomDet = {
            // alert('Name');
            Name: $('#txtCategoryName').val().trim(),
            ID: $('#txtCategoryNameID').val().trim(),
            //alert(Name)
        }
        console.log(RoomDet);
        $.ajax({

            url: "/Employee/InsertCate",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    LoadCate();
                    LoadCateTable();
                    $('#txtCategoryName').val('');
                    $('#txtCategoryNameID').val('');
                    $('#modal-dialog-Category').modal('hide');
                    alert('Inserted Successfully..');
                }
                else if (d == "Update") {
                    LoadCate();
                    LoadCateTable();
                    $('#txtCategoryName').val('');
                    $('#txtCategoryNameID').val('');
                    $('#modal-dialog-Category').modal('hide');
                    alert('Update Successfully...');

                }
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});

//Add Canteen
$('#btnCanteenNamesave').click(function () {

   // alert('Canteen');
    //validation 
    var isAllValid = true;

    if ($('#txtCanteenName').val().trim() == '') {
        document.getElementById("Canerror").innerHTML = "Enter the Canteen Name";
        isAllValid = false;
    }
    else {

        document.getElementById("Canerror").innerHTML = " "; // remove it
    }


    //Save if valid
    if (isAllValid) {
        var RoomDet = {
            // alert('Name');
            Name: $('#txtCanteenName').val().trim(),
            ID: $('#txtCanteenID').val().trim(),
            //alert(Name)
        }
        console.log(RoomDet);
        $.ajax({

            url: "/Employee/InsertCanteen",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    LoadCanteen();
                    LoadCanteenTable();
                    LoadTeaCanteen();
                    $('#txtCanteenName').val('');
                    $('#txtCanteenID').val('');
                    $('#modal-dialog-Canteen').modal('hide');
                    alert('Inserted Successfully..');
                }
                else if (d == "Update") {
                    LoadCanteen();
                    LoadCanteenTable();
                    LoadTeaCanteen();
                    $('#txtCanteenName').val('');
                    $('#txtCanteenID').val('');
                    $('#modal-dialog-Canteen').modal('hide');
                    alert('Update Successfully...');

                }
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});

$('#btnCanteenNameOptsave').click(function () {

    // alert('Canteen');
    //validation 
    var isAllValid = true;

    if ($('#txtCanteenNameOpt').val().trim() == '') {
        document.getElementById("CanOpterror").innerHTML = "Enter the Canteen Operator Name";
        isAllValid = false;
    }
    else {

        document.getElementById("CanOpterror").innerHTML = " "; // remove it
    }


    //Save if valid
    if (isAllValid) {
        var RoomDet = {
            // alert('Name');
            Name: $('#txtCanteenNameOpt').val().trim(),
            ID: $('#txtCanteenOptID').val().trim(),
            //alert(Name)
        }
        console.log(RoomDet);
        $.ajax({

            url: "/Employee/InsertCanteenOpt",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    LoadCanteenOpt();
                    LoadCanteenOptTable();
                   
                    $('#txtCanteenNameOpt').val('');
                    $('#txtCanteenOptID').val('');
                    $('#modal-dialog-CanteenOpt').modal('hide');
                    alert('Inserted Successfully..');
                }
                else if (d == "Update") {
                    LoadCanteenOpt();
                    LoadCanteenOptTable();
                    $('#txtCanteenNameOpt').val('');
                    $('#txtCanteenOptID').val('');
                    $('#modal-dialog-CanteenOpt').modal('hide');
                    alert('Update Successfully...');

                }
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});

//Add Agent
$('#btnSourcesave').click(function () {

    //alert('Canteen');
    //validation 
    var isAllValid = true;

    if ($('#txtSourceName').val().trim() == '') {
        document.getElementById("Sourceerror").innerHTML = "Enter the Source Name";
        isAllValid = false;
    }
    else {

        document.getElementById("Sourceerror").innerHTML = " "; // remove it
    }


    //Save if valid
    if (isAllValid) {
        var RoomDet = {
            // alert('Name');
            Name: $('#txtSourceName').val().trim(),
            ID: $('#txtSourceID').val().trim(),
            Deduction1: $('#txt8HoursAmt').val().trim(),
            Deduction2: $('#txt12HoursAmt').val().trim(),
            //alert(Name)
        }
        console.log(RoomDet);
        $.ajax({

            url: "/Employee/InsertAGent",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    LoadAgent();
                    LoadAgentTable();
                    
                    $('#txtSourceName').val('');
                    $('#txtSourceID').val('');
                    $('#txt8HoursAmt').val('');
                    $('#txt12HoursAmt').val('');
                    $('#modal-dialog-Source').modal('hide');
                    alert('Inserted Successfully..');
                }
                else if (d == "Update") {
                    LoadAgent();
                    LoadAgentTable();
                    
                    $('#txtSourceName').val('');
                    $('#txtSourceID').val('');
                    $('#txt8HoursAmt').val('');
                    $('#txt12HoursAmt').val('');
                    $('#modal-dialog-Source').modal('hide');
                    alert('Update Successfully...');

                }
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});

$('#btnDesignsave').click(function () {

    //alert('hi');
    //validation 
    var isAllValid = true;

    if ($('#ddlMstDept').val() == '0') {
        document.getElementById("ddlMstDepterror").innerHTML = "Select the Department";
        isAllValid = false;
    }
    else {

        document.getElementById("ddlMstDepterror").innerHTML = " "; // remove it
    }

    if ($('#txtMstDesignation').val().trim() == '') {
        document.getElementById("Designationerror").innerHTML = "Enter the Designation";
        isAllValid = false;
    }
    else {

        document.getElementById("Designationerror").innerHTML = " "; // remove it
    }
    if ($('#txtMstWorkerCode').val().trim() == '') {
        document.getElementById("WorkerCodeerror").innerHTML = "Enter the WorkerCode";
        isAllValid = false;
    }
    else {

        document.getElementById("WorkerCodeerror").innerHTML = " "; // remove it
    }

    //Save if valid
    if (isAllValid) {
        var RoomDet = {
            Name: $('#txtMstDesignation').val().trim(),
            ID: $('#txtMstDesignationID').val().trim(),
            DeptName: $('#ddlMstDept').val(),
            AgentName: $('#txtMstWorkerCode').val().trim(),
        }
        console.log(RoomDet);
        $.ajax({
            url: "/Employee/InsertDesignation",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    LoadDesignation();
                    LoadDesignationDataTable();

                    $('#ddlMstDept').val('0').change();
                    $('#txtMstDesignation').val('');
                    $('#txtMstDesignationID').val('');
                    $('#txtMstWorkerCode').val('');
                    $('#modal-dialog2').modal('hide');
                    alert('Inserted Successfully..');
                }
                else if (d == "Update") {
                    LoadDesignation();
                    LoadDesignationDataTable();
                    $('#ddlMstDept').val('0').change();
                    $('#txtMstDesignation').val('');
                    $('#txtMstDesignationID').val('');
                    $('#txtMstWorkerCode').val('');
                    $('#modal-dialog2').modal('hide');
                    alert('Update Successfully...');

                }
                else if (d == "Already") {
                    LoadDesignation();
                    LoadDesignationDataTable();

                    alert('Already Exists...');

                }
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});


$('#btnEmpTypesave').click(function () {

    //alert('hi');
    //validation 
    var isAllValid = true;
    if ($('#ddlMstCategory').val() == '0') {
         document.getElementById("MstCategoryerror").innerHTML = "Select the Category";
        isAllValid = false;
    }
    else {

         document.getElementById("MstCategoryerror").innerHTML = " "; // remove it
    }
    if ($('#txtEmpType').val().trim() == '') {
        document.getElementById("MstEmpTypeerror").innerHTML = "Enter the Employee Type";
        isAllValid = false;
    }
    else {

        document.getElementById("MstEmpTypeerror").innerHTML = " "; // remove it
    }


    //Save if valid
    if (isAllValid) {
        var RoomDet = {
            Category: $('#ddlMstCategory').val(),
            Name: $('#txtEmpType').val().trim(),
            ID: $('#txtEmpTypeID').val().trim(),
        }
        console.log(RoomDet);
        $.ajax({
            url: "/Employee/InsertEmployeeType",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    LoadEmployeeType();
                    $('#ddlMstCategory').val('0').change();
                    $('#txtEmpType').val('');
                     $("#txtEmpTypeID").val('');
                     $('#modal-dialog1').modal('hide');
                    alert('Inserted Successfully..');

                }
                else if (d == "Update") {
                    LoadEmployeeType();
                    $('#ddlMstCategory').val('0').change();
                    $('#txtEmpType').val('');
                    $("#txtEmpTypeID").val('');
                    $('#modal-dialog1').modal('hide');
                    alert('Update Successfully...');

                }
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});

$('#btnQualifysave').click(function () {

    //alert('hi');
    //validation 
    var isAllValid = true;

    if ($('#txtMstQulification').val().trim() == '') {
        document.getElementById("MstQulificationerror").innerHTML = "Enter the Qualification";
        isAllValid = false;
    }
    else {

        document.getElementById("MstQulificationerror").innerHTML = " "; // remove it
    }


    //Save if valid
    if (isAllValid) {
        var RoomDet = {
            Name: $('#txtMstQulification').val().trim(),
            ID: $('#txtMstQulificationID').val().trim(),
        }
        console.log(RoomDet);
        $.ajax({
            url: "/Employee/InsertQualification",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    LoadQualification();
                    LoadQualificationDataTable();
                    $('#txtMstQulification').val('');
                    $('#txtMstQulificationID').val('');
                    $('#modal-dialog4').modal('hide');
                    alert('Inserted Successfully..');
                }
                else if (d == "Update") {
                    LoadQualification();
                    LoadQualificationDataTable();
                    $('#txtMstQulification').val('');
                    $('#txtMstQulificationID').val('');
                    $('#modal-dialog4').modal('hide');
                    alert('Update Successfully...');
                }
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});

$('#btnBanksave').click(function () {

    //alert('hi');
    //validation 
    var isAllValid = true;

    if ($('#txtMstBankName').val().trim() == '') {
        document.getElementById("MstBankNameerror").innerHTML = "Enter the BankName";
        isAllValid = false;
    }
    else {

        document.getElementById("MstBankNameerror").innerHTML = " "; // remove it
    }

    if ($('#txtMstBranchCode').val().trim() == '') {
        document.getElementById("MstBranchCodeerror").innerHTML = "Enter the Branch Code";
        isAllValid = false;
    }
    else {

        document.getElementById("MstBranchCodeerror").innerHTML = " "; // remove it
    }


    //Save if valid
    if (isAllValid) {
        var RoomDet = {
            ID: $('#txtMstBankNameID').val().trim(),
            BankName: $('#txtMstBankName').val().trim(),
            Branch: $('#txtMstBranchCode').val().trim(),
        }
        console.log(RoomDet);
        $.ajax({
            url: "/Employee/InsertBank",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    LoadBank();
                    LoadBankDataTable();
                    $('#txtMstBankNameID').val('');
                    $('#txtMstBankName').val('');
                    $('#txtMstBranchCode').val('');
                    $('#modal-dialog12').modal('hide');
                    alert('Inserted Successfully..');
                }
                else if (d == "Update") {
                    LoadBank();
                    LoadBankDataTable();
                    $('#txtMstBankNameID').val('');
                    $('#txtMstBankName').val('');
                    $('#txtMstBranchCode').val('');
                    $('#modal-dialog12').modal('hide');
                    alert('Update Successfully...');
                }
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});

$('#btnCastesave').click(function () {

    //alert('hi');
    //validation 
    var isAllValid = true;

    if ($('#txtMstCaste').val().trim() == '') {
        document.getElementById("MstCasteerror").innerHTML = "Enter the Caste";
        isAllValid = false;
    }
    else {

        document.getElementById("MstCasteerror").innerHTML = " "; // remove it
    }

   

    //Save if valid
    if (isAllValid) {
        var RoomDet = {
            ID: $('#txtMstCasteID').val().trim(),
            Caste: $('#txtMstCaste').val().trim(),
        }
        console.log(RoomDet);
        $.ajax({
            url: "/Employee/InsertCaste",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    LoadCaste();
                    LoadCasteDataTable();
                    $('#txtMstCaste').val('');
                    $('#txtMstCasteID').val('');
                     $('#modal-dialog13').modal('hide');
                    alert('Inserted Successfully..');
                }
                else if (d == "Update") {
                    LoadCaste();
                    LoadCasteDataTable();
                    $('#txtMstCaste').val('');
                    $('#txtMstCasteID').val('');
                    $('#modal-dialog13').modal('hide');
                   alert('Update Successfully...');
                }
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});

$('#btnCommunitysave').click(function () {

    //alert('hi');
    //validation 
    var isAllValid = true;

    if ($('#txtMstCommunity').val().trim() == '') {
        document.getElementById("MstCommunityerror").innerHTML = "Enter the Community";
        isAllValid = false;
    }
    else {

        document.getElementById("MstCommunityerror").innerHTML = " "; // remove it
    }

    //Save if valid
    if (isAllValid) {
        var RoomDet = {
            ID: $('#txtMstCommunityID').val().trim(),
            Community: $('#txtMstCommunity').val().trim(),
        }
        console.log(RoomDet);
        $.ajax({
            url: "/Employee/InsertCommunity",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    LoadCommunity();
                    LoadCommunityDataTable();
                    $('#txtMstCommunity').val('');
                    $('#txtMstCommunityID').val('');
                    $('#modal-dialog23').modal('hide');
                    alert('Inserted Successfully..');
                }
                else if (d == "Update") {
                    LoadCommunity();
                    LoadCommunityDataTable();
                    $('#txtMstCommunity').val('');
                    $('#txtMstCommunityID').val('');
                    $('#modal-dialog23').modal('hide');
                    alert('Update Successfully...');
                }
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});

$('#btnEmpSave').click(function () {

    //alert('hi');
    //validation 
    var isAllValid = true;

   

    //Save if valid
    if (isAllValid) {
        var EmpDet = {
            Category: $('#ddlCategory').val(),
            SubCatName: $('#ddlSubCatName').val(),
            Shift: $('#ddlShift').val(),
            MachineID: $('#txtMachineID').val().trim(),
            ExistingCode: $('#txtExistingCode').val().trim(),
            Name: $('#txtFirstName').val().trim(),
            LastName: $('#txtLastName').val().trim(),
            DOB: $('#txtDOB').val().trim(),
            Age: $('#txtAge').val().trim(),
            Gender: $('#ddlGender').val(),
            DOJ: $('#txtDOJ').val().trim(),
            RejoinDate: $('#txtRejoinDate').val().trim(),
            DeptName: $('#ddlDeptName').val(),
            Designation: $('#ddlDesignation').val(),
            EmpType: $('#ddlEmpType').val(),
            Qualification: $('#ddlQulify').val(),
            EmpMobileNo: $('#txtEmpMobileNo').val().trim(),
            OTEligible: $('#ddlOTEligible').val(),
            PFEligible: $('#ddlPFEligible').val(),
            PFNo: $('#txtPFNo').val().trim(),
            PFDate: $('#txtPFDate').val().trim(),
            ESIEligible: $('#ddlESIEligible').val(),
            ESINo: $('#txtESINo').val().trim(),
            ESIDate: $('#txtESIDate').val().trim(),
            HostelRoom: $('#txtHostelRoom').val().trim(),
            BusRoute: $('#txtBusRoute').val().trim(),
            BusNo: $('#txtBusNo').val().trim(),
            ActiveMode: $('input:radio[name="ActYes"]:checked').val(),
            ReleaveDate: $('#txtReleaveDate').val().trim(),
            IsNonAdmin: $('#chkIsNonAdmin').val(),
            SalaryThrough: $('input:radio[name="SalaryThrough"]:checked').val(),
            BankName: $('#ddlBankName').val(),
            IFSCCode: $('#txtIFSCCode').val().trim(),
            Branch: $('#txtBranch').val().trim(),
            BasicSal: $('#txtBasicSal').val().trim(),
            OTSal: $('#txtOTSal').val().trim(),
            Allowance1: $('#txtAllowance1').val().trim(),
            Allowance2: $('#txtAllowance2').val().trim(),
            Deduction1: $('#txtDeduction1').val().trim(),
            Deduction2: $('#txtDeduction2').val().trim(),
            PFSal: $('#txtPFSal').val().trim(),
            VPFSal: $('#txtVPFSal').val().trim(),
            Others1: $('#txtOthers1').val().trim(),
            Others2: $('#txtOthers2').val().trim(),

            
        //}
        //var PerDet = {

            MartialStatus: $('#ddlMartialStatus').val(),
            Nationality: $('#ddlNationality').val(),
            Religion: $('#ddlReligion').val(),
            Height: $('#txtHeight').val().trim(),
            Weight: $('#txtWeight').val().trim(),
            PhyChallenged: $('input:radio[name="PhyChallenged"]:checked').val(),
            PhyReason: $('#txtPhyReason').val().trim(),
            StdWorkingHrs: $('#txtStdWorkingHrs').val().trim(),
            IncentAmt: $('#txtIncentAmt').val().trim(),
            BloodGrp: $('#ddlBloodGrp').val(),
            Caste: $('#ddlCaste').val(),
            Community: $('#ddlCommunity').val(),
            AgentName: $('#ddlAgentName').val(),
            AgentType: $('#txtAgentType').val().trim(),
            RecruitThrg: $('#txtRecruitThrg').val().trim(),
            RecruitMobile: $('#txtRecruitMobile').val().trim(),

            WeekOff: $('#ddlWeekOff').val(),
            Nominee: $('#txtNominee').val().trim(),
            FatherName: $('#txtFatherName').val().trim(),
            MotherName: $('#txtMotherName').val().trim(),
            GuardianName: $('#txtGuardianName').val().trim(),
            PermAddr: $('#txtPermAddr').val().trim(),
            PermTaluk: $('#txtPermTaluk').val().trim(),
            PermDist: $('#txtPermDist').val().trim(),
            TempAddr: $('#txtTempAddr').val().trim(),
            TempTaluk: $('#txtTempTaluk').val().trim(),
            TempDist: $('#txtTempDist').val().trim(),
            State: $('#ddlState').val(),
            IdenMark1: $('#txtIdenMark1').val().trim(),
            IdenMark2: $('#txtIdenMark2').val().trim(),
            ParentMob1: $('#txtParentMob1').val().trim(),
            ParentMob2: $('#txtParentMob2').val().trim(),
            DocType: $('#ddlDocType').val(),
            DocNo: $('#txtDocNo').val().trim(),
            DocDesc: $('#txtDocDesc').val().trim(),
            ConvenyAmt: $('#txtConveny').val().trim(),
            SplAmt: $('#txtSplAmt').val().trim(),



        }
        //alert(PerDet.DocDesc);
      
        $.ajax({
            url: "/Employee/InsertEmpProfile",
            type: "POST",
            data: JSON.stringify(EmpDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                   ClearEmployeeDetails();
                    alert('Inserted Successfully..');
                }
                else if (d == "Update") {
                   ClearEmployeeDetails();
                    alert('Update Successfully...');
                }
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});

$('#btnEmpClear').click(function () {
ClearEmployeeDetails();
});

function ClearEmployeeDetails() {

//alert('hi');
    document.getElementById('txtMachineID').disabled = false;
    $('#ddlCategory').val('0').change();
    $('#ddlSubCatName').val('0').change();
    $('#ddlShift').val('0').change();
   
    $('#txtMachineID').val('');
    $('#txtExistingCode').val('');
    $('#txtTicketNo').val('');
    $('#txtFirstName').val('');
    $('#txtLastName').val('');
    $('#txtDOB').val('');
    $('#txtAge').val('');
    $('#ddlGender').val('0').change();
    $('#txtDOJ').val('');
    $('#txtRejoinDate').val('');
    $('#ddlDeptName').val('0').change();
    $('#ddlDesignation').val('0').change();
    $('#ddlEmpType').val('0').change();
    $('#ddlQulify').val('0').change();
    $('#txtEmpMobileNo').val('');
    $('#ddlOTEligible').val('0').change();
    $('#ddlPFEligible').val('0').change();
    $('#txtPFNo').val('');
    $('#txtPFDate').val('');
    $('#ddlESIEligible').val('0').change();
    $('#txtESINo').val('');
    $('#txtESIDate').val('');
    $('#txtHostelRoom').val('');
    $('#txtBusRoute').val('');
    $('#txtBusNo').val('');
    $("input[name=ActYes][value='Yes']").attr('checked', true);
    $('#txtReleaveDate').val('');
    $('#chkIsNonAdmin').attr('checked', false);
    $("input[name=SalaryThrough][value='Cash']").attr('checked', true);
    $('#ddlBankName').val('0').change();
    $('#txtIFSCCode').val('');
    $('#txtBranch').val('');
     $('#txtBasicSal').val('0.0');
     $('#txtOTSal').val('0.0');
     $('#txtAllowance1').val('0.0');
     $('#txtAllowance2').val('0.0');
     $('#txtDeduction1').val('0.0');
     $('#txtDeduction2').val('0.0');
     $('#txtPFSal').val('0.0');
     $('#txtVPFSal').val('0.0');
     $('#txtOthers1').val('0.0');
     $('#txtOthers2').val('0.0');
     $('#ddlMartialStatus').val('0').change();
     $('#ddlNationality').val('Indian').change();
     $('#ddlReligion').val('Indian').change();
     $('#txtHeight').val('');
     $('#txtWeight').val('');
     $("input[name=PhyChallenged][value='No']").attr('checked', true);
     $('#txtPhyReason').val('');
     $('#txtStdWorkingHrs').val('');
     $('#txtIncentAmt').val('');
     $('#ddlBloodGrp').val('0').change();
     $('#ddlCaste').val('0').change();
     $('#ddlCommunity').val('0').change();
     $('#ddlAgentName').val('0').change();
     $('#txtAgentType').val('');
     $('#txtRecruitThrg').val('');
     $('#txtRecruitMobile').val('');
     $('#ddlWeekOff').val('0').change();
     $('#txtNominee').val('');
     $('#txtFatherName').val('');
     $('#txtMotherName').val('');
     $('#txtGuardianName').val('');
     $('#txtPermAddr').val('');
     $('#txtPermTaluk').val('');
     $('#txtPermDist').val('');
     $('#txtTempAddr').val('');
     $('#txtTempTaluk').val('');
     $('#txtTempDist').val('');
     $('#ddlState').val('0').change();
     $('#txtIdenMark1').val('');
     $('#txtIdenMark2').val('');
     $('#txtParentMob1').val('');
     $('#txtParentMob2').val('');
     $('#ddlDocType').val('0').change();
     $('#txtDocNo').val('');
     $('#txtDocDesc').val('');
}