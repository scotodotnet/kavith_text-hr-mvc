﻿$(function () {
    $('#example8').DataTable();
});

function EditEmployee(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetEditEmployee/?TransID=' + index + '',
        success: function (data) {
            var url = "/Employee/EmployeeProfile";
            window.location.href = url;
        }
    });

}
function LoadEmployeeMainDetails() {


    var ActiveMode = $('input:radio[name="ActYes"]:checked').val();

    $.ajax({
        type: "GET",
        url: '/Employee/GetEmployee/?ActiveMode=' + ActiveMode + '',
        success: function (data) {
            var data1 = $('#example8').DataTable();
            data1.clear();
            if (data.length != 0) {
                $.each(data, function (key, item) {
                    data1.row.add([
                    item.MachineID,
                    item.ExistingCode,
                    item.Name,
                    item.DeptName,
                    
                    item.EmpType,
                    
                   "<button class='btn btn-success' type='button' onClick='EditEmployee(\"" + item.MachineID + "\")'><i class='fa fa-pencil'></i></button>"
                    ]).draw();
                });
            }
            else {
                var data1 = $('#example8').DataTable();
                data1.clear();
            }

        }
    });
}
function DeleteEmployee(index) {

    $.ajax({
        type: "GET",
        url: '/Employee/GetDeleteEmployee/?ID=' + index + '',
        success: function (data) {
            if (data == "Delete") {
                LoadEmployeeMainDetails();

                alert('Deleted Successfully..');
               
            }
            else if (data == "Already") {
               
                alert('Employee Type Exists in Employee Details...');
                //$('#modal-dialog2').modal('hide');

            }
        }
    });
}

$(document).ready(function () {
    LoadEmployeeMainDetails();

    $('input[type=radio][name=ActYes]').change(function () {
        LoadEmployeeMainDetails();
    });

});

$('#btnEmpAdd').click(function () {
    $.ajax({
        url: '/Employee/AddEmployee',
        success: function () {
            var url = "/Employee/EmployeeProfile";
            window.location.href = url;
        },
        error: function () {
            alert('Error. Please try again.');

        }
    });
});