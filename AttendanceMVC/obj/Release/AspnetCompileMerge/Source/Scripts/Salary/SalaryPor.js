﻿$(function () {

    //alert('Test')
    $('.datepicker').datepicker(
        {
            format: "dd/mm/yyyy",
            autoclose: true
        });

    $('.select2').select2();
    $('#AttnConv8').DataTable();

});
function LoadEmployeeType() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Employee/GetEmployeeType',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlEmpType').empty();
            $("#ddlEmpType").append($('<option></option>').val('0').text('--Select Employee Type--'));
            $.each(data, function (i, val) {

                $("#ddlEmpType").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

$(document).ready(function () {
    LoadEmployeeType();
    LoadGoodsDataTable();
});

$("#btnAttendMove").click(function () {

    var FromDate = $('#txtFromDate').val();
    var ToDate = $('#txtToDate').val();
    var WagesType = $('#ddlEmpType').val();


    var isAllValid = true;

    if ($('#txtFromDate').val().trim() == '') {

        document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

        isAllValid = false;
    }
    else {

        document.getElementById("FromDateerror").innerHTML = " ";  // remove it
    }

    if ($('#txtToDate').val().trim() == '') {

        document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

        isAllValid = false;
    }
    else {

        document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
    }

    if (isAllValid) {

        $.ajax({
            type: "GET",
            url: '/SalaryProcess/AttendanceConversionSave/?FromDate=' + FromDate + '&ToDate=' + ToDate + '&WagesType=' + WagesType + '',
            success: function (data) {
                var data1 = $('#AttnConv8').DataTable();
                data1.clear();
                $.each(data, function (key, item) {

                    data1.row.add([
                    item.EmpNo,
                    item.ExistingCode,
                    item.EmpName,
                    item.DeptName,
                    item.AttnDate,
                    item.LogTimeIn,
                    item.LeaveDesc,
                    item.LogTimeOut,
                    item.LeaveType,
                    item.FormName,
                  
                    ]).draw();
                });
            }
        });

    }
});

function LoadGoodsDataTable() {

    $.ajax({
        type: "GET",
        url: '/SalaryProcess/GetEmpID',
        success: function (data) {
            var data1 = $('#example1').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.EmpNo,
                item.ExistingCode,
                item.EmpName,
                item.DeptName,
               "<button class='btn btn-success' type='button' onClick='EditEmpID(\"" + item.EmpNo + "\")'><i class='fa fa-pencil'></i></button> "
                ]).draw();
            });
        }
    });
}
//Selete MachineID to Datatable 
function EditEmpID(index) {
    $.ajax({
        type: "GET",
        url: '/SalaryProcess/GetEditEmpID/?EmpNo=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;
                $("#txtMachineID").val(item.EmpNo);
                $("#txtExistingCode").val(item.ExistingCode);
                $("#txtname").val(item.EmpName);
                $("#txtEmpType").val(item.WagesType);
                $("#txtDeprt").val(item.DeptName);
                $("#txtDesign").val(item.LeaveDesc);
                $("#txtSalary").val(item.BrokerID);
                $('#modal-dialog').modal('hide');
            });

        }
    });
}

//Add Manual Deduction 
$('#btnSaveDeduction').click(function (e) {



    //validation of order
    var isAllValid = true;

    if ($('#txtMachineID').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("MachineError").innerHTML = " "; // remove it
        e.preventDefault();
    }

    if ($('#txtFromDate').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("FromDateError").innerHTML = " "; // remove it
        e.preventDefault();
    }

    if ($('#txtToDate').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("ToDateError").innerHTML = " "; // remove it
        e.preventDefault();
    }

    //Save if valid

    if (isAllValid) {

        //Save data to database
        var data = {
            EmpNo: $('#txtMachineID').val().trim(),
            ExistingCode: $('#txtExistingCode').val().trim(),
            EmpName: $('#txtname').val().trim(),
            WagesType: $('#txtEmpType').val().trim(),
            Allowance3: $('#txtAllow1').val().trim(),
            Allowance4: $('#txtAllow2').val().trim(),
            Allowance5: $('#txtAllow3').val().trim(),
            Deduction3: $('#txtDeduct1').val().trim(),
            Deduction4: $('#txtDeduct2').val().trim(),
            Deduction5: $('#txtDeduct3').val().trim(),
            AttnDate: $('#txtFromDate').val().trim(),
            AttnDateStr: $('#txtToDate').val().trim()


        }

        console.log(data);
        $.ajax({
            url: '/SalaryProcess/SaveDeduction',
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                //check is successfully save to database 
                if (d == "Insert") {
                    alert('Manual Deduction Added...,');
                    ClearDeduction();
                }
                else if (d == "Update") {

                    alert('Manual Deduction Update...,');
                    ClearDeduction();

                }
                else {
                    alert('Failed');
                }
                //$('#btnSave').val('Save');
            },
            error: function () {
                alert('Error. Please try again.');
                //$('#btnSave').val('Save');
            }
        });
    }

});
function ClearDeduction() {
    $('#txtMachineID').val('');
    $('#txtExistingCode').val('');
    $('#txtname').val('');
    $('#txtEmpType').val('');
    $('#txtAllow1').val('0.0');
    $('#txtAllow2').val('0.0');
    $('#txtAllow3').val('0.0');
    $('#txtDeduct1').val('0.0');
    $('#txtDeduct2').val('0.0');
    $('#txtDeduct3').val('0.0');
    $('#txtFromDate').val('');
    $('#txtToDate').val('');
    $("#txtDeprt").val('');
    $("#txtDesign").val('');
    $("#txtSalary").val('');
}

$('#btnClearDeduction').click(function (e) {
    ClearDeduction();
});

$('#btnPayslip').click(function (e) {

    var FromDate = $('#txtFromDate').val().trim();
    var ToDate = $('#txtToDate').val().trim();
    var WagesType = $('#ddlEmpType').val().trim();

    //validation of order
    var isAllValid = true;

    alert(FromDate);

   
    if ($('#txtToDate').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("ToDateError").innerHTML = " "; // remove it
        e.preventDefault();
    }

    //Save if valid

    if (isAllValid) {
       

        $.ajax({
            url: '/SalaryProcess/GeneratePaylip/?FromDate=' + FromDate + '&ToDate=' + ToDate + '&WagesType=' + WagesType + '',
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                //check is successfully save to database 
                if (d == "Insert") {
                    alert('Paylip Convert...,');
                    
                }
                else if (d == "Error") {

                    alert('Transfer Attendance First...,');
                    

                }
                else {
                    alert('Failed');
                }
                //$('#btnSave').val('Save');
            },
            error: function () {
                alert('Error. Please try again.');
                //$('#btnSave').val('Save');
            }
        });
    }

});

$('#btnCalc').click(function () {
    
    var isAllValid = true;

    if ($('#txtAdvance').val().trim() == '') {
        document.getElementById("AdvanceError").innerHTML = "Enter the Advance Amount";
        isAllValid = false;
    }
    else {

        document.getElementById("AdvanceError").innerHTML = " "; // remove it       
    }

    if ($('#txtNoMonth').val().trim() == '') {
        document.getElementById("NoMonthError").innerHTML = "Enter the No Of Month";
        isAllValid = false;
    }
    else {

        document.getElementById("NoMonthError").innerHTML = " "; // remove it
       
    }

    
    //if valid
    if (isAllValid) {
        var budget = parseFloat($("#txtAdvance").val()) || 0;
        var ppc = parseFloat($("#txtNoMonth").val()) || 0;

        var value = budget / ppc;
        //alert(value);
        if (!isNaN(value) && value !== Infinity) {
            $("#txtMonDeduct").val(value);
        }
    }

});