﻿function LoadIPAddress() {

    $.ajax({
        type: "GET",
        url: '/Biometric/GetIPAddress',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlIPAddress').empty();
            $("#ddlIPAddress").append($('<option></option>').val('0').text('-Select-'));
            $.each(data, function (i, val) {

                $("#ddlIPAddress").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function LoadLastDate() {

    $.ajax({
        type: "GET",
        url: '/Biometric/GetLastDate',
        success: function (data) {

            if (data != "") {
                $('#txtLastDate').val(data);
            }
            else {
                $('#txtLastDate').val('');
            }
        }
    })
}

function ProgressBarShow() {
    //  alert("test");
    $('#Download_loader').show();
}

function ProgressBarHide() {
    $('#Download_loader').hide();
}

function cannot() {
    //alert('cannot');
    $('#lblDwnCmpltd').html("Machine Can't Ping....");
}

$(document).ready(function () {
    LoadIPAddress();
    LoadLastDate();
    GetIsAdmin();
});
function LoadLastDate() {

    $.ajax({
        type: "GET",
        url: '/Biometric/GetUserID',
        success: function (data) {

            if (data == "Scoto") {
                $('#BtnConversionID').show();
            }
            else {
                $('#BtnConversionID').hide();
            }
        }
    })
}

//BtnDownloadClear
$("#BtnDownload").click(function () {


    var IPAddress = $('#ddlIPAddress').val();

    var isAllValid = true;



    if ($('#ddlIPAddress').val() == '0') {

        document.getElementById("IPAddresserror").innerHTML = "Select the IP Address";

        isAllValid = false;
    }
    else {

        document.getElementById("IPAddresserror").innerHTML = " ";  // remove it
    }

    if (isAllValid) {
        ProgressBarShow();
        $.ajax({
            type: "GET",
            url: '/Biometric/DownloadDataWithoutManulAttend/?IPAddress=' + IPAddress + '',
            success: function (data) {

                if (data == "Not Connected") {
                    //alert('Unable to connect the device');
                    ProgressBarHide();
                    cannot();
                }
                else if (data == "DOWNLOAD COMPLETED") {
                    //alert("DOWNLOAD COMPLETED....");
                    ProgressBarHide();
                    $('#lblDwnCmpltd').html("DOWNLOAD COMPLETED....");
                }

            },
            error: function () {
                alert('Error. Please try again.');

            }
        });

    }
});

$("#BtnConversionID").click(function () {
  
        ProgressBarShow();
        $.ajax({
            type: "GET",
            url: '/Biometric/IDConvert',
            success: function (data) {

                if (data == "Error on ID Convert") {
                    //alert('Unable to connect the device');
                    $('#lblDwnCmpltd').html("Error on ID Convert");
                    ProgressBarHide();
                    cannot();
                }
                else if (data == "ID CONVERTION COMPLETED") {
                    //alert("DOWNLOAD COMPLETED....");
                    ProgressBarHide();
                    $('#lblDwnCmpltd').html("ID CONVERTION COMPLETED....");
                }

            },
            error: function () {
                alert('Error. Please try again.');

            }
        });

});


$("#BtnConversion").click(function () {


    var isAllValid = true;

    var FromDate = $('#txtFromDate').val().trim();
    var ToDate = $('#txToDate').val().trim();


    if ($('#txtFromDate').val() == '') {

        document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

        isAllValid = false;
    }
    else {

        document.getElementById("FromDateerror").innerHTML = " ";  // remove it
    }

    if ($('#txToDate').val() == '') {

        document.getElementById("ToDateerror").innerHTML = "Enter the To Date";

        isAllValid = false;
    }
    else {

        document.getElementById("ToDateerror").innerHTML = " ";  // remove it
    }

    if (isAllValid) {
        ProgressBarShow();
        $.ajax({
            type: "GET",
            url: '/Biometric/GetWorkingDaysSaveDB/?FromDate=' + FromDate + '&ToDate=' + ToDate + '',
            success: function (data) {

                if (data == "Already Exists") {
                    ProgressBarHide();
                    alert('Already Data Exists.');
                }
                else if (data == "Completed") {
                    ProgressBarHide();
                    alert("CONVERSION COMPLETED....");
                    LoadLastDate();
                }
            },
            error: function () {
                alert('Error. Please try again.');

            }
        });
    }
});
