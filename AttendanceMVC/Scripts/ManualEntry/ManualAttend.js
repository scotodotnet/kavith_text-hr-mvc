﻿var GobalDate;
$(function () {

    //alert('Test')
    $('.datepicker').datepicker(
        {
            format: "dd/mm/yyyy",
            autoclose: true
        });

    $('.select2').select2();
    $('#example1').DataTable();
   
   

  
    $('#AttendDataTable').DataTable();

});

$(document).ready(function () {
    LoadGoodsDataTable();
  
    LoadAttendanceDataTable();
   
});



function ClearManAtted() {
    $('#txtMachineID').val('');
    $('#txtExistingCode').val('');
    $('#txtname').val('');
    $('#txtDateIN').val('');
    $('#txtTimeIN').val('');

    $('#txtDateOUT').val('');
    $('#txtTimeOUT').val('');
}




$('#btnAddManAtted').click(function () {
    $('#DisplayManAttendMain').css('display', 'none');
    $('#DisplayManAttendSub').css('display', 'block');
});






$('#btnClearManAttend').click(function () {
    ClearManAtted();
});




//Add Manual Attendance 
$('#btnSaveManAttend').click(function (e) {



    //validation of order
    var isAllValid = true;

    if ($('#txtDateIN').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("INdateError").innerHTML = " "; // remove it
        e.preventDefault();
    }

    if ($('#txtTimeIN').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("IntimeError").innerHTML = " "; // remove 
        e.preventDefault();
    }

    if ($('#txtDateOUT').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("OUTdateError").innerHTML = " "; // remove it
        e.preventDefault();
    }

    if ($('#txtTimeOUT').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("OUTtimeError").innerHTML = " "; // remove it
        e.preventDefault();
    }



    //Save if valid

    if (isAllValid) {

        //Save data to database
        var data = {
            EmpNo: $('#txtMachineID').val().trim(),
            ExistingCode: $('#txtExistingCode').val().trim(),
            EmpName: $('#txtname').val().trim(),
            AttnDate: $('#txtDateIN').val().trim(),
            TimeIn: $('#txtTimeIN').val().trim(),
            AttnDateStr: $('#txtDateOUT').val().trim(),
            TimeOut: $('#txtTimeOUT').val().trim()


        }

        console.log(data);
        $.ajax({
            url: '/Manual/SaveManulAtt',
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                //check is successfully save to database 
                if (d == "Insert") {
                    alert('Manual Attendance Added...,');
                    ClearManAtted();
                    LoadAttendanceDataTable();
                }
                else if (d == "Update") {

                    alert('Manual Attedance Update...,');
                    ClearManAtted();
                    LoadAttendanceDataTable();

                }
                else {
                    alert('Failed');
                }
                //$('#btnSave').val('Save');
            },
            error: function () {
                alert('Error. Please try again.');
                //$('#btnSave').val('Save');
            }
        });
    }
    $('#DisplayManAttendMain').css('display', 'block');
    $('#DisplayManAttendSub').css('display', 'none');

});


//Load EmployeeDetails Datatable 
function LoadGoodsDataTable() {

    $.ajax({
        type: "GET",
        url: '/Manual/GetEmpID',
        success: function (data) {
            var data1 = $('#example1').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.EmpNo,
                item.ExistingCode,
                item.EmpName,
                item.DeptName,
               "<button class='btn btn-success' type='button' onClick='EditEmpID(\"" + item.EmpNo + "\")'><i class='fa fa-pencil'></i></button> "
                ]).draw();
            });
        }
    });
}



//Load Attendance Datatable a
function LoadAttendanceDataTable() {

    $.ajax({
        type: "GET",
        url: '/Manual/GetAttendanceLoad',
        success: function (data) {
            var data1 = $('#AttendDataTable').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.EmpNo,
                item.ExistingCode,
                item.EmpName,
                item.AttnDate,
                item.TimeIn,
                item.TimeOut,

                ]).draw();
            });
        }
    });
}




//Selete MachineID to Datatable 
function EditEmpID(index) {

    $.ajax({
        type: "GET",
        url: '/Manual/GetEditEmpID/?EmpNo=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;
                $("#txtMachineID").val(item.EmpNo);
                $("#txtExistingCode").val(item.ExistingCode);
                $("#txtname").val(item.EmpName);

                $('#modal-dialog').modal('hide');
            });

        }
    });

}





